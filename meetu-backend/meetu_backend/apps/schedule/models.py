import uuid

from django.db import models

from meetu_backend.apps.authentication.models import UserProfile

# Create your models here.
class Schedule(models.Model):
    owner = models.ForeignKey(UserProfile, on_delete=models.CASCADE, default=1)
    title = models.CharField(max_length=128)
    parent_id = models.UUIDField(default=uuid.uuid4)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
