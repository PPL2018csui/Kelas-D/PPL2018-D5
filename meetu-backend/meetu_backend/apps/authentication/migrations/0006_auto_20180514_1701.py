# Generated by Django 2.0.2 on 2018-05-14 17:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0005_notification'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='repetition',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
    ]
