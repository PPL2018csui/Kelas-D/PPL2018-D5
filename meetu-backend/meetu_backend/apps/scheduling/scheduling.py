from datetime import datetime
from dateutil.relativedelta import relativedelta
from pytz import UTC

from meetu_backend.apps.schedule.models import Schedule
from meetu_backend.apps.meetup.models import MeetUp
from meetu_backend.apps.group.models import Group

class BasicScheduling:

    UTC_ZERO = datetime(2000, 1, 1, 0, 0, tzinfo=UTC)

    @staticmethod
    def convert_to_minute(time):
        return 60 * time.hour + time.minute
    
    @staticmethod
    def get_next_date(date, repeat_type, delta):
        result = date
        if repeat_type == "DAILY":
            result = result + relativedelta(days=delta)
        if repeat_type == "WEEKLY":
            result = result + relativedelta(weeks=delta)
        if repeat_type == "MONTHLY":
            result = result + relativedelta(months=delta)
        return result
    
    @staticmethod
    def apply_time_delta_object(schedules, delta):
        # inplace apply
        for schedule in schedules:
            schedule.start_time += relativedelta(hours=delta)
            schedule.end_time += relativedelta(hours=delta)
    
    @staticmethod
    def apply_time_delta_dict(schedules, delta):
        for schedule in schedules:
            schedule['start_time'] += relativedelta(hours=delta)
            schedule['end_time'] += relativedelta(hours=delta)

    def retrieve_all_schedules(self, group_id, start_date, end_date):
        members = Group.objects.get(id=group_id).members.all()
        groups_related = Group.objects.filter(membership__user_profile__in=members)

        # fetch meetups
        meetups = MeetUp.objects.filter(group__in=groups_related, start_time__gte=start_date, end_time__lt=end_date)
        
        # fetch schedules
        schedules = Schedule.objects.filter(owner__in=members, start_time__gte=start_date, end_time__lt=end_date)

        schedules = list(schedules)
        schedules.extend(list(meetups))
        schedules.sort(key=lambda schedule: schedule.start_time)

        return schedules
    
    def find_daily_available_schedule(self, schedules, thres):
        unavailable = [0 for i in range(60 * 24)]

        for schedule in schedules:
            start_time = BasicScheduling.convert_to_minute(schedule.start_time)
            end_time = BasicScheduling.convert_to_minute(schedule.end_time)

            unavailable[start_time] += 1
            unavailable[end_time] -= 1
        
        for i in range(1, 60 * 24):
            unavailable[i] += unavailable[i-1]
        
        available = set()
        for i in range(60 * 24):
            if unavailable[i] <= thres:
                available.add(i)
        
        return available
    
    def get_raw_available_schedules(self, schedules, start_date, end_date, thres):
        iterator = 0
        availables = []

        current_date = start_date
        while current_date < end_date:
            year = current_date.year
            month = current_date.month
            day = current_date.day

            simple_current_date = datetime(year, month, day)

            current_date_schedules = []
            while (iterator < len(schedules)):
                iterator_date = schedules[iterator].start_time
                simple_iterator_date = datetime(iterator_date.year, iterator_date.month, iterator_date.day)

                if simple_iterator_date == simple_current_date:
                    current_date_schedules.append(schedules[iterator])
                    iterator += 1
                else:
                    break
                
            current_availables = self.find_daily_available_schedule(current_date_schedules, thres)
            availables.append(current_availables)
            
            current_date = current_date + relativedelta(days=1)
            
        return availables
    
    def get_schedules_minute_status(self, availables, start_date, end_date, repeat_type, repeat_count):
        minutes_status = []

        for i in range(len(availables)):
            day = start_date + relativedelta(days=i)

            if BasicScheduling.get_next_date(day, repeat_type, repeat_count-1) < end_date:
                possible_minutes = [True for j in range(60 * 24)]
                
                for rel_date in range(repeat_count):
                    cur_date = BasicScheduling.get_next_date(day, repeat_type, rel_date)
                    index = (cur_date - start_date).days

                    for minute in range(60 * 24):
                        if minute not in availables[index]:
                            possible_minutes[minute] = False
                
                minutes_status.append(possible_minutes)
        
        return minutes_status
    
    def get_available_schedules_interval(self, minutes_status, start_date):
        intervals = []

        for i in range(len(minutes_status)):
            status = minutes_status[i]
            day = start_date + relativedelta(days=i)
            minute = 0

            day_intervals = []

            while minute < 60 * 24 - 1:
                if status[minute]:
                    end_min = minute
                    while (end_min < 60 * 24 - 1) and (status[end_min]):
                        end_min += 1

                    start_time = datetime(day.year, day.month, day.day, minute // 60, minute % 60, tzinfo=UTC)
                    end_time = datetime(day.year, day.month, day.day, end_min // 60, end_min % 60, tzinfo=UTC)

                    interval = {}
                    interval['start_time'] = start_time
                    interval['end_time'] = end_time
                    day_intervals.append(interval)

                    minute = end_min
                else:
                    minute += 1

            if len(day_intervals) > 0:
                intervals.append(day_intervals)

        return intervals
    
    def get_available_schedules(self, group_id, time_zone, start_date, end_date, repeat_type, repeat_count, thres=0):
        if start_date > end_date:
            raise ValueError("Start date must be lower or equal than end date")
        elif (repeat_type != "ONCE") and (repeat_type != "DAILY") and (repeat_type != "WEEKLY") and (repeat_type != "MONTHLY"):
            raise ValueError("Repeat type must be either 'ONCE', 'DAILY', 'WEEKLY', 'MONTHLY'")
        elif repeat_count <= 0:
            raise ValueError("Repeat count must be higher than 0")
        elif thres < 0:
            raise ValueError("Minimum unavailable person must be >= 0")

        # make exclusive right
        end_date = end_date + relativedelta(days=1)

        # equivalent; better than treating it differently
        if repeat_type == "ONCE":
            repeat_type = "DAILY"
            repeat_count = 1

        zone = (time_zone - BasicScheduling.UTC_ZERO).seconds // 3600

        schedules = self.retrieve_all_schedules(group_id, start_date, end_date)
        BasicScheduling.apply_time_delta_object(schedules, -zone)
        start_date += relativedelta(hours=-zone)
        end_date += relativedelta(hours=-zone)

        availables = self.get_raw_available_schedules(schedules, start_date, end_date, thres)
        minutes_status = self.get_schedules_minute_status(availables, start_date, end_date, repeat_type, repeat_count)
        available_schedules = self.get_available_schedules_interval(minutes_status, start_date)

        for daily_schedule in available_schedules:
            BasicScheduling.apply_time_delta_dict(daily_schedule, zone)

        return available_schedules
