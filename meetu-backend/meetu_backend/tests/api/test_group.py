import json

from django.urls import reverse

from rest_framework.test import APITestCase
from rest_framework import status
from push_notifications.models import GCMDevice

from django.contrib.auth import get_user_model
from meetu_backend.apps.authentication.models import UserProfile
from meetu_backend.apps.group.models import Group, Membership

User = get_user_model()

class GroupTestCase(APITestCase):
    user = None
    user_profile = None
    group = None
    user_2 = None
    user_profile_2 = None

    def setUp(self):
        self.user = User.objects.create_user(username='rickyasd90@gmail.com', email='rickyasd90@gmail.com', password='asdasd123')
        self.user_profile = UserProfile.objects.create(user=self.user)
        self.group = Group.objects.create(owner=self.user_profile, title='PPL')
        Membership.objects.create(user_profile=self.user_profile, group=self.group, type='Owner')
        self.user_2 = User.objects.create_user(username='rickyasd91@gmail.com', email='rickyasd91@gmail.com', password='asdasd123')
        self.user_profile_2 = UserProfile.objects.create(user=self.user_2)
        GCMDevice.objects.create(registration_id="token1", cloud_message_type="FCM", user=self.user)
        GCMDevice.objects.create(registration_id="token2", cloud_message_type="FCM", user=self.user_2)

    def tearDown(self):
        User.objects.all().delete()
        Group.objects.all().delete()

    def test_can_get_all_group(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        response = self.client.get(reverse('group-list'), HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(len(response.data), 0)

    def test_can_create_group(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'title': 'Ricky test'
        }
        response = self.client.post(reverse('group-list'), HTTP_AUTHORIZATION='JWT ' + token, data=json.dumps(data),
                                content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_can_create_group_with_members(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'title': 'Ricky test',
            'members': [self.user_profile_2.id, ]
        }
        response = self.client.post(reverse('group-list'), HTTP_AUTHORIZATION='JWT ' + token, data=json.dumps(data),
                                content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_can_not_create_group_with_no_title(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
        }
        response = self.client.post(reverse('group-list'), data=data, HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_can_not_create_group_members_doesnt_exist(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'title': 'Ricky Test',
            'members': [1477, 5388,]
        }
        response = self.client.post(reverse('group-list'), HTTP_AUTHORIZATION='JWT ' + token, data=json.dumps(data),
                                content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_can_not_create_group_invalid_members(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'title': 'Ricky Test',
            'members': [[1], [1],]
        }
        response = self.client.post(reverse('group-list'), HTTP_AUTHORIZATION='JWT ' + token, data=json.dumps(data),
                                content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_can_get_group_detail(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        response = self.client.get(reverse('group-detail', kwargs={'pk': self.group.id}), HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cannot_get_group_detail_group_doesnt_exist(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        response = self.client.get(reverse('group-detail', kwargs={'pk': 123456789}), HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_get_group_detail_group_non_member(self):
        user_cred = {
            'username': 'rickyasd91@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        response = self.client.get(reverse('group-detail', kwargs={'pk': self.group.id}), HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_can_add_member_to_group(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'user_profile': self.user_profile_2.id,
            'group': self.group.id
        }
        response = self.client.post(reverse('add-member'), data=json.dumps(data), HTTP_AUTHORIZATION='JWT ' + token,
            content_type="application/json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_cannot_add_member_to_group_group_doesnt_exist(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'user_profile': self.user_profile_2.id,
            'group': 123456789
        }
        response = self.client.post(reverse('add-member'), data=data, HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_add_member_to_group_group_empty(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'user_profile': self.user_profile_2.id
        }
        response = self.client.post(reverse('add-member'), data=data, HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_add_member_to_group_unauthorized(self):
        user_cred = {
            'username': 'rickyasd91@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'user_profile': self.user_profile_2.id,
            'group': self.group.id
        }
        response = self.client.post(reverse('add-member'), data=data, HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_cannot_add_member_to_group_user_profile_doesnt_exist(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'user_profile': 123456789,
            'group': self.group.id
        }
        response = self.client.post(reverse('add-member'), data=data, HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
