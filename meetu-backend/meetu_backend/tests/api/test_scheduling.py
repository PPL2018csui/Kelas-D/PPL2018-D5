from datetime import datetime
from pytz import UTC

from django.urls import reverse
from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase
from rest_framework import status

from meetu_backend.tests.utils.utils import setup_scheduling_test

User = get_user_model()

class SchedulingListViewTestCase(APITestCase):

    @classmethod
    def setUpTestData(cls):
        setup_scheduling_test()
    
    def setUp(self):
        User.objects.create_user(username='rickyasd90@gmail.com', email='rickyasd90@gmail.com', password='asdasd123')

    def tearDown(self):
        User.objects.filter(username='rickyasd90@gmail.com').delete()

    def test_get_invalid_request_time(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']
        data = {
            'time_zone': 0,
            'start_date': 1,
            'end_date': 2,
        }

        response = self.client.post(reverse('recommendation-list'), data=data, HTTP_AUTHORIZATION='JWT ' + token, format='json')
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)
    
    def test_get_invalid_request_general(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']
        data = {
            'group_id': 1,
            'time_zone': datetime(1900, 1, 1, tzinfo=UTC),
            'start_date': datetime(2018, 2, 1, tzinfo=UTC),
            'end_date': datetime(2018, 2, 1, tzinfo=UTC),
            'repeat_type': -1,
            'repeat_count': 1
        }

        response = self.client.post(reverse('recommendation-list'), data=data, HTTP_AUTHORIZATION='JWT ' + token, format='json')
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_get_recommendation(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']
        data = {
            'group_id': 1,
            'time_zone': datetime(1900, 1, 1, 0, 0, tzinfo=UTC),
            'start_date': datetime(2018, 2, 1, tzinfo=UTC),
            'end_date': datetime(2018, 2, 1, tzinfo=UTC),
            'repeat_type': 'ONCE',
            'repeat_count': 1
        }

        expected_data =  [
            [
                {
                    'end_time': datetime(2018, 2, 1, 7, 0, tzinfo=UTC),
                    'start_time': datetime(2018, 2, 1, 0, 0, tzinfo=UTC)
                },
                {
                    'end_time': datetime(2018, 2, 1, 13, 0, tzinfo=UTC),
                    'start_time': datetime(2018, 2, 1, 12, 45, tzinfo=UTC)
                },
                {
                    'end_time': datetime(2018, 2, 1, 14, 30, tzinfo=UTC),
                    'start_time': datetime(2018, 2, 1, 14, 15, tzinfo=UTC)
                },
                {
                    'end_time': datetime(2018, 2, 1, 23, 59, tzinfo=UTC),
                    'start_time': datetime(2018, 2, 1, 15, 15, tzinfo=UTC)
                }
            ]
        ]
        
        response = self.client.post(reverse('recommendation-list'), data=data, HTTP_AUTHORIZATION='JWT ' + token, format='json')
        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(expected_data, response.data)

