import json

from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from push_notifications.models import GCMDevice

from meetu_backend.apps.authentication.models import UserProfile

User = get_user_model()

class RegisterTestCase(APITestCase):

    def test_can_register(self):
        data = {
            "username": "rickyputra98@gmail.com",
            "password": "asdasd123",
            "first_name": "Ricky",
            "last_name": "Putra",
        }
        response = self.client.post(reverse('register'), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cannot_register_with_empty_password(self):
        data = {
            "username": "rickyputra98@gmail.com",
            "password": "",
            "first_name": "Ricky",
            "last_name": "Putra",
        }
        response = self.client.post(reverse('register'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_register_with_same_email_address(self):
        data = {
            "username": "rickyputra98@gmail.com",
            "password": "asdasd123",
            "first_name": "Ricky",
            "last_name": "Putra",
        }
        response = self.client.post(reverse('register'), data=data)
        data = {
            "username": "rickyputra98@gmail.com",
            "password": "asdasd123",
            "first_name": "Ricky",
            "last_name": "Putra",
        }
        response = self.client.post(reverse('register'), data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class LoginTestCase(APITestCase):

    def setUp(self):
        User.objects.create_user(username='rickyputra98@gmail.com', email='rickyputra98@gmail.com', password='asdasd123')

    def test_can_login(self):
        data = {
            'username': 'rickyputra98@gmail.com',
            'password': 'asdasd123'
        }
        response = self.client.post(reverse('login'), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cannot_login_wrong_password(self):
        data = {
            'username': 'rickyputra98@gmail.com',
            'password': 'asdasd12'
        }
        response = self.client.post(reverse('login'), data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_cannot_login_user_doesnt_exist(self):
        data = {
            'username': 'asdasd@gmail.com',
            'password': 'asdasd123'
        }
        response = self.client.post(reverse('login'), data=data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

class ChangePasswordTestCase(APITestCase):

    def setUp(self):
        User.objects.create_user(username='rickyputra98@gmail.com', email='rickyputra98@gmail.com', password='asdasd123')

    def tearDown(self):
        User.objects.all().delete()

    def test_can_change_password(self):
        user_cred = {
            'username': 'rickyputra98@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'password': 'ricky123',
            'confirm_password': 'ricky123'
        }
        response = self.client.post(reverse("change-password"),
            data=data,
            format='json',
            HTTP_AUTHORIZATION="JWT " + token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cannot_change_password_not_match(self):
        user_cred = {
            'username': 'rickyputra98@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'password': 'ricky123',
            'confirm_password': 'ricky12'
        }
        response = self.client.post(reverse("change-password"),
            data=data,
            format='json',
            HTTP_AUTHORIZATION="JWT " + token)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class ProfileTestCase(APITestCase):

    def setUp(self):
        user = User.objects.create_user(username='rickyputra98@gmail.com', email='rickyputra98@gmail.com', password='asdasd123')
        UserProfile.objects.create(user=user)

    def tearDown(self):
        User.objects.all().delete()

    def test_can_get_profile(self):
        user_cred = {
            'username': 'rickyputra98@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        response = self.client.get(reverse("profile"),
            format='json',
            HTTP_AUTHORIZATION="JWT " + token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class EditProfileTestCase(APITestCase):
    def setUp(self):
        user = User.objects.create_user(username='rickyputra98@gmail.com', email='rickyputra98@gmail.com', password='asdasd123')
        UserProfile.objects.create(user=user)

    def tearDown(self):
        User.objects.all().delete()

    def test_can_edit_profile(self):
        user_cred = {
            'username': 'rickyputra98@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            "first_name": "Hehe",
            "last_name": "Hihi",
            "phone_number": "08123123123",
            "location": "Pintu Besi",
        }
        response = self.client.post(reverse("edit-profile"),
            data=data,
            format='json',
            HTTP_AUTHORIZATION="JWT " + token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UserSearchTestCase(APITestCase):

    def setUp(self):
        user = User.objects.create_user(username='rickyputra98@gmail.com', email='rickyputra98@gmail.com', password='asdasd123')
        UserProfile.objects.create(user=user)

    def tearDown(self):
        User.objects.all().delete()
        UserProfile.objects.all().delete()

    def test_can_search_user(self):
        user_cred = {
            'username': 'rickyputra98@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'username': 'rickyputra98@gmail.com'
        }
        response = self.client.post(reverse('search'), data=data, HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_cannot_search_user_user_doesnt_exist(self):
        user_cred = {
            'username': 'rickyputra98@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'username': 'rickyputra981@gmail.com'
        }
        response = self.client.post(reverse('search'), data=data, HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class UpdateTokenTestCase(APITestCase):

    def setUp(self):
        user = User.objects.create_user(username='rickyputra98@gmail.com', email='rickyputra98@gmail.com', password='asdasd123')
        UserProfile.objects.create(user=user)
        self.device = GCMDevice.objects.create(registration_id="token123", user=user, cloud_message_type="FCM")
        self.user = User.objects.create_user(username='ricky@gmail.com', email='ricky@gmail.com', password='asdasd123')
        UserProfile.objects.create(user=self.user)

    def tearDown(self):
        GCMDevice.objects.all().delete()
        User.objects.all().delete()
        UserProfile.objects.all().delete()

    def test_can_update_token(self):
        user_cred = {
            'username': 'rickyputra98@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'token': 'token456'
        }
        response = self.client.post(reverse('update-token'), data=json.dumps(data), HTTP_AUTHORIZATION='JWT ' + token, content_type="application/json")
        self.device.refresh_from_db()
        self.assertEqual(self.device.registration_id, 'token456')

    def test_can_update_token_new(self):
        user_cred = {
            'username': 'ricky@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = {
            'token': 'token456'
        }
        response = self.client.post(reverse('update-token'), data=json.dumps(data), HTTP_AUTHORIZATION='JWT ' + token, content_type="application/json")
        self.assertEqual(GCMDevice.objects.get(user__id=self.user.id).registration_id, 'token456')

class NotificationTestCase(APITestCase):
    def setUp(self):
        user = User.objects.create_user(username='rickyputra98@gmail.com', email='rickyputra98@gmail.com', password='asdasd123')
        UserProfile.objects.create(user=user)

    def test_can_get_notification_list(self):
        user_cred = {
            'username': 'rickyputra98@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']
        response = self.client.get(reverse('notification'), HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)