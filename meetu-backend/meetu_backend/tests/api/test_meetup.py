import json

from datetime import datetime

from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework.test import APITestCase
from rest_framework import status

from django.contrib.auth import get_user_model
from meetu_backend.apps.authentication.models import UserProfile
from meetu_backend.apps.group.models import Group, Membership
from meetu_backend.apps.meetup.models import MeetUp
from meetu_backend.apps.schedule.models import Schedule

User = get_user_model()

class MeetUpTestCase(APITestCase):

    def setUp(self):
        user = User.objects.create_user(username='rickyasd90@gmail.com', email='rickyasd90@gmail.com', password='asdasd123')
        user_profile = UserProfile.objects.create(user=user, phone_number='+9999999999', location='Fasilkom UI')
        group1 = Group.objects.create(owner=user_profile, title='PPL')
        Membership.objects.create(user_profile=user_profile, group=group1, type='Owner')
        user2 = User.objects.create_user(username='rickyasd91@gmail.com', email='rickyasd91@gmail.com', password='asdasd123')
        user_profile2 = UserProfile.objects.create(user=user2, phone_number='+9999999999', location='Fasilkom UI')

    def tearDown(self):
        User.objects.filter(username='rickyasd90@gmail.com').delete()

    def test_can_get_all_meetup(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']
        response = self.client.get(reverse('meetup-list'), HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_post_meetup(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = [{
                    "group": {
                        "id": 1,
                        "owner": 1,
                        "title": "TestGroup"
                    },
                    "title": "Test",
                    "start_time": "2018-01-31T00:00:00Z",
                    "end_time": "2018-01-31T00:15:00Z"
                }]

        response = self.client.post(reverse('meetup-list'), data=data, format='json', HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(MeetUp.objects.filter(title='Test').count(), 1)

    def test_can_post_meetup_daily(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = [{
                    "group": {
                        "id": 1,
                        "owner": 1,
                        "title": "TestGroup"
                    },
                    "title": "Test",
                    "start_time": "2018-01-01T00:00:00Z",
                    "end_time": "2018-01-01T00:15:00Z"
                }, {
                    "group": {
                        "id": 1,
                        "owner": 1,
                        "title": "TestGroup"
                    },
                    "title": "Test",
                    "start_time": "2018-01-02T00:00:00Z",
                    "end_time": "2018-01-02T00:15:00Z"
                }]

        response = self.client.post(reverse('meetup-list'), data=data, format='json', HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(MeetUp.objects.filter(title='Test').count(), 2)

    def test_can_post_meetup_weekly(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = [{
                    "group": {
                        "id": 1,
                        "owner": 1,
                        "title": "TestGroup"
                    },
                    "title": "Test",
                    "start_time": "2018-01-01T00:00:00Z",
                    "end_time": "2018-01-01T00:15:00Z"
                }, {
                    "group": {
                        "id": 1,
                        "owner": 1,
                        "title": "TestGroup"
                    },
                    "title": "Test",
                    "start_time": "2018-01-08T00:00:00Z",
                    "end_time": "2018-01-08T00:15:00Z"
                }]

        response = self.client.post(reverse('meetup-list'), data=data, format='json', HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(MeetUp.objects.filter(title='Test').count(), 2)

    def test_can_post_meetup_monthly(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = [{
                    "group": {
                        "id": 1,
                        "owner": 1,
                        "title": "TestGroup"
                    },
                    "title": "Test",
                    "start_time": "2018-01-01T00:00:00Z",
                    "end_time": "2018-01-01T00:15:00Z"
                }, {
                    "group": {
                        "id": 1,
                        "owner": 1,
                        "title": "TestGroup"
                    },
                    "title": "Test",
                    "start_time": "2018-02-01T00:00:00Z",
                    "end_time": "2018-02-01T00:15:00Z"
                }]

        response = self.client.post(reverse('meetup-list'), data=data, format='json', HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(MeetUp.objects.filter(title='Test').count(), 2)

    def test_cannot_post_meetup_conflict_with_other_meetup(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = [
            {
                "group": {
                    "id": 1,
                    "owner": 1,
                    "title": "TestGroup"
                },

                "title": "Test 2",
                "start_time": "2018-05-31T00:00:00Z",
                "end_time": "2018-05-31T00:15:00Z"
            },
        ]

        response = self.client.post(reverse('meetup-list'), data=data, format='json', HTTP_AUTHORIZATION='JWT ' + token)
        response = self.client.post(reverse('meetup-list'), data=data, format='json', HTTP_AUTHORIZATION='JWT ' + token)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_post_meetup_conflict_with_other_schedule(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        Schedule.objects.create(title='Test 1', start_time="2018-03-31T00:00:00Z", end_time="2018-03-31T00:15:00Z")
        data = [
            {
                "group": {
                    "id": 1,
                    "owner": 1,
                    "title": "TestGroup"
                },

                "title": "Test 2",
                "start_time": "2018-03-31T00:00:00Z",
                "end_time": "2018-03-31T00:15:00Z"
            },
        ]

        response = self.client.post(reverse('meetup-list'), data=data, format='json', HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_post_meetup_when_group_doesnt_exist(self):
        user_cred = {
            'username': 'rickyasd91@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']
        data = [
            {
                "group": {
                    "id": 2,
                    "owner": 1,
                    "title": "TestGroup"
                },
                "title": "Test 2",
                "start_time": "2018-03-31T00:00:00Z",
                "end_time": "2018-03-31T00:15:00Z"
            },
        ]

        response = self.client.post(reverse('meetup-list'), data=data, format='json', HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_cannot_post_meetup_when_not_a_member(self):
        user_cred = {
            'username': 'rickyasd91@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']
        data = [
            {
                "group": {
                    "id": 1,
                    "owner": 1,
                    "title": "TestGroup"
                },
                "title": "Test 2",
                "start_time": "2018-03-31T00:00:00Z",
                "end_time": "2018-03-31T00:15:00Z"
            },
        ]

        response = self.client.post(reverse('meetup-list'), data=data, format='json', HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_bad_request_post_meetup(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']

        data = [
            {
                "title": "Test 2",
                "start_time": "2018-03-31T00:00:00Z",
                "end_time": "2018-03-31T00:15:00Z"
            },
        ]
        response = self.client.post(reverse('meetup-list'), data=data, format='json', HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
