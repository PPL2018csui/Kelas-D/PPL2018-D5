from datetime import datetime    

from django.urls import reverse
from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase
from rest_framework import status

from meetu_backend.apps.schedule.models import Schedule
from meetu_backend.apps.authentication.models import UserProfile
from meetu_backend.apps.group.models import Group, Membership

User = get_user_model()

class ScheduleListViewTestCase(APITestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def setUp(self):
        user = User.objects.create_user(username='rickyasd90@gmail.com', email='rickyasd90@gmail.com', password='asdasd123')
        user_profile = UserProfile.objects.create(user=user)
        Schedule.objects.create(title='Test 1', start_time=datetime.now(), end_time=datetime.now(), owner=user_profile)
        group1 = Group.objects.create(owner=user_profile, title='PPL')
        Membership.objects.create(user_profile=user_profile, group=group1, type='Owner')

    def tearDown(self):
        User.objects.filter(username='rickyasd90@gmail.com').delete()

    def test_can_post_schedules(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']
        data = [
            {
                'title': 'Test 2',
                'start_time': '2018-03-06T23:59',
                'end_time':'2018-03-06T23:59'
            }
        ]
        response = self.client.post(reverse('schedule-list'), data=data, HTTP_AUTHORIZATION='JWT ' + token, format='json')
        self.assertEqual(Schedule.objects.filter(title='Test 2').count(), 1)

    def test_cannot_post_schedule_with_same_time_from_2_different_request(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']
        data = [
                {
                    'title': 'Test 3',
                    'start_time': '2018-03-06T23:59',
                    'end_time':'2018-03-06T23:59'
                }
        ]
        response = self.client.post(reverse('schedule-list'), data=data, HTTP_AUTHORIZATION='JWT ' + token, format='json')
        response = self.client.post(reverse('schedule-list'), data=data, HTTP_AUTHORIZATION='JWT ' + token, format='json')
        
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
    
    def test_cannot_post_schedule_with_same_time_meetup_schedule(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']        
        data = [{
                    "group": {
                        "id": 1,
                        "owner": 1,
                        "title": "TestGroup"
                    },
                    "title": "Test",
                    'start_time': '2018-03-06T23:59',
                    'end_time':'2018-03-06T23:59'
                }]

        response = self.client.post(reverse('meetup-list'), data=data, format='json', HTTP_AUTHORIZATION='JWT ' + token)
        data = [
                {
                    'title': 'Test 3',
                    'start_time': '2018-03-06T23:59',
                    'end_time':'2018-03-06T23:59'
                }
        ]
        response = self.client.post(reverse('schedule-list'), data=data, HTTP_AUTHORIZATION='JWT ' + token, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_can_delete_schedule_by_parent_id(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']
        data = {
            'parent_id': Schedule.objects.filter(title='Test 1').first().parent_id
        }
        response = self.client.delete(reverse('schedule-list'), data=data, HTTP_AUTHORIZATION='JWT ' + token, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Schedule.objects.filter(title='Test 1').count(), 0)

    def test_can_get_all_schedule(self):
        user_cred = {
            'username': 'rickyasd90@gmail.com',
            'password': 'asdasd123'
        }
        token = self.client.post(reverse('login'), data=user_cred).data['token']
        response = self.client.get(reverse('schedule-list'), HTTP_AUTHORIZATION='JWT ' + token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
