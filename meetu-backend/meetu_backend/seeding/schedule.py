import uuid

from pytz import UTC
from datetime import datetime
from django.contrib.auth import get_user_model

from meetu_backend.apps.authentication.models import UserProfile
from meetu_backend.apps.schedule.models import Schedule

User = get_user_model()

def setup_seeding_data():
    user, created = User.objects.get_or_create(
        username='rickyputra98@gmail.com', first_name='Ricky', last_name='Putra')
    user.set_password('dummypassword')
    user.save()
    user_profile, created = UserProfile.objects.get_or_create(user=user, phone_number='+9999999999', location='Fasilkom UI')
    schedules = [
        {
            'owner': user_profile,
            'title': 'Kelas Aljabar Linear 1',
            'parent_id': uuid.uuid4(),
            'start_time': datetime(2018, 4, 1, 1, 30, tzinfo=UTC),
            'end_time': datetime(2018, 4, 1, 3, 15, tzinfo=UTC),
        },
        {
            'owner': user_profile,
            'title': 'Kelas Analisis Numerik 2',
            'parent_id': uuid.uuid4(),
            'start_time': datetime(2018, 4, 1, 3, 0, tzinfo=UTC),
            'end_time': datetime(2018, 4, 1, 5, 15, tzinfo=UTC)
        },
        {
            'owner': user_profile,
            'title': 'Kelas Matematika Dasar 2',
            'parent_id': uuid.uuid4(),
            'start_time': datetime(2018, 4, 1, 6, 0, tzinfo=UTC),
            'end_time': datetime(2018, 4, 1, 7, 15, tzinfo=UTC)
        },
        {
            'owner': user_profile,
            'title': 'Meditasi',
            'parent_id': uuid.uuid4(),
            'start_time': datetime(2018, 4, 1, 8, 0, tzinfo=UTC),
            'end_time': datetime(2018, 4, 1, 9, 0, tzinfo=UTC)
        },
        {
            'owner': user_profile,
            'title': 'Refleksi diri',
            'parent_id': uuid.uuid4(),
            'start_time': datetime(2018, 3, 8, 10, 0, tzinfo=UTC),
            'end_time': datetime(2018, 3, 8, 11, 0, tzinfo=UTC)
        },
        {
            'owner': user_profile,
            'title': 'Membaca buku Stephen Hawking',
            'parent_id': uuid.uuid4(),
            'start_time': datetime(2018, 3, 2, 6, 0, tzinfo=UTC),
            'end_time': datetime(2018, 3, 2, 8, 45, tzinfo=UTC)
        },
        {
            'owner': user_profile,
            'title': 'Membaca biografi Bill Gates',
            'parent_id': uuid.uuid4(),
            'start_time': datetime(2018, 3, 6, 12, 0, tzinfo=UTC),
            'end_time': datetime(2018, 3, 6, 13, 45, tzinfo=UTC)
        }
    ]

    for schedule in schedules:
        Schedule.objects.create(**schedule)
