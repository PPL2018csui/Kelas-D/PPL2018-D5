from datetime import datetime
from dateutil.relativedelta import relativedelta
from pytz import UTC

from django.contrib.auth import get_user_model
from meetu_backend.apps.authentication.models import UserProfile
from meetu_backend.apps.group.models import Group, Membership
from meetu_backend.apps.meetup.models import MeetUp

User = get_user_model()


def setup_seeding_data():
    user1, created = User.objects.get_or_create(
        username='rickyputra98@gmail.com', first_name='Ricky', last_name='Putra')
    user1.set_password('dummypassword')
    user1.save()

    user2, created = User.objects.get_or_create(
        username='luthfisulaiman@gmail.com', first_name='Luthfi', last_name='Sulaiman')
    user2.set_password('dummypassword')
    user2.save()

    user3, created = User.objects.get_or_create(
        username='azure97@gmail.com', first_name='Muhammad', last_name='Ayaz')
    user3.set_password('dummypassword')
    user3.save()

    user4, created = User.objects.get_or_create(
        username='graceanglc@gmail.com', first_name='Grace', last_name='Angelica')
    user4.set_password('dummypassword')
    user4.save()

    user5, created = User.objects.get_or_create(
        username='andrehendra@gmail.com', first_name='Andre', last_name='Hendra')
    user5.set_password('dummypassword')
    user5.save()

    user6, created = User.objects.get_or_create(
        username='irmalaily@gmail.com', first_name='Irma', last_name='Laily')
    user6.set_password('dummypassword')
    user6.save()

    user7, created = User.objects.get_or_create(
        username='ferdinand@gmail.com', first_name='Ferdinand', last_name='Chandra')
    user7.set_password('dummypassword')
    user7.save()

    user8, created = User.objects.get_or_create(
        username='halim.kustiawanto@gmail.com', first_name='Kustiawanto', last_name='Halim')
    user8.set_password('dummypassword')
    user8.save()

    user9, created = User.objects.get_or_create(
        username='gilangjanuar@gmail.com', first_name='Gilang', last_name='Januar')
    user9.set_password('dummypassword')
    user9.save()

    user10, created = User.objects.get_or_create(
        username='stephenjaya@gmail.com', first_name='Stephen', last_name='Jaya')
    user10.set_password('dummypassword')
    user10.save()

    user_profile1, created = UserProfile.objects.get_or_create(
        user=user1, phone_number='+9999999999', location='Fasilkom UI')
    user_profile2, created = UserProfile.objects.get_or_create(
        user=user2, phone_number='+9999999998', location='Fasilkom UI')
    user_profile3, created = UserProfile.objects.get_or_create(
        user=user3, phone_number='+9999999997', location='Fasilkom UI')
    user_profile4, created = UserProfile.objects.get_or_create(
        user=user4, phone_number='+9999999996', location='Fasilkom UI')
    user_profile5, created = UserProfile.objects.get_or_create(
        user=user5, phone_number='+9999999995', location='Fasilkom UI')
    user_profile6, created = UserProfile.objects.get_or_create(
        user=user6, phone_number='+9999999994', location='Fasilkom UI')
    user_profile7, created = UserProfile.objects.get_or_create(
        user=user7, phone_number='+9999999993', location='Fasilkom UI')
    user_profile8, created = UserProfile.objects.get_or_create(
        user=user8, phone_number='+9999999992', location='Fasilkom UI')
    user_profile9, created = UserProfile.objects.get_or_create(
        user=user9, phone_number='+9999999991', location='Fasilkom UI')
    user_profile10, created = UserProfile.objects.get_or_create(
        user=user10, phone_number='+9999999990', location='Fasilkom UI')

    group1, created = Group.objects.get_or_create(
        owner=user_profile1, title='PPL')
    group2, created = Group.objects.get_or_create(
        owner=user_profile1, title='MIC')
    group3, created = Group.objects.get_or_create(
        owner=user_profile1, title='Ristek')
    group4, created = Group.objects.get_or_create(
        owner=user_profile4, title='BEM')

    Membership.objects.get_or_create(
        group=group1, user_profile=user_profile1, type='Owner')
    Membership.objects.get_or_create(
        group=group1, user_profile=user_profile2, type='Member')
    Membership.objects.get_or_create(
        group=group1, user_profile=user_profile3, type='Member')
    Membership.objects.get_or_create(
        group=group1, user_profile=user_profile4, type='Member')

    Membership.objects.get_or_create(
        group=group2, user_profile=user_profile1, type='Owner')
    Membership.objects.get_or_create(
        group=group2, user_profile=user_profile3, type='Member')
    Membership.objects.get_or_create(
        group=group2, user_profile=user_profile4, type='Member')
    Membership.objects.get_or_create(
        group=group2, user_profile=user_profile2, type='Member')

    Membership.objects.get_or_create(
        group=group3, user_profile=user_profile1, type='Owner')
    Membership.objects.get_or_create(
        group=group3, user_profile=user_profile4, type='Member')
    Membership.objects.get_or_create(
        group=group3, user_profile=user_profile2, type='Member')

    Membership.objects.get_or_create(
        group=group4, user_profile=user_profile1, type='Owner')
    Membership.objects.get_or_create(
        group=group4, user_profile=user_profile3, type='Member')
