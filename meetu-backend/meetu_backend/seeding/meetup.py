from datetime import datetime
from dateutil.relativedelta import relativedelta
from pytz import UTC

from django.contrib.auth import get_user_model
from meetu_backend.apps.authentication.models import UserProfile
from meetu_backend.apps.group.models import Group, Membership
from meetu_backend.apps.meetup.models import MeetUp

User = get_user_model()

def setup_seeding_data():
    user, created = User.objects.get_or_create(
        username='rickyputra98@gmail.com', first_name='Ricky', last_name='Putra')
    user.set_password('dummypassword')
    user.save()
    user_profile, created = UserProfile.objects.get_or_create(user=user, phone_number='+9999999999', location='Fasilkom UI')
    group1, created = Group.objects.get_or_create(owner=user_profile, title='PPL')
    group2, created = Group.objects.get_or_create(owner=user_profile, title='MIC')
    group3, created = Group.objects.get_or_create(owner=user_profile, title='Ristek')
    Membership.objects.get_or_create(group=group1, user_profile=user_profile, type='Owner')
    Membership.objects.get_or_create(group=group2, user_profile=user_profile, type='Owner')
    Membership.objects.get_or_create(group=group3, user_profile=user_profile, type='Owner')

    meetups = [
        {
            'title': 'Daily Standup',
            'group': group1,
            'start_time': datetime(2018, 2, 1, 7, 30, tzinfo=UTC),
            'end_time': datetime(2018, 2, 1, 8, 15, tzinfo=UTC),
        },
        {
            'title': 'Sprint Review 1',
            'group': group1,
            'start_time': datetime(2018, 2, 1, 1, 0, tzinfo=UTC),
            'end_time': datetime(2018, 2, 1, 3, 15, tzinfo=UTC)
        },
        {
            'title': 'Sprint Review 2',
            'group': group1,
            'start_time': datetime(2018, 3, 1, 1, 0, tzinfo=UTC),
            'end_time': datetime(2018, 3, 1, 3, 15, tzinfo=UTC)
        },
        {
            'title': 'Rapat PI MIC',
            'group': group2,
            'start_time': datetime(2018, 2, 1, 12, 0, tzinfo=UTC),
            'end_time': datetime(2018, 2, 1, 13, 0, tzinfo=UTC)
        },
        {
            'title': 'Rapat PI MIC',
            'group': group2,
            'start_time': datetime(2018, 2, 8, 12, 0, tzinfo=UTC),
            'end_time': datetime(2018, 2, 8, 13, 0, tzinfo=UTC)
        },
        {
            'title': 'Internal Class Web Dev',
            'group': group3,
            'start_time': datetime(2018, 2, 2, 12, 0, tzinfo=UTC),
            'end_time': datetime(2018, 2, 2, 13, 45, tzinfo=UTC)
        },
        {
            'title': 'Internal Class Web Dev',
            'group': group3,
            'start_time': datetime(2018, 2, 6, 12, 0, tzinfo=UTC),
            'end_time': datetime(2018, 2, 6, 13, 45, tzinfo=UTC)
        }
    ]

    for meetup in meetups:
        title = meetup['title']
        start_time = meetup['start_time']
        end_time = meetup['end_time']
        meetup_group = meetup['group']

        MeetUp.objects.create(group=meetup_group, title=title, start_time=start_time, end_time=end_time)
