def sort_group_members_by_member_name_and_current_user(group, current_user_profile_id):
    group_members_sorted = sorted(group['members'], key=lambda k: (k['user']['first_name'].lower(), k['user']['last_name'].lower()))

    # get current user profile
    current_user_profile = None
    for x in group_members_sorted:
        if x['id'] == current_user_profile_id:
            current_user_profile = x
            break

    # remove current user profile from list
    group_members_sorted[:] = [m for m in group_members_sorted if m.get('id') != current_user_profile_id]

    # append in the fron
    group_members_sorted.insert(0, current_user_profile)

    group['members'] = group_members_sorted

    return group