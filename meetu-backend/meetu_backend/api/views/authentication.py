import jwt
import firebase_admin

from firebase_admin import credentials
from firebase_admin.auth import verify_id_token
from django.contrib.auth import get_user_model
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework_jwt.settings import api_settings
from push_notifications.models import GCMDevice

from meetu_backend.apps.authentication.models import Notification, UserProfile
from meetu_backend.settings import is_run_in_prod_env
from meetu_backend.api.serializers.authentication import (
    UserSerializer,
    RegisterSerializer,
    LoginSerializer,
    ChangePasswordSerializer,
    EditProfileSerializer,
    NotificationSerializer,
    UserProfileSerializer,
)
from meetu_backend.settings import SECRET_KEY
import jwt

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

User = get_user_model()

if is_run_in_prod_env(): # pragma: no cover
    cred = credentials.Certificate('meetu_backend/api/views/serviceAccount.json')
else:
    cred = credentials.Certificate('meetu_backend/api/views/serviceAccountDev.json')
default_app = firebase_admin.initialize_app(cred)

class RegisterView(APIView):

    serializer_class = UserSerializer
    permission_classes = (AllowAny,)


    def post(self, request, format=None):
        ser = RegisterSerializer(data=request.data)
        if ser.is_valid():
            user = ser.save()
            ser_user = self.serializer_class(user)
            data = {
                'user': ser_user.data,
                'token': jwt_encode_handler(jwt_payload_handler(user))
            }
            return Response(data)

        return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

class LoginView(APIView):

    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        ser = LoginSerializer(data=request.data)
        if ser.is_valid():
            user = User.objects.get(username=ser.validated_data)
            user_ser = UserSerializer(instance=user)
            data = {
                "user": user_ser.data,
                "token": jwt_encode_handler(jwt_payload_handler(user))
            }
            return Response(data)
        return Response(ser.errors, status=status.HTTP_401_UNAUTHORIZED)

class ChangePasswordView(APIView):

    def post(self, request, format=None):
        data = dict(request.data)
        data['user'] = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        ser = ChangePasswordSerializer(data=data)
        if ser.is_valid():
            ser.save()
            return Response()
        return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

class ProfileView(APIView):

    def get(self, request, format=None):
        user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        return Response(UserSerializer(User.objects.get(id=user_id)).data)


class EditProfileView(APIView):


    def post(self, request, format=None):
        data = request.data
        data['user'] = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        ser = EditProfileSerializer(data=data)
        ser.is_valid()
        user = ser.save()
        return Response(UserSerializer(user).data)\

class UserSearchView(APIView):

    def post(self, request, format=None):
        user = User.objects.filter(username=request.data.get('username', None)).first()
        if user:
            return Response(UserSerializer(user).data)
        return Response({'error_message': 'User not found'}, status=status.HTTP_404_NOT_FOUND)


class LoginSocialView(APIView): # pragma: no cover

    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        try:
            decoded_token = verify_id_token(request.data['token'])
            user = User.objects.get(username=request.data['username'])
            data = {
                "user": UserSerializer(user).data,
                "token": jwt_encode_handler(jwt_payload_handler(user))
            }
            return Response(data)
        except User.DoesNotExist:
            user = User.objects.create_user(username=request.data['username'], first_name=request.data['first_name'], last_name=request.data['last_name'])
            user_profile = UserProfile.objects.create(user=user)
            user.refresh_from_db()
            data = {
                "user": UserSerializer(user).data,
                "token": jwt_encode_handler(jwt_payload_handler(user))
            }
            return Response(data)
        except:
            return Response({'error_message': 'Token not valid'}, status=status.HTTP_401_UNAUTHORIZED)

class UpdateTokenView(APIView):

    def post(self, request, format=None):
        user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        user = User.objects.get(id=user_id)
        try:
            device = GCMDevice.objects.get(user=user, cloud_message_type="FCM")
        except GCMDevice.DoesNotExist:
            device = GCMDevice.objects.create(user=user, cloud_message_type="FCM", registration_id=request.data['token'])
        device.registration_id = request.data['token']
        device.save()
        return Response()

class NotificationListView(APIView):

    def get(self, request, format=None):
        user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        user = User.objects.get(id=user_id)
        notifications = Notification.objects.filter(user=user).order_by('-created_date')[:20]
        return Response(NotificationSerializer(notifications, many=True).data)
