from datetime import date
import jwt

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from push_notifications.models import GCMDevice

from meetu_backend.api.serializers.meetup import MeetUpSerializer, MeetUpCreateSerializer
from meetu_backend.apps.authentication.models import UserProfile, Notification
from meetu_backend.seeding.meetup import setup_seeding_data
from meetu_backend.apps.meetup.models import MeetUp
from meetu_backend.apps.group.models import Group
from meetu_backend.settings import SECRET_KEY

class MeetUpListView(APIView):

    def get(self, request, format=None):
        user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        user_profile = UserProfile.objects.get(user__id=user_id)
        meetups = MeetUp.objects.filter(group__members__in=[user_profile])
        return Response(MeetUpSerializer(meetups.order_by('start_time'), many=True).data)

    def post(self, request, format=None):
        group_id = None
        try:
            for d in request.data:
                d['group'] = d['group']['id']
                group_id = d['group']
        except Exception as e:
            return Response({"error_message": "Group must not empty"}, status=status.HTTP_400_BAD_REQUEST)

        try:
            group = Group.objects.get(id=group_id)
            user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
            user_profile = UserProfile.objects.get(user__id=user_id)
            if user_profile not in group.members.all():
                return Response({"error_message": "You're not a member of this group"}, status=status.HTTP_401_UNAUTHORIZED)
        except Exception as e:
            return Response({"error_message": "Bad request data"}, status=status.HTTP_400_BAD_REQUEST)

        meetups_serializer = MeetUpCreateSerializer(data=request.data, many=True)
        if meetups_serializer.is_valid():
            meetups = meetups_serializer.save()
            repetition_type = None
            if len(meetups) == 1:
                repetition_type = 'One-time event'
            else:
                date1 = date(meetups[0].start_time.year, meetups[0].start_time.month, meetups[0].start_time.day)
                date2 = date(meetups[1].start_time.year, meetups[1].start_time.month, meetups[1].start_time.day)
                delta = date2 - date1
                if delta.days == 1:
                    repetition_type = 'Daily event for %s day(s)' % len(meetups)
                elif delta.days == 7:
                    repetition_type = 'Weekly event for %s week(s)' % len(meetups)
                else:
                    repetition_type = 'Monthly event for %s month(s)'  % len(meetups)
            for member in group.members.all(): # pragma: no cover
                if member.id == user_profile.id:
                    continue
                data = {}
                data['parent_id'] = str(meetups[0].parent_id)
                data['meetup_title'] = meetups[0].title
                data['type'] = 'MeetUp'
                data['group'] = group.title
                data['start_time'] = meetups[0].start_time.isoformat()
                data['end_time'] = meetups[0].end_time.isoformat()
                data['repetition'] = repetition_type
                devices = GCMDevice.objects.filter(user__id=member.user.id)
                devices.send_message("You have been invited by %s group to join %s meetup" % (group.title, meetups[0].title), title="You\'re invited!", extra=data)
                Notification.objects.create(
                    title=meetups[0].title,
                    body='You have been invited by %s group to join %s meetup' % (group.title, meetups[0].title),
                    notification_type='MeetUp',
                    user=member.user,
                    inviter=group.title,
                    repetition=repetition_type,
                    start_time=data['start_time'],
                    end_time=data['end_time'],
                )
            response = {"success": True, "message": "Post success"}
            return Response(response, status=status.HTTP_201_CREATED)
        return Response(meetups_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
