from datetime import datetime
from pytz import UTC

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from meetu_backend.apps.scheduling.scheduling import BasicScheduling

class SchedulingListView(APIView):

    # time_zone used here is date(1900, 1, 1) in user device
    def post(self, request, format=None):
        data = request.data

        try:
            start_time = data.get('start_date', None)
            end_time = data.get('end_date', None)
            time_zone = data.get('time_zone', None)

            start_time = datetime.strptime(start_time, '%Y-%m-%dT%H:%M:%SZ')
            end_time = datetime.strptime(end_time, '%Y-%m-%dT%H:%M:%SZ')
            time_zone = datetime.strptime(time_zone, '%Y-%m-%dT%H:%M:%SZ')
        except Exception as e:
            response = {'error_message': 'Invalid date format'}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)

        start_time = start_time.replace(tzinfo=UTC)
        end_time = end_time.replace(tzinfo=UTC)
        time_zone = time_zone.replace(tzinfo=UTC)

        group_id = data.get('group_id', None)
        repeat_type = data.get('repeat_type', None)
        repeat_count = data.get('repeat_count', None)

        basic_scheduling = BasicScheduling()

        try:
            availables = basic_scheduling.get_available_schedules(group_id, time_zone, start_time, end_time, repeat_type, repeat_count)
        except Exception as e:
            response = {'error_message': str(e)}
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        
        return Response(availables, status=status.HTTP_200_OK)
