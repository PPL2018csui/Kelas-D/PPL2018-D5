import jwt

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import QueryDict

from meetu_backend.api.serializers.group import GroupMinSerializer, GroupSerializer, AddMemberSerializer, AddMemberBulkSerializer
from meetu_backend.seeding.group import setup_seeding_data
from meetu_backend.apps.authentication.models import UserProfile
from meetu_backend.apps.group.models import Group, Membership
from meetu_backend.settings import SECRET_KEY
from meetu_backend.api.utils.group import sort_group_members_by_member_name_and_current_user

class GroupListView(APIView):

    def get(self, request, format=None):
        # TODO: Fix this later
        user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        user_profile = UserProfile.objects.get(user__id=user_id)
        groups = Group.objects.filter(members__in=[user_profile]).distinct().order_by('title')
        return Response(GroupMinSerializer(groups, many=True).data)

    def post(self, request, format=None):
        user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        user_profile_id = UserProfile.objects.get(user__id=user_id).id

        group_data = {}
        group_data['title'] = request.data.get('title', None)
        group_data['owner'] = user_profile_id
        group_ser = GroupMinSerializer(data=group_data)

        members_data = {'members': []}
        if 'members' in request.data:
            members_data['members'] = request.data['members']
        member_ser = AddMemberBulkSerializer(data=members_data)

        if group_ser.is_valid() and member_ser.is_valid():
            group = group_ser.save()
            members_data['group'] = group.id
            member_ser = AddMemberBulkSerializer(data=members_data)
            member_ser.is_valid()
            group = member_ser.save()
            return Response(sort_group_members_by_member_name_and_current_user(GroupSerializer(group).data, user_profile_id),
                status=status.HTTP_201_CREATED)
        elif not group_ser.is_valid():
            return Response(group_ser.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(member_ser.errors, status=status.HTTP_400_BAD_REQUEST)


class GroupDetailView(APIView):

    def get(self, request, pk, format=None):
        user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        user_profile = UserProfile.objects.get(user__id=user_id)
        try:
            group = Group.objects.get(id=pk)
        except Group.DoesNotExist:
            return Response({'error_message': 'Group doesn\'t exist'}, status=status.HTTP_400_BAD_REQUEST)

        if user_profile not in group.members.all():
            return Response({'error_message': 'You\'re not part of this group'}, status=status.HTTP_401_UNAUTHORIZED)

        return Response(sort_group_members_by_member_name_and_current_user(GroupSerializer(group).data, user_profile.id))

class AddMemberView(APIView):

    def post(self, request):
        user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        user_profile = UserProfile.objects.get(user__id=user_id)

        try:
            group = Group.objects.get(id=request.data['group'])
        except Group.DoesNotExist:
            return Response({'error_message': 'Group doesn\'t exist'}, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({'error_message': 'Group must not empty'}, status=status.HTTP_400_BAD_REQUEST)

        if user_profile not in group.members.all():
            return Response({'error_message': 'You\'re not part of this group'}, status=status.HTTP_401_UNAUTHORIZED)

        data = dict(request.data)
        data['referrer_name'] = user_profile.user.first_name + " " + user_profile.user.last_name
        ser = AddMemberSerializer(data=data)
        if ser.is_valid():
            group = ser.save()
            return Response(sort_group_members_by_member_name_and_current_user(GroupSerializer(group).data, user_profile.id),
                status=status.HTTP_201_CREATED)
        return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)