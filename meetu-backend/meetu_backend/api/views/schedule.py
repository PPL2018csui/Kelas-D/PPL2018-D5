import jwt
import json

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from meetu_backend.api.serializers.schedule import ScheduleSerializer
from meetu_backend.apps.schedule.models import Schedule
from meetu_backend.apps.authentication.models import UserProfile
from meetu_backend.settings import SECRET_KEY

class ScheduleListView(APIView):

    def get(self, request, format=None):
        user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        response = ScheduleSerializer(Schedule.objects.filter(owner__user__id=user_id).order_by('start_time'), many=True).data
        return Response(response)

    def post(self, request, format=None):
        user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        user_profile = UserProfile.objects.get(user__id=user_id)
        data = request.data
        
        for d in data:
            d['owner'] = user_profile.id

        ser = ScheduleSerializer(data=data, many=True)
        if ser.is_valid():
            ser.save()
            response = {"success": True, "message": "Post success"}
            return Response(response)
        return Response(ser.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, format=None):
        user_id = jwt.decode(request.META['HTTP_AUTHORIZATION'].split(" ")[1], SECRET_KEY, algorithms=['HS256'])['user_id']
        parent_id = request.data.get('parent_id', None)
        Schedule.objects.filter(parent_id=parent_id, owner__user__id=user_id).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
