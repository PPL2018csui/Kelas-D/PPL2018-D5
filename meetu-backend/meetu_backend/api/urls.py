from django.conf.urls import url
from meetu_backend.api.views import (
    schedule,
    meetup,
    scheduling,
    group,
    authentication,
)

urlpatterns = [
    url(r'^login/', authentication.LoginView.as_view(), name='login'),
    url(r'^login-social/', authentication.LoginSocialView.as_view(), name='login-social'),
    url(r'^register/', authentication.RegisterView.as_view(), name='register'),
    url(r'^change-password/$', authentication.ChangePasswordView.as_view(), name='change-password'),
    url(r'^profile/$', authentication.ProfileView.as_view(), name='profile'),
    url(r'^edit-profile/$', authentication.EditProfileView.as_view(), name='edit-profile'),
    url(r'^update-token/', authentication.UpdateTokenView.as_view(), name='update-token'),
    url(r'^notifications/', authentication.NotificationListView.as_view(), name='notification'),
    url(r'^schedules/$', schedule.ScheduleListView.as_view(), name='schedule-list'),
    url(r'^meetups/$', meetup.MeetUpListView.as_view(), name='meetup-list'),
    url(r'^recommendation/$', scheduling.SchedulingListView.as_view(), name='recommendation-list'),
    url(r'^groups/$', group.GroupListView.as_view(), name='group-list'),
    url(r'^groups/(?P<pk>\d+)/$', group.GroupDetailView.as_view(), name='group-detail'),
    url(r'^groups/add-member/$', group.AddMemberView.as_view(), name='add-member'),
    url(r'^groups/search/$', authentication.UserSearchView.as_view(), name='search'),
]
