import uuid

from rest_framework import serializers

from meetu_backend.apps.meetup.models import MeetUp
from meetu_backend.apps.group.models import Group
from meetu_backend.apps.schedule.models import Schedule
from meetu_backend.api.serializers.group import GroupMinSerializer

class MeetUpSerializer(serializers.ModelSerializer):
    group = GroupMinSerializer()

    class Meta:
        model = MeetUp
        fields = '__all__'

class MeetUpCreateSerializer(serializers.ModelSerializer):
    group = serializers.PrimaryKeyRelatedField(queryset=Group.objects.all(), required=True)
    repetition_type = serializers.CharField(required=False, allow_blank=True)

    class Meta:
        model = MeetUp
        fields = '__all__'
        read_only_fields = ['parent_id',]

    def validate(self, attrs):
        members = attrs['group'].members.all()
        for schedule in Schedule.objects.filter(owner__in=members):
            if ((schedule.start_time < attrs['end_time'] < schedule.end_time) or (attrs['start_time'] < schedule.end_time < attrs['end_time']) \
                or (attrs['start_time'] == schedule.start_time and attrs['end_time'] == schedule.end_time)):
                raise serializers.ValidationError('Conflict exists')
        for meetup in MeetUp.objects.filter(group__members__in=members):
            if ((meetup.start_time < attrs['end_time'] < meetup.end_time) or (attrs['start_time'] < meetup.end_time < attrs['end_time']) \
                or (attrs['start_time'] == meetup.start_time and attrs['end_time'] == meetup.end_time)):
                raise serializers.ValidationError('Conflict exists')
        attrs['parent_id'] = uuid.uuid4()
        return attrs
