import uuid

from rest_framework import serializers
from push_notifications.models import GCMDevice

from meetu_backend.apps.group.models import Group, Membership
from meetu_backend.apps.authentication.models import UserProfile
from meetu_backend.api.serializers.authentication import UserProfileReverseSerializer
from meetu_backend.apps.authentication.models import Notification

class GroupMinSerializer(serializers.ModelSerializer):

    class Meta:
        model = Group
        fields = ['id', 'owner', 'title']

    def create(self, validated_data):
        group = Group.objects.create(owner=validated_data['owner'], title=validated_data['title'])
        Membership.objects.create(user_profile=validated_data['owner'], type='Owner', group=group)
        return group

class GroupSerializer(serializers.ModelSerializer):
    members = UserProfileReverseSerializer(many=True)

    class Meta:
        model = Group
        fields = '__all__'

class AddMemberSerializer(serializers.Serializer):
    user_profile = serializers.IntegerField()
    referrer_name = serializers.CharField(required=False, allow_blank=True)
    group = serializers.IntegerField()

    def create(self, validated_data):
        group = Group.objects.get(id=validated_data['group'])
        user_profile = UserProfile.objects.get(id=validated_data['user_profile'])
        device = GCMDevice.objects.get(user=user_profile.user)
        Notification.objects.create(
            title=group.title,
            body='You have been invited by %s to join %s group' % (validated_data['referrer_name'], group.title),
            notification_type='Group',
            user=user_profile.user,
            inviter=validated_data['referrer_name']
        )
        data = GroupMinSerializer(group).data
        data['referrer_name'] = validated_data['referrer_name']
        data['type'] = 'Group'
        data['group_title'] = group.title
        try:
            device.send_message("You have been invited by %s to join %s group" % (validated_data['referrer_name'], group.title), title="You\'re invited!", extra=data)
        except Exception as e:
            pass
        if Membership.objects.filter(user_profile=user_profile, group=group).count() == 0:
            Membership.objects.create(user_profile=user_profile, group=group, type='Member')
        return group

class AddMemberBulkSerializer(serializers.Serializer):
    members = serializers.ListField()
    group = serializers.IntegerField(required=False)

    def validate(self, attrs):
        try:
            for member in attrs['members']:
                UserProfile.objects.get(id=member)
        except UserProfile.DoesNotExist:
            raise serializers.ValidationError('User profile doesn\'t exist')
        except:
            raise serializers.ValidationError('Invalid data')

        return attrs

    def create(self, validated_data):
        group = Group.objects.get(id=validated_data['group'])
        for member in validated_data['members']:
            if member == group.owner.id: # pragma: no cover
                continue
            user_profile = UserProfile.objects.get(id=member)
            device = GCMDevice.objects.get(user=user_profile.user)
            name = "%s %s" % (group.owner.user.first_name, group.owner.user.last_name)
            data = GroupMinSerializer(group).data
            data['referrer_name'] = name
            data['type'] = 'Group'
            data['group_title'] = group.title
            Notification.objects.create(
                title=group.title,
                body='You have been invited by %s to join %s group' % (name, group.title),
                notification_type='Group',
                user=user_profile.user,
                inviter=name
            )
            try:
                device.send_message("You have been invited by %s to join %s group" % (name, group.title), title="You\'re invited!", extra=data)
            except Exception as e:
                pass
            if Membership.objects.filter(user_profile=user_profile, group=group).count() == 0:
                Membership.objects.create(user_profile=user_profile, group=group, type='Member')
        return group