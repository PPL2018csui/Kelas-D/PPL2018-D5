from django.contrib.auth import get_user_model, authenticate
from django.core.validators import RegexValidator
from rest_framework import serializers

from meetu_backend.apps.authentication.models import UserProfile, Notification

User = get_user_model()

def field_length(fieldname):
    field = next(field for field in User._meta.fields if field.name == fieldname)
    return field.max_length

def create_and_save_user(validated_data):
    # create user
    email = validated_data['username'].lower() # set to lower
    username = email.lower()
    password = validated_data['password']
    first_name = validated_data['first_name'] if 'first_name' in validated_data else ''
    last_name = validated_data['last_name'] if 'last_name' in validated_data else ''

    user = User.objects.create_user(username, email, password, first_name=first_name)

    if first_name:
        user.first_name = first_name
    if last_name:
        user.last_name = last_name

    user.save()

    # create profile when new user is created
    UserProfile.objects.get_or_create(user=user)

    return user

class NotificationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Notification
        fields = '__all__'

class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    userprofile = UserProfileSerializer()

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'userprofile', )

class UserMinSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', )

class UserProfileReverseSerializer(serializers.ModelSerializer):
    user = UserMinSerializer()

    class Meta:
        model = UserProfile
        fields = '__all__'

class LoginSerializer(serializers.Serializer):

    username = serializers.CharField(max_length=field_length('email'), required=True)
    password = serializers.CharField(max_length=field_length('password'), required=True)

    def authenticate(self, username=None, password=None):
        """ Authenticate a user based on email address as the user name. """
        try:
            user = User.objects.get(username=username)
            if user.check_password(password):
                username = user.get_username()
                return authenticate(username=username, password=password)
        except User.DoesNotExist:
            return None

    def validate(self, attrs):
        username = attrs['username'].lower() # force lowercase email
        user = self.authenticate(username=username,
                            password=attrs['password'])
        if user is None:
            raise serializers.ValidationError('Wrong email or password')
        return user

class RegisterSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=field_length('password'), required=True)
    username = serializers.EmailField(max_length=field_length('email'), required=True)
    first_name = serializers.CharField(max_length=128, required=True)
    last_name = serializers.CharField(max_length=128, required=False)

    def validate(self, attrs):
        password = attrs['password']
        username = attrs['username']

        username = username.lower()
        if User._default_manager.filter(username__iexact=username).exists():
            raise serializers.ValidationError(
                'A user with that email address already exists')

        return attrs

    def create(self, validated_data):
        return create_and_save_user(validated_data)

class ChangePasswordSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=field_length('password'), required=True)
    confirm_password = serializers.CharField(max_length=field_length('password'), required=True)
    user = serializers.IntegerField()

    def validate(self, attrs):
        if attrs['password'] != attrs['confirm_password']:
            raise serializers.ValidationError('Password doesn\'t match')

        return attrs

    def create(self, validated_data):
        user = User.objects.get(id=validated_data['user'])
        user.set_password(validated_data['password'])
        user.save()
        return user

class EditProfileSerializer(serializers.Serializer):
    user = serializers.IntegerField()
    first_name = serializers.CharField(max_length=128, required=False)
    last_name = serializers.CharField(max_length=128, required=False)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = serializers.CharField(validators=[phone_regex], max_length=17, required=False)
    location = serializers.CharField(required=False)
    profile_image_url = serializers.CharField(required=False)

    def create(self, validated_data):
        user_data = {}
        if 'first_name' in validated_data:
            user_data['first_name'] = validated_data['first_name']
        if 'last_name' in validated_data:
            user_data['last_name'] = validated_data['last_name']
        User.objects.filter(id=validated_data['user']).update(**user_data)

        user = User.objects.get(id=validated_data['user'])
        validated_data.pop('first_name', None)
        validated_data.pop('last_name', None)
        validated_data.pop('user', None)
        user_profile_data = validated_data.copy()
        user_profile = UserProfile.objects.filter(user=user).update(**user_profile_data)

        return user
