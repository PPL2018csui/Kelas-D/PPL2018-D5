import uuid

from rest_framework import serializers

from meetu_backend.apps.schedule.models import Schedule
from meetu_backend.apps.meetup.models import MeetUp

class ScheduleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Schedule
        fields = '__all__'
        read_only_fields = ['parent_id',]

    def validate(self, attrs):
        for schedule in Schedule.objects.filter(owner=attrs['owner']):
            if ((schedule.start_time < attrs['end_time'] < schedule.end_time) or (attrs['start_time'] < schedule.end_time < attrs['end_time']) \
                or (attrs['start_time'] == schedule.start_time and attrs['end_time'] == schedule.end_time)):
                raise serializers.ValidationError('Conflict exists')
        for meetup in MeetUp.objects.filter(group__members__in=[attrs['owner']]):
            if ((meetup.start_time < attrs['end_time'] < meetup.end_time) or (attrs['start_time'] < meetup.end_time < attrs['end_time']) \
                or (attrs['start_time'] == meetup.start_time and attrs['end_time'] == meetup.end_time)):
                raise serializers.ValidationError('Conflict exists')
        attrs['parent_id'] = uuid.uuid4()
        return attrs
