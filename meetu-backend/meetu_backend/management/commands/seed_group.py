from django.core.management.base import BaseCommand, CommandError
from meetu_backend.seeding.group import setup_seeding_data

class Command(BaseCommand):
    help = 'Seed Group Data'

    def handle(self, *args, **options):
        self.stdout.write('===> Start seeding process for group')

        try:
            setup_seeding_data()
            self.stdout.write(self.style.SUCCESS('Successfully seed group data'))
        except Exception as e:
            self.stdout.write(self.style.ERROR('Failed seed group data, error: "%s"' % e))