from django.core.management.base import BaseCommand, CommandError
from meetu_backend.seeding.meetup import setup_seeding_data

class Command(BaseCommand):
    help = 'Seed Schedule Data'

    def handle(self, *args, **options):
        self.stdout.write('===> Start seeding process for meetup')

        try:
            setup_seeding_data()
            self.stdout.write(self.style.SUCCESS('Successfully seed meetup data'))
        except Exception as e:
            self.stdout.write(self.style.ERROR('Failed seed meetup data, error: "%s"' % e))