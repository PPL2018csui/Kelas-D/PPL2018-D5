package id.meetu.meetu.module.meetup.view.activity;

import android.os.Bundle;
import android.view.View;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.module.meetup.view.fragment.LoadingScreenFragment;
import id.meetu.meetu.module.meetup.view.fragment.TimeNotFoundFragment;


public class LoadingScreenActivity extends BaseActivity {

  @OnClick(R.id.add_meetup_back_button)
  public void onClickBackButton(View v) {
    super.onBackPressed();
  }

  @Override
  public int findLayout() {
    return R.layout.activity_loading_screen;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(findLayout());
    ButterKnife.bind(this);

    showLoadingScreenError();
  }

  private void showLoadingScreen() {
    getFragmentManager().beginTransaction()
        .replace(R.id.fragment_loading_screen, new LoadingScreenFragment())
        .commit();
  }

  private void showLoadingScreenError() {
    getFragmentManager().beginTransaction()
        .replace(R.id.fragment_loading_screen, new TimeNotFoundFragment())
        .commit();
  }

}
