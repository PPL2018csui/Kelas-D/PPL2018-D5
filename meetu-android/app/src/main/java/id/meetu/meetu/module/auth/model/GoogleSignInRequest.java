package id.meetu.meetu.module.auth.model;

import com.google.gson.annotations.SerializedName;

public class GoogleSignInRequest extends RegisterRequest {

  @SerializedName("token")
  protected String idToken;

  public GoogleSignInRequest(String idToken, String username, String firstName, String lastName) {
    super(username, firstName, lastName, "");
    this.idToken = idToken;
  }

  public String getIdToken() {
    return idToken;
  }

  public void setIdToken(String idToken) {
    this.idToken = idToken;
  }
}
