package id.meetu.meetu.module.meetup.model;

import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

public class TimeRecommendationRequest {

  @SerializedName("group_id")
  private int groupId;

  @SerializedName("time_zone")
  private DateTime timeZone;

  @SerializedName("start_date")
  private DateTime startDate;

  @SerializedName("end_date")
  private DateTime endDate;

  @SerializedName("repeat_type")
  private String repeatType;

  @SerializedName("repeat_count")
  int repeatCount;

  public TimeRecommendationRequest(int groupId, DateTime timeZone, DateTime startDate,
      DateTime endDate, String repeatType, int repeatCount) {
    this.groupId = groupId;
    this.timeZone = timeZone;
    this.startDate = startDate;
    this.endDate = endDate;
    this.repeatType = repeatType;
    this.repeatCount = repeatCount;
  }

  public int getGroupId() {
    return groupId;
  }

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }

  public DateTime getTimeZone() {
    return this.timeZone;
  }

  public void setTimeZone(DateTime timeZone) {
    this.timeZone = timeZone;
  }

  public DateTime getStartDate() {
    return this.startDate;
  }

  public void setStartDate(DateTime startDate) {
    this.startDate = startDate;
  }

  public DateTime getEndDate() {
    return this.endDate;
  }

  public void setEndDate(DateTime endDate) {
    this.endDate = endDate;
  }

  public String getRepeatType() {
    return this.repeatType;
  }

  public void setRepeatType(String repeatType) {
    this.repeatType = repeatType;
  }

  public int getRepeatCount() {
    return this.repeatCount;
  }

  public void setRepeatCount(int repeatCount) {
    this.repeatCount = repeatCount;

  }
}
