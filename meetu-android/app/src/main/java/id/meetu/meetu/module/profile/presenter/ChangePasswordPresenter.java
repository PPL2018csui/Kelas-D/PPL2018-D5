package id.meetu.meetu.module.profile.presenter;

import id.meetu.meetu.module.profile.contract.ChangePasswordContract;
import id.meetu.meetu.module.profile.model.ChangePasswordRequest;
import id.meetu.meetu.module.profile.service.ProfileService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordPresenter implements ChangePasswordContract.Presenter {

  private ChangePasswordContract.View view;
  private ProfileService profileService;

  public ChangePasswordPresenter(ChangePasswordContract.View view) {
    this.view = view;
    this.profileService = new ProfileService();
  }

  public ChangePasswordPresenter(ChangePasswordContract.View view, ProfileService profileService) {
    this.view = view;
    this.profileService = profileService;
  }

  public boolean isValidNewPassword(String newPassword) {
    if ("".equals(newPassword)) {
      view.setNewPasswordError("This field can't be empty");
      return false;
    }
    return true;
  }

  public boolean isValidVerifyPassword(String newPassword, String verifyPassword) {
    if ("".equals(verifyPassword)) {
      view.setVerifyPasswordError("This field can't be empty");
      return false;
    }

    if (isValidNewPassword(newPassword) && !newPassword.equals(verifyPassword)) {
      view.setVerifyPasswordError("New password doesn't match");
      return false;
    }
    return true;
  }

  @Override
  public void submit(String password, String confirmPassword) {
    view.disableAllError();

    boolean isValid = true;
    isValid &= isValidNewPassword(password);
    isValid &= isValidVerifyPassword(password, confirmPassword);

    if (isValid) {
      view.onLoading();
      profileService.changePassword(new Callback<Void>() {
        @Override
        public void onResponse(Call<Void> call, Response<Void> response) {
          if (response.isSuccessful()) {
            view.onFinishLoading();
            view.onSubmitSuccess("Change password success!");
          } else {
            view.onFinishLoading();
            view.onSubmitFail("Failed to change password");
          }
        }

        @Override
        public void onFailure(Call<Void> call, Throwable t) {
          view.onFinishLoading();
          view.onSubmitFail("Something is wrong with your connection");
        }
      }, new ChangePasswordRequest(password, confirmPassword));
    }
  }
}
