package id.meetu.meetu.module.notification.contract;

import id.meetu.meetu.base.BaseContract;
import id.meetu.meetu.module.notification.model.NotificationDetail;
import java.util.List;

public interface NotificationListContract {

  interface View extends BaseContract.View {
    void onGetNotificationsSuccess(List<NotificationDetail> notifications);
    void onGetNotificationsError(String error);
  }

  interface Presenter {
    void getNotifications();
  }
}
