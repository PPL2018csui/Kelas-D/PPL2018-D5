package id.meetu.meetu.module.auth.presenter;

import id.meetu.meetu.module.auth.contract.AuthContract;
import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.GoogleSignInRequest;
import id.meetu.meetu.module.auth.service.AuthService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthPresenter implements AuthContract.Presenter {

  private AuthContract.View view;
  private AuthService service;

  public AuthPresenter(AuthContract.View view) {
    this.view = view;
    this.service = new AuthService();
  }

  public AuthPresenter(AuthContract.View view, AuthService service) {
    this.view = view;
    this.service = service;
  }

  @Override
  public void googleSignIn(String idToken, String userName, String firstName, String lastName) {
    if (firstName == null) {
      firstName = "";
    }
    if (lastName == null) {
      lastName = "";
    }
    GoogleSignInRequest request = new GoogleSignInRequest(idToken, userName, firstName, lastName);

    service.postGoogleSignIn(new Callback<AuthResponse>() {
      @Override
      public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
        if (response.isSuccessful()) {
          AuthResponse authResponse = response.body();

          view.onFinishGoogleSignIn(authResponse.getToken(), authResponse.getUser());
        } else {
          view.onErrorGoogleSignIn("Failed to sign in via Google");
        }
      }

      @Override
      public void onFailure(Call<AuthResponse> call, Throwable t) {
        view.onErrorGoogleSignIn("Something is wrong with your connection");
      }
    }, request);
  }
}
