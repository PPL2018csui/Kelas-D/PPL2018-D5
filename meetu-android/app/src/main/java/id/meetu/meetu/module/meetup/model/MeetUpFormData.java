package id.meetu.meetu.module.meetup.model;

import id.meetu.meetu.util.DateUtil.RepeatType;
import org.joda.time.DateTime;

/**
 * Created by ayaz97 on 17/04/18.
 */

public class MeetUpFormData {

  private String meetUpName;
  private int groupId;
  private String groupName;
  private int groupOwner;
  private DateTime startDate;
  private DateTime endDate;
  private RepeatType repeatType;
  private int repeatCount;
  private DateTime startTime;
  private DateTime endTime;

  // testing purpose
  public MeetUpFormData() {
  }

  private MeetUpFormData(String meetUpName, int groupId, String groupName, int groupOwner,
      DateTime startDate,
      DateTime endDate,
      RepeatType repeatType, int repeatCount, DateTime startTime, DateTime endTime) {
    this.meetUpName = meetUpName;
    this.groupId = groupId;
    this.groupName = groupName;
    this.groupOwner = groupOwner;
    this.startDate = startDate;
    this.endDate = endDate;
    this.repeatType = repeatType;
    this.repeatCount = repeatCount;
    this.startTime = startTime;
    this.endTime = endTime;
  }

  public String getMeetUpName() {
    return meetUpName;
  }

  public void setMeetUpName(String meetUpName) {
    this.meetUpName = meetUpName;
  }

  public int getGroupId() {
    return groupId;
  }

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }

  public String getGroupName() {
    return groupName;
  }

  public void setGroupName(String groupName) {
    this.groupName = groupName;
  }

  public int getGroupOwner() {
    return groupOwner;
  }

  public void setGroupOwner(int groupOwner) {
    this.groupOwner = groupOwner;
  }

  public DateTime getStartDate() {
    return startDate;
  }

  public void setStartDate(DateTime startDate) {
    this.startDate = startDate;
  }

  public DateTime getEndDate() {
    return endDate;
  }

  public void setEndDate(DateTime endDate) {
    this.endDate = endDate;
  }

  public RepeatType getRepeatType() {
    return repeatType;
  }

  public void setRepeatType(RepeatType repeatType) {
    this.repeatType = repeatType;
  }

  public int getRepeatCount() {
    return repeatCount;
  }

  public void setRepeatCount(int repeatCount) {
    this.repeatCount = repeatCount;
  }

  public DateTime getStartTime() {
    return startTime;
  }

  public void setStartTime(DateTime startTime) {
    this.startTime = startTime;
  }

  public DateTime getEndTime() {
    return endTime;
  }

  public void setEndTime(DateTime endTime) {
    this.endTime = endTime;
  }

  public static class Builder {

    private String meetUpName;
    private int groupId;
    private String groupName;
    private int groupOwner;
    private DateTime startDate;
    private DateTime endDate;
    private RepeatType repeatType;
    private int repeatCount;
    private DateTime startTime;
    private DateTime endTime;

    public Builder() {
    }

    public Builder setMeetUpName(String meetUpName) {
      this.meetUpName = meetUpName;
      return this;
    }

    public Builder setGroupId(int groupId) {
      this.groupId = groupId;
      return this;
    }

    public Builder setGroupName(String groupName) {
      this.groupName = groupName;
      return this;
    }

    public Builder setGroupOwner(int groupOwner) {
      this.groupOwner = groupOwner;
      return this;
    }

    public Builder setStartDate(DateTime startDate) {
      this.startDate = startDate;
      return this;
    }

    public Builder setEndDate(DateTime endDate) {
      this.endDate = endDate;
      return this;
    }

    public Builder setRepeatType(RepeatType repeatType) {
      this.repeatType = repeatType;
      return this;
    }

    public Builder setRepeatCount(int repeatCount) {
      this.repeatCount = repeatCount;
      return this;
    }

    public Builder setStartTime(DateTime startTime) {
      this.startTime = startTime;
      return this;
    }

    public Builder setEndTime(DateTime endTime) {
      this.endTime = endTime;
      return this;
    }

    public MeetUpFormData create() {
      return new MeetUpFormData(meetUpName, groupId, groupName, groupOwner, startDate, endDate,
          repeatType,
          repeatCount,
          startTime, endTime);
    }
  }
}
