package id.meetu.meetu.service;

import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.model.UserSearchRequest;
import id.meetu.meetu.module.group.model.AddMemberRequest;
import id.meetu.meetu.module.group.model.CreateGroupRequest;
import id.meetu.meetu.module.group.model.GroupMemberResponse;
import java.util.List;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.base.BaseApi;
import id.meetu.meetu.module.group.model.GroupResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Andre on 4/11/2018.
 */

public class GroupApi extends BaseApi<GroupApi.Service> {

  @Override
  public String findUrl() {
    return BuildConfig.BACKEND_URL;
  }

  @Override
  public Service create() {
    return build().create(Service.class);
  }

  public interface Service {

    @GET("api/groups/")
    Call<List<GroupResponse>> getGroups();

    @POST("api/groups/search/")
    Call<User> searchUser(@Body UserSearchRequest searchRequest);

    @GET("api/groups/{id}/")
    Call<GroupMemberResponse> getGroupMember(@Path("id") int groupId);

    @POST("api/groups/")
    Call<GroupMemberResponse> createGroup(@Body CreateGroupRequest createRequest);

    @POST("api/groups/add-member/")
    Call<GroupMemberResponse> addMember(@Body AddMemberRequest addRequest);
  }
}
