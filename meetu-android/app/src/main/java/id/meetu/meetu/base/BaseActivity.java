package id.meetu.meetu.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;
import butterknife.ButterKnife;
import id.meetu.meetu.firebase.FirebaseContract;
import id.meetu.meetu.firebase.FirebasePresenter;
import id.meetu.meetu.module.notification.view.fragment.GroupInvitationDialogFragment;
import id.meetu.meetu.module.notification.view.fragment.MeetUpInvitationDialogFragment;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.ProgressBarView;
import id.meetu.meetu.util.SharedPrefManager;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by stephen on 3/3/18.
 */


public abstract class BaseActivity extends AppCompatActivity implements BaseContract.View,
    FirebaseContract.View {

  protected ProgressBarView progressBarView;
  private FirebasePresenter firebasePresenter;
  private Lock lock;
  private int barCounter;

  private BroadcastReceiver firebaseReceiver = new BroadcastReceiver() {
    @Override
    public void onReceive(Context context, Intent intent) {
      String type = intent.getStringExtra("type");
      switch (type) {
        case Constant.FIREBASE_EVENT_TOKEN:
          updateFirebaseToken(intent.getStringExtra("token"));
          break;
        case Constant.FIREBASE_EVENT_MEETUP:
          showMeetUpConfirmationDialog(intent);
          break;
        case Constant.FIREBASE_EVENT_GROUP:
          showGroupDialog(intent);
          break;
      }
      ;
    }
  };

  @LayoutRes
  public abstract int findLayout();

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(findLayout());
    ButterKnife.bind(this);
    SharedPrefManager.getInstance(getApplicationContext());

    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    progressBarView = new ProgressBarView(this);
    firebasePresenter = new FirebasePresenter(this);
    lock = new ReentrantLock();
    barCounter = 0;
  }

  @Override
  public void onLoading() {
    try {
      lock.lock();
      barCounter++;

      if (barCounter == 1) {
        progressBarView.show();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
      }
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void onFinishLoading() {
    try {
      lock.lock();
      barCounter--;

      if (barCounter == 0) {
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
              @Override
              public void run() {
                progressBarView.hide();
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
              }
            }, 500);
          }
        });
      }
    } finally {
      lock.unlock();
    }
  }

  public void showMeetUpConfirmationDialog(Intent intent) {
    String title = intent.getStringExtra("meetup_title");
    String group = intent.getStringExtra("group");
    String startDate = intent.getStringExtra("start_time");
    String endDate = intent.getStringExtra("end_time");
    String repetition = intent.getStringExtra("repetition");

    MeetUpInvitationDialogFragment fragment = MeetUpInvitationDialogFragment
        .newInstance(title, group, startDate, endDate, repetition);
    fragment.show(getFragmentManager(), "meetup-confirmation");
  }

  public void showGroupDialog(Intent intent) {
    String title = intent.getStringExtra("group_title");
    String invitor = intent.getStringExtra("referrer_name");

    GroupInvitationDialogFragment fragment = GroupInvitationDialogFragment
        .newInstance(title, invitor);
    fragment.show(getFragmentManager(), "group-confirmation");
  }

  public void updateFirebaseToken(String token) {
    if (token == null) {
      return;
    }

    SharedPrefManager manager = SharedPrefManager.getInstance(null);

    String savedToken = manager.readString(SharedPrefManager.FIREBASE_TOKEN, null);

    firebasePresenter.updateFirebaseToken(token);
  }

  @Override
  public void onFinishUpdateFirebaseToken(String token) {
    SharedPrefManager manager = SharedPrefManager.getInstance(null);
    manager.writeString(SharedPrefManager.FIREBASE_TOKEN, token);
  }

  @Override
  public void onFailUpdateFirebaseToken(String error) {
    Toast.makeText(this, error, Toast.LENGTH_LONG).show();
  }

  @Override
  protected void onResume() {
    super.onResume();
    LocalBroadcastManager.getInstance(this)
        .registerReceiver(firebaseReceiver, new IntentFilter(Constant.FIREBASE_EVENT));
  }

  @Override
  protected void onPause() {
    LocalBroadcastManager.getInstance(this).unregisterReceiver(firebaseReceiver);
    super.onPause();
  }
}
