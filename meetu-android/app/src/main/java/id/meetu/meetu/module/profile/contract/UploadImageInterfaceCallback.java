package id.meetu.meetu.module.profile.contract;

public interface UploadImageInterfaceCallback {

  void onUploadCallback();

  void onFinishUploadCallback(String imageURL);

}
