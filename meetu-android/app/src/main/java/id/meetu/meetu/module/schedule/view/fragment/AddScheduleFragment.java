package id.meetu.meetu.module.schedule.view.fragment;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.module.schedule.model.ScheduleInfo;
import id.meetu.meetu.module.schedule.presenter.SchedulePresenter;
import id.meetu.meetu.module.schedule.presenter.SchedulePresenterListener;
import id.meetu.meetu.util.AutoCompleteDropDownView;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.DateUtil;
import id.meetu.meetu.util.DateUtil.RepeatType;
import id.meetu.meetu.util.SelectRepetitionFragment;
import java.util.List;
import org.joda.time.DateTime;

/**
 * Created by ayaz97 on 11/04/18.
 */

public class AddScheduleFragment extends DialogFragment implements
    SelectRepetitionFragment.Listener, SchedulePresenterListener {

  @BindView(R.id.schedule_name)
  EditText scheduleName;

  @BindView(R.id.schedule_date)
  EditText scheduleDate;

  @BindView(R.id.schedule_from_time)
  EditText scheduleFromTime;

  @BindView(R.id.schedule_to_time)
  EditText scheduleToTime;

  @BindView(R.id.schedule_type_dropdown)
  AutoCompleteDropDownView scheduleType;

  @BindView(R.id.button_cancel)
  Button cancelButton;

  @BindView(R.id.button_create_schedule)
  Button createButton;

  @BindView(R.id.schedule_name_text)
  TextInputLayout scheduleNameText;

  @BindView(R.id.schedule_date_text)
  TextInputLayout scheduleDateText;

  @BindView(R.id.schedule_from_text)
  TextInputLayout scheduleFromText;

  @BindView(R.id.schedule_to_text)
  TextInputLayout scheduleToText;

  @BindView(R.id.schedule_type_text)
  TextInputLayout scheduleTypeText;

  DatePickerDialog.OnDateSetListener dateListener;
  TimePickerDialog.OnTimeSetListener fromTimeListener;
  TimePickerDialog.OnTimeSetListener toTimeListener;

  ArrayAdapter<String> scheduleTypeAdapter;
  int selectedTypeIndex = 0;

  RepeatType repeatType = null;
  int repeatCount = -1;

  SchedulePresenter presenter = new SchedulePresenter(this);

  public static AddScheduleFragment newInstance() {
    AddScheduleFragment fragment = new AddScheduleFragment();

    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    scheduleTypeAdapter = new ArrayAdapter<>(getActivity(), R.layout.layout_group_spinner_item,
        Constant.EVENT_TYPE_FORM);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_add_schedule, container, false);
    ButterKnife.bind(this, v);

    scheduleType.setAdapter(scheduleTypeAdapter);
    scheduleType.setText(scheduleTypeAdapter.getItem(0).toString());

    scheduleType.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
        selectedTypeIndex = position;

        if (selectedTypeIndex == 1) {
          SelectRepetitionFragment fragment = SelectRepetitionFragment
              .newInstance(repeatType, repeatCount, AddScheduleFragment.this);
          fragment.show(getChildFragmentManager(), "repetition");
        }
      }
    });

    preparePickerListener();

    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    getDialog().setCanceledOnTouchOutside(false);

    return v;
  }

  void preparePickerListener() {
    dateListener = new OnDateSetListener() {
      @Override
      public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        updateScheduleDate(new DateTime(year, month + 1, dayOfMonth, 0, 0));
      }
    };

    fromTimeListener = new OnTimeSetListener() {
      @Override
      public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        updateScheduleFromTime(hourOfDay, minute);
      }
    };

    toTimeListener = new OnTimeSetListener() {
      @Override
      public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        updateScheduleToTime(hourOfDay, minute);
      }
    };
  }

  void resetAllError() {
    scheduleNameText.setErrorEnabled(false);
    scheduleDateText.setErrorEnabled(false);
    scheduleFromText.setErrorEnabled(false);
    scheduleToText.setErrorEnabled(false);
  }

  boolean isValidInput() {
    boolean isValid = true;

    if ("".equals(scheduleName.getText().toString().trim())) {
      isValid = false;
      scheduleNameText.setError(getText(R.string.error_form_empty));
    }

    if ("".equals(scheduleDate.getText().toString().trim())) {
      isValid = false;
      scheduleDateText.setError(getText(R.string.error_form_empty));
    }

    if ("".equals(scheduleFromTime.getText().toString().trim())) {
      isValid = false;
      scheduleFromText.setError(getText(R.string.error_form_empty));
    }

    if ("".equals(scheduleToTime.getText().toString().trim())) {
      isValid = false;
      scheduleToText.setError(getText(R.string.error_form_empty));
    }

    if (!("".equals(scheduleFromTime.getText().toString().trim())) && !(""
        .equals(scheduleToTime.getText().toString().trim()))) {

      DateTime startTime = DateUtil
          .parseHourMinuteString(scheduleFromTime.getText().toString().trim(), ":");
      DateTime endTime = DateUtil.parseHourMinuteString(scheduleToTime.getText().toString().trim(), ":");

      if (startTime.isAfter(endTime)) {
        isValid = false;
        scheduleToText.setError(getText(R.string.error_end_before_start));
      }
    }

    return isValid;
  }

  @OnClick(R.id.button_cancel)
  public void onClickCancel() {
    getDialog().dismiss();
  }

  @OnClick(R.id.button_create_schedule)
  public void onClickCreate() {
    resetAllError();
    if (!isValidInput()) {
      return;
    }

    String title = scheduleName.getText().toString().trim();

    DateTime date = DateUtil.parseDateString(scheduleDate.getText().toString().trim());
    int year = date.getYear();
    int month = date.getMonthOfYear();
    int day = date.getDayOfMonth();

    DateTime startTime = DateUtil.parseHourMinuteString(scheduleFromTime.getText().toString().trim(), ":");
    DateTime start = new DateTime(year, month, day, startTime.getHourOfDay(),
        startTime.getMinuteOfHour());

    DateTime endTime = DateUtil.parseHourMinuteString(scheduleToTime.getText().toString().trim(), ":");
    DateTime end = new DateTime(year, month, day, endTime.getHourOfDay(),
        endTime.getMinuteOfHour());

    if (selectedTypeIndex == 0) {
      repeatType = RepeatType.ONCE;
      repeatCount = 1;
    }

    presenter.postSchedules(title, start, end, repeatType, repeatCount);
  }

  @OnClick(R.id.schedule_date)
  public void onClickDate() {
    createScheduleDateDialog();
  }

  @OnClick(R.id.schedule_from_time)
  public void onClickFromTime() {
    createScheduleFromTimeDialog();
  }

  @OnClick(R.id.schedule_to_time)
  public void onClickToTime() {
    createScheduleToTimeDialog();
  }

  void createScheduleDateDialog() {
    String dateString = scheduleDate.getText().toString().trim();
    if ("".equals(dateString)) {
      dateString = DateUtil.getDateString(new DateTime());
    }
    DateTime date = DateUtil.parseDateString(dateString);

    int year = date.getYear();
    int month = date.getMonthOfYear();
    int day = date.getDayOfMonth();

    DatePickerDialog datePick = new DatePickerDialog(getActivity(), R.style.DialogTheme,
        dateListener,
        year, month - 1, day);
    datePick.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
    datePick.show();
  }

  void createScheduleFromTimeDialog() {
    String dateString = scheduleFromTime.getText().toString().trim();

    if ("".equals(dateString)) {
      new TimePickerDialog(getActivity(), R.style.DialogTheme, fromTimeListener, 0, 0, true).show();
    } else {
      DateTime date = DateUtil.parseHourMinuteString(dateString, ":");

      if (date == null) {
        new TimePickerDialog(getActivity(), R.style.DialogTheme, fromTimeListener, 0, 0, true)
            .show();
      } else {
        int hour = date.getHourOfDay();
        int minute = date.getMinuteOfHour();

        new TimePickerDialog(getActivity(), R.style.DialogTheme, fromTimeListener, hour, minute,
            true).show();
      }
    }
  }

  void createScheduleToTimeDialog() {
    String dateString = scheduleToTime.getText().toString().trim();
    DateTime dateFrom = DateUtil.parseHourMinuteString(scheduleFromTime.getText().toString().trim(), ":");

    if ("".equals(dateString)) {
      if (dateFrom != null) {
        new TimePickerDialog(getActivity(), R.style.DialogTheme, toTimeListener,
            dateFrom.getHourOfDay(), dateFrom.getMinuteOfHour(), true).show();
      } else {
        new TimePickerDialog(getActivity(), R.style.DialogTheme, toTimeListener, 0, 0, true).show();
      }
    } else {
      DateTime date = DateUtil.parseHourMinuteString(dateString, ":");

      if (date == null) {
        if (dateFrom != null) {
          new TimePickerDialog(getActivity(), R.style.DialogTheme, toTimeListener,
              dateFrom.getHourOfDay(), dateFrom.getMinuteOfHour(), true).show();
        } else {
          new TimePickerDialog(getActivity(), R.style.DialogTheme, toTimeListener, 0, 0, true)
              .show();
        }
      } else {
        int hour = date.getHourOfDay();
        int minute = date.getMinuteOfHour();

        if (dateFrom != null) {
          new TimePickerDialog(getActivity(), R.style.DialogTheme, toTimeListener,
              dateFrom.getHourOfDay(), dateFrom.getMinuteOfHour(), true).show();
        } else {
          new TimePickerDialog(getActivity(), R.style.DialogTheme, toTimeListener, hour, minute,
              true)
              .show();
        }
      }
    }
  }

  void updateScheduleFromTime(int hour, int minute) {
    String time = DateUtil.getHourMinuteString(hour, minute, ":");

    scheduleFromTime.setText(time);
  }

  void updateScheduleToTime(int hour, int minute) {
    String time = DateUtil.getHourMinuteString(hour, minute, ":");

    scheduleToTime.setText(time);
  }

  void updateScheduleDate(DateTime date) {
    String dateString = DateUtil.getDateString(date);

    scheduleDate.setText(dateString);
  }

  public void onCancelRepetition() {
    if (repeatCount == -1) {
      selectedTypeIndex = 0;
      scheduleType.setText(scheduleTypeAdapter.getItem(0).toString(), false);
    } else {
      scheduleType.setText(DateUtil.getRepetitionText(repeatType, repeatCount), false);
    }
  }

  public void onConfirmRepetition(RepeatType repeatType, int repeatCount) {
    this.repeatType = repeatType;
    this.repeatCount = repeatCount;

    scheduleType.setText(DateUtil.getRepetitionText(repeatType, repeatCount), false);
  }

  public void onGetSuccess(List<ScheduleInfo> schedules) {
  }

  public void onPostSuccess(String msg) {
    Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    getDialog().dismiss();
  }

  @Override
  public void onPostSuccess(List<ScheduleInfo> schedules) {
    ScheduleFragment schedFragment = (ScheduleFragment) getActivity().getFragmentManager()
        .findFragmentById(R.id.schedule);
    for (ScheduleInfo schedule : schedules) {
      schedule.setId(schedule.getId() + schedFragment.adapter.getScheduleList().size());
    }

    schedFragment.updateDateMap(schedules);
    schedFragment.setScheduleByDate(schedFragment.currentDate);
    for (ScheduleInfo schedule : schedules) {
      schedFragment.scheduleDays.add(schedule.getDate());
    }
    CalendarFragment calendarFrag = (CalendarFragment) getFragmentManager()
        .findFragmentById(R.id.calendar);
    calendarFrag.updateCalendar(schedFragment.scheduleDays);
  }

  public void onDeleteSuccess(String msg) {
  }

  public void onError(String msg) {
    Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
  }
}
