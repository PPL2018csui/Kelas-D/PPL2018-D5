package id.meetu.meetu.module.profile.service;

import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.profile.model.ChangePasswordRequest;
import id.meetu.meetu.module.profile.model.EditProfileRequest;
import id.meetu.meetu.service.ProfileApi;
import retrofit2.Call;
import retrofit2.Callback;

public class ProfileService {

  private ProfileApi.Service service;

  public ProfileService() {
    service = new ProfileApi().create();
  }

  public ProfileService(ProfileApi.Service service) {
    this.service = service;
  }

  public void changePassword(Callback<Void> callback, ChangePasswordRequest request) {
    Call<Void> call = service.changePassword(request);
    call.enqueue(callback);
  }

  public void editProfile(Callback<AuthResponse> callback, EditProfileRequest request) {
    Call<AuthResponse> call = service.editProfile(request);
    call.enqueue(callback);
  }

  public void getProfile(Callback<User> callback) {
    Call<User> call = service.getProfile();
    call.enqueue(callback);
  }
}
