package id.meetu.meetu.module.schedule.service;

import android.util.Log;
import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.schedule.model.ScheduleResponse;
import id.meetu.meetu.service.ScheduleApi;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by ayaz97 on 12/03/18.
 */

public class ScheduleService {

  private ScheduleApi.Service service;

  public ScheduleService() {
    this.service = new ScheduleApi().create();
  }

  public ScheduleService(ScheduleApi.Service service) {
    this.service = service;
  }

  public ScheduleApi.Service getService() {
    return service;
  }

  public void getSchedules(Callback<List<ScheduleResponse>> callback) {
    Call<List<ScheduleResponse>> call = service.getSchedules();
    call.enqueue(callback);
  }

  public void postSchedules(List<ScheduleResponse> scheduleResponse, Callback<BaseResponse> callback) {
    Call<BaseResponse> call = service.postSchedules(scheduleResponse);
    call.enqueue(callback);
  }

  public void deleteSchedules(String parentId, Callback<BaseResponse> callback) {
    Map<String, String> request = new HashMap<>();
    request.put("parent_id", parentId);

    Call<BaseResponse> call = service.deleteSchedules(request);
    call.enqueue(callback);
  }
}
