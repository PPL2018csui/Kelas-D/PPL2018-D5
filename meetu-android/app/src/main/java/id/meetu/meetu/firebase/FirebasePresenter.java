package id.meetu.meetu.firebase;

import android.util.Log;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.UpdateTokenRequest;
import id.meetu.meetu.module.auth.service.AuthService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FirebasePresenter implements FirebaseContract.Presenter {

  private static final String TAG = "FirebasePresenter";

  private FirebaseContract.View view;
  private AuthService authService;

  public FirebasePresenter(FirebaseContract.View view) {
    this.view = view;
    this.authService = new AuthService();
  }

  public FirebasePresenter(FirebaseContract.View view, AuthService authService) {
    this.view = view;
    this.authService = authService;
  }

  @Override
  public void updateFirebaseToken(final String token) {
    UpdateTokenRequest request = new UpdateTokenRequest(token);

    authService.updateFirebaseToken(new Callback<Void>() {
      @Override
      public void onResponse(Call<Void> call, Response<Void> response) {
        if(response.isSuccessful()) {
          Log.v(TAG, "Firebase token update successful");
          view.onFinishUpdateFirebaseToken(token);
        } else {
          Log.e(TAG, "Firebase token update unsuccessful");
          view.onFailUpdateFirebaseToken("Failed to update firebase token, please restart the app");
        }
      }

      @Override
      public void onFailure(Call<Void> call, Throwable t) {
        Log.e(TAG, t.getMessage());
        view.onFailUpdateFirebaseToken("Something is wrong with your connection");
      }
    }, request);
  }
}
