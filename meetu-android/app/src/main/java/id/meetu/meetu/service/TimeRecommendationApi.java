package id.meetu.meetu.service;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.base.BaseApi;
import id.meetu.meetu.module.meetup.model.TimeRecommendationRequest;
import id.meetu.meetu.module.meetup.model.TimeRecommendationResponse;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class TimeRecommendationApi extends BaseApi<TimeRecommendationApi.Service> {

  @Override
  public String findUrl() {
    return BuildConfig.BACKEND_URL;
  }

  @Override
  public Service create() {
    return build().create(Service.class);
  }

  public interface Service {

    @POST("api/recommendation/")
    Call<List<List<TimeRecommendationResponse>>> postTimeRecommendations(
        @Body TimeRecommendationRequest timeRecommendationRequest);
  }
}
