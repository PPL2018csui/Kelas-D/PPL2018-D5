package id.meetu.meetu.module.meetup.contract;

import id.meetu.meetu.module.group.model.GroupDetail;
import id.meetu.meetu.module.meetup.model.MeetUpFormData;
import java.util.List;

/**
 * Created by ayaz97 on 17/04/18.
 */

public interface AddMeetUpFormContract {

  interface View {

    void resetAllError();

    void setMeetupNameError(String error);

    void setGroupError(String error);

    void setStartDateError(String error);

    void setEndDateError(String error);

    void populateGroup(List<GroupDetail> groups);

    void onFetchGroupError(String error);

    void onFinishSubmit(MeetUpFormData data);
  }

  interface Presenter {

    void submit(MeetUpFormData data);

    void fetchGroups();
  }
}
