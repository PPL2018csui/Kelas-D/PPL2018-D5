package id.meetu.meetu.module.group.contract;

import id.meetu.meetu.base.BaseContract;
import id.meetu.meetu.module.group.model.GroupDetail;
import java.util.List;

public interface GroupListContract {

  interface View extends BaseContract.View {

    void onGetSuccess(List<GroupDetail> groups);

    void onError(String message);
  }

  interface Presenter {

    void getGroups();
  }
}
