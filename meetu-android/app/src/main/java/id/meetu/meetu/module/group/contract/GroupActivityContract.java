package id.meetu.meetu.module.group.contract;

import id.meetu.meetu.module.group.model.GroupMembers;

public interface GroupActivityContract {

  interface View {

    void onShowGroupMembers(int id);

    void onShowCreateGroup(GroupMembers members);

    void onAddPeopleCreateGroup(GroupMembers members);

    void onAddPeopleMemberDetail(int groupId);

    void onSaveGroup();

    void onShowAddMember(GroupMembers currentMembers, int mode);

    void changeActionText(String text);
  }
}
