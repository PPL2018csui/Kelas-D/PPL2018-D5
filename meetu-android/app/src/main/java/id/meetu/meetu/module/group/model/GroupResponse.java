package id.meetu.meetu.module.group.model;

import com.google.gson.annotations.SerializedName;

import id.meetu.meetu.base.BaseResponse;

/**
 * Created by ayaz97 on 27/03/18.
 */

public class GroupResponse extends BaseResponse {

  @SerializedName("id")
  private int id;

  @SerializedName("owner")
  private int owner;

  @SerializedName("title")
  private String title;

  public GroupResponse(int id, int owner, String title) {
    this.id = id;
    this.owner = owner;
    this.title = title;
  }
  public int getId() { return id; }

  public void setId(int id) { this.id = id; }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getOwner() {
    return owner;
  }

  public void setOwner(int owner) {
    this.owner = owner;
  }
}
