package id.meetu.meetu.module.schedule.presenter;

import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.meetup.model.MeetUpResponse;
import id.meetu.meetu.module.meetup.service.MeetUpService;
import id.meetu.meetu.module.schedule.model.ScheduleInfo;
import id.meetu.meetu.module.schedule.model.ScheduleResponse;
import id.meetu.meetu.module.schedule.service.ScheduleService;
import id.meetu.meetu.util.DateInterval;
import id.meetu.meetu.util.DateUtil;
import id.meetu.meetu.util.DateUtil.RepeatType;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 12/03/18.
 */

public class SchedulePresenter {

  private SchedulePresenterListener listener;
  private ScheduleService scheduleService;
  private MeetUpService meetUpService;

  public SchedulePresenter(SchedulePresenterListener listener) {
    this.listener = listener;
    this.scheduleService = new ScheduleService();
    this.meetUpService = new MeetUpService();
  }

  public SchedulePresenter(SchedulePresenterListener listener, ScheduleService scheduleService,
      MeetUpService meetUpService) {
    this.listener = listener;
    this.scheduleService = scheduleService;
    this.meetUpService = meetUpService;
  }

  public SchedulePresenterListener getListener() {
    return listener;
  }

  public List<ScheduleInfo> getSchedulesInfo(List<ScheduleResponse> schedules, boolean isMeetup) {
    int idx = 1;
    ArrayList<ScheduleInfo> listSchedule = new ArrayList<>();
    for (ScheduleResponse schedule : schedules) {
      String title = schedule.getTitle();
      DateTime startTime = schedule.getStartTime();
      DateTime endTime = schedule.getEndTime();

      String startDetail = DateUtil
          .getHourMinuteString(startTime.getHourOfDay(), startTime.getMinuteOfHour(), ".");
      String endDetail = DateUtil
          .getHourMinuteString(endTime.getHourOfDay(), endTime.getMinuteOfHour(), ".");
      String time = startDetail + " - " + endDetail;


      ScheduleInfo scheduleInfo = new ScheduleInfo(idx, title, startTime, time,isMeetup);
      idx += 1;
      listSchedule.add(scheduleInfo);
    }
    return listSchedule;
  }

  public void getSchedules() {
    scheduleService.getSchedules(new Callback<List<ScheduleResponse>>() {
      @Override
      public void onResponse(Call<List<ScheduleResponse>> call,
          Response<List<ScheduleResponse>> response) {
        if (response.isSuccessful()) {
          List<ScheduleInfo> listSchedule = getSchedulesInfo(response.body(), false);
          listener.onGetSuccess(listSchedule);
        } else {
          listener.onError("Failed to get Schedules");
        }
      }

      @Override
      public void onFailure(Call<List<ScheduleResponse>> call, Throwable t) {
        listener.onError(t.getMessage());
      }
    });
  }

  public void getMeetUps() {
    meetUpService.getMeetUps(new Callback<List<MeetUpResponse>>() {
      @Override
      public void onResponse(Call<List<MeetUpResponse>> call,
          Response<List<MeetUpResponse>> response) {
        if (response.isSuccessful()) {
          List<MeetUpResponse> meetUpResponses = response.body();
          List<ScheduleResponse> scheduleResponses = new ArrayList<>();
          for (MeetUpResponse resp : meetUpResponses) {
            scheduleResponses
                .add(new ScheduleResponse(resp.getTitle(), resp.getStartTime(), resp.getEndTime()));
          }

          List<ScheduleInfo> scheduleInfos = getSchedulesInfo(scheduleResponses, true);
          listener.onGetSuccess(scheduleInfos);
        } else {
          listener.onError("Failed to get MeetUps");
        }
      }

      @Override
      public void onFailure(Call<List<MeetUpResponse>> call, Throwable t) {
        listener.onError("Something is wrong with your connection");
      }
    });
  }

  public void postSchedules(String title, DateTime start, DateTime end, RepeatType repeatType,
      int repeatTimes) {
    List<DateInterval> intervals = DateUtil
        .getEventRepetition(start, end, repeatType, repeatTimes);
    final List<ScheduleResponse> listSchedule = new ArrayList<>();

    for (DateInterval interval : intervals) {
      listSchedule.add(
          new ScheduleResponse(title, interval.getStart(), interval.getEnd()));
    }

    final String scheduleTitle = title;

    scheduleService.postSchedules(listSchedule, new Callback<BaseResponse>() {
      @Override
      public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
        if (response.isSuccessful()) {
          List<ScheduleInfo> listScheduleInfo = getSchedulesInfo(listSchedule, false);

          listener.onPostSuccess("Successfully created " + scheduleTitle);
          listener.onPostSuccess(listScheduleInfo);
        } else {
          // TODO fetch error from backend
          listener.onError("Conflict exists with current schedules");
        }
      }

      @Override
      public void onFailure(Call<BaseResponse> call, Throwable t) {
        listener.onError("There is problem with your connection");
      }
    });
  }

  public void deleteSchedules(String parentId) {
    scheduleService.deleteSchedules(parentId, new Callback<BaseResponse>() {
      @Override
      public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
        if (response.isSuccessful()) {
          listener.onDeleteSuccess("Delete schedule successful");
        } else {
          listener.onError("Something is wrong");
        }
      }

      @Override
      public void onFailure(Call<BaseResponse> call, Throwable t) {
        listener.onError(t.getMessage());
      }
    });
  }
}
