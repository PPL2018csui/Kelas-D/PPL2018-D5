package id.meetu.meetu.module.group.model;

/**
 * Created by Andre on 4/11/2018.
 */

public class GroupDetail {

  private int id;
  private String title;
  private int owner;

  private final int MAX_ID = Integer.MAX_VALUE;

  public GroupDetail(int id, String title, int owner) {
    this.id = id;
    this.title = title;
    this.owner = owner;
  }

  public GroupDetail() {
    this.id = MAX_ID;
    this.title = "";
    this.owner = MAX_ID;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public int getOwner() {
    return owner;
  }

  public void setOwner(int owner) {
    this.owner = owner;
  }
}