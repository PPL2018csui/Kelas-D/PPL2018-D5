package id.meetu.meetu.util;

import android.animation.LayoutTransition;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

public class ProgressBarView {

  private FrameLayout progressBarLayout;
  private View progressBarBaseView;
  private View progressBarHideView;

  private Context mContext;
  private boolean withOverlay;

  public ProgressBarView(Context context) {
    mContext = context;
    progressBarBaseView = ((Activity) context).findViewById(android.R.id.content).getRootView();
    progressBarHideView = progressBarBaseView;
    this.withOverlay = false;
    initProgressBar(withOverlay);
  }

  public void initProgressBar(boolean withOverlay) {
    ViewGroup layout = (ViewGroup) progressBarBaseView;

    FrameLayout.LayoutParams paramsProgressBar = new FrameLayout.LayoutParams(
        FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
    paramsProgressBar.gravity = Gravity.CENTER;

    ProgressBar mProgressBar = new ProgressBar(mContext, null,
        android.R.attr.progressBarStyleLarge);
    mProgressBar.setLayoutParams(paramsProgressBar);
    mProgressBar.setIndeterminate(true);

    FrameLayout.LayoutParams params = new
        FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
        FrameLayout.LayoutParams.MATCH_PARENT);

    progressBarLayout = new FrameLayout(mContext);
    progressBarLayout.setAlpha((float) 0.4);
    if (withOverlay) {
      progressBarLayout.setBackgroundColor(Color.BLACK);
    }
    progressBarLayout.setLayoutTransition(new LayoutTransition());
    progressBarLayout.setClickable(true);
    progressBarLayout.setVisibility(View.GONE);

    progressBarLayout.addView(mProgressBar);

    layout.addView(progressBarLayout, params);

    hide();
  }

  public void setWithOverlay(boolean withOverlay) {
    this.withOverlay = withOverlay;
    initProgressBar(withOverlay);
  }

  public void setDefaultProgressBarBaseView() {
    this.progressBarBaseView = ((Activity) mContext).findViewById(android.R.id.content).getRootView();
    initProgressBar(withOverlay);
  }

  public void setProgressBarBaseView(View progressBarBaseView) {
    this.progressBarBaseView = progressBarBaseView;
    initProgressBar(withOverlay);
  }

  public void setProgressBarHideView(View progressBarHideView) {
    this.progressBarHideView = progressBarHideView;
  }

  public void show() {
    progressBarLayout.setVisibility(View.VISIBLE);
    if (!withOverlay) {
      progressBarHideView.setVisibility(View.GONE);
    }
  }

  public void hide() {
    progressBarLayout.setVisibility(View.GONE);
    if (!withOverlay) {
      progressBarHideView.setVisibility(View.VISIBLE);
    }
  }
}
