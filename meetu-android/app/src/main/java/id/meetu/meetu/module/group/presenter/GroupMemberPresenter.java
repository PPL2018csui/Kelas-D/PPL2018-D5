package id.meetu.meetu.module.group.presenter;

import id.meetu.meetu.module.group.contract.GroupMembersContract;
import id.meetu.meetu.module.group.model.GroupMemberDetail;
import id.meetu.meetu.module.group.model.GroupMemberResponse;
import id.meetu.meetu.module.group.model.GroupMembers;
import id.meetu.meetu.module.group.model.Member;
import id.meetu.meetu.module.group.service.GroupService;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GroupMemberPresenter implements GroupMembersContract.Presenter {

  private GroupMembersContract.View view;
  private GroupService groupService;

  public GroupMemberPresenter(GroupMembersContract.View view) {
    this.view = view;
    this.groupService = new GroupService();
  }

  public GroupMemberPresenter(GroupMembersContract.View view, GroupService groupService) {
    this.view = view;
    this.groupService = groupService;
  }

  @Override
  public void getMembersInGroup(final int groupId) {
    view.onLoading();
    groupService.getGroupMember(new Callback<GroupMemberResponse>() {
      @Override
      public void onResponse(Call<GroupMemberResponse> call,
          Response<GroupMemberResponse> response) {
        if (response.isSuccessful()) {
          List<Member> members = response.body().getMembers();
          List<GroupMemberDetail> groupMembers = new ArrayList<>();
          for (Member member : members) {
            String fullName =
                member.getMemberProfile().getFirstName() + " " + member.getMemberProfile()
                    .getLastName();
            String profileImageUrl = member.getProfileImageUrl();

            GroupMemberDetail groupMember = new GroupMemberDetail(member.getUserId(), fullName,
                profileImageUrl);
            groupMembers.add(groupMember);
          }
          GroupMembers groupWithMembers = new GroupMembers(response.body().getId(),
              response.body().getTitle(),
              response.body().getCreatedDate(), groupMembers);

          view.onFinishLoading();
          view.onGetMembersSuccess(groupWithMembers);
        } else {
          view.onFinishLoading();
          view.onGetMembersError("There is problem with your connection");
        }
      }

      @Override
      public void onFailure(Call<GroupMemberResponse> call, Throwable t) {
        view.onFinishLoading();
        view.onGetMembersError("There is problem with your connection");
      }
    }, groupId);
  }
}
