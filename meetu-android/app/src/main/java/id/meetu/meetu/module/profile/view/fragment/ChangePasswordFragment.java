package id.meetu.meetu.module.profile.view.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.profile.contract.ChangePasswordContract;
import id.meetu.meetu.module.profile.contract.ProfileActivityContract;
import id.meetu.meetu.module.profile.presenter.ChangePasswordPresenter;

public class ChangePasswordFragment extends BaseFragment implements ChangePasswordContract.View {

  @BindView(R.id.new_password_text)
  TextInputLayout newPasswordText;

  @BindView(R.id.new_password)
  EditText newPassword;

  @BindView(R.id.verify_new_password_text)
  TextInputLayout verifyPasswordText;

  @BindView(R.id.verify_new_password)
  EditText verifyPassword;

  ChangePasswordContract.Presenter presenter;
  ProfileActivityContract.View activity;

  public static ChangePasswordFragment newInstance(ProfileActivityContract.View profileActivity) {
    ChangePasswordFragment fragment = new ChangePasswordFragment();

    Bundle args = new Bundle();

    fragment.setArguments(args);
    fragment.activity = profileActivity;
    fragment.presenter = new ChangePasswordPresenter(fragment);

    return fragment;
  }

  @OnClick(R.id.change_password_back_button)
  public void onClickBackButton(View v) {
    activity.onBackPressed();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_change_password, container, false);
    ButterKnife.bind(this, v);
    progressBarView.setWithOverlay(true);

    return v;
  }

  @OnClick(R.id.change_password_button)
  public void onChangePasswordButtonClicked(View v) {
    presenter.submit(newPassword.getText().toString(), verifyPassword.getText().toString());
  }

  @Override
  public void disableAllError() {
    newPasswordText.setErrorEnabled(false);
    verifyPasswordText.setErrorEnabled(false);
  }

  @Override
  public void setNewPasswordError(String error) {
    newPasswordText.setError(error);
  }

  @Override
  public void setVerifyPasswordError(String error) {
    verifyPasswordText.setError(error);
  }

  @Override
  public void onSubmitSuccess(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    activity.showProfile();
  }

  @Override
  public void onSubmitFail(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
  }
}
