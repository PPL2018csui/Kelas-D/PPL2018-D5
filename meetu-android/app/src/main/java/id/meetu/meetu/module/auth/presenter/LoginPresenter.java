package id.meetu.meetu.module.auth.presenter;

import id.meetu.meetu.module.auth.contract.LoginContract;
import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.LoginRequest;
import id.meetu.meetu.module.auth.service.AuthService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 17/04/18.
 */

public class LoginPresenter implements LoginContract.Presenter {

  private LoginContract.View view;
  private AuthService authService;

  public LoginPresenter(LoginContract.View view) {
    this.view = view;
    authService = new AuthService();
  }

  public LoginPresenter(LoginContract.View view, AuthService authService) {
    this.view = view;
    this.authService = authService;
  }

  @Override
  public void cancel() {
    view.onCancel();
  }

  @Override
  public void submit(String username, String password) {
    view.disableAllError();

    boolean isValid = true;
    isValid &= isValidUsername(username);
    isValid &= isValidPassword(password);

    if (isValid) {
      view.onLoading();
      authService.postLogin(new Callback<AuthResponse>() {
        @Override
        public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
          if (response.isSuccessful()) {
            AuthResponse auth = response.body();
            view.onFinishLoading();
            view.onLoginSuccess(auth.getToken(), auth.getUser());
          } else {
            view.onFinishLoading();
            view.onLoginFail();
          }
        }


        @Override
        public void onFailure(Call<AuthResponse> call, Throwable t) {
          view.onFinishLoading();
          view.onLoginError();
        }
      }, new LoginRequest(username, password));
    }
  }

  private boolean isValidUsername(String username) {
    if ("".equals(username)) {
      view.setUsernameError("This field can't be empty");
      return false;
    }

    return true;
  }

  private boolean isValidPassword(String password) {
    if ("".equals(password)) {
      view.setPasswordError("This field can't be empty");
      return false;
    }

    return true;
  }
}
