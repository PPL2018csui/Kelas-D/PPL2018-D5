package id.meetu.meetu.module.auth.view;

import static id.meetu.meetu.util.Constant.LOGIN_REGISTER_SUCCESS_CODE;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.module.auth.contract.LoginContract;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.presenter.LoginPresenter;
import id.meetu.meetu.module.home.view.activity.HomeActivity;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.SharedPrefManager;

/**
 * Created by luthfi on 15/04/18.
 */

public class LoginActivity extends AuthActivity implements LoginContract.View {

  @BindView(R.id.form_login_username_text)
  TextInputLayout usernameText;
  @BindView(R.id.form_login_username)
  EditText username;
  @BindView(R.id.form_login_password_text)
  TextInputLayout passwordText;
  @BindView(R.id.form_login_password)
  EditText password;
  @BindView(R.id.confirm_login_button)
  Button confirmButton;
  @BindView(R.id.cancel_login)
  ImageButton cancelButton;
  private LoginContract.Presenter presenter;

  @Override
  public int findLayout() {
    return R.layout.activity_login;
  }

  @Override
  public void updateFirebaseToken(String token) {
    // emptied as before login, no account is associated in backend
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ButterKnife.bind(this);
    presenter = new LoginPresenter(this);
    progressBarView.setWithOverlay(true);
  }

  @Override
  public void disableAllError() {
    usernameText.setErrorEnabled(false);
    passwordText.setErrorEnabled(false);
  }

  @Override
  public void setUsernameError(String error) {
    usernameText.setError(error);
  }

  @Override
  public void setPasswordError(String error) {
    passwordText.setError(error);
  }

  @Override
  public void onCancel() {
    onBackPressed();
  }

  @Override
  public void onLoginSuccess(String token, User user) {
    Toast.makeText(this, "Login success!", Toast.LENGTH_SHORT).show();
    SharedPrefManager manager = SharedPrefManager.getInstance(this);

    manager.writeString("token", token);
    manager.writeInt(SharedPrefManager.USER_ID_KEY, user.getId());
    manager.writeInt(SharedPrefManager.USER_PROFILE_ID_KEY, user.getUserProfile().getId());
    manager.writeString(SharedPrefManager.USER_FIRST_NAME, user.getFirstName());
    manager.writeString(SharedPrefManager.USER_LAST_NAME, user.getLastName());
    manager
        .writeString(SharedPrefManager.USER_PHONE_NUMBER, user.getUserProfile().getPhoneNumber());
    manager.writeString(SharedPrefManager.USER_LOCATION, user.getUserProfile().getLocation());
    manager.writeString(SharedPrefManager.USER_EMAIL, user.getUsername());
    manager.writeString(SharedPrefManager.USER_PROFILE_IMAGE_URL,
        user.getUserProfile().getProfileImageUrl());

    Intent intent = new Intent(this, HomeActivity.class);
    intent.putExtra(Constant.FIRST_TIME_HOME, true);

    startActivity(intent);
    setResult(LOGIN_REGISTER_SUCCESS_CODE);
    finish();
  }

  @Override
  public void onLoginFail() {
    Toast.makeText(this, "Wrong username or password", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onLoginError() {
    Toast.makeText(this, "There is problem with your connection", Toast.LENGTH_SHORT).show();
  }

  @OnClick(R.id.confirm_login_button)
  public void onClickLogin() {
    String username = this.username.getText().toString().trim();
    String password = this.password.getText().toString();

    presenter.submit(username, password);
  }

  @OnClick(R.id.cancel_login)
  public void onClickCancel() {
    presenter.cancel();
  }
}