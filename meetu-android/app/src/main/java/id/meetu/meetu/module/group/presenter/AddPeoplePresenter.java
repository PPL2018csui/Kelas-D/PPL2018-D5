package id.meetu.meetu.module.group.presenter;

import android.util.Log;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.model.UserProfile;
import id.meetu.meetu.module.auth.model.UserSearchRequest;
import id.meetu.meetu.module.group.contract.AddPeopleContract;
import id.meetu.meetu.module.group.model.AddMemberRequest;
import id.meetu.meetu.module.group.model.GroupMemberDetail;
import id.meetu.meetu.module.group.model.GroupMemberResponse;
import id.meetu.meetu.module.group.model.GroupMembers;
import id.meetu.meetu.module.group.model.Member;
import id.meetu.meetu.module.group.service.GroupService;
import id.meetu.meetu.util.Constant;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPeoplePresenter implements AddPeopleContract.Presenter {

  private AddPeopleContract.View view;
  private GroupService groupService;

  public AddPeoplePresenter(AddPeopleContract.View view) {
    this.view = view;
    this.groupService = new GroupService();
  }

  public AddPeoplePresenter(AddPeopleContract.View view, GroupService groupService) {
    this.view = view;
    this.groupService = groupService;
  }

  @Override
  public void searchPeople(String username) {
    view.hideLayoutsAndErrors();

    if (!isValidUsername(username)) {
      view.setUsernameError("This field can't be empty");
      return;
    }

    UserSearchRequest request = new UserSearchRequest(username);

    view.onLoading();
    groupService.searchUser(new Callback<User>() {
      @Override
      public void onResponse(Call<User> call, Response<User> response) {
        if (response.isSuccessful()) {
          User user = response.body();
          UserProfile profile = user.getUserProfile();

          int userId = profile.getId();
          String name = user.getFirstName() + " " + user.getLastName();
          String profileImageUrl = profile.getProfileImageUrl();

          view.onGetPeopleSuccess(new GroupMemberDetail(userId, name, profileImageUrl));
        } else {
          view.onGetPeopleNotExist();
        }
      }

      @Override
      public void onFailure(Call<User> call, Throwable t) {
        view.onGetPeopleError("Something is wrong with your connection");
      }
    }, request);
    view.onFinishLoading();
  }

  @Override
  public void onAdd(GroupMembers members, GroupMemberDetail newMember, int mode) {
    if (newMember == null) {
      view.onSaveError("No user selected");
      return;
    }

    if (isMember(members, newMember)) {
      view.onSaveError("This user is already in the group");
      return;
    }

    if (mode == Constant.GROUP_CREATE_MODE) {
      members.getGroupMember().add(newMember);
      view.onSaveSuccess("Add success!", members);
    } else {
      AddMemberRequest request = new AddMemberRequest(newMember.getId(), members.getGroupId());
      view.onLoading();
      groupService.addMember(new Callback<GroupMemberResponse>() {
        @Override
        public void onResponse(Call<GroupMemberResponse> call,
            Response<GroupMemberResponse> response) {
          if (response.isSuccessful()) {
            GroupMemberResponse body = response.body();

            int id = body.getId();
            String groupName = body.getTitle();
            DateTime created = body.getCreatedDate();
            List<GroupMemberDetail> members = new ArrayList<>();

            for (Member member : body.getMembers()) {
              User user = member.getMemberProfile();

              int userId = member.getUserId();
              String name = user.getFirstName() + " " + user.getLastName();
              String profileImageUrl = member.getProfileImageUrl();

              members.add(new GroupMemberDetail(userId, name, profileImageUrl));
            }

            GroupMembers group = new GroupMembers(id, groupName, created, members);

            view.onFinishLoading();
            view.onSaveSuccess("Add success!", group);
          } else {
            view.onFinishLoading();
            view.onSaveError("Failed to add this user");
          }
        }

        @Override
        public void onFailure(Call<GroupMemberResponse> call, Throwable t) {
          view.onFinishLoading();
          view.onSaveError("Something is wrong with your connection");
        }
      }, request);
    }
  }

  private boolean isValidUsername(String username) {
    if ("".equals(username)) {
      return false;
    }
    return true;
  }

  private boolean isMember(GroupMembers members, GroupMemberDetail newMember) {
    for (GroupMemberDetail member : members.getGroupMember()) {
      if (member.getId() == newMember.getId()) {
        return true;
      }
    }

    return false;
  }
}
