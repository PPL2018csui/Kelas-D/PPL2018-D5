package id.meetu.meetu.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.joda.time.DateTime;

/**
 * Created by stephen on 3/3/18.
 */

public class Constant {

  public static final DateTime UTC_ZERO = new DateTime(2000, 1, 1, 0, 0);

  public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
  public static final String DATE_FORMAT2 = "yyyy-MM-dd'T'HH:mm:ss'.'SSSSSS'Z'";
  public static final String TEXT_DATE_FORMAT = "dd/MM/yyyy";

  public static final List<String> EVENT_TYPE_FORM =
      Collections.unmodifiableList(Arrays.asList("One-time Event", "With Repetition"));

  public static final List<String> REPETITION_TYPE_FORM = Collections
      .unmodifiableList(Arrays.asList("Day", "Week", "Month"));

  public static final int MAX_DAY_REPETITION = 31;
  public static final int MAX_WEEK_REPETITION = 52;
  public static final int MAX_MONTH_REPETITION = 12;

  public static final String PRIMARY_FONT = "fonts/lato.ttf";
  public static final String PRIMARY_FONT_LIGHT = "fonts/lato_light.ttf";

  public static final int LOGIN_REGISTER_SUCCESS_CODE = 100;
  public static final int LOGIN_REQUEST_CODE = 2;
  public static final int REGISTER_REQUEST_CODE = 3;
  public static final int GOOGLE_LOGIN_CODE = 101;

  public static final int PICK_PHOTO_FOR_AVATAR = 4;

  public static final int GROUP_CREATE_MODE = 0;
  public static final int GROUP_ADD_MEMBER_MODE = 1;

  public static final String FIREBASE_EVENT = "FirebaseEvent";
  public static final String FIREBASE_EVENT_TOKEN = "FirebaseToken";
  public static final String FIREBASE_EVENT_MEETUP = "MeetUp";
  public static final String FIREBASE_EVENT_GROUP = "Group";

  public static final String FIRST_TIME_HOME = "FirstTimeHome";

  public static final int ITEM_TYPE_EVENT = 0;
  public static final int ITEM_TYPE_INVITATION = 1;

  public static final String EMPTY_TOKEN = "-";

  public static final int PERMISSION_READ_EXTERNAL = 11;
}
