package id.meetu.meetu.module.auth.view;

import static id.meetu.meetu.util.Constant.LOGIN_REGISTER_SUCCESS_CODE;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.LinearLayout;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;
import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.module.auth.contract.AuthContract;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.presenter.AuthPresenter;
import id.meetu.meetu.module.home.view.activity.HomeActivity;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.SharedPrefManager;

public abstract class AuthActivity extends BaseActivity implements AuthContract.View {

  @BindView(R.id.login_with_google)
  LinearLayout loginButton;

  private FirebaseAuth auth;
  private GoogleSignInClient googleSignInClient;
  private AuthPresenter presenter;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ButterKnife.bind(this);

    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestIdToken(BuildConfig.FIREBASE_WEB)
        .requestEmail()
        .build();

    googleSignInClient = GoogleSignIn.getClient(this, gso);
    auth = FirebaseAuth.getInstance();

    googleSignInClient.signOut();
    auth.signOut();

    presenter = new AuthPresenter(this);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (requestCode == Constant.GOOGLE_LOGIN_CODE) {
      onLoading();
      Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
      try {
        GoogleSignInAccount account = task.getResult(ApiException.class);
        firebaseAuth(account);
      } catch (ApiException e) {
        onFinishLoading();
        Toast.makeText(this, "Failed to get account", Toast.LENGTH_LONG).show();
      }
    }
  }


  @Override
  public void onFinishGoogleSignIn(String token, User user) {
    onFinishLoading();
    finishAuth(token, user);
  }

  public void finishAuth(String token, User user) {
    SharedPrefManager manager = SharedPrefManager.getInstance(this);

    manager.writeString("token", token);

    manager.writeInt(SharedPrefManager.USER_ID_KEY, user.getId());
    manager.writeInt(SharedPrefManager.USER_PROFILE_ID_KEY, user.getUserProfile().getId());
    manager.writeString(SharedPrefManager.USER_FIRST_NAME, user.getFirstName());
    manager.writeString(SharedPrefManager.USER_LAST_NAME, user.getLastName());
    manager
        .writeString(SharedPrefManager.USER_PHONE_NUMBER, user.getUserProfile().getPhoneNumber());
    manager.writeString(SharedPrefManager.USER_PROFILE_IMAGE_URL,
        user.getUserProfile().getProfileImageUrl());
    manager.writeString(SharedPrefManager.USER_LOCATION, user.getUserProfile().getLocation());
    manager.writeString(SharedPrefManager.USER_EMAIL, user.getUsername());

    Intent intent = new Intent(this, HomeActivity.class);
    intent.putExtra(Constant.FIRST_TIME_HOME, true);
    startActivity(intent);
    setResult(LOGIN_REGISTER_SUCCESS_CODE);
    finish();
  }

  @Override
  public void onErrorGoogleSignIn(String error) {
    onFinishLoading();
    Toast.makeText(this, error, Toast.LENGTH_LONG).show();
  }

  public void loginWithBackend(GoogleSignInAccount account, FirebaseUser user) {
    final String email = user.getEmail();
    final String firstName = account.getGivenName();
    final String lastName = account.getFamilyName();

    user.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
      @Override
      public void onComplete(@NonNull Task<GetTokenResult> task) {
        if (task.isSuccessful()) {
          String idToken = task.getResult().getToken();
          presenter.googleSignIn(idToken, email, firstName, lastName);
        } else {
          onFinishLoading();
          Toast.makeText(AuthActivity.this, "Failed to retrieve Firebase ID", Toast.LENGTH_SHORT)
              .show();
        }
      }
    });
  }

  public void firebaseAuth(final GoogleSignInAccount account) {
    String idToken = account.getIdToken();

    AuthCredential credential = GoogleAuthProvider.getCredential(idToken, null);
    auth.signInWithCredential(credential)
        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
          @Override
          public void onComplete(@NonNull Task<AuthResult> task) {
            if (task.isSuccessful()) {
              FirebaseUser user = auth.getCurrentUser();
              loginWithBackend(account, user);
            } else {
              onFinishLoading();
              Toast.makeText(AuthActivity.this, "Failed to connect with Firebase",
                  Toast.LENGTH_SHORT).show();
            }
          }
        });
  }


  public void googleSignIn() {
    Intent signInIntent = googleSignInClient.getSignInIntent();
    startActivityForResult(signInIntent, Constant.GOOGLE_LOGIN_CODE);
  }

  @OnClick(R.id.login_with_google)
  public void onClickGoogleLogin() {
    googleSignIn();
  }
}
