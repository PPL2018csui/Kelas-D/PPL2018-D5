package id.meetu.meetu.module.schedule.view.fragment;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import id.meetu.meetu.R;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import org.joda.time.DateTime;

public class CalendarFragment extends Fragment {

  // how many days to show, defaults to six weeks, 42 days
  private static final int DAYS_COUNT = 42;
  private static final int DAY_OF_THE_WEEK = Calendar.MONDAY;
  @BindView(R.id.calendar_header)
  LinearLayout header;
  @BindView(R.id.date_display_today)
  Button btnToday;
  @BindView(R.id.calendar_prev_button)
  ImageButton btnPrev;
  @BindView(R.id.calendar_next_button)
  ImageButton btnNext;
  @BindView(R.id.date_display_day)
  TextView txtDateDay;
  @BindView(R.id.date_display_date)
  TextView txtDisplayDate;
  @BindView(R.id.date_display_year)
  TextView txtDateYear;
  @BindView(R.id.calendar_grid)
  GridView gridView;
  private Calendar currentDate = Calendar.getInstance(Locale.US);

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_calendar, container, false);
    ButterKnife.bind(this, v);

    gridView.setVerticalScrollBarEnabled(false);
    updateCalendar();
    updateHeaderText();

    return v;
  }

  public void updateCalendar() {
    updateCalendar(null);
  }

  private int getWeekIndex(int weekIndex, Calendar currentCalendar) {
    int firstDayWeekPosition = currentCalendar.getFirstDayOfWeek();
    if (firstDayWeekPosition == 1) {
      return weekIndex;
    } else {

      if (weekIndex == 1) {
        return 7;
      } else {
        return weekIndex - 1;
      }
    }
  }

  public void updateCalendar(Set<DateTime> schedules) {
    ArrayList<Date> cells = new ArrayList<>();
    Calendar calendar = (Calendar) currentDate.clone();

    calendar.set(Calendar.DAY_OF_MONTH, 1);
    calendar.setFirstDayOfWeek(DAY_OF_THE_WEEK);

    int firstDayOfMonth = calendar.get(Calendar.DAY_OF_WEEK);
    int dayOfMonthIndex = getWeekIndex(firstDayOfMonth, calendar);

    final Calendar startCalendar = (Calendar) calendar.clone();
    //Add required number of days
    startCalendar.add(Calendar.DATE, -(dayOfMonthIndex - 1));

    while (cells.size() < DAYS_COUNT) {
      cells.add(startCalendar.getTime());
      startCalendar.add(Calendar.DAY_OF_MONTH, 1);
    }
    gridView.setAdapter(new CalendarAdapter(getActivity(), cells, schedules));
  }

  public void updateHeaderText() {
    SimpleDateFormat sdf = new SimpleDateFormat("EEEE,dd MMM,yyyy");
    String[] dateToday = sdf.format(currentDate.getTime()).split(",");
    txtDateDay.setText(dateToday[0]);
    txtDisplayDate.setText(dateToday[1]);
    txtDateYear.setText(dateToday[2]);
  }

  public void onDateChange(DateTime date) {
    ScheduleFragment scheduleFrag = (ScheduleFragment) getFragmentManager().findFragmentById(R.id.schedule);
    scheduleFrag.setScheduleByDate(date);
  }

  @OnClick(R.id.calendar_prev_button)
  public void prevMonth() {
    currentDate.add(Calendar.MONTH, -1);
    onDateChange(new DateTime(currentDate.getTime()));
    updateCalendar();
    updateHeaderText();
  }

  @OnClick(R.id.calendar_next_button)
  public void nextMonth() {
    currentDate.add(Calendar.MONTH, 1);
    onDateChange(new DateTime(currentDate.getTime()));
    updateCalendar();
    updateHeaderText();
  }

  @OnClick(R.id.date_display_today)
  public void goToday() {
    currentDate.setTime(new Date());
    onDateChange(new DateTime(currentDate.getTime()));
    updateCalendar();
    updateHeaderText();
  }

  @OnTouch(R.id.calendar_grid)
  public boolean preventGridViewScrolling(View v, MotionEvent event) {
    return event.getAction() == MotionEvent.ACTION_MOVE;
  }

  private class CalendarAdapter extends ArrayAdapter<Date> {

    private Set<DateTime> scheduleDays;
    private LayoutInflater inflater;

    public CalendarAdapter(Context context, ArrayList<Date> days, Set<DateTime> scheduleDays) {
      super(context, R.layout.layout_calendar_date, days);
      this.scheduleDays = scheduleDays;
      inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
      Calendar calendar = Calendar.getInstance(Locale.US);
      final Date date = getItem(position);
      calendar.setTime(date);
      int day = calendar.get(Calendar.DATE);
      int month = calendar.get(Calendar.MONTH);
      int year = calendar.get(Calendar.YEAR);

      Date today = new Date();
      Calendar calendarToday = Calendar.getInstance(Locale.US);
      calendarToday.setTime(today);

      // show date
      if (view == null) {
        view = inflater.inflate(R.layout.layout_calendar_date, parent, false);
      }

      view.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View view) {
          currentDate.setTime(date);
          onDateChange(new DateTime(date));
          updateCalendar();
          updateHeaderText();
        }
      });

      TextView txtView = view.findViewById(R.id.date_text);
      txtView.setTextColor(getResources().getColor(R.color.darkBlue65Opacity));
      int backgroundId = R.drawable.transparent_circle;


      if (day == currentDate.get(Calendar.DATE) &&
          month == currentDate.get(Calendar.MONTH) &&
          year == currentDate.get(Calendar.YEAR)) {
        // if a date is clicked, show a ring around the date
        backgroundId = R.drawable.ring_circle;
      }

      if (day == calendarToday.get(Calendar.DATE) &&
          month == calendarToday.get(Calendar.MONTH) &&
          month == currentDate.get(Calendar.MONTH) &&
          year == calendarToday.get(Calendar.YEAR)) {
        // if it is today, set it to circular blue with white text

        txtView.setTextColor(Color.WHITE);
        backgroundId = R.drawable.blue_circle;
      }

      if (month == currentDate.get(Calendar.MONTH)) {
        txtView.setText(String.format("%02d", calendar.get(Calendar.DATE)));
      } else {
        txtView.setText("");
        view.setEnabled(false);
      }
      txtView.setBackgroundResource(backgroundId);

      return view;
    }
  }
}
