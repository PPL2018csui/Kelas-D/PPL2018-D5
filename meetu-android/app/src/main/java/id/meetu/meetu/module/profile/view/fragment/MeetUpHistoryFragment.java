package id.meetu.meetu.module.profile.view.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.home.presenter.HomePresenter;
import id.meetu.meetu.module.home.presenter.HomePresenterListener;
import id.meetu.meetu.module.meetup.model.MeetUpDetail;
import id.meetu.meetu.module.meetup.view.adapter.MeetUpDetailAdapter;
import id.meetu.meetu.module.profile.contract.ProfileActivityContract;
import java.util.ArrayList;
import java.util.List;

public class MeetUpHistoryFragment extends BaseFragment implements HomePresenterListener {

  @BindView(R.id.content)
  LinearLayout hideView;

  @BindView(R.id.card_list)
  RecyclerView recyclerView;

  @BindView(R.id.no_history)
  TextView noHistoryText;

  ProfileActivityContract.View activity;
  HomePresenter presenter = new HomePresenter(this);
  MeetUpDetailAdapter adapter;

  public static MeetUpHistoryFragment newInstance(ProfileActivityContract.View profileActivity) {
    MeetUpHistoryFragment fragment = new MeetUpHistoryFragment();

    Bundle args = new Bundle();

    fragment.setArguments(args);
    fragment.activity = profileActivity;

    return fragment;
  }

  @OnClick(R.id.meetup_history_back_button)
  public void onBackButtonClicked(View v) {
    activity.onBackPressed();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_meetup_history, container, false);
    ButterKnife.bind(this, v);
    progressBarView.setProgressBarHideView(hideView);

    List<MeetUpDetail> meetUpDetails = new ArrayList<>();
    adapter = new MeetUpDetailAdapter(v.getContext(), meetUpDetails);
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerView.setAdapter(adapter);

    presenter.getMeetUps();

    return v;
  }

  public void checkEmptyList() {
    if (((MeetUpDetailAdapter) recyclerView.getAdapter()).getMeetUpLists().isEmpty()) {
      recyclerView.setVisibility(View.GONE);
      noHistoryText.setVisibility(View.VISIBLE);
    } else {
      recyclerView.setVisibility(View.VISIBLE);
      noHistoryText.setVisibility(View.GONE);
    }
  }

  @Override
  public void onGetMeetUpSuccess(List<MeetUpDetail> meetUpList) {
    List<MeetUpDetail> meetUpHistory = new ArrayList<>();
    for (MeetUpDetail meetup : meetUpList) {
      if (meetup.getDateTime().isBeforeNow()) {
        meetUpHistory.add(meetup);
      }
    }
    adapter.setMeetUpLists(meetUpHistory);
    checkEmptyList();
  }

  @Override
  public void onError(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
  }

  // needs to be here due to current presenter
  @Override
  public void onGetProfileSuccess(User user) {
  }

}
