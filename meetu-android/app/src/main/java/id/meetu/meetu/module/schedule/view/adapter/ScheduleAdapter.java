package id.meetu.meetu.module.schedule.view.adapter;

/**
 * Created by angelica on 08/03/18.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.meetu.meetu.R;
import id.meetu.meetu.module.schedule.model.ScheduleInfo;
import java.util.List;
import org.joda.time.format.DateTimeFormat;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ScheduleViewHolder> {

  private Context mCtx;
  private List<ScheduleInfo> allSchedules;

  public ScheduleAdapter(Context mCtx, List<ScheduleInfo> allSchedules) {
    this.mCtx = mCtx;
    this.allSchedules = allSchedules;
  }

  @Override
  public void onBindViewHolder(ScheduleViewHolder holder, int position) {
    ScheduleInfo info = allSchedules.get(position);
    String date = DateTimeFormat.forPattern("EEEE, dd MMMM").print(info.getDate());

    holder.vTitle.setText(info.getTitle());
    holder.vDate.setText(date);
    holder.vTime.setText(info.getTime());

    if (info.isMeetup()) {
      holder.vLayout.setBackgroundResource(R.drawable.cardview_background);
      holder.vType.setText(R.string.meetup);
    }
  }

  @Override
  public ScheduleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.
        from(this.mCtx).
        inflate(R.layout.layout_cardview_schedule, null);

    return new ScheduleViewHolder(itemView);
  }

  @Override
  public int getItemCount() {
    return allSchedules.size();
  }

  public List<ScheduleInfo> getScheduleList() {
    return this.allSchedules;
  }

  class ScheduleViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.card_layout)
    LinearLayout vLayout;
    @BindView(R.id.title)
    TextView vTitle;
    @BindView(R.id.date)
    TextView vDate;
    @BindView(R.id.time)
    TextView vTime;
    @BindView(R.id.type)
    TextView vType;

    public ScheduleViewHolder(View itemView) {
      super(itemView);

      ButterKnife.bind(this, itemView);
    }
  }
}
