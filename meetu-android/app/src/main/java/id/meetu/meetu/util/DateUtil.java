package id.meetu.meetu.util;

import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Created by ayaz97 on 12/03/18.
 */

public class DateUtil {

  public static String getRepetitionText(RepeatType repeatType, int repeatCount) {
    String mode = "day";
    if (repeatType == RepeatType.MONTHLY) {
      mode = "month";
    } else if (repeatType == RepeatType.WEEKLY) {
      mode = "week";
    }

    String text = "Every " + mode + " for " + repeatCount + " " + mode + "(s)";
    return text;
  }

  public static List<String> getListForTimeDropdown(RepeatType mode) {
    int maxRepetition = 1;

    if (mode == RepeatType.DAILY) {
      maxRepetition = Constant.MAX_DAY_REPETITION;
    } else if (mode == RepeatType.WEEKLY) {
      maxRepetition = Constant.MAX_WEEK_REPETITION;
    } else if (mode == RepeatType.MONTHLY) {
      maxRepetition = Constant.MAX_MONTH_REPETITION;
    }

    List<String> times = new ArrayList<>();
    for (int i = 1; i <= maxRepetition; i++) {
      times.add(Integer.toString(i));
    }

    return times;
  }

  public static String getHourMinuteString(int hour, int minute, String separator) {
    String format = "HH" + separator + "mm";
    DateTimeFormatter dtf = DateTimeFormat.forPattern(format);
    DateTime date = new DateTime(1, 1, 1, hour, minute);

    return dtf.print(date);
  }

  public static DateTime parseHourMinuteString(String date, String separator) {
    String format = "HH" + separator + "mm";
    DateTimeFormatter dtf = DateTimeFormat.forPattern(format);
    DateTime result = null;

    try {
      result = dtf.parseDateTime(date);
    } catch (Exception e) {
      result = null;
    }

    return result;
  }

  public static String getDateString(DateTime date) {
    String format = Constant.TEXT_DATE_FORMAT;
    DateTimeFormatter dtf = DateTimeFormat.forPattern(format);

    return dtf.print(date);
  }

  public static DateTime parseDateString(String date) {
    String format = Constant.TEXT_DATE_FORMAT;
    DateTimeFormatter dtf = DateTimeFormat.forPattern(format);
    DateTime result = null;

    try {
      result = dtf.parseDateTime(date);
    } catch (Exception e) {
      result = null;
    }

    return result;
  }

  public static DateTime getNextRepetition(DateTime schedule, int time, RepeatType repeatType) {
    DateTime nextRepetition = null;

    if (repeatType == RepeatType.DAILY) {
      nextRepetition = schedule.plusDays(time);
    } else if (repeatType == RepeatType.WEEKLY) {
      nextRepetition = schedule.plusWeeks(time);
    } else if (repeatType == RepeatType.MONTHLY) {
      nextRepetition = schedule.plusMonths(time);
    }

    return nextRepetition;
  }

  public static List<DateInterval> getEventRepetition(DateTime start, DateTime end, RepeatType repeatType,
      int repeatTimes) {
    List<DateInterval> schedule = new ArrayList<>();

    if (repeatType == RepeatType.ONCE) {
      schedule.add(new DateInterval(start, end));
    } else {
      for (int i = 0; i < repeatTimes; i++) {
        DateTime currentStart = getNextRepetition(start, i, repeatType);
        DateTime currentEnd = getNextRepetition(end, i, repeatType);

        schedule.add(new DateInterval(currentStart, currentEnd));
      }
    }

    return schedule;
  }

  public enum RepeatType {
    ONCE,
    DAILY,
    WEEKLY,
    MONTHLY
  }
}