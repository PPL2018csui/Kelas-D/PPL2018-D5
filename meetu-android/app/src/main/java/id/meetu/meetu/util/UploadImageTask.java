package id.meetu.meetu.util;

import static java.util.UUID.randomUUID;

import android.os.AsyncTask;
import android.util.Log;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;

import id.meetu.meetu.module.profile.contract.UploadImageInterfaceCallback;
import java.io.InputStream;

public class UploadImageTask extends AsyncTask<InputStream, Void, String> {

  private UploadImageInterfaceCallback callback;

  @Override
  protected void onPreExecute() {
    super.onPreExecute();
    callback.onUploadCallback();
  }

  @Override
  protected void onPostExecute(String s) {
    super.onPostExecute(s);
    callback.onFinishUploadCallback(this.getImageURL("images", s));
  }

  public UploadImageTask(UploadImageInterfaceCallback callback) {
    this.callback = callback;
  }

  public static final String storageConnectionString = "DefaultEndpointsProtocol=https;"
      + "AccountName=meetu;"
      + "AccountKey=lbzEyiBbkvfjlZ10BKwguFHLaYAtcZSlB5BuXIZzdkIq8q45oDPOxMZYSk0LGqWUWXrXjK9VqvSLdQEFtzKKBA==";

  public static CloudBlobContainer getContainer() throws Exception {
    // Retrieve storage account from connection-string.

    CloudStorageAccount storageAccount = CloudStorageAccount
        .parse(storageConnectionString);

    // Create the blob client.
    CloudBlobClient blobClient = storageAccount.createCloudBlobClient();

    // Get a reference to a container.
    // The container name must be lower case
    CloudBlobContainer container = blobClient.getContainerReference("images");

    return container;
  }

  public static String getImageURL(String containerName, String imageName) {
    return "https://meetu.blob.core.windows.net/" + containerName + "/" + imageName;
  }

  @Override
  protected String doInBackground(InputStream... images) {
    try {
      CloudBlobContainer container = getContainer();

      container.createIfNotExists();

      String imageName = randomUUID().toString();

      CloudBlockBlob imageBlob = container.getBlockBlobReference(imageName);
      imageBlob.upload(images[0], images[0].available());

      return imageName;
    } catch (Exception e) {
      return null;
    }
  }

}