package id.meetu.meetu.module.auth.model;

import com.google.gson.annotations.SerializedName;

public class UpdateTokenRequest {

  @SerializedName("token")
  private String token;

  public UpdateTokenRequest(String token) {
    this.token = token;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
