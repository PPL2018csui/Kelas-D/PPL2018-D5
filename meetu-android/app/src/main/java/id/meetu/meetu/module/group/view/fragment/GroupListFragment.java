package id.meetu.meetu.module.group.view.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.group.contract.GroupActivityContract;
import id.meetu.meetu.module.group.contract.GroupListContract;
import id.meetu.meetu.module.group.model.GroupDetail;
import id.meetu.meetu.module.group.presenter.GroupListPresenter;
import id.meetu.meetu.module.group.view.adapter.GroupAdapter;
import id.meetu.meetu.util.RecyclerViewClickListener;
import java.util.ArrayList;
import java.util.List;

public class GroupListFragment extends BaseFragment implements GroupListContract.View {

  @BindView(R.id.recyclerView)
  RecyclerView recyclerView;

  @BindView(R.id.no_groups)
  TextView noGroups;

  @BindView(R.id.create_group)
  Button createGroupButton;

  @BindView(R.id.content)
  RelativeLayout contentView;

  private GroupAdapter groupAdapter;
  private GroupListContract.Presenter presenter;
  private GroupActivityContract.View activity;

  public static GroupListFragment newInstance(GroupActivityContract.View activity) {
    GroupListFragment fragment = new GroupListFragment();
    Bundle args = new Bundle();

    fragment.setArguments(args);
    fragment.presenter = new GroupListPresenter(fragment);
    fragment.activity = activity;
    return fragment;
  }

  @OnClick(R.id.create_group)
  public void onCreateGroupPressed(View v) {
    activity.onShowCreateGroup(null);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_groups, container, false);

    ButterKnife.bind(this, v);
    activity.changeActionText("List Group");
    progressBarView.setProgressBarHideView(contentView);

    noGroups.setVisibility(View.GONE);

    recyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
    recyclerView.setHasFixedSize(true);

    List<GroupDetail> groupList = new ArrayList<>();
    groupAdapter = new GroupAdapter(v.getContext(), groupList, new RecyclerViewClickListener() {
      @Override
      public void onItemClick(View v, int position) {
        int groupId = groupAdapter.getGroupList().get(position).getId();
        activity.onShowGroupMembers(groupId);
      }
    });
    recyclerView.setAdapter(groupAdapter);

    presenter.getGroups();

    return v;
  }

  public void checkEmptyList() {
    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) createGroupButton.getLayoutParams();
    if (((GroupAdapter) recyclerView.getAdapter()).getGroupList().isEmpty()) {
      recyclerView.setVisibility(View.GONE);
      noGroups.setVisibility(View.VISIBLE);
      params.addRule(RelativeLayout.BELOW, R.id.no_group_layout);
    } else {
      recyclerView.setVisibility(View.VISIBLE);
      noGroups.setVisibility(View.GONE);
      params.addRule(RelativeLayout.BELOW, R.id.list);
    }
  }

  @Override
  public void onGetSuccess(List<GroupDetail> groups) {
    groupAdapter.setGroupList(groups);
    checkEmptyList();
  }

  @Override
  public void onError(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
  }
}
