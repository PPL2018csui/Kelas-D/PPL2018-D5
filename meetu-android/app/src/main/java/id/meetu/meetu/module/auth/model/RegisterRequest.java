package id.meetu.meetu.module.auth.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ayaz97 on 16/04/18.
 */

public class RegisterRequest {

  @SerializedName("username")
  protected String username;

  @SerializedName("first_name")
  protected String firstName;

  @SerializedName("last_name")
  protected String lastName;

  @SerializedName("password")
  protected String password;

  public RegisterRequest(String username, String firstName, String lastName, String password) {
    this.username = username;
    this.firstName = firstName;
    this.lastName = lastName;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

}
