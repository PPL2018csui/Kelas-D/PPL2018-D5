package id.meetu.meetu.module.schedule.model;

import org.joda.time.DateTime;

/**
 * Created by angelica on 08/03/18.
 */

public class ScheduleInfo {

  private final int MAX_ID = Integer.MAX_VALUE;
  private int id;
  private String title;
  private DateTime date;
  private String time;
  private boolean isMeetup;

  public ScheduleInfo(int id, String title, DateTime date, String time) {
    this.id = id;
    this.title = title;
    this.date = date;
    this.time = time;
    this.isMeetup = false;
  }

  public ScheduleInfo(int id, String title, DateTime date, String time, boolean isMeetup) {
    this.id = id;
    this.title = title;
    this.date = date;
    this.time = time;
    this.isMeetup = false;
    this.isMeetup = isMeetup;
  }

  public ScheduleInfo() {
    this.id = MAX_ID;
    this.title = "";
    this.date = new DateTime();
    this.time = "";
    this.isMeetup = false;
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTitle() {
    return this.title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public DateTime getDate() {
    return this.date;
  }

  public void setDate(DateTime date) {
    this.date = date;
  }

  public String getTime() {
    return this.time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public boolean isMeetup() {
    return isMeetup;
  }

  public void setMeetup(boolean meetup) {
    isMeetup = meetup;
  }
}
