package id.meetu.meetu.module.auth.model;

import com.google.gson.annotations.SerializedName;

public class UserSearchRequest {

  @SerializedName("username")
  private String username;

  public UserSearchRequest(String username) {
    this.username = username;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }
}
