package id.meetu.meetu.firebase;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import id.meetu.meetu.util.Constant;
import java.util.Map;


public class CustomFirebaseMessagingService extends FirebaseMessagingService {

  private static final String TAG = "FirebaseMsgService";

  @Override
  public void onMessageReceived(RemoteMessage remoteMessage) {
    Log.d(TAG, "From: " + remoteMessage.getFrom());

    // Check if message contains a data payload.
    if (remoteMessage.getData().size() > 0) {
      Map<String, String> data = remoteMessage.getData();
      Intent intent = new Intent(Constant.FIREBASE_EVENT);

      for(Map.Entry<String, String> entry : data.entrySet()) {
        intent.putExtra(entry.getKey(), entry.getValue());
        Log.d(TAG, entry.getKey() + " " + entry.getValue());
      }

      LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
  }
}
