package id.meetu.meetu.module.group.contract;

import id.meetu.meetu.base.BaseContract;
import id.meetu.meetu.module.group.model.GroupMemberDetail;
import id.meetu.meetu.module.group.model.GroupMembers;
import java.util.List;

public interface GroupMembersContract {

  interface View extends BaseContract.View {

    void onGetMembersSuccess(GroupMembers groupWithMembers);

    void onGetMembersError(String message);
  }

  interface Presenter {

    void getMembersInGroup(int groupId);
  }
}
