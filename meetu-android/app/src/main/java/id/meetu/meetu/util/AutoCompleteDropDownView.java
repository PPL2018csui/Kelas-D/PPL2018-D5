package id.meetu.meetu.util;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;

public class AutoCompleteDropDownView extends AppCompatAutoCompleteTextView {

  private boolean isPopup;

  public AutoCompleteDropDownView(Context context) {
    super(context);
  }

  public AutoCompleteDropDownView(Context arg0, AttributeSet arg1) {
    super(arg0, arg1);
  }

  public AutoCompleteDropDownView(Context arg0, AttributeSet arg1, int arg2) {
    super(arg0, arg1, arg2);
  }

  @Override
  public boolean enoughToFilter() {
    return true;
  }

  @Override
  protected void onFocusChanged(boolean focused, int direction,
      Rect previouslyFocusedRect) {
    super.onFocusChanged(focused, direction, previouslyFocusedRect);
    if (focused) {
      performFiltering("", 0);
      InputMethodManager imm = (InputMethodManager) getContext()
          .getSystemService(Context.INPUT_METHOD_SERVICE);
      imm.hideSoftInputFromWindow(getWindowToken(), 0);
      setKeyListener(null);
      dismissDropDown();
    } else {
      isPopup = false;
    }
  }


  @Override
  public boolean onTouchEvent(MotionEvent event) {

    switch (event.getAction()) {
      case MotionEvent.ACTION_UP: {
        if (isPopup) {
          dismissDropDown();
        } else {
          requestFocus();
          showDropDown();
        }
        break;
      }
    }

    return super.onTouchEvent(event);
  }

  @Override
  public void showDropDown() {
    super.showDropDown();
    isPopup = true;
  }

  @Override
  public void dismissDropDown() {
    super.dismissDropDown();
    isPopup = false;
  }
}
