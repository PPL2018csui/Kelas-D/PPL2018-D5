package id.meetu.meetu.module.group.presenter;

import id.meetu.meetu.module.group.contract.CreateGroupContract;
import id.meetu.meetu.module.group.model.CreateGroupRequest;
import id.meetu.meetu.module.group.model.GroupMemberDetail;
import id.meetu.meetu.module.group.model.GroupMemberResponse;
import id.meetu.meetu.module.group.model.GroupMembers;
import id.meetu.meetu.module.group.service.GroupService;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateGroupPresenter implements CreateGroupContract.Presenter {

  private GroupService groupService;
  private CreateGroupContract.View view;

  public CreateGroupPresenter(CreateGroupContract.View view) {
    this.view = view;
    this.groupService = new GroupService();
  }

  public CreateGroupPresenter(CreateGroupContract.View view, GroupService service) {
    this.view = view;
    this.groupService = service;
  }

  private boolean isValidGroupName(String name) {
    return !("".equals(name));
  }

  @Override
  public void submit(GroupMembers groupMembers) {
    view.resetAllError();

    final String name = groupMembers.getTitle();

    if (!isValidGroupName(name)) {
      view.setGroupNameError("This field can't be empty");
      return;
    }

    List<Integer> userIds = new ArrayList<>();
    for (GroupMemberDetail member : groupMembers.getGroupMember()) {
      userIds.add(member.getId());
    }

    CreateGroupRequest request = new CreateGroupRequest(name, userIds);

    view.onLoading();
    groupService.createGroup(new Callback<GroupMemberResponse>() {
      @Override
      public void onResponse(Call<GroupMemberResponse> call,
          Response<GroupMemberResponse> response) {
        if (response.isSuccessful()) {
          view.onFinishLoading();
          view.onCreateSuccess("Group " + name + " created");
        } else {
          view.onFinishLoading();
          view.onCreateError("Failed to create group");
        }
      }

      @Override
      public void onFailure(Call<GroupMemberResponse> call, Throwable t) {
        view.onFinishLoading();
        view.onCreateError("Something is wrong with your connection");
      }
    }, request);
  }
}
