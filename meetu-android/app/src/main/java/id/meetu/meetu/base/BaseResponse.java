package id.meetu.meetu.base;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ayaz97 on 04/03/18.
 */

public class BaseResponse {
    @SerializedName("success")
    private boolean success;

    @SerializedName("error_message")
    private String errorMessage;

    @SerializedName("message")
    private String message;

    public boolean getSuccess() {
        return success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getMessage() {
        return message;
    }
}
