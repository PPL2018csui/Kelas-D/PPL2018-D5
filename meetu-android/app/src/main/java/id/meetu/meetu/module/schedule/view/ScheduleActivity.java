package id.meetu.meetu.module.schedule.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.module.schedule.view.fragment.AddScheduleFragment;
import id.meetu.meetu.module.schedule.view.fragment.CalendarFragment;
import id.meetu.meetu.module.schedule.view.fragment.ScheduleFragment;

public class ScheduleActivity extends BaseActivity {

  @BindView(R.id.add_schedule_button)
  Button addScheduleBtn;

  @OnClick(R.id.add_schedule_back_button)
  public void onClickAddScheduleBackButton(View v) {
    onBackPressed();
  }

  @Override
  public int findLayout() {
    return R.layout.activity_schedule;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_schedule);
    ButterKnife.bind(this);

    showCalendar();
    showSchedule();
  }

  public void showCalendar() {
    getFragmentManager().beginTransaction()
        .replace(R.id.calendar, new CalendarFragment()).commit();
  }

  public void showSchedule() {
    getFragmentManager().beginTransaction()
        .replace(R.id.schedule, new ScheduleFragment()).commit();
  }

  @OnClick(R.id.add_schedule_button)
  public void onAddScheduleButtonClick() {
    AddScheduleFragment fragment = AddScheduleFragment.newInstance();

    fragment.show(getFragmentManager(), "add_schedule");
  }
}
