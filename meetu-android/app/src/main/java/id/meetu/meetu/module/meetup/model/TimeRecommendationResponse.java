package id.meetu.meetu.module.meetup.model;

import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

public class TimeRecommendationResponse {

  @SerializedName("start_time")
  DateTime startTime;

  @SerializedName("end_time")
  DateTime endTime;

  public TimeRecommendationResponse(DateTime startTime, DateTime endTime) {
    this.startTime = startTime;
    this.endTime = endTime;
  }

  public DateTime getStartTime() {
    return startTime;
  }

  public void setStartTime(DateTime startTime) {
    this.startTime = startTime;
  }

  public DateTime getEndTime() {
    return endTime;
  }

  public void setEndTime(DateTime endTime) {
    this.endTime = endTime;
  }
}
