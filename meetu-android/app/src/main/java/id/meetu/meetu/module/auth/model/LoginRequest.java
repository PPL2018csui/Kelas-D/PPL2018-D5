package id.meetu.meetu.module.auth.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ayaz97 on 16/04/18.
 */

public class LoginRequest {

  @SerializedName("username")
  private String username;

  @SerializedName("password")
  private String password;

  public LoginRequest(String username, String password) {
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
