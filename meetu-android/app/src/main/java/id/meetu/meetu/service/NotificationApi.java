package id.meetu.meetu.service;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.base.BaseApi;
import id.meetu.meetu.module.notification.model.NotificationResponse;
import id.meetu.meetu.service.NotificationApi.Service;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;


public class NotificationApi extends BaseApi<Service> {

  @Override
  public String findUrl() {
    return BuildConfig.BACKEND_URL;
  }

  @Override
  public Service create() {
    return build().create(Service.class);
  }

  public interface Service {

    @GET("api/notifications")
    Call<List<NotificationResponse>> getNotifications();
  }
}
