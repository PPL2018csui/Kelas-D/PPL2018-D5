package id.meetu.meetu.module.meetup.view.adapter;

/**
 * Created by angelica on 13/04/18.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.meetu.meetu.R;
import id.meetu.meetu.module.meetup.model.TimeRecommendationDetail;
import id.meetu.meetu.util.DateUtil;
import id.meetu.meetu.util.RecyclerViewClickListener;
import java.util.List;


public class TimeRecommendationAdapter extends
    RecyclerView.Adapter<TimeRecommendationAdapter.TimeRecommendationHolder> {

  private Context mCtx;
  private List<TimeRecommendationDetail> allTimeRecommend;
  private RecyclerViewClickListener listener;

  public TimeRecommendationAdapter(Context mCtx, List<TimeRecommendationDetail> allTimeRecommend,
      RecyclerViewClickListener listener) {
    this.mCtx = mCtx;
    this.allTimeRecommend = allTimeRecommend;
    this.listener = listener;
  }

  @Override
  public void onBindViewHolder(TimeRecommendationHolder holder, int position) {
    TimeRecommendationDetail info = allTimeRecommend.get(position);
    String startDetail = DateUtil
        .getHourMinuteString(info.getStartTime().getHourOfDay(),
            info.getStartTime().getMinuteOfHour(),
            ".");
    String endDetail = DateUtil
        .getHourMinuteString(info.getEndTime().getHourOfDay(), info.getEndTime().getMinuteOfHour(),
            ".");
    String interval = startDetail + " - " + endDetail;
    holder.vTime.setText(interval);

    if(info.getAvailableMembers() == info.getMembers()) {
      holder.vMemberAvailable.setText(
          "All Members Available");
    } else {
      holder.vMemberAvailable.setText(
          info.getAvailableMembers() + " out of " + info.getMembers() + " Members Available");
    }
  }

  @Override
  public TimeRecommendationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView = LayoutInflater.from(this.mCtx)
        .inflate(R.layout.layout_cardview_recommendation, null);
    final TimeRecommendationHolder itemViewHolder = new TimeRecommendationHolder(itemView);
    itemView.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        listener.onItemClick(v, itemViewHolder.getAdapterPosition());
      }
    });
    return itemViewHolder;
  }

  @Override
  public int getItemCount() {
    return allTimeRecommend.size();
  }

  public List<TimeRecommendationDetail> getTimeRecommendList() {
    return this.allTimeRecommend;
  }

  public void setTimeRecommendList(List<TimeRecommendationDetail> timeRecommendList) {
    this.allTimeRecommend = timeRecommendList;
    notifyDataSetChanged();
  }

  public static class TimeRecommendationHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.time)
    TextView vTime;

    @BindView(R.id.member_available)
    TextView vMemberAvailable;

    public TimeRecommendationHolder(View v) {
      super(v);
      ButterKnife.bind(this, v);
    }
  }
}
