package id.meetu.meetu.module.meetup.view.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.module.meetup.contract.AddMeetUpActivityContract;
import id.meetu.meetu.module.meetup.model.TimeRecommendationDetail;
import id.meetu.meetu.module.meetup.view.adapter.TimeRecommendationAdapter;
import id.meetu.meetu.util.RecyclerViewClickListener;
import id.meetu.meetu.util.SharedPrefManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Created by angelica on 10/04/18.
 */

public class TimeRecommendationFragment extends Fragment implements
    IntervalPickerFragment.Listener {

  @BindView(R.id.schedule_prev_button)
  ImageButton prevButton;

  @BindView(R.id.schedule_next_button)
  ImageButton nextButton;

  @BindView(R.id.date_display)
  TextView date;

  @BindView(R.id.card_list)
  RecyclerView recyclerView;

  private List<DateTime> recommendedDate;
  private Map<String, List<TimeRecommendationDetail>> recommendationMap;
  private int datePosition = 0;
  private TimeRecommendationAdapter timeRecommendationAdapter;
  private List<List<TimeRecommendationDetail>> allRecommendations;

  private AddMeetUpActivityContract.View activity;

  public static TimeRecommendationFragment newInstance(
      List<List<TimeRecommendationDetail>> allRecommendations,
      AddMeetUpActivityContract.View activity) {
    TimeRecommendationFragment fragment = new TimeRecommendationFragment();
    Bundle args = new Bundle();

    fragment.recommendationMap = new TreeMap<>();
    fragment.recommendedDate = new ArrayList<>();
    fragment.allRecommendations = allRecommendations;
    fragment.activity = activity;
    fragment.setArguments(args);

    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_time_recommendation, container, false);
    ButterKnife.bind(this, v);

    LinearLayoutManager llm = new LinearLayoutManager(getActivity());
    llm.setOrientation(LinearLayoutManager.VERTICAL);
    recyclerView.setLayoutManager(llm);
    recyclerView.setHasFixedSize(true);

    List<TimeRecommendationDetail> timeRecommendationList = new ArrayList<>();
    timeRecommendationAdapter = new TimeRecommendationAdapter(getActivity(),
        timeRecommendationList, new RecyclerViewClickListener() {
      @Override
      public void onItemClick(View v, int position) {
        showIntervalPickerDialog(position);
      }
    });

    populateTimeRecommendation();

    return v;
  }

  public int getDatePosition() {
    return this.datePosition;
  }

  public void setDatePosition(int datePosition) {
    if (datePosition < 0) {
      this.datePosition = recommendedDate.size() - 1;
    } else if (datePosition >= recommendedDate.size()) {
      this.datePosition = 0;
    } else {
      this.datePosition = datePosition;
    }
  }

  public void updatePosition() {
    String textDate = DateTimeFormat.forPattern("dd MMMM")
        .print(this.recommendedDate.get(datePosition));
    List<TimeRecommendationDetail> recommendedTime = this.recommendationMap.get(textDate);
    timeRecommendationAdapter.setTimeRecommendList(recommendedTime);
    recyclerView.setAdapter(timeRecommendationAdapter);
    date.setText(textDate);
  }

  @OnClick(R.id.schedule_prev_button)
  public void prevDate() {
    this.setDatePosition(this.datePosition - 1);
    updatePosition();
  }

  @OnClick(R.id.schedule_next_button)
  public void nextDate() {
    this.setDatePosition(this.datePosition + 1);
    updatePosition();
  }

  private void populateTimeRecommendation() {
    for (List<TimeRecommendationDetail> date : allRecommendations) {
      DateTime dateTime = date.get(0).getStartTime();
      this.recommendedDate.add(dateTime);

      String textDate = DateTimeFormat.forPattern("dd MMMM")
          .print(dateTime);
      this.recommendationMap.put(textDate, date);
    }

    updatePosition();
  }

  private void showIntervalPickerDialog(int position) {
    TimeRecommendationDetail detail = allRecommendations.get(datePosition).get(position);
    DateTime start = detail.getStartTime();
    DateTime end = detail.getEndTime();

    IntervalPickerFragment fragment = IntervalPickerFragment.newInstance(start, end, this);
    fragment.show(getChildFragmentManager(), "interval_picker");
  }

  @Override
  public void onConfirm(DateTime start, DateTime end) {
    long chosenDate = recommendedDate.get(datePosition).getMillis();
    SharedPrefManager manager = SharedPrefManager.getInstance(null);

    manager.writeLong(SharedPrefManager.MEETUP_FORM_START_TIME_KEY, start.getMillis());
    manager.writeLong(SharedPrefManager.MEETUP_FORM_END_TIME_KEY, end.getMillis());
    manager.writeLong(SharedPrefManager.MEETUP_FORM_CHOSEN_START_KEY, chosenDate);

    activity.onFinishPickInterval();
  }

}
