package id.meetu.meetu.module.group.model;

public class GroupMemberDetail {

  private final int MAX_ID = Integer.MAX_VALUE;
  private int id;
  private String name;
  private String profileImageUrl;

  public GroupMemberDetail() {
    this.id = MAX_ID;
    this.name = "Luthfi Aulia Sulaiman";
  }

  public GroupMemberDetail(int id, String name, String profileImageUrl) {
    this.id = id;
    this.name = name;
    this.profileImageUrl = profileImageUrl;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getProfileImageUrl() {
    return profileImageUrl;
  }

  public void setProfileImageUrl(String profileImageUrl) {
    this.profileImageUrl = profileImageUrl;
  }
}
