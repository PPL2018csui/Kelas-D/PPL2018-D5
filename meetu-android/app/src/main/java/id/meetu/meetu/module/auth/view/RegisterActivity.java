package id.meetu.meetu.module.auth.view;

import static id.meetu.meetu.util.Constant.LOGIN_REGISTER_SUCCESS_CODE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.TypedValue;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.module.auth.contract.RegisterContract;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.presenter.RegisterPresenter;
import id.meetu.meetu.module.home.view.activity.HomeActivity;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.SharedPrefManager;

/**
 * Created by luthfi on 15/04/18.
 */

public class RegisterActivity extends AuthActivity implements RegisterContract.View {

  @BindView(R.id.form_register_firstname_text)
  TextInputLayout firstNameText;
  @BindView(R.id.form_register_firstname)
  EditText firstName;
  @BindView(R.id.form_register_lastname_text)
  TextInputLayout lastNameText;
  @BindView(R.id.form_register_lastname)
  EditText lastName;
  @BindView(R.id.form_register_email_text)
  TextInputLayout emailText;
  @BindView(R.id.form_register_email)
  EditText email;
  @BindView(R.id.form_register_password_text)
  TextInputLayout passwordText;
  @BindView(R.id.form_register_password)
  EditText password;
  @BindView(R.id.form_register_verify_password_text)
  TextInputLayout verifyPasswordText;
  @BindView(R.id.form_register_verify_password)
  EditText verifyPassword;
  private RegisterContract.Presenter presenter;

  @Override
  public int findLayout() {
    return R.layout.activity_registration;
  }

  @Override
  public void updateFirebaseToken(String token) {
    // emptied as before login, no account is associated in backend
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ButterKnife.bind(this);

    presenter = new RegisterPresenter(this);
    progressBarView.setWithOverlay(true);
  }

  @Override
  public void disableAllError() {
    firstNameText.setErrorEnabled(false);
    emailText.setErrorEnabled(false);
    lastNameText.setErrorEnabled(false);
    passwordText.setErrorEnabled(false);
    verifyPasswordText.setErrorEnabled(false);
    resetMarginOnError(firstNameText);
    resetMarginOnError(emailText);
    resetMarginOnError(lastNameText);
    resetMarginOnError(passwordText);
    resetMarginOnError(verifyPasswordText);
  }

  @SuppressLint("ResourceAsColor")
  @Override
  public void setFirstNameError(String error) {
    firstNameText.setError(error);
    setMarginOnError(firstNameText);
  }

  @Override
  public void setLastNameError(String error) {
    lastNameText.setError(error);
    setMarginOnError(lastNameText);
  }

  @Override
  public void setEmailError(String error) {
    emailText.setError(error);
    setMarginOnError(emailText);
  }

  @Override
  public void setPasswordError(String error) {
    passwordText.setError(error);
    setMarginOnError(passwordText);
  }

  @Override
  public void setVerifyPasswordError(String error) {
    verifyPasswordText.setError(error);
    setMarginOnError(verifyPasswordText);
  }

  @Override
  public void onCancel() {
    onBackPressed();
  }

  @Override
  public void onRegisterSuccess(String token, User user) {
    Toast.makeText(this, "Register success!", Toast.LENGTH_SHORT).show();
    SharedPrefManager manager = SharedPrefManager.getInstance(this);

    manager.writeString("token", token);
    manager.writeInt(SharedPrefManager.USER_ID_KEY, user.getId());
    manager.writeInt(SharedPrefManager.USER_PROFILE_ID_KEY, user.getUserProfile().getId());
    manager.writeString(SharedPrefManager.USER_FIRST_NAME, user.getFirstName());
    manager.writeString(SharedPrefManager.USER_LAST_NAME, user.getLastName());
    manager
        .writeString(SharedPrefManager.USER_PHONE_NUMBER, user.getUserProfile().getPhoneNumber());
    manager.writeString(SharedPrefManager.USER_LOCATION, user.getUserProfile().getLocation());
    manager.writeString(SharedPrefManager.USER_EMAIL, user.getUsername());
    manager.writeString(SharedPrefManager.USER_PROFILE_IMAGE_URL,
        user.getUserProfile().getProfileImageUrl());

    Intent intent = new Intent(this, HomeActivity.class);
    intent.putExtra(Constant.FIRST_TIME_HOME, true);
    startActivity(intent);

    setResult(LOGIN_REGISTER_SUCCESS_CODE);
    finish();
  }

  @Override
  public void onRegisterFail() {
    Toast.makeText(this, "Username has been taken", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onRegisterError() {
    Toast.makeText(this, "There is problem with your connection", Toast.LENGTH_SHORT).show();
  }

  @OnClick(R.id.confirm_register_button)
  public void onClickRegister() {
    String firstName = this.firstName.getText().toString().trim();
    String lastName = this.lastName.getText().toString().trim();
    String email = this.email.getText().toString().trim();
    String password = this.password.getText().toString();
    String verifyPassword = this.verifyPassword.getText().toString();

    presenter.submit(email, firstName, lastName, password, verifyPassword);
  }

  @OnClick(R.id.cancel_register)
  public void onClickCancel() {
    presenter.cancel();
  }

  public static int getPixelValue(Context context, int dimenId) {
    Resources resources = context.getResources();
    return (int) TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dimenId,
        resources.getDisplayMetrics()
    );
  }

  public void setMarginOnError(LinearLayout layout) {
    int bottom = getPixelValue(this,
        (int) getResources().getDimension(R.dimen.registration_margin));
    LayoutParams params = (LinearLayout.LayoutParams) layout.getLayoutParams();
    params.setMargins(0, 0, 0, bottom);
    layout.setLayoutParams(params);
  }

  public void resetMarginOnError(LinearLayout layout) {
    LayoutParams params = (LinearLayout.LayoutParams) layout.getLayoutParams();
    params.setMargins(0, 0, 0, 0);
    layout.setLayoutParams(params);
  }
}
