package id.meetu.meetu.module.profile.contract;

public interface ProfileActivityContract {

  interface View {

    void onBackPressed();

    void showProfile();

    void showEditProfileForm();

    void showChangePassword();

    void showMeetUpHistory();

    void showMyGroup();

    void showSchedule();

    void onLogout();
  }
}
