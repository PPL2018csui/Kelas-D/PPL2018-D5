package id.meetu.meetu.module.notification.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.meetu.meetu.R;
import id.meetu.meetu.module.notification.model.NotificationDetail;
import id.meetu.meetu.util.Constant;
import java.util.List;

/**
 * Created by luthfi on 09/05/18.
 */

public class NotificationAdapter extends
    RecyclerView.Adapter<RecyclerView.ViewHolder> {

  private Context mCtx;
  private List<NotificationDetail> notificationList;

  public NotificationAdapter(Context mCtx, List<NotificationDetail> notificationList) {
    this.mCtx = mCtx;
    this.notificationList = notificationList;
  }

  @Override
  public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

    LayoutInflater inflater = LayoutInflater.from(mCtx);
    if (viewType == Constant.ITEM_TYPE_EVENT) {
      View view = inflater.inflate(R.layout.layout_cardview_notifications_event, null);
      return new EventViewHolder(view);
    } else if (viewType == Constant.ITEM_TYPE_INVITATION) {
      View view = inflater.inflate(R.layout.layout_cardview_notifications_group_invite, null);
      return new GroupInviteViewHolder(view);
    }
    return null;
  }

  @Override
  public void onBindViewHolder(ViewHolder holder, int position) {
    NotificationDetail detail = notificationList.get(position);
    final int itemType = getItemViewType(position);

    switch (itemType) {
      case Constant.ITEM_TYPE_EVENT:
        EventViewHolder holderEvent = (EventViewHolder) holder;
        holderEvent.notificationsHeader.setText(detail.getHeader());
        holderEvent.notificationsEvent.setText(detail.getTitle());
        holderEvent.notificationsGroup.setText(detail.getGroups());
        holderEvent.notificationsDate.setText(detail.getDate());
        holderEvent.notificationsTime.setText(detail.getLocationTime());
        holderEvent.notificationsRepetition.setText(detail.getRepetition());
        break;
      case Constant.ITEM_TYPE_INVITATION:
        GroupInviteViewHolder holderGroup = (GroupInviteViewHolder) holder;
        holderGroup.notificationsHeader.setText(detail.getHeader());
        holderGroup.notificationsGroup.setText(detail.getGroups());
        holderGroup.notificationsInvitee.setText(detail.getPerson());
        break;
    }

  }

  @Override
  public int getItemViewType(int position) {
    if (notificationList.get(position).getNotificationType() == 0) {
      return Constant.ITEM_TYPE_EVENT;
    } else {
      return Constant.ITEM_TYPE_INVITATION;
    }
  }

  @Override
  public int getItemCount() {
    return notificationList.size();
  }

  public void setNotificationList(List<NotificationDetail> notificationList) {
    this.notificationList = notificationList;
    notifyDataSetChanged();
  }

  public List<NotificationDetail> getNotificationList() {
    return this.notificationList;
  }

  class EventViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.notifications_icon)
    ImageView notificationIcon;

    @BindView(R.id.notifications_header)
    TextView notificationsHeader;

    @BindView(R.id.notifications_event)
    TextView notificationsEvent;

    @BindView(R.id.notifications_group)
    TextView notificationsGroup;

    @BindView(R.id.notifications_date)
    TextView notificationsDate;

    @BindView(R.id.notifications_time)
    TextView notificationsTime;

    @BindView(R.id.notifications_repetition)
    TextView notificationsRepetition;

    public EventViewHolder(View itemView) {
      super(itemView);

      ButterKnife.bind(this, itemView);
    }
  }

  class GroupInviteViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.notifications_icon)
    ImageView notificationIcon;

    @BindView(R.id.notification_invite_header)
    TextView notificationsHeader;

    @BindView(R.id.notification_invite_group)
    TextView notificationsGroup;

    @BindView(R.id.notification_invite_invitee)
    TextView notificationsInvitee;

    public GroupInviteViewHolder(View itemView) {
      super(itemView);

      ButterKnife.bind(this, itemView);
    }
  }
}
