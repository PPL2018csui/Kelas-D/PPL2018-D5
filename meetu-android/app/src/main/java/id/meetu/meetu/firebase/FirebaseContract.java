package id.meetu.meetu.firebase;

import id.meetu.meetu.base.BaseContract;

public class FirebaseContract {

  public interface View extends BaseContract.View {

    void onFinishUpdateFirebaseToken(String token);

    void onFailUpdateFirebaseToken(String error);
  }

  public interface Presenter {

    void updateFirebaseToken(String token);
  }
}
