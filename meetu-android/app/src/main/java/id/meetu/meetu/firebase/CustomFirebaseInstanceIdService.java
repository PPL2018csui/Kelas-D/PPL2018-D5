package id.meetu.meetu.firebase;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import id.meetu.meetu.util.Constant;


public class CustomFirebaseInstanceIdService extends FirebaseInstanceIdService {

  private static final String TAG = "FirebaseInstanceService";

  @Override
  public void onTokenRefresh() {
    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
    Log.d(TAG, "Refreshed token: " + refreshedToken);

    Intent intent = new Intent(Constant.FIREBASE_EVENT);
    intent.putExtra("type", Constant.FIREBASE_EVENT_TOKEN);
    intent.putExtra(Constant.FIREBASE_EVENT_TOKEN, refreshedToken);

    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
  }
}
