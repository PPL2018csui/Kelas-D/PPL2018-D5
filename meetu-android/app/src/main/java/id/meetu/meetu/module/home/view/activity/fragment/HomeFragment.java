package id.meetu.meetu.module.home.view.activity.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.home.presenter.HomePresenter;
import id.meetu.meetu.module.home.presenter.HomePresenterListener;
import id.meetu.meetu.module.home.view.activity.HomeActivity;
import id.meetu.meetu.module.meetup.model.MeetUpDetail;
import id.meetu.meetu.module.meetup.view.activity.AddMeetupActivity;
import id.meetu.meetu.module.meetup.view.adapter.MeetUpDetailAdapter;
import id.meetu.meetu.util.BottomNavigationViewHelper;
import id.meetu.meetu.util.SharedPrefManager;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luthfi on 04/03/18.
 */

public class HomeFragment extends BaseFragment implements HomePresenterListener {

  @BindView(R.id.swipe_layout)
  SwipeRefreshLayout swipeRefreshLayout;
  @BindView(R.id.card_list)
  RecyclerView recyclerView;
  @BindView(R.id.navbar_bottom)
  BottomNavigationViewEx bottomNav;
  @BindView(R.id.no_meetUp)
  TextView noEventsTxtView;
  @BindView(R.id.content)
  LinearLayout baseView;
  @BindView(R.id.base)
  LinearLayout hideView;
  private MeetUpDetailAdapter adapter;
  private HomePresenter presenter = new HomePresenter(this);

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_home, container, false);
    ButterKnife.bind(this, view);
    progressBarView.setProgressBarBaseView(baseView);
    progressBarView.setProgressBarHideView(hideView);
    noEventsTxtView.setVisibility(View.GONE);

    recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
    recyclerView.setHasFixedSize(true);

    swipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
      @Override
      public void onRefresh() {
        presenter.getMeetUps();
        swipeRefreshLayout.setRefreshing(false);
      }
    });

    List<MeetUpDetail> meetUpDetails = new ArrayList<>();
    adapter = new MeetUpDetailAdapter(view.getContext(), meetUpDetails);
    recyclerView.setAdapter(adapter);

    presenter.getMeetUps();
    presenter.getProfile();

    return view;
  }

  @Override
  public void onResume() {
    super.onResume();

    BottomNavigationViewHelper
        .setupBottomNavigationView(getActivity(), bottomNav, HomeActivity.ACTIVITY_NUM);
    bottomNav.setTextVisibility(false);
  }

  public void checkEmptyList() {
    if (((MeetUpDetailAdapter) recyclerView.getAdapter()).getMeetUpList().isEmpty()) {
      recyclerView.setVisibility(View.GONE);
      noEventsTxtView.setVisibility(View.VISIBLE);
    } else {
      recyclerView.setVisibility(View.VISIBLE);
      noEventsTxtView.setVisibility(View.GONE);
    }
  }

  @Override
  public void onGetMeetUpSuccess(List<MeetUpDetail> meetUpList) {
    adapter.setMeetUpLists(meetUpList);
    checkEmptyList();
  }

  @Override
  public void onError(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    checkEmptyList();
  }

  @Override
  public void onGetProfileSuccess(User user) {
    SharedPrefManager manager = SharedPrefManager.getInstance(null);

    manager.writeInt(SharedPrefManager.USER_ID_KEY, user.getId());
    manager.writeInt(SharedPrefManager.USER_PROFILE_ID_KEY, user.getUserProfile().getId());
    manager.writeString(SharedPrefManager.USER_FIRST_NAME, user.getFirstName());
    manager.writeString(SharedPrefManager.USER_LAST_NAME, user.getLastName());
    manager
        .writeString(SharedPrefManager.USER_PHONE_NUMBER, user.getUserProfile().getPhoneNumber());
    manager.writeString(SharedPrefManager.USER_LOCATION, user.getUserProfile().getLocation());
    manager.writeString(SharedPrefManager.USER_EMAIL, user.getUsername());
    manager.writeString(SharedPrefManager.USER_PROFILE_IMAGE_URL,
        user.getUserProfile().getProfileImageUrl());
  }

  @OnClick(R.id.fab)
  public void onClick(View v) {
    startActivity(new Intent(getActivity(), AddMeetupActivity.class));
  }

  @Override
  public void onLoading() {
    View view = getView();
    if (view != null) {
      getView().setClickable(false);
    }
    progressBarView.show();
  }

  @Override
  public void onFinishLoading() {
    View view = getView();
    if (view != null) {
      getView().setClickable(true);
    }
    if (getActivity() != null) {
      getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          Handler handler = new Handler();
          handler.postDelayed(new Runnable() {
            @Override
            public void run() {
              progressBarView.hide();
            }
          }, 500);
        }
      });
    }
  }
}