package id.meetu.meetu.service;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.base.BaseApi;
import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.GoogleSignInRequest;
import id.meetu.meetu.module.auth.model.LoginRequest;
import id.meetu.meetu.module.auth.model.RegisterRequest;
import id.meetu.meetu.module.auth.model.UpdateTokenRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by ayaz97 on 16/04/18.
 */

public class AuthApi extends BaseApi<AuthApi.Service> {

  @Override
  public String findUrl() {
    return BuildConfig.BACKEND_URL;
  }

  @Override
  public AuthApi.Service create() {
    return build().create(AuthApi.Service.class);
  }

  public interface Service {

    @POST("api/login/")
    Call<AuthResponse> postLogin(@Body LoginRequest loginRequest);

    @POST("api/register/")
    Call<AuthResponse> postRegister(@Body RegisterRequest registerRequest);

    @POST("api/login-social/")
    Call<AuthResponse> postGoogleSignIn(@Body GoogleSignInRequest googleSignInRequest);

    @POST("api/update-token/")
    Call<Void> updateFirebaseToken(@Body UpdateTokenRequest updateTokenRequest);
  }
}
