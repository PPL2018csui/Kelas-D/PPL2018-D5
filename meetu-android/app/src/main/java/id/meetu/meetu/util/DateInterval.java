package id.meetu.meetu.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Created by ayaz97 on 07/03/18.
 */

public class DateInterval {

  private DateTime start;
  private DateTime end;

  public DateInterval(DateTime start, DateTime end) {
    this.start = start;
    this.end = end;
  }

  public DateTime getStart() {
    return start;
  }

  public void setStart(DateTime start) {
    this.start = start;
  }

  public DateTime getEnd() {
    return end;
  }

  public void setEnd(DateTime end) {
    this.end = end;
  }

  @Override
  public boolean equals(Object other) {
    if (other == null) {
      return false;
    }

    if (!(other instanceof DateInterval)) {
      return false;
    }

    DateInterval otherInterval = (DateInterval) other;
    return start.equals(otherInterval.getStart()) && end.equals(otherInterval.getEnd());
  }
}
