package id.meetu.meetu.module.auth.contract;

import id.meetu.meetu.base.BaseContract;

import id.meetu.meetu.module.auth.model.User;

public interface RegisterContract {

  interface View extends BaseContract.View{

    void disableAllError();

    void setFirstNameError(String error);

    void setLastNameError(String error);

    void setEmailError(String error);

    void setPasswordError(String error);

    void setVerifyPasswordError(String error);

    void onCancel();

    void onRegisterSuccess(String token, User user);

    void onRegisterFail();

    void onRegisterError();
  }

  interface Presenter {

    void cancel();

    void submit(String username, String phone, String email, String password,
        String verifyPassword);
  }
}
