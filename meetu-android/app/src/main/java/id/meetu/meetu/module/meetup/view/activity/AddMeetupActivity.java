package id.meetu.meetu.module.meetup.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.module.home.view.activity.HomeActivity;
import id.meetu.meetu.module.meetup.contract.AddMeetUpActivityContract;
import id.meetu.meetu.module.meetup.model.TimeRecommendationDetail;
import id.meetu.meetu.module.meetup.presenter.AddMeetupPresenter;
import id.meetu.meetu.module.meetup.view.fragment.AddMeetupConfirmationFormFragment;
import id.meetu.meetu.module.meetup.view.fragment.AddMeetupFormFragment;
import id.meetu.meetu.module.meetup.view.fragment.LoadingScreenFragment;
import id.meetu.meetu.module.meetup.view.fragment.TimeNotFoundFragment;
import id.meetu.meetu.module.meetup.view.fragment.TimeRecommendationFragment;
import id.meetu.meetu.util.DateUtil.RepeatType;
import id.meetu.meetu.util.SharedPrefManager;
import java.util.List;
import org.joda.time.DateTime;


public class AddMeetupActivity extends BaseActivity implements AddMeetUpActivityContract.View {

  private AddMeetUpActivityContract.Presenter presenter;

  @Override
  public void onBackPressed() {
    startActivity(new Intent(this, HomeActivity.class));
    finish();
  }

  @Override
  public void showMeetUpConfirmationDialog(Intent intent) {
    // emptied to make filling form free from notification
  }

  @Override
  public void showGroupDialog(Intent intent) {
    // emptied to make filling form free from notification
  }

  @OnClick(R.id.add_meetup_back_button)
  public void onClickBackButton(View v) {
    onBackPressed();
  }

  @Override
  public int findLayout() {
    return R.layout.activity_add_meetup;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(findLayout());
    ButterKnife.bind(this);

    presenter = new AddMeetupPresenter(this);

    showAddMeetupForm();
  }

  @Override
  public void onFinishFillForm() {
    showLoadingScreen();

    SharedPrefManager manager = SharedPrefManager.getInstance(null);
    DateTime startDate = new DateTime(
        manager.readLong(SharedPrefManager.MEETUP_FORM_START_DATE_KEY, 0));
    DateTime endDate = new DateTime(
        manager.readLong(SharedPrefManager.MEETUP_FORM_END_DATE_KEY, 0));
    RepeatType repeatType = RepeatType.valueOf(
        manager.readString(SharedPrefManager.MEETUP_FORM_REPEAT_TYPE_KEY, RepeatType.ONCE.name()));
    int repeatCount = manager.readInt(SharedPrefManager.MEETUP_FORM_REPEAT_COUNT_KEY, 1);
    int groupId = manager.readInt(SharedPrefManager.MEETUP_FORM_GROUP_ID_KEY, -1);

    presenter.fetchTimeRecommendation(groupId, startDate, endDate, repeatType, repeatCount);
  }

  @Override
  public void onFinishGetRecommendation(List<List<TimeRecommendationDetail>> recommendation) {
    showTimeRecommendation(recommendation);
  }

  @Override
  public void onFinishEmptyRecommendation() {
    showNotFoundScreen();
  }

  @Override
  public void onFinishPickInterval() {
    showAddMeetupConfirmation();
  }

  @Override
  public void onFinishCreateMeetUp() {
    Intent intent = new Intent(this, HomeActivity.class);
    finish();
    startActivity(intent);
  }

  @Override
  public void onRetryCreateMeetUp() {
    Intent intent = new Intent(this, AddMeetupActivity.class);
    finish();
    startActivity(intent);
  }

  @Override
  public void onError(String error) {
    Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    onBackPressed();
  }

  private void showAddMeetupConfirmation() {
    AddMeetupConfirmationFormFragment fragment = AddMeetupConfirmationFormFragment
        .newInstance(this);

    getFragmentManager().beginTransaction()
        .replace(R.id.form_add_meetup, fragment)
        .addToBackStack(null)
        .commit();
  }

  private void showAddMeetupForm() {
    AddMeetupFormFragment fragment = AddMeetupFormFragment.newInstance(this);

    getFragmentManager().beginTransaction()
        .replace(R.id.form_add_meetup, fragment)
        .commit();
  }

  private void showTimeRecommendation(List<List<TimeRecommendationDetail>> recommendations) {
    TimeRecommendationFragment fragment = TimeRecommendationFragment
        .newInstance(recommendations, this);

    getFragmentManager().popBackStack();
    getFragmentManager().beginTransaction()
        .replace(R.id.form_add_meetup, fragment).addToBackStack(null)
        .commit();
  }

  private void showLoadingScreen() {
    LoadingScreenFragment fragment = LoadingScreenFragment.newInstance();

    getFragmentManager().beginTransaction().replace(R.id.form_add_meetup, fragment)
        .addToBackStack(null).commit();
  }

  private void showNotFoundScreen() {
    TimeNotFoundFragment fragment = TimeNotFoundFragment.newInstance(this);

    getFragmentManager().popBackStack();
    getFragmentManager().beginTransaction().replace(R.id.form_add_meetup, fragment)
        .addToBackStack(null).commit();
  }
}
