package id.meetu.meetu.module.group.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;
import org.joda.time.DateTime;

public class GroupMemberResponse {

  @SerializedName("id")
  private int id;

  @SerializedName("members")
  private List<Member> members;

  @SerializedName("title")
  private String title;

  @SerializedName("created_date")
  private DateTime createdDate;

  public GroupMemberResponse(int id, List<Member> members, String title, DateTime createdDate) {
    this.id = id;
    this.members = members;
    this.title = title;
    this.createdDate = createdDate;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public List<Member> getMembers() {
    return members;
  }

  public void setMembers(List<Member> members) {
    this.members = members;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public DateTime getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(DateTime createdDate) {
    this.createdDate = createdDate;
  }
}
