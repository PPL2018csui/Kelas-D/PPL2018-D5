package id.meetu.meetu.module.meetup.service;

import id.meetu.meetu.module.meetup.model.TimeRecommendationRequest;
import id.meetu.meetu.module.meetup.model.TimeRecommendationResponse;
import id.meetu.meetu.service.TimeRecommendationApi;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;

public class TimeRecommendationService {

  private TimeRecommendationApi.Service service;

  public TimeRecommendationService() {
    service = new TimeRecommendationApi().create();
  }

  public TimeRecommendationService(TimeRecommendationApi.Service service) {
    this.service = service;
  }

  public TimeRecommendationApi.Service getService() {
    return service;
  }

  public void postTimeRecommendations(TimeRecommendationRequest request,
      Callback<List<List<TimeRecommendationResponse>>> callback) {
    Call<List<List<TimeRecommendationResponse>>> call = service.postTimeRecommendations(request);
    call.enqueue(callback);
  }
}
