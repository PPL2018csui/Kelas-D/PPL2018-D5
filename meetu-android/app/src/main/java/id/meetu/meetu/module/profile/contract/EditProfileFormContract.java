package id.meetu.meetu.module.profile.contract;

import id.meetu.meetu.base.BaseContract;
import id.meetu.meetu.module.profile.model.EditProfileFormData;

/**
 * Created by Andre on 5/4/2018.
 */

public interface EditProfileFormContract {

  interface View extends BaseContract.View {

    void resetAllError();

    void setFirstNameError(String error);

    void setLastNameError(String error);

    void setPhoneNumberError(String error);

    void setLocationError(String error);

    void onSubmitSuccess(EditProfileFormData data, String message);

    void onSubmitFail(String message);
  }

  interface Presenter {

    void submit(EditProfileFormData data);
  }
}