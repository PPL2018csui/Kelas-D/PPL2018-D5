package id.meetu.meetu.module.group.model;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class CreateGroupRequest {

  @SerializedName("title")
  private String title;

  @SerializedName("members")
  private List<Integer> userIds;

  public CreateGroupRequest(String title, List<Integer> userIds) {
    this.title = title;
    this.userIds = userIds;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<Integer> getUserIds() {
    return userIds;
  }

  public void setUserIds(List<Integer> userIds) {
    this.userIds = userIds;
  }
}
