package id.meetu.meetu.module.profile.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import butterknife.ButterKnife;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.module.group.view.activity.GroupActivity;
import id.meetu.meetu.module.profile.contract.ProfileActivityContract;
import id.meetu.meetu.module.profile.view.fragment.ChangePasswordFragment;
import id.meetu.meetu.module.profile.view.fragment.DisplayProfileFragment;
import id.meetu.meetu.module.profile.view.fragment.EditProfileFormFragment;
import id.meetu.meetu.module.profile.view.fragment.MeetUpHistoryFragment;
import id.meetu.meetu.module.schedule.view.ScheduleActivity;
import id.meetu.meetu.module.splash.view.SplashScreenActivity;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.SharedPrefManager;

/**
 * Created by luthfi on 04/03/18.
 */

public class ProfileActivity extends BaseActivity implements ProfileActivityContract.View {

  public static final int ACTIVITY_NUM = 0;

  @Override
  public void onBackPressed() {
    if (getFragmentManager().getBackStackEntryCount() > 0) {
      getFragmentManager().popBackStack();
    } else {
      super.onBackPressed();
      overridePendingTransition(0, 0);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ButterKnife.bind(this);
    progressBarView.setWithOverlay(true);
    
    showProfile();
  }

  @Override
  public int findLayout() {
    return R.layout.activity_profile;
  }

  public void showProfile() {
    DisplayProfileFragment fragment = DisplayProfileFragment.newInstance(this);
    getFragmentManager().beginTransaction()
        .replace(R.id.profile_display, fragment).commit();
  }

  public void showEditProfileForm() {
    EditProfileFormFragment fragment = EditProfileFormFragment.newInstance(this);
    getFragmentManager().beginTransaction()
        .replace(R.id.profile_display, fragment).addToBackStack(null).commit();
  }

  public void showChangePassword() {
    ChangePasswordFragment fragment = ChangePasswordFragment.newInstance(this);
    getFragmentManager().beginTransaction()
        .replace(R.id.profile_display, fragment).addToBackStack(null).commit();
  }

  public void showMeetUpHistory() {
    MeetUpHistoryFragment fragment = MeetUpHistoryFragment.newInstance(this);
    getFragmentManager().beginTransaction()
        .replace(R.id.profile_display, fragment).addToBackStack(null).commit();
  }

  public void showMyGroup() {
    startActivity(new Intent(ProfileActivity.this, GroupActivity.class));
  }

  public void showSchedule() {
    startActivity(new Intent(ProfileActivity.this, ScheduleActivity.class));
  }

  public void onLogout() {
    onLoading();
    updateFirebaseToken(Constant.EMPTY_TOKEN);
  }

  @Override
  public void onFinishUpdateFirebaseToken(String token) {
    onFinishLoading();
    super.onFinishUpdateFirebaseToken(token);
    SharedPrefManager.getInstance(null).clear();
    Intent intent = new Intent(ProfileActivity.this, SplashScreenActivity.class);
    startActivity(intent);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    startActivity(intent);
    ProfileActivity.this.finish();
  }

  @Override
  public void onFailUpdateFirebaseToken(String error) {
    onFinishLoading();
    Toast.makeText(this, "There is a problem with your connection", Toast.LENGTH_LONG).show();
  }
}

