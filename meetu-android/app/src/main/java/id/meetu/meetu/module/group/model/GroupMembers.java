package id.meetu.meetu.module.group.model;

import java.util.List;
import org.joda.time.DateTime;

public class GroupMembers {

  private int groupId;

  private String title;

  private DateTime createdDate;

  private List<GroupMemberDetail> groupMember;

  public GroupMembers(int groupId, String title, DateTime createdDate,
      List<GroupMemberDetail> groupMember) {
    this.groupId = groupId;
    this.title = title;
    this.createdDate = createdDate;
    this.groupMember = groupMember;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public DateTime getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(DateTime createdDate) {
    this.createdDate = createdDate;
  }

  public List<GroupMemberDetail> getGroupMember() {
    return groupMember;
  }

  public void setGroupMember(
      List<GroupMemberDetail> groupMember) {
    this.groupMember = groupMember;
  }

  public int getGroupId() {
    return groupId;
  }

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }
}
