package id.meetu.meetu.module.meetup.view.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.module.meetup.contract.AddMeetUpActivityContract;
import id.meetu.meetu.module.meetup.view.activity.AddMeetupActivity;

/**
 * Created by luthfi on 12/04/18.
 */

public class TimeNotFoundFragment extends Fragment {

  private AddMeetUpActivityContract.View activity;

  public static TimeNotFoundFragment newInstance(AddMeetUpActivityContract.View activity) {
    TimeNotFoundFragment fragment = new TimeNotFoundFragment();
    Bundle args = new Bundle();

    fragment.setArguments(args);
    fragment.activity = activity;

    return fragment;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_loading_screen_not_found, container, false);
    ButterKnife.bind(this, view);
    return view;
  }

  @OnClick(R.id.retry_add_meetup_button)
  public void onClickRetryAddMeetupButton(View v) {
    activity.onRetryCreateMeetUp();
  }
}
