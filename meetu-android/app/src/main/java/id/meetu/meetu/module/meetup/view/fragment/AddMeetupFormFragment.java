package id.meetu.meetu.module.meetup.view.fragment;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import id.meetu.meetu.R;
import id.meetu.meetu.module.group.model.GroupDetail;
import id.meetu.meetu.module.meetup.contract.AddMeetUpActivityContract;
import id.meetu.meetu.module.meetup.contract.AddMeetUpFormContract;
import id.meetu.meetu.module.meetup.model.MeetUpFormData;
import id.meetu.meetu.module.meetup.presenter.AddMeetupFormPresenter;
import id.meetu.meetu.util.AutoCompleteDropDownView;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.DateUtil;
import id.meetu.meetu.util.DateUtil.RepeatType;
import id.meetu.meetu.util.SelectRepetitionFragment;
import id.meetu.meetu.util.SharedPrefManager;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;

/**
 * Created by ricky on 09/04/18.
 */

public class AddMeetupFormFragment extends Fragment implements SelectRepetitionFragment.Listener,
    AddMeetUpFormContract.View {

  @BindView(R.id.meetup_name_text)
  TextInputLayout meetupNameText;

  @BindView(R.id.meetup_name)
  EditText meetupName;

  @BindView(R.id.start_date_text)
  TextInputLayout startDateText;

  @BindView(R.id.start_date)
  EditText startDate;

  @BindView(R.id.end_date_text)
  TextInputLayout endDateText;

  @BindView(R.id.end_date)
  EditText endDate;

  @BindView(R.id.group_dropdown)
  AutoCompleteDropDownView groupDropDown;

  @BindView(R.id.group_dropdown_text)
  TextInputLayout groupDropDownText;

  @BindView(R.id.schedule_type_dropdown)
  AutoCompleteDropDownView scheduleTypeDropDown;

  @BindView(R.id.schedule_type_dropdown_text)
  TextInputLayout scheduleTypeDropDownText;

  DatePickerDialog.OnDateSetListener dateForStart;
  DatePickerDialog.OnDateSetListener dateForEnd;

  List<GroupDetail> groups;
  ArrayAdapter<String> groupAdapter;

  ArrayAdapter<String> scheduleTypeAdapter;
  int selectedTypeIndex = 0;

  RepeatType repeatType = RepeatType.ONCE;
  int repeatCount = 1;

  DateTime start = new DateTime();
  DateTime end = new DateTime();

  int groupId = -1;
  int groupOwner = -1;

  AddMeetUpFormContract.Presenter presenter;
  AddMeetUpActivityContract.View activity;

  public static AddMeetupFormFragment newInstance(AddMeetUpActivityContract.View activity) {
    AddMeetupFormFragment fragment = new AddMeetupFormFragment();
    Bundle args = new Bundle();
    SharedPrefManager manager = SharedPrefManager.getInstance(null);

    args.putString("name", manager.readString(SharedPrefManager.MEETUP_FORM_NAME_KEY, ""));
    args.putLong("start_date", manager.readLong(SharedPrefManager.MEETUP_FORM_START_DATE_KEY, -1));
    args.putLong("end_date", manager.readLong(SharedPrefManager.MEETUP_FORM_END_DATE_KEY, -1));
    args.putString("repeat_type",
        manager.readString(SharedPrefManager.MEETUP_FORM_REPEAT_TYPE_KEY, RepeatType.ONCE.name()));
    args.putInt("repeat_count", manager.readInt(SharedPrefManager.MEETUP_FORM_REPEAT_COUNT_KEY, 1));
    args.putInt("group_id", manager.readInt(SharedPrefManager.MEETUP_FORM_GROUP_ID_KEY, -1));
    args.putString("group_title",
        manager.readString(SharedPrefManager.MEETUP_FORM_GROUP_TITLE_KEY, ""));
    args.putInt("group_owner", manager.readInt(SharedPrefManager.MEETUP_FORM_GROUP_OWNER_KEY, -1));

    fragment.setArguments(args);
    fragment.activity = activity;
    fragment.presenter = new AddMeetupFormPresenter(fragment);
    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_form_add_meetup, container, false);
    ButterKnife.bind(this, v);

    // TODO find out why it doesn't work to put them in the onCreate
    // app crash when onBackPressed
    groupAdapter = new ArrayAdapter<String>(getActivity(),
        R.layout.layout_group_spinner_item, new ArrayList<String>());

    scheduleTypeAdapter = new ArrayAdapter<>(getActivity(), R.layout.layout_group_spinner_item,
        Constant.EVENT_TYPE_FORM);

    // initiate calendar
    prepareDateListener();

    groupDropDown.setAdapter(groupAdapter);

    groupDropDown.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        groupOwner = groups.get(position).getOwner();
        groupId = groups.get(position).getId();
      }
    });

    scheduleTypeDropDown.setAdapter(scheduleTypeAdapter);
    scheduleTypeDropDown.setText(scheduleTypeAdapter.getItem(0).toString());

    scheduleTypeDropDown.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long l) {
        selectedTypeIndex = position;

        if (selectedTypeIndex == 1) {
          SelectRepetitionFragment fragment = SelectRepetitionFragment
              .newInstance(repeatType, repeatCount, AddMeetupFormFragment.this);
          fragment.show(getChildFragmentManager(), "repetition");
        } else {
          repeatCount = 1;
          repeatType = RepeatType.ONCE;
        }
      }
    });

    startDate.setInputType(InputType.TYPE_NULL);
    endDate.setInputType(InputType.TYPE_NULL);

    // prepare input from previous form
    meetupName.setText(getArguments().getString("name"));
    groupDropDown.setText(getArguments().getString("group_title"));
    repeatType = RepeatType.valueOf(getArguments().getString("repeat_type"));
    repeatCount = getArguments().getInt("repeat_count");
    if (repeatType != RepeatType.ONCE) {
      selectedTypeIndex = 1;
      updateScheduleType();
    }
    if (getArguments().getLong("start_date") != -1) {
      start = new DateTime(getArguments().getLong("start_date"));
      end = new DateTime(getArguments().getLong("end_date"));

      updateLabelStartDate();
      updateLabelEndDate();
    }

    groupId = getArguments().getInt("group_id");
    groupOwner = getArguments().getInt("group_owner");

    presenter.fetchGroups();

    return v;
  }

  @Override
  public void resetAllError() {
    meetupNameText.setErrorEnabled(false);
    startDateText.setErrorEnabled(false);
    endDateText.setErrorEnabled(false);
    groupDropDownText.setErrorEnabled(false);
  }

  @Override
  public void setMeetupNameError(String error) {
    meetupNameText.setError(error);
  }

  @Override
  public void setStartDateError(String error) {
    startDateText.setError(error);
  }

  @Override
  public void setEndDateError(String error) {
    endDateText.setError(error);
  }

  @Override
  public void setGroupError(String error) {
    groupDropDownText.setError(error);
  }

  @Override
  public void onFetchGroupError(String error) {
    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
  }

  @Override
  public void populateGroup(List<GroupDetail> groups) {
    this.groups = groups;
    List<String> groupNames = new ArrayList<>();

    for (GroupDetail group : groups) {
      groupNames.add(group.getTitle());
    }
    
    if (groupNames.size() <= 3) {
      groupDropDown.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    groupAdapter.clear();
    groupAdapter.addAll(groupNames);
    groupAdapter.notifyDataSetChanged();
  }

  @Override
  public void onFinishSubmit(MeetUpFormData data) {
    SharedPrefManager manager = SharedPrefManager.getInstance(null);
    manager.writeString(SharedPrefManager.MEETUP_FORM_NAME_KEY, data.getMeetUpName());
    manager
        .writeLong(SharedPrefManager.MEETUP_FORM_START_DATE_KEY, data.getStartDate().getMillis());
    manager.writeLong(SharedPrefManager.MEETUP_FORM_END_DATE_KEY, data.getEndDate().getMillis());
    manager.writeString(SharedPrefManager.MEETUP_FORM_REPEAT_TYPE_KEY, data.getRepeatType().name());
    manager.writeInt(SharedPrefManager.MEETUP_FORM_REPEAT_COUNT_KEY, data.getRepeatCount());
    manager.writeString(SharedPrefManager.MEETUP_FORM_GROUP_TITLE_KEY, data.getGroupName());
    manager.writeInt(SharedPrefManager.MEETUP_FORM_GROUP_ID_KEY, data.getGroupId());
    manager.writeInt(SharedPrefManager.MEETUP_FORM_GROUP_OWNER_KEY, data.getGroupOwner());

    activity.onFinishFillForm();
  }

  @OnFocusChange(R.id.group_dropdown)
  public void onFocusChangeGroupDropDown(View v, boolean hasFocus) {
    if (groupDropDown.getEditableText().toString().trim().isEmpty()) {
      groupDropDown.setHint("Pick Your Group");
    }
  }

  @OnFocusChange(R.id.schedule_type_dropdown)
  public void onFocusChangeScheduleTypeDropDown(View v, boolean hasFocus) {
    if (scheduleTypeDropDown.getEditableText().toString().trim().isEmpty()) {
      scheduleTypeDropDown.setHint("Pick Repetition Type");
    }
  }

  @OnFocusChange(R.id.start_date)
  public void onFocusChangeStartDate(View v, boolean hasFocus) {
    if (hasFocus) {
      DatePickerDialog startDate = new DatePickerDialog(getActivity(), R.style.DialogTheme,
          dateForStart, start.getYear(),
          start.getMonthOfYear() - 1, start.getDayOfMonth());
      startDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
      startDate.show();
    }
  }

  @OnClick(R.id.start_date)
  public void onClickStartDate(View view) {
    DatePickerDialog startDate = new DatePickerDialog(getActivity(), R.style.DialogTheme,
        dateForStart, start.getYear(),
        start.getMonthOfYear() - 1, start.getDayOfMonth());
    startDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
    startDate.show();
  }

  @OnFocusChange(R.id.end_date)
  public void onFocusChangeEndDate(View v, boolean hasFocus) {
    if (hasFocus) {
      DatePickerDialog endDate = new DatePickerDialog(getActivity(), R.style.DialogTheme,
          dateForEnd, end.getYear(),
          end.getMonthOfYear() - 1, end.getDayOfMonth());
      endDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
      endDate.show();
    }
  }

  @OnClick(R.id.end_date)
  public void onClickEndDate(View view) {
    DatePickerDialog endDate = new DatePickerDialog(getActivity(), R.style.DialogTheme, dateForEnd,
        end.getYear(),
        end.getMonthOfYear() - 1, end.getDayOfMonth());
    endDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
    endDate.show();
  }

  @OnClick(R.id.continue_button)
  public void onClickContinue() {
    MeetUpFormData data = new MeetUpFormData.Builder()
        .setMeetUpName(meetupName.getText().toString().trim())
        .setGroupId(groupId)
        .setGroupName(groupDropDown.getText().toString().trim())
        .setGroupOwner(groupOwner)
        .setRepeatType(repeatType).setRepeatCount(repeatCount)
        .setStartDate(DateUtil.parseDateString(startDate.getText().toString().trim()))
        .setEndDate(DateUtil.parseDateString(endDate.getText().toString().trim())).create();

    presenter.submit(data);
  }

  private void prepareDateListener() {
    dateForStart = new DatePickerDialog.OnDateSetListener() {

      @Override
      public void onDateSet(DatePicker view, int year, int monthOfYear,
          int dayOfMonth) {
        start = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0);
        updateLabelStartDate();
      }

    };

    dateForEnd = new DatePickerDialog.OnDateSetListener() {

      @Override
      public void onDateSet(DatePicker view, int year, int monthOfYear,
          int dayOfMonth) {
        end = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0);
        updateLabelEndDate();
      }

    };

  }

  private void updateLabelStartDate() {
    startDate.setText(DateUtil.getDateString(start));
  }

  private void updateLabelEndDate() {
    endDate.setText(DateUtil.getDateString(end));
  }

  private void updateScheduleType() {
    if (selectedTypeIndex == 0) {
      scheduleTypeDropDown.setText(scheduleTypeAdapter.getItem(0).toString(), false);
    } else {
      scheduleTypeDropDown.setText(DateUtil.getRepetitionText(repeatType, repeatCount));
    }
  }

  public void onCancelRepetition() {
    updateScheduleType();
  }

  public void onConfirmRepetition(RepeatType repeatType, int repeatCount) {
    this.repeatType = repeatType;
    this.repeatCount = repeatCount;

    updateScheduleType();
  }
}
