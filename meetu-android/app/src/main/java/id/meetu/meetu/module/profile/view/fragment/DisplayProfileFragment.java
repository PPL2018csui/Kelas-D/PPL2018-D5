package id.meetu.meetu.module.profile.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.squareup.picasso.Picasso;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.profile.contract.ProfileActivityContract;
import id.meetu.meetu.module.profile.view.activity.ProfileActivity;
import id.meetu.meetu.util.BottomNavigationViewHelper;
import id.meetu.meetu.util.SharedPrefManager;

/**
 * Created by luthfi on 02/05/18.
 */

public class DisplayProfileFragment extends BaseFragment {

  @BindView(R.id.profile_image_main)
  ImageView profileImage;

  @BindView(R.id.profile_name)
  TextView profileNameText;

  @BindView(R.id.phone_number)
  TextView phoneNumberText;

  @BindView(R.id.email)
  TextView emailText;

  @BindView(R.id.profile_location)
  TextView profileLocationText;

  @BindView(R.id.group_name)
  TextView groupNameText;

  @BindView(R.id.group_list_member_button)
  ImageButton groupListButton;

  @BindView(R.id.navbar_bottom)
  BottomNavigationViewEx bottomNav;

  private ProfileActivityContract.View activity;

  public static DisplayProfileFragment newInstance(ProfileActivityContract.View profileActivity) {
    DisplayProfileFragment fragment = new DisplayProfileFragment();

    Bundle args = new Bundle();

    fragment.setArguments(args);
    fragment.activity = profileActivity;

    return fragment;
  }

  @OnClick(R.id.add_meetup_calendar_button)
  public void onClickCalendar(View v) {
    activity.showSchedule();
  }

  @OnClick(R.id.button_edit_profile)
  public void onEditProfileButtonClicked(View v) {
    activity.showEditProfileForm();
  }

  @OnClick(R.id.button_logout)
  public void onLogoutButtonClicked(View v) {
    activity.onLogout();
  }

  @OnClick(R.id.change_password)
  public void onChangePasswordButtonClicked(View v) {
    activity.showChangePassword();
  }

  @OnClick(R.id.meetup_history)
  public void onMeetUpHistoryButtonClicked(View v) {
    activity.showMeetUpHistory();
  }

  @OnClick(R.id.my_groups)
  public void onMyGroupButtonClicked(View v) {
    activity.showMyGroup();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_profile, container, false);
    ButterKnife.bind(this, v);

    groupListButton.setVisibility(View.GONE);

    SharedPrefManager manager = SharedPrefManager.getInstance(null);

    groupNameText.setText(manager.readString(SharedPrefManager.USER_FIRST_NAME, "") + "'s Group");
    String fullName = manager.readString(SharedPrefManager.USER_FIRST_NAME, "") + " " + manager
        .readString(SharedPrefManager.USER_LAST_NAME, "");
    profileNameText.setText(fullName);
    phoneNumberText
        .setText(manager.readString(SharedPrefManager.USER_PHONE_NUMBER, "No phone number"));
    emailText.setText(manager.readString(SharedPrefManager.USER_EMAIL, ""));
    profileLocationText.setText(manager.readString(SharedPrefManager.USER_LOCATION, "No location"));

    String profileImageURL = manager.readString(SharedPrefManager.USER_PROFILE_IMAGE_URL, null);
    Picasso.with(getActivity()).load(profileImageURL).placeholder(R.drawable.dummy_user)
        .error(R.drawable.dummy_user).fit().centerInside().into(profileImage);

    return v;
  }

  @Override
  public void onResume() {
    super.onResume();
    BottomNavigationViewHelper
        .setupBottomNavigationView(getActivity(), bottomNav, ProfileActivity.ACTIVITY_NUM);
    bottomNav.setTextVisibility(false);
  }
}
