package id.meetu.meetu.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ayaz97 on 18/04/18.
 */

public class SharedPrefManager {

  public static final String PREF_FILE = "meetu_prefs";

  public static final String USER_ID_KEY = "user_id";
  public static final String USER_PROFILE_ID_KEY = "user_profile_id";
  public static final String USER_NAME_KEY = "user_name";

  public static final String MEETUP_FORM_NAME_KEY = "meetup_form_name";
  public static final String MEETUP_FORM_GROUP_ID_KEY = "meetup_form_group_id";
  public static final String MEETUP_FORM_GROUP_TITLE_KEY = "meetup_form_group_title";
  public static final String MEETUP_FORM_GROUP_OWNER_KEY = "meetup_form_group_owner";
  public static final String MEETUP_FORM_START_DATE_KEY = "meetup_form_start_date";
  public static final String MEETUP_FORM_END_DATE_KEY = "meetup_form_end_date";
  public static final String MEETUP_FORM_REPEAT_TYPE_KEY = "meetup_form_repeat_type";
  public static final String MEETUP_FORM_REPEAT_COUNT_KEY = "meetup_form_repeat_count";
  public static final String MEETUP_FORM_CHOSEN_START_KEY = "meetup_form_chosen_start";
  public static final String MEETUP_FORM_START_TIME_KEY = "meetup_form_start_time";
  public static final String MEETUP_FORM_END_TIME_KEY = "meetup_form_end_time";

  public static final String USER_FIRST_NAME = "edit_profile_form_first_name";
  public static final String USER_LAST_NAME = "edit_profile_form_last_name";
  public static final String USER_PHONE_NUMBER = "edit_profile_form_phone_number";
  public static final String USER_EMAIL = "edit_profile_form_email";
  public static final String USER_LOCATION = "edit_profile_form_location";
  public static final String USER_PROFILE_IMAGE_URL = "edit_profile_form_profile_image_url";

  public static final String FIREBASE_TOKEN = "firebase_token";


  private static SharedPrefManager sharedPrefManager;
  private SharedPreferences sharedPref;

  private SharedPrefManager(Context ctx) {
    sharedPref = ctx.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
  }

  // call once in starting activity using applicationContext()
  public static SharedPrefManager getInstance(Context ctx) {
    if (sharedPrefManager == null) {
      sharedPrefManager = new SharedPrefManager(ctx.getApplicationContext());
    }
    return sharedPrefManager;
  }

  // ONLY USE THIS IN TESTING!
  public static void resetManager() {
    sharedPrefManager = null;
  }

  public void clear() {
    sharedPref.edit().clear().apply();
  }

  public String readString(String key, String defaultValue) {
    return sharedPref.getString(key, defaultValue);
  }

  public void writeString(String key, String value) {
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putString(key, value);
    editor.apply();
  }

  public boolean readBoolean(String key, boolean defaultValue) {
    return sharedPref.getBoolean(key, defaultValue);
  }

  public void writeBoolean(String key, boolean value) {
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putBoolean(key, value);
    editor.apply();
  }

  public int readInt(String key, int defaultValue) {
    return sharedPref.getInt(key, defaultValue);
  }

  public void writeInt(String key, int value) {
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putInt(key, value);
    editor.apply();
  }

  public long readLong(String key, long defaultValue) {
    return sharedPref.getLong(key, defaultValue);
  }

  public void writeLong(String key, long value) {
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putLong(key, value);
    editor.apply();
  }

  public void remove(String key) {
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.remove(key);
    editor.apply();
  }
}
