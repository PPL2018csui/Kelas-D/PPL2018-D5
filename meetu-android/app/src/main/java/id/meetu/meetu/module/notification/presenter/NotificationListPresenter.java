package id.meetu.meetu.module.notification.presenter;

import id.meetu.meetu.module.notification.contract.NotificationListContract;
import id.meetu.meetu.module.notification.model.NotificationDetail;
import id.meetu.meetu.module.notification.model.NotificationResponse;
import id.meetu.meetu.module.notification.service.NotificationService;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.DateUtil;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NotificationListPresenter implements NotificationListContract.Presenter {

  private NotificationListContract.View view;
  private NotificationService notificationService;

  public NotificationListPresenter(NotificationListContract.View view) {
    this.view = view;
    this.notificationService = new NotificationService();
  }

  public NotificationListPresenter(NotificationListContract.View view,
      NotificationService notificationService) {
    this.view = view;
    this.notificationService = notificationService;
  }

  @Override
  public void getNotifications() {
    view.onLoading();
    notificationService.getNotifications(new Callback<List<NotificationResponse>>() {
      @Override
      public void onResponse(Call<List<NotificationResponse>> call,
          Response<List<NotificationResponse>> response) {
        if (response.isSuccessful()) {
          List<NotificationDetail> notifications = new ArrayList<>();

          int idx = 1;
          for (NotificationResponse notif : response.body()) {
            NotificationDetail detail;

            if ("MeetUp".equals(notif.getNotificationType())) {
              detail = createMeetUpDetail(idx, notif);
            } else {
              detail = createGroupDetail(idx, notif);
            }

            notifications.add(detail);
            idx++;
          }

          view.onFinishLoading();
          view.onGetNotificationsSuccess(notifications);
        } else {
          view.onFinishLoading();
          view.onGetNotificationsError("Failed to get notifications");
        }
      }

      @Override
      public void onFailure(Call<List<NotificationResponse>> call, Throwable t) {
        view.onFinishLoading();
        view.onGetNotificationsError("Something is wrong with your connection");
      }
    });
  }

  public NotificationDetail createMeetUpDetail(int idx, NotificationResponse notif) {
    DateTime startTime = notif.getStartTime();
    DateTime endTime = notif.getEndTime();

    DateTimeFormatter dateDtf = DateTimeFormat.forPattern("EEEE, dd MMMM yyyy");

    String header = "You Are Invited!";
    String title = notif.getTitle();
    String inviter = notif.getInviter();
    String date = "From " + dateDtf.print(startTime);
    String time = DateUtil
        .getHourMinuteString(startTime.getHourOfDay(), startTime.getMinuteOfHour(), ".");
    time += " - ";
    time += DateUtil
        .getHourMinuteString(endTime.getHourOfDay(), endTime.getMinuteOfHour(), ".");
    String repetition = notif.getRepetition();

    return new NotificationDetail(idx, Constant.ITEM_TYPE_EVENT, header, title, inviter,
        date, time, repetition, endTime);
  }

  public NotificationDetail createGroupDetail(int idx, NotificationResponse notif) {
    String header = "You Have Been Invited!";
    String title = notif.getTitle();
    String inviter = notif.getInviter();

    return new NotificationDetail(idx, Constant.ITEM_TYPE_INVITATION, header, title,
        inviter);
  }
}
