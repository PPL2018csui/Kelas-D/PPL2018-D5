package id.meetu.meetu.module.splash.view;

import static id.meetu.meetu.util.Constant.LOGIN_REGISTER_SUCCESS_CODE;
import static id.meetu.meetu.util.Constant.LOGIN_REQUEST_CODE;
import static id.meetu.meetu.util.Constant.REGISTER_REQUEST_CODE;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.module.auth.view.LoginActivity;
import id.meetu.meetu.module.auth.view.RegisterActivity;
import id.meetu.meetu.module.home.view.activity.HomeActivity;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.SharedPrefManager;

/**
 * Created by luthfi on 15/04/18.
 */

public class SplashScreenActivity extends BaseActivity {

  @Override
  public int findLayout() {
    return R.layout.activity_splash_screen;
  }

  @Override
  public void updateFirebaseToken(String token) {
    // emptied as before login, no account is associated in backend
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ButterKnife.bind(this);

    if (SharedPrefManager.getInstance(this).readString("token", null) != null) {
      Intent intent = new Intent(this, HomeActivity.class);
      if (getIntent().getExtras() != null) {
        intent.putExtras(getIntent().getExtras());
      }
      intent.putExtra(Constant.FIRST_TIME_HOME, true);

      startActivity(intent);
      finish();
    }
  }

  @OnClick(R.id.register_button)
  public void onClickRegister(View v) {
    startActivityForResult(new Intent(SplashScreenActivity.this, RegisterActivity.class),
        LOGIN_REQUEST_CODE);
  }

  @OnClick(R.id.login_button)
  public void onClickLogin(View v) {
    startActivityForResult(new Intent(SplashScreenActivity.this, LoginActivity.class),
        REGISTER_REQUEST_CODE);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == LOGIN_REQUEST_CODE || requestCode == REGISTER_REQUEST_CODE) {
      if (resultCode == LOGIN_REGISTER_SUCCESS_CODE) {
        SplashScreenActivity.this.finish();
      }
    }
  }

}
