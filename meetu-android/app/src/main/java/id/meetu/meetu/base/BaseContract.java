package id.meetu.meetu.base;

public interface BaseContract {
  interface View {
    void onLoading();

    void onFinishLoading();
  }
}
