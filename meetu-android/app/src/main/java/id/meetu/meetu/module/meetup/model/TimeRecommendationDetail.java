package id.meetu.meetu.module.meetup.model;

import org.joda.time.DateTime;

public class TimeRecommendationDetail {

  private final int MAX_ID = Integer.MAX_VALUE;
  private int id;
  private DateTime startTime;
  private DateTime endTime;
  private int members;
  private int availableMembers;

  public TimeRecommendationDetail(int id, DateTime startTime, DateTime endTime, int members,
      int availableMembers) {
    this.id = id;
    this.startTime = startTime;
    this.endTime = endTime;
    this.members = members;
    this.availableMembers = availableMembers;
  }

  public TimeRecommendationDetail() {
    this.id = MAX_ID;
    this.startTime = new DateTime();
    this.endTime = new DateTime();
    this.members = 0;
    this.availableMembers = 0;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public DateTime getStartTime() {
    return this.startTime;
  }

  public void setStartTime(DateTime startTime) {
    this.startTime = startTime;
  }

  public DateTime getEndTime() {
    return this.endTime;
  }

  public void setEndTime(DateTime endTime) {
    this.endTime = endTime;
  }

  public int getMembers() {
    return members;
  }

  public void setMembers(int members) {
    this.members = members;
  }

  public int getAvailableMembers() {
    return availableMembers;
  }

  public void setAvailableMembers(int availableMembers) {
    this.availableMembers = availableMembers;
  }
}