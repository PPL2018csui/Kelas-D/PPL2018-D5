package id.meetu.meetu.base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.SharedPrefManager;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request.Builder;
import okhttp3.Response;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ayaz97 on 04/03/18.
 */

public abstract class BaseApi<E> {

  public abstract String findUrl();

  public abstract E create();

  protected Gson gsonBuild() {
    return new GsonBuilder()
        .setDateFormat(Constant.DATE_FORMAT)
        .registerTypeAdapter(DateTime.class, new JsonSerializer<DateTime>() {
          @Override
          public JsonElement serialize(DateTime json, Type typeOfSrc,
              JsonSerializationContext context) {
            DateTimeFormatter dtf = DateTimeFormat.forPattern(Constant.DATE_FORMAT);
            json = json.withZone(DateTimeZone.getDefault());
            json = json.withZone(DateTimeZone.UTC);

            return new JsonPrimitive(dtf.print(json));
          }
        }).registerTypeAdapter(DateTime.class, new JsonDeserializer<DateTime>() {
          @Override
          public DateTime deserialize(JsonElement jsonElement, Type typeOF,
              JsonDeserializationContext context) throws JsonParseException {
            List<String> formats = Arrays.asList(Constant.DATE_FORMAT, Constant.DATE_FORMAT2);

            for (String format : formats) {
              DateTimeFormatter dtf = DateTimeFormat.forPattern(format);

              try {
                DateTime date = dtf.parseDateTime(jsonElement.getAsString());
                LocalDateTime localDate = new LocalDateTime(date);
                DateTime utcDate = localDate.toDateTime(DateTimeZone.UTC);
                DateTime curDate = utcDate.withZone(DateTimeZone.getDefault());

                return curDate;
              } catch (Exception e) {

              }
            }

            throw new JsonParseException("Unparseable date");
          }
        })
        .create();
  }

  protected Retrofit build() {
    OkHttpClient httpClient = new OkHttpClient.Builder()
        .addInterceptor(new Interceptor() {
          @Override
          public Response intercept(Chain chain) throws IOException {
            Builder ongoing = chain.request().newBuilder();
            ongoing.addHeader("Accept", "application/json;versions=1");
            String token = SharedPrefManager.getInstance(null).readString("token", null);
            if (token != null) {
              ongoing.addHeader("Authorization", String.format("JWT %s", token));
            }
            return chain.proceed(ongoing.build());
          }
        })
        .build();

    return new Retrofit.Builder()
        .client(httpClient)
        .baseUrl(findUrl())
        .addConverterFactory(GsonConverterFactory.create(gsonBuild()))
        .build();
  }
}
