package id.meetu.meetu.module.home.presenter;

import id.meetu.meetu.base.BaseContract;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.meetup.model.MeetUpDetail;
import java.util.List;

/**
 * Created by ayaz97 on 27/03/18.
 */

public interface HomePresenterListener extends BaseContract.View {

  void onGetMeetUpSuccess(List<MeetUpDetail> meetUps);

  void onError(String message);

  void onGetProfileSuccess(User user);
}
