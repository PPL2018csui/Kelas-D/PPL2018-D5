package id.meetu.meetu.module.profile.model;


/**
 * Created by Andre on 5/3/2018.
 */


public class EditProfileFormData {

  private String firstName;
  private String lastName;
  private String phoneNumber;
  private String email;
  private String location;
  private String profileImageURL;


  // testing purpose
  public EditProfileFormData() {
  }

  private EditProfileFormData(String firstName, String lastName, String phoneNumber, String email,
      String location, String profileImageURL) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.location = location;
    this.profileImageURL = profileImageURL;
  }

  public String getProfileImageURL() {
    return profileImageURL;
  }

  public void setProfileImageURL(String profileImageURL) {
    this.profileImageURL = profileImageURL;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }


  public static class Builder {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String location;
    private String profileImageURL;

    public Builder() {
    }

    public id.meetu.meetu.module.profile.model.EditProfileFormData.Builder setFirstName(
        String firstName) {
      this.firstName = firstName;
      return this;
    }

    public id.meetu.meetu.module.profile.model.EditProfileFormData.Builder setLastName(
        String lastName) {
      this.lastName = lastName;
      return this;
    }

    public id.meetu.meetu.module.profile.model.EditProfileFormData.Builder setPhoneNumber(
        String phoneNumber) {
      this.phoneNumber = phoneNumber;
      return this;
    }

    public id.meetu.meetu.module.profile.model.EditProfileFormData.Builder setEmail(
        String email) {
      this.email = email;
      return this;
    }

    public id.meetu.meetu.module.profile.model.EditProfileFormData.Builder setLocation(
        String location) {
      this.location = location;
      return this;
    }

    public id.meetu.meetu.module.profile.model.EditProfileFormData.Builder setProfileImageURL(
        String profileImageURL
    ) {
      this.profileImageURL = profileImageURL;
      return this;
    }


    public id.meetu.meetu.module.profile.model.EditProfileFormData create() {
      return new id.meetu.meetu.module.profile.model.EditProfileFormData(firstName, lastName,
          phoneNumber, email,
          location, profileImageURL);
    }
  }
}

