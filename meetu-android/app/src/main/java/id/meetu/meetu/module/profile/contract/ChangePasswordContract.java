package id.meetu.meetu.module.profile.contract;

import id.meetu.meetu.base.BaseContract;

public interface ChangePasswordContract {

  interface View extends BaseContract.View {

    void disableAllError();

    void setNewPasswordError(String error);

    void setVerifyPasswordError(String error);

    void onSubmitSuccess(String message);

    void onSubmitFail(String error);
  }

  interface Presenter {

    void submit(String password, String confirmPassword);
  }
}
