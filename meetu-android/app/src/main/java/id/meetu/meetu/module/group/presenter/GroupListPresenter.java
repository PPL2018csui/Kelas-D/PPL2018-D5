package id.meetu.meetu.module.group.presenter;

import id.meetu.meetu.module.group.contract.GroupListContract;
import java.util.ArrayList;
import java.util.List;

import id.meetu.meetu.module.group.model.GroupDetail;
import id.meetu.meetu.module.group.service.GroupService;
import id.meetu.meetu.module.group.model.GroupResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andre on 4/11/2018.
 */

public class GroupListPresenter implements GroupListContract.Presenter {

  private GroupListContract.View view;
  private GroupService groupService;

  public GroupListPresenter(GroupListContract.View view) {
    this.view = view;
    this.groupService = new GroupService();
  }

  public GroupListPresenter(GroupListContract.View view, GroupService groupService) {
    this.view = view;
    this.groupService = groupService;
  }

  public void getGroups() {
    view.onLoading();
    groupService.getGroups(new Callback<List<GroupResponse>>() {
      @Override
      public void onResponse(Call<List<GroupResponse>> call,
          Response<List<GroupResponse>> response) {

        if (response.isSuccessful()) {
          List<GroupDetail> groups = new ArrayList<>();

          for (GroupResponse group : response.body()) {
            int id = group.getId();
            String title = group.getTitle();
            int owner = group.getOwner();

            GroupDetail detail = new GroupDetail(id, title, owner);

            groups.add(detail);

          }
          view.onFinishLoading();
          view.onGetSuccess(groups);
        } else {
          view.onFinishLoading();
          view.onError("Something is wrong");
        }
      }

      @Override
      public void onFailure(Call<List<GroupResponse>> call, Throwable t) {
        view.onFinishLoading();
        view.onError("Something is wrong with your connection");
      }
    });
  }

}