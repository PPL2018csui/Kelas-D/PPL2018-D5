package id.meetu.meetu.module.auth.contract;

import id.meetu.meetu.module.auth.model.User;

public interface AuthContract {

  interface View {

    void onFinishGoogleSignIn(String token, User user);

    void onErrorGoogleSignIn(String error);
  }

  interface Presenter {

    void googleSignIn(String idToken, String userName, String firstName, String lastName);
  }
}
