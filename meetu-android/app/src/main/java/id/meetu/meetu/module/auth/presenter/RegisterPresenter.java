package id.meetu.meetu.module.auth.presenter;

import id.meetu.meetu.module.auth.contract.RegisterContract;
import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.RegisterRequest;
import id.meetu.meetu.module.auth.service.AuthService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by angelica on 19/04/18.
 */

public class RegisterPresenter implements RegisterContract.Presenter {

  private RegisterContract.View view;
  private AuthService authService;

  public RegisterPresenter(RegisterContract.View view) {
    this.view = view;
    this.authService = new AuthService();
  }

  public RegisterPresenter(RegisterContract.View view, AuthService authService) {
    this.view = view;
    this.authService = authService;
  }

  @Override
  public void cancel() {
    view.onCancel();
  }

  @Override
  public void submit(String email, String firstName, String lastName, String password,
      String verifyPassword) {
    view.disableAllError();

    boolean isValid = true;
    isValid &= isValidEmail(email);
    isValid &= isValidFirstName(firstName);
    isValid &= isValidLastName(lastName);
    isValid &= isValidPassword(password);
    isValid &= isValidVerifyPassword(verifyPassword, password);

    if (isValid) {
      view.onLoading();
      authService.postRegister(new Callback<AuthResponse>() {
        @Override
        public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
          if (response.isSuccessful()) {
            AuthResponse auth = response.body();
            view.onFinishLoading();
            view.onRegisterSuccess(auth.getToken(), auth.getUser());
          } else {
            view.onFinishLoading();
            view.onRegisterFail();
          }
        }

        @Override
        public void onFailure(Call<AuthResponse> call, Throwable t) {
          view.onFinishLoading();
          view.onRegisterError();
        }
      }, new RegisterRequest(email, firstName, lastName, password));
    }

  }

  private boolean isValidEmail(String email) {
    if ("".equals(email)) {
      view.setEmailError("This field can't be empty");
      return false;
    }
    String regxEmail = "^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
    if (!email.matches(regxEmail)) {
      view.setEmailError("Email format is invalid");
      return false;
    }
    return true;
  }

  private boolean isValidFirstName(String firstName) {
    if ("".equals(firstName)) {
      view.setFirstNameError("This field can't be empty");
      return false;
    }
    return true;
  }

  public boolean isValidLastName(String lastName) {
    if ("".equals(lastName)) {
      view.setLastNameError("This field can't be empty");
      return false;
    }
    return true;
  }

  private boolean isValidPassword(String password) {
    if ("".equals(password)) {
      view.setPasswordError("This field can't be empty");
      return false;
    }
    return true;
  }

  private boolean isValidVerifyPassword(String verifyPassword, String password) {
    if ("".equals(verifyPassword)) {
      view.setVerifyPasswordError("This field can't be empty");
      return false;
    }
    if (!"".equals(password) && !verifyPassword.equals(password)) {
      view.setVerifyPasswordError("Password doesn't match with this field");
      return false;
    }
    return true;
  }
}