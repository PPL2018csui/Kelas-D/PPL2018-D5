package id.meetu.meetu.module.meetup.presenter;

import id.meetu.meetu.module.group.model.GroupDetail;
import id.meetu.meetu.module.group.model.GroupResponse;
import id.meetu.meetu.module.group.service.GroupService;
import id.meetu.meetu.module.meetup.contract.AddMeetUpFormContract;
import id.meetu.meetu.module.meetup.model.MeetUpFormData;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 17/04/18.
 */

public class AddMeetupFormPresenter implements AddMeetUpFormContract.Presenter {

  private AddMeetUpFormContract.View view;
  private GroupService service;

  public AddMeetupFormPresenter(AddMeetUpFormContract.View view) {
    this.view = view;
    this.service = new GroupService();
  }

  public AddMeetupFormPresenter(AddMeetUpFormContract.View view, GroupService service) {
    this.view = view;
    this.service = service;
  }

  private boolean isValidName(String name) {
    if ("".equals(name)) {
      view.setMeetupNameError("This field can't be empty");
      return false;
    }

    return true;
  }

  private boolean isValidGroup(String group) {
    if ("".equals(group)) {
      view.setGroupError("This field can't be empty");
      return false;
    }

    return true;
  }

  private boolean isValidStartDate(DateTime startDate) {
    if (startDate == null) {
      view.setStartDateError("This field can't be empty");
      return false;
    }

    return true;
  }

  private boolean isValidEndDate(DateTime startDate, DateTime endDate) {
    if (endDate == null) {
      view.setEndDateError("This field can't be empty");
      return false;
    } else if (startDate != null && startDate.compareTo(endDate) > 0) {
      view.setEndDateError("End date can't be lower than start date");
      return false;
    }

    return true;
  }

  public void submit(MeetUpFormData data) {
    view.resetAllError();

    boolean isValid = true;

    isValid &= isValidName(data.getMeetUpName());
    isValid &= isValidGroup(data.getGroupName());
    isValid &= isValidStartDate(data.getStartDate());
    isValid &= isValidEndDate(data.getStartDate(), data.getEndDate());

    if (isValid) {
      view.onFinishSubmit(data);
    }
  }

  public void fetchGroups() {
    service.getGroups(new Callback<List<GroupResponse>>() {
      @Override
      public void onResponse(Call<List<GroupResponse>> call,
          Response<List<GroupResponse>> response) {
        if (response.isSuccessful()) {
          List<GroupDetail> groupDetails = new ArrayList<>();

          for (GroupResponse group : response.body()) {
            groupDetails.add(new GroupDetail(group.getId(), group.getTitle(), group.getOwner()));
          }

          view.populateGroup(groupDetails);
        } else {
          view.onFetchGroupError("Something is wrong");
        }
      }

      @Override
      public void onFailure(Call<List<GroupResponse>> call, Throwable t) {
        view.onFetchGroupError("Failed to retrieve your groups");
      }
    });
  }
}
