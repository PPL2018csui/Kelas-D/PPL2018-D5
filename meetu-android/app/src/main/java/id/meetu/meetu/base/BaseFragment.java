package id.meetu.meetu.base;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.WindowManager;
import id.meetu.meetu.util.ProgressBarView;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class BaseFragment extends Fragment implements BaseContract.View {

  protected ProgressBarView progressBarView;
  private Lock lock;
  private int barCounter;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    progressBarView = new ProgressBarView(getActivity());
    lock = new ReentrantLock();
    barCounter = 0;
  }

  @Override
  public void onLoading() {
    try {
      lock.lock();
      barCounter++;

      if (barCounter == 1) {
        progressBarView.show();
      }

      if (getActivity() != null) {
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
      }
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void onFinishLoading() {
    try {
      lock.lock();
      barCounter--;

      if (barCounter == 0) {
        if (getActivity() != null) {
          getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
          getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
              Handler handler = new Handler();
              handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                  progressBarView.hide();
                }
              }, 500);
            }
          });
        } else {
          // to hide in case we are popping from backstack, and activity is null
          progressBarView.hide();
        }
      }
    } finally {
      lock.unlock();
    }
  }
}
