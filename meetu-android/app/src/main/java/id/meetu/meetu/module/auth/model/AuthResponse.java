package id.meetu.meetu.module.auth.model;

import com.google.gson.annotations.SerializedName;
import id.meetu.meetu.base.BaseResponse;

/**
 * Created by ayaz97 on 16/04/18.
 */

public class AuthResponse extends BaseResponse {

  @SerializedName("token")
  private String token;

  @SerializedName("user")
  private User user;

  public AuthResponse(String token, User user) {
    this.token = token;
    this.user = user;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
