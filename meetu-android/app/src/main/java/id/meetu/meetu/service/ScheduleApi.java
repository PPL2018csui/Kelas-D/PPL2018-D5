package id.meetu.meetu.service;
import id.meetu.meetu.base.BaseApi;
import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.module.schedule.model.ScheduleResponse;
import java.util.List;
import java.util.Map;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;

/**
 * Created by ayaz97 on 12/03/18.
 */

public class ScheduleApi extends BaseApi<ScheduleApi.Service> {

  @Override
  public String findUrl() {
    return BuildConfig.BACKEND_URL;
  }

  @Override
  public Service create() {
    return build().create(Service.class);
  }

  public interface Service {

    @GET("api/schedules/")
    Call<List<ScheduleResponse>> getSchedules();

    @POST("api/schedules/")
    Call<BaseResponse> postSchedules(@Body List<ScheduleResponse> listScheduleResponse);

    @HTTP(method = "DELETE", path = "/api/schedules/", hasBody = true)
    Call<BaseResponse> deleteSchedules(@Body Map<String, String> request);
  }
}
