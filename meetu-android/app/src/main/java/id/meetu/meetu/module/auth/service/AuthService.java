package id.meetu.meetu.module.auth.service;

import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.GoogleSignInRequest;
import id.meetu.meetu.module.auth.model.LoginRequest;
import id.meetu.meetu.module.auth.model.RegisterRequest;
import id.meetu.meetu.module.auth.model.UpdateTokenRequest;
import id.meetu.meetu.service.AuthApi;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by ayaz97 on 16/04/18.
 */

public class AuthService {

  private AuthApi.Service service;

  public AuthService() {
    this.service = new AuthApi().create();
  }

  public AuthService(AuthApi.Service service) {
    this.service = service;
  }

  public AuthApi.Service getService() {
    return service;
  }

  public void postLogin(Callback<AuthResponse> callback, LoginRequest request) {
    Call<AuthResponse> call = service.postLogin(request);
    call.enqueue(callback);
  }

  public void postRegister(Callback<AuthResponse> callback, RegisterRequest request) {
    Call<AuthResponse> call = service.postRegister(request);
    call.enqueue(callback);
  }

  public void postGoogleSignIn(Callback<AuthResponse> callback, GoogleSignInRequest request) {
    Call<AuthResponse> call = service.postGoogleSignIn(request);
    call.enqueue(callback);
  }

  public void updateFirebaseToken(Callback<Void> callback, UpdateTokenRequest request) {
    Call<Void> call = service.updateFirebaseToken(request);
    call.enqueue(callback);
  }
}
