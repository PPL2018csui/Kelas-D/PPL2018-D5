package id.meetu.meetu.module.notification.view.fragment;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;

/**
 * Created by ayaz97 on 10/04/18.
 */

public class GroupInvitationDialogFragment extends DialogFragment {

  @BindView(R.id.notification_group_name)
  TextView notificationGroupName;

  @BindView(R.id.notification_group_invitor)
  TextView notificationGroupInvitor;

  public static GroupInvitationDialogFragment newInstance(String group, String invitor) {
    GroupInvitationDialogFragment fragment = new GroupInvitationDialogFragment();

    Bundle args = new Bundle();
    args.putString("group", group);
    args.putString("invitor", invitor);

    fragment.setArguments(args);

    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_group_invitation, container, false);
    ButterKnife.bind(this, v);

    notificationGroupName.setText(getArguments().getString("group"));
    notificationGroupInvitor.setText(getArguments().getString("invitor"));

    getDialog().setCanceledOnTouchOutside(false);
    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    return v;
  }

  @OnClick(R.id.ok_button)
  public void onOkButtonClick() {
    getDialog().dismiss();
  }
}
