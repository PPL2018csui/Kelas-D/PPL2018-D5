package id.meetu.meetu.module.meetup.presenter;

import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.group.model.GroupResponse;
import id.meetu.meetu.module.meetup.contract.AddMeetUpConfirmationContract;
import id.meetu.meetu.module.meetup.model.MeetUpFormData;
import id.meetu.meetu.module.meetup.model.MeetUpResponse;
import id.meetu.meetu.module.meetup.service.MeetUpService;
import id.meetu.meetu.util.DateInterval;
import id.meetu.meetu.util.DateUtil;
import id.meetu.meetu.util.DateUtil.RepeatType;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 19/04/18.
 */

public class AddMeetupConfirmationPresenter implements AddMeetUpConfirmationContract.Presenter {

  private AddMeetUpConfirmationContract.View view;
  private MeetUpService service;

  public AddMeetupConfirmationPresenter(AddMeetUpConfirmationContract.View view) {
    this.view = view;
    this.service = new MeetUpService();
  }

  public AddMeetupConfirmationPresenter(AddMeetUpConfirmationContract.View view,
      MeetUpService service) {
    this.view = view;
    this.service = service;
  }

  private boolean isValidMeetUpName(String name) {
    if ("".equals(name)) {
      view.setMeetUpNameError("This field can't be empty");
      return false;
    }

    return true;
  }

  @Override
  public void submit(MeetUpFormData data) {
    view.resetAllError();

    boolean isValid = true;
    isValid &= isValidMeetUpName(data.getMeetUpName());

    if (isValid) {
      view.onLoading();
      final String title = data.getMeetUpName();

      int groupId = data.getGroupId();
      int groupOwner = data.getGroupOwner();
      String groupTitle = data.getGroupName();

      GroupResponse groupResp = new GroupResponse(groupId, groupOwner, groupTitle);

      DateTime date = data.getStartDate();
      DateTime startTime = data.getStartTime();
      DateTime endTime = data.getEndTime();

      DateTime startDate = new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
          startTime.getHourOfDay(), startTime.getMinuteOfHour());
      DateTime endDate = new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
          endTime.getHourOfDay(), endTime.getMinuteOfHour());
      RepeatType repeatType = data.getRepeatType();
      int repeatCount = data.getRepeatCount();

      List<DateInterval> intervals = DateUtil
          .getEventRepetition(startDate, endDate, repeatType, repeatCount);

      List<MeetUpResponse> request = new ArrayList<>();
      for (DateInterval interval : intervals) {
        request.add(new MeetUpResponse(title, groupResp, interval.getStart(), interval.getEnd()));
      }

      service.postMeetUps(request, new Callback<BaseResponse>() {
        @Override
        public void onResponse(Call<BaseResponse> call,
            Response<BaseResponse> response) {
          if (response.isSuccessful()) {
            view.onFinishLoading();
            view.onFinishPostMeetUp("Successfully created " + title);
          } else {
            view.onFinishLoading();
            view.onErrorPostMeetUp("Failed to create MeetUp");
          }
        }

        @Override
        public void onFailure(Call<BaseResponse> call, Throwable t) {
          view.onFinishLoading();
          view.onErrorPostMeetUp("Something is wrong with your connection");
        }
      });
    }
  }
}
