package id.meetu.meetu.module.group.view.activity;

import android.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.module.group.contract.GroupActivityContract;
import id.meetu.meetu.module.group.model.GroupMembers;
import id.meetu.meetu.module.group.view.fragment.AddPeopleFragment;
import id.meetu.meetu.module.group.view.fragment.CreateGroupFragment;
import id.meetu.meetu.module.group.view.fragment.GroupListFragment;
import id.meetu.meetu.module.group.view.fragment.GroupMemberFragment;

public class GroupActivity extends BaseActivity implements GroupActivityContract.View {

  @BindView(R.id.group_action_bar_title)
  public TextView actionBarTextView;

  @Override
  public void onBackPressed() {
    super.onBackPressed();

    // Since group list is pushed to backstack, we need to do this to return to previous activity
    if (getFragmentManager().getBackStackEntryCount() == 0) {
      super.onBackPressed();
    }
  }

  @OnClick(R.id.group_detail_back_button)
  public void onClickGroupDetailBackButton(View v) {
    onBackPressed();
  }

  @Override
  public int findLayout() {
    return R.layout.activity_group;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_group);
    ButterKnife.bind(this);

    showGroups();
  }

  private void showGroups() {
    GroupListFragment fragment = GroupListFragment.newInstance(this);
    getFragmentManager().beginTransaction()
        .replace(R.id.group, fragment).addToBackStack("groupList").commit();
  }

  private void showCreateGroup(GroupMembers members) {
    CreateGroupFragment fragment = CreateGroupFragment.newInstance(members, this);
    getFragmentManager().beginTransaction()
        .replace(R.id.group, fragment).addToBackStack(null).commit();
  }

  private void showGroupMembers(int groupId) {
    GroupMemberFragment fragment = GroupMemberFragment.newInstance(this, groupId);

    getFragmentManager().beginTransaction().replace(R.id.group, fragment)
        .addToBackStack(null).commit();
  }

  private void showAddMember(GroupMembers members, int mode) {
    AddPeopleFragment fragment = AddPeopleFragment.newInstance(this, members, mode);

    getFragmentManager().beginTransaction().replace(R.id.group, fragment).addToBackStack(null)
        .commit();
  }

  @Override
  public void onShowGroupMembers(int groupId) {
    showGroupMembers(groupId);
  }

  @Override
  public void onShowCreateGroup(GroupMembers members) {
    showCreateGroup(members);
  }

  @Override
  public void onAddPeopleCreateGroup(GroupMembers members) {
    getFragmentManager().popBackStack();

    showCreateGroup(members);
  }

  @Override
  public void onAddPeopleMemberDetail(int groupId) {
    getFragmentManager().popBackStack();
    getFragmentManager().popBackStack();

    showGroupMembers(groupId);
  }

  @Override
  public void onSaveGroup() {
    FragmentManager fManager = getFragmentManager();
    int backIdx = fManager.getBackStackEntryCount() - 1;

    while (!("groupList".equals(fManager.getBackStackEntryAt(backIdx).getName()))) {
      fManager.popBackStack();
      backIdx--;
    }
  }

  @Override
  public void onShowAddMember(GroupMembers currentMembers, int mode) {
    showAddMember(currentMembers, mode);
  }

  @Override
  public void changeActionText(String text) {
    actionBarTextView.setText(text);
  }
}
