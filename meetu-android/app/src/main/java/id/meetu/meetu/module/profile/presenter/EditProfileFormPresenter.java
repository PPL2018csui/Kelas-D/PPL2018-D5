package id.meetu.meetu.module.profile.presenter;

import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.profile.contract.EditProfileFormContract;
import id.meetu.meetu.module.profile.model.EditProfileFormData;
import id.meetu.meetu.module.profile.model.EditProfileRequest;
import id.meetu.meetu.module.profile.service.ProfileService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andre on 5/4/2018.
 */


public class EditProfileFormPresenter implements EditProfileFormContract.Presenter {

  private EditProfileFormContract.View view;
  private ProfileService profileService;

  public EditProfileFormPresenter(EditProfileFormContract.View view) {
    this.view = view;
    this.profileService = new ProfileService();
  }

  public EditProfileFormPresenter(EditProfileFormContract.View view,
      ProfileService profileService) {
    this.view = view;
    this.profileService = profileService;
  }

  private boolean isValidFirstName(String firstName) {
    if ("".equals(firstName)) {
      view.setFirstNameError("This field can't be empty");
      return false;
    }

    return true;
  }

  private boolean isValidLastName(String lastName) {
    if ("".equals(lastName)) {
      view.setLastNameError("This field can't be empty");
      return false;
    }

    return true;
  }

  private boolean isValidPhoneNumber(String phoneNumber) {
    if ("".equals(phoneNumber)) {
      view.setPhoneNumberError("This field can't be empty");
      return false;
    }
    String regxPhone = "^(^\\+62\\s?|^0)(\\d{3,4}){2}\\d{3,4}$";
    if (!phoneNumber.matches(regxPhone)) {
      view.setPhoneNumberError("Phone format is invalid");
      return false;
    }
    return true;
  }

  private boolean isValidLocation(String location) {
    if ("".equals(location)) {
      view.setLocationError("This field can't be empty");
      return false;
    }

    return true;
  }

  public void submit(final EditProfileFormData data) {
    view.resetAllError();

    boolean isValid = true;

    isValid &= isValidFirstName(data.getFirstName());
    isValid &= isValidLastName(data.getLastName());
    isValid &= isValidPhoneNumber(data.getPhoneNumber());
    isValid &= isValidLocation(data.getLocation());

    if (isValid) {
      EditProfileRequest request = new EditProfileRequest(data.getFirstName(), data.getLastName(),
          data.getPhoneNumber(), data.getLocation(), data.getProfileImageURL());
      view.onLoading();
      profileService.editProfile(new Callback<AuthResponse>() {
        @Override
        public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
          if (response.isSuccessful()) {
            view.onFinishLoading();
            view.onSubmitSuccess(data, "Successfully edited profile");
          } else {
            view.onFinishLoading();
            view.onSubmitFail("Failed to edit profile");
          }
        }

        @Override
        public void onFailure(Call<AuthResponse> call, Throwable t) {
          view.onFinishLoading();
          view.onSubmitFail("Something is wrong with your connection");
        }
      }, request);
    }
  }


}
