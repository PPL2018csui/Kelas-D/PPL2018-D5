package id.meetu.meetu.module.notification.service;

import id.meetu.meetu.module.notification.model.NotificationResponse;
import id.meetu.meetu.service.NotificationApi;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;

public class NotificationService {

  private NotificationApi.Service service;

  public NotificationService() {
    this.service = new NotificationApi().create();
  }

  public NotificationService(NotificationApi.Service service) {
    this.service = service;
  }

  public void getNotifications(Callback<List<NotificationResponse>> callback) {
    Call<List<NotificationResponse>> call = service.getNotifications();
    call.enqueue(callback);
  }
}
