package id.meetu.meetu.module.group.contract;

import id.meetu.meetu.base.BaseContract;
import id.meetu.meetu.module.group.model.GroupMembers;

public interface CreateGroupContract {

  interface View extends BaseContract.View {

    void resetAllError();

    void setGroupNameError(String error);

    void onCreateSuccess(String message);

    void onCreateError(String message);
  }

  interface Presenter {

    void submit(GroupMembers groupMembers);
  }
}
