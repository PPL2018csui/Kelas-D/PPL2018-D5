package id.meetu.meetu.module.group.model;

import com.google.gson.annotations.SerializedName;
import id.meetu.meetu.module.auth.model.User;

public class Member {

  @SerializedName("id")
  int userId;

  @SerializedName("phone_number")
  String phoneNumber;

  @SerializedName("location")
  String location;

  @SerializedName("user")
  User memberProfile;

  @SerializedName("profile_image_url")
  String profileImageUrl;

  public Member(int userId, String phoneNumber, String location,
      User memberProfile, String profileImageUrl) {
    this.userId = userId;
    this.phoneNumber = phoneNumber;
    this.location = location;
    this.memberProfile = memberProfile;
    this.profileImageUrl = profileImageUrl;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public User getMemberProfile() {
    return memberProfile;
  }

  public void setMemberProfile(User memberProfile) {
    this.memberProfile = memberProfile;
  }

  public String getProfileImageUrl() {
    return profileImageUrl;
  }

  public void setProfileImageUrl(String profileImageUrl) {
    this.profileImageUrl = profileImageUrl;
  }

}
