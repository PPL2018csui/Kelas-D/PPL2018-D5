package id.meetu.meetu.module.auth.contract;

import id.meetu.meetu.base.BaseContract;

import id.meetu.meetu.module.auth.model.User;

/**
 * Created by ayaz97 on 17/04/18.
 */

public interface LoginContract {

  interface View extends BaseContract.View {

    void disableAllError();

    void setUsernameError(String error);

    void setPasswordError(String error);

    void onCancel();

    void onLoginSuccess(String token, User user);

    void onLoginFail();

    void onLoginError();
  }

  interface Presenter {

    void cancel();

    void submit(String username, String password);
  }
}
