package id.meetu.meetu.module.profile.view.fragment;

import static android.support.v4.content.ContextCompat.checkSelfPermission;
import static id.meetu.meetu.util.Constant.PICK_PHOTO_FOR_AVATAR;

import android.Manifest.permission;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.profile.contract.EditProfileFormContract;
import id.meetu.meetu.module.profile.contract.ProfileActivityContract;
import id.meetu.meetu.module.profile.contract.UploadImageInterfaceCallback;
import id.meetu.meetu.module.profile.model.EditProfileFormData;
import id.meetu.meetu.module.profile.presenter.EditProfileFormPresenter;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.SharedPrefManager;
import id.meetu.meetu.util.UploadImageTask;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Created by Andre on 5/4/2018.
 */


public class EditProfileFormFragment extends BaseFragment implements EditProfileFormContract.View,
    UploadImageInterfaceCallback {

  static String imageURL;
  @BindView(R.id.first_name_text)
  TextInputLayout firstNameText;
  @BindView(R.id.first_name)
  EditText firstName;
  @BindView(R.id.last_name_text)
  TextInputLayout lastNameText;
  @BindView(R.id.last_name)
  EditText lastName;
  @BindView(R.id.phone_number_text)
  TextInputLayout phoneNumberText;
  @BindView(R.id.phone_number)
  EditText phoneNumber;
  @BindView(R.id.email_text)
  TextInputLayout emailText;
  @BindView(R.id.email)
  EditText email;
  @BindView(R.id.location_text)
  TextInputLayout locationText;
  @BindView(R.id.location)
  EditText location;
  @BindView(R.id.profile_image)
  CircleImageView profileImage;
  EditProfileFormContract.Presenter presenter;
  ProfileActivityContract.View activity;

  public static EditProfileFormFragment newInstance(
      ProfileActivityContract.View activity) {
    EditProfileFormFragment fragment = new EditProfileFormFragment();
    Bundle args = new Bundle();
    SharedPrefManager manager = SharedPrefManager.getInstance(null);

    args.putString("first_name", manager.readString(SharedPrefManager.USER_FIRST_NAME, ""));
    args.putString("last_name", manager.readString(SharedPrefManager.USER_LAST_NAME, ""));
    args.putString("phone_number",
        manager.readString(SharedPrefManager.USER_PHONE_NUMBER, ""));
    args.putString("email", manager.readString(SharedPrefManager.USER_EMAIL, ""));
    args.putString("location",
        manager.readString(SharedPrefManager.USER_LOCATION, ""));

    imageURL = manager.readString(SharedPrefManager.USER_PROFILE_IMAGE_URL, null);
    args.putString("profile_image_url", imageURL);

    fragment.setArguments(args);
    fragment.activity = activity;
    fragment.presenter = new EditProfileFormPresenter(fragment);
    return fragment;
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
      @NonNull int[] grantResults) {
    if (requestCode == Constant.PERMISSION_READ_EXTERNAL) {
      if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        startPhotoPick();
      } else {
        Toast.makeText(getActivity(), "Permission for picking photo was denied", Toast.LENGTH_LONG)
            .show();
      }
    }
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
      if (data == null) {
        //Display an error
        return;
      }
      try {
        // load image
        InputStream inputStream = getActivity().getContentResolver()
            .openInputStream(data.getData());
        // compress
        Bitmap bmp = BitmapFactory.decodeStream(inputStream);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, bos);
        InputStream compressedImage = new ByteArrayInputStream(bos.toByteArray());

        // upload
        new UploadImageTask(this).execute(compressedImage);
      } catch (Exception e) {
        if (!getActivity().isFinishing()) {
          Toast.makeText(getActivity(), "Unexpected thing happened", Toast.LENGTH_SHORT).show();
        }
      }
    }
  }

  @OnClick(R.id.pick_image)
  public void onClickEditProfileImage(View v) {
    // API >= 23
    if (VERSION.SDK_INT >= 23) {
      if (checkSelfPermission(getActivity(), permission.READ_EXTERNAL_STORAGE)
          == PackageManager.PERMISSION_GRANTED) {
        startPhotoPick();
      } else {
        // Permission is missing and must be requested.
        requestPhotoPermission();
      }
    } else {
      startPhotoPick();
    }
  }

  // on marshmallow or higher device
  @TargetApi(Build.VERSION_CODES.M)
  public void requestPhotoPermission() {
    requestPermissions(
        new String[]{permission.READ_EXTERNAL_STORAGE}, Constant.PERMISSION_READ_EXTERNAL);
  }

  public void startPhotoPick() {
    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
    intent.setType("image/*");
    startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
  }

  @OnClick(R.id.edit_profile_back_button)
  public void onClickBackButton(View v) {
    activity.onBackPressed();
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_form_edit_profile, container, false);
    ButterKnife.bind(this, v);
    progressBarView.setWithOverlay(true);

    // prepare input from previous form
    firstName.setText(getArguments().getString("first_name"));
    lastName.setText(getArguments().getString("last_name"));
    phoneNumber.setText(getArguments().getString("phone_number"));
    email.setText(getArguments().getString("email"));
    location.setText(getArguments().getString("location"));
    Picasso.with(getActivity())
        .load(getArguments().getString("profile_image_url")).fit()
        .centerInside().error(R.drawable.dummy_user).placeholder(R.drawable.dummy_user)
        .into(profileImage);

    return v;
  }

  @Override
  public void resetAllError() {
    firstNameText.setErrorEnabled(false);
    lastNameText.setErrorEnabled(false);
    phoneNumberText.setErrorEnabled(false);
    emailText.setErrorEnabled(false);
    locationText.setErrorEnabled(false);
  }

  @Override
  public void setFirstNameError(String error) {
    firstNameText.setError(error);
  }

  @Override
  public void setLastNameError(String error) {
    lastNameText.setError(error);
  }

  @Override
  public void setPhoneNumberError(String error) {
    phoneNumberText.setError(error);
  }

  @Override
  public void setLocationError(String error) {
    locationText.setError(error);
  }

  @Override
  public void onSubmitSuccess(EditProfileFormData data, String message) {
    SharedPrefManager manager = SharedPrefManager.getInstance(null);
    manager.writeString(SharedPrefManager.USER_FIRST_NAME, data.getFirstName());
    manager.writeString(SharedPrefManager.USER_LAST_NAME, data.getLastName());
    manager.writeString(SharedPrefManager.USER_PHONE_NUMBER, data.getPhoneNumber());
    manager.writeString(SharedPrefManager.USER_EMAIL, data.getEmail());
    manager.writeString(SharedPrefManager.USER_LOCATION, data.getLocation());
    manager.writeString(SharedPrefManager.USER_PROFILE_IMAGE_URL, data.getProfileImageURL());

    if (!getActivity().isFinishing()) {
      Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    activity.showProfile();
  }

  @Override
  public void onSubmitFail(String message) {
    if (!getActivity().isFinishing()) {
      Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
  }

  @OnClick(R.id.update_button)
  public void onClickUpdate() {
    EditProfileFormData data = new EditProfileFormData.Builder()
        .setFirstName(firstName.getText().toString().trim())
        .setLastName(lastName.getText().toString().trim())
        .setPhoneNumber(phoneNumber.getText().toString().trim())
        .setEmail(email.getText().toString().trim())
        .setLocation(location.getText().toString().trim())
        .setProfileImageURL(imageURL)
        .create();

    presenter.submit(data);
  }

  @Override
  public void onUploadCallback() {
    this.onLoading();
  }

  @Override
  public void onFinishUploadCallback(String imageURL) {
    this.onFinishLoading();
    this.imageURL = imageURL;
    Picasso.with(getActivity()).load(imageURL).fit().centerInside().into(profileImage);
  }
}
