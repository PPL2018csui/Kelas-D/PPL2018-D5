package id.meetu.meetu.module.group.model;

import com.google.gson.annotations.SerializedName;

public class AddMemberRequest {

  @SerializedName("user_profile")
  private int userId;

  @SerializedName("group")
  private int groupId;

  public AddMemberRequest(int userId, int groupId) {
    this.userId = userId;
    this.groupId = groupId;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public int getGroupId() {
    return groupId;
  }

  public void setGroupId(int groupId) {
    this.groupId = groupId;
  }
}
