package id.meetu.meetu.module.notification.model;

import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

public class NotificationResponse {

  @SerializedName("title")
  private String title;

  @SerializedName("notification_type")
  private String notificationType;

  @SerializedName("inviter")
  private String inviter;

  @SerializedName("repetition")
  private String repetition;

  @SerializedName("start_time")
  private DateTime startTime;

  @SerializedName("end_time")
  private DateTime endTime;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getNotificationType() {
    return notificationType;
  }

  public void setNotificationType(String notificationType) {
    this.notificationType = notificationType;
  }

  public String getInviter() {
    return inviter;
  }

  public void setInviter(String inviter) {
    this.inviter = inviter;
  }

  public String getRepetition() {
    return repetition;
  }

  public void setRepetition(String repetition) {
    this.repetition = repetition;
  }

  public DateTime getStartTime() {
    return startTime;
  }

  public void setStartTime(DateTime startTime) {
    this.startTime = startTime;
  }

  public DateTime getEndTime() {
    return endTime;
  }

  public void setEndTime(DateTime endTime) {
    this.endTime = endTime;
  }
}
