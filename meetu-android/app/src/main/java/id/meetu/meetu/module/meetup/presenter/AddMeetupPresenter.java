package id.meetu.meetu.module.meetup.presenter;

import id.meetu.meetu.module.meetup.contract.AddMeetUpActivityContract;
import id.meetu.meetu.module.meetup.model.TimeRecommendationDetail;
import id.meetu.meetu.module.meetup.model.TimeRecommendationRequest;
import id.meetu.meetu.module.meetup.model.TimeRecommendationResponse;
import id.meetu.meetu.module.meetup.service.TimeRecommendationService;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.DateUtil.RepeatType;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 19/04/18.
 */

public class AddMeetupPresenter implements AddMeetUpActivityContract.Presenter {

  private AddMeetUpActivityContract.View view;
  private TimeRecommendationService recommendationService;

  public AddMeetupPresenter(AddMeetUpActivityContract.View view) {
    this.view = view;
    this.recommendationService = new TimeRecommendationService();
  }

  public AddMeetupPresenter(AddMeetUpActivityContract.View view,
      TimeRecommendationService service) {
    this.view = view;
    this.recommendationService = service;
  }

  @Override
  public void fetchTimeRecommendation(int groupId, DateTime startDate, DateTime endDate,
      RepeatType repeatType, int repeatCount) {
    DateTime timeZone = Constant.UTC_ZERO;

    final TimeRecommendationRequest request = new TimeRecommendationRequest(groupId, timeZone,
        startDate, endDate,
        repeatType.name(), repeatCount);
    recommendationService.postTimeRecommendations(request,
        new Callback<List<List<TimeRecommendationResponse>>>() {
          @Override
          public void onResponse(Call<List<List<TimeRecommendationResponse>>> call,
              Response<List<List<TimeRecommendationResponse>>> response) {

            if (response.isSuccessful()) {
              List<List<TimeRecommendationResponse>> listRecommendationDateResponse = response
                  .body();
              List<List<TimeRecommendationDetail>> listRecommendationDate = new ArrayList<>();
              int idx = 1;

              for (List<TimeRecommendationResponse> timesInDate : listRecommendationDateResponse) {
                List<TimeRecommendationDetail> timeRecommendation = new ArrayList<>();
                for (TimeRecommendationResponse time : timesInDate) {
                  DateTime startTime = time.getStartTime();
                  DateTime endTime = time.getEndTime();

                  timeRecommendation
                      .add(new TimeRecommendationDetail(idx++, startTime, endTime, 5, 5));
                }
                listRecommendationDate.add(timeRecommendation);
              }

              if (listRecommendationDate.isEmpty()) {
                view.onFinishEmptyRecommendation();
              } else {
                view.onFinishGetRecommendation(listRecommendationDate);
              }
            } else {
              view.onError("Something is wrong");
            }
          }

          @Override
          public void onFailure(Call<List<List<TimeRecommendationResponse>>> call, Throwable t) {
            view.onError("Something is wrong with your connection");
          }
        });
  }
}
