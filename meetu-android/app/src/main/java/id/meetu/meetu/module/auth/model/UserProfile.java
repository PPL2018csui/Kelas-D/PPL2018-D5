package id.meetu.meetu.module.auth.model;

import com.google.gson.annotations.SerializedName;
import id.meetu.meetu.module.group.model.Group;
import java.util.List;

/**
 * Created by ricky on 11/03/18.
 */

public class UserProfile {

  @SerializedName("id")
  private int id;

  @SerializedName("phone_number")
  private String phoneNumber;

  @SerializedName("location")
  private String location;

  @SerializedName("user")
  private int user;

  @SerializedName("groups")
  private List<Group> groups;

  @SerializedName("profile_image_url")
  private String profileImageUrl;

  public int getUser() {
    return user;
  }

  public void setUser(int user) {
    this.user = user;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public List<Group> getGroups() {
    return groups;
  }

  public void setGroups(List<Group> groups) {
    this.groups = groups;
  }

  public String getProfileImageUrl() {
    return profileImageUrl;
  }

  public void setProfileImageUrl(String profileImageUrl) {
    this.profileImageUrl = profileImageUrl;
  }
}
