package id.meetu.meetu.module.meetup.view.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.meetup.contract.AddMeetUpActivityContract;
import id.meetu.meetu.module.meetup.contract.AddMeetUpConfirmationContract;
import id.meetu.meetu.module.meetup.model.MeetUpFormData;
import id.meetu.meetu.module.meetup.presenter.AddMeetupConfirmationPresenter;
import id.meetu.meetu.util.AutoCompleteDropDownView;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.DateUtil;
import id.meetu.meetu.util.DateUtil.RepeatType;
import id.meetu.meetu.util.SharedPrefManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.joda.time.DateTime;

/**
 * Created by ricky on 09/04/18.
 */

public class AddMeetupConfirmationFormFragment extends BaseFragment implements
    AddMeetUpConfirmationContract.View {

  @BindView(R.id.meetup_name_confirmation_text)
  TextInputLayout meetupNameText;

  @BindView(R.id.meetup_name_confirmation)
  EditText meetupName;

  @BindView(R.id.date_confirmation)
  EditText date;

  @BindView(R.id.from_confirmation)
  EditText from;

  @BindView(R.id.to_confirmation)
  EditText to;

  @BindView(R.id.group_dropdown_confirmation)
  AutoCompleteDropDownView groupDropDown;

  @BindView(R.id.group_dropdown_text_confirmation)
  TextInputLayout groupDropDownText;

  @BindView(R.id.schedule_type_dropdown_confirmation)
  AutoCompleteDropDownView scheduleTypeDropDown;

  @BindView(R.id.schedule_type_dropdown_text_confirmation)
  TextInputLayout scheduleTypeDropDownText;

  // rather tiring to handle from the dropdown
  private RepeatType repeatType;
  private int repeatCount;
  private int groupId;
  private int groupOwner;

  private AddMeetUpActivityContract.View view;
  private AddMeetUpConfirmationContract.Presenter presenter;

  public static AddMeetupConfirmationFormFragment newInstance(AddMeetUpActivityContract.View view) {
    AddMeetupConfirmationFormFragment fragment = new AddMeetupConfirmationFormFragment();
    Bundle args = new Bundle();

    SharedPrefManager manager = SharedPrefManager.getInstance(null);
    args.putString("name", manager.readString(SharedPrefManager.MEETUP_FORM_NAME_KEY, ""));
    args.putString("group_title",
        manager.readString(SharedPrefManager.MEETUP_FORM_GROUP_TITLE_KEY, ""));
    args.putInt("group_id", manager.readInt(SharedPrefManager.MEETUP_FORM_GROUP_ID_KEY, -1));
    args.putInt("group_owner", manager.readInt(SharedPrefManager.MEETUP_FORM_GROUP_OWNER_KEY, -1));
    args.putLong("chosen_date",
        manager.readLong(SharedPrefManager.MEETUP_FORM_CHOSEN_START_KEY, -1));
    args.putString("repeat_type",
        manager.readString(SharedPrefManager.MEETUP_FORM_REPEAT_TYPE_KEY, RepeatType.ONCE.name()));
    args.putInt("repeat_count", manager.readInt(SharedPrefManager.MEETUP_FORM_REPEAT_COUNT_KEY, 1));
    args.putLong("start_time", manager.readLong(SharedPrefManager.MEETUP_FORM_START_TIME_KEY, -1));
    args.putLong("end_time", manager.readLong(SharedPrefManager.MEETUP_FORM_END_TIME_KEY, -1));

    fragment.setArguments(args);
    fragment.view = view;

    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_add_meetup_confirmation_form, container, false);
    ButterKnife.bind(this, v);
    progressBarView.setWithOverlay(true);

    presenter = new AddMeetupConfirmationPresenter(this);

    DateTime chosenDate = new DateTime(getArguments().getLong("chosen_date"));
    DateTime startTime = new DateTime(getArguments().getLong("start_time"));
    DateTime endTime = new DateTime(getArguments().getLong("end_time"));

    repeatType = RepeatType.valueOf(getArguments().getString("repeat_type"));
    repeatCount = getArguments().getInt("repeat_count");
    groupOwner = getArguments().getInt("group_owner");
    groupId = getArguments().getInt("group_id");

    meetupName.setText(getArguments().getString("name"));
    groupDropDown.setText(getArguments().getString("group_title"));
    date.setText(DateUtil.getDateString(chosenDate));
    from.setText(
        DateUtil.getHourMinuteString(startTime.getHourOfDay(), startTime.getMinuteOfHour(), ":"));
    to.setText(
        DateUtil.getHourMinuteString(endTime.getHourOfDay(), endTime.getMinuteOfHour(), ":"));

    if (repeatType == RepeatType.ONCE) {
      scheduleTypeDropDown.setText(Constant.EVENT_TYPE_FORM.get(0));
    } else {
      scheduleTypeDropDown.setText(DateUtil.getRepetitionText(repeatType, repeatCount));
    }

    // set cursor to the end of edittext (meetup name)
    meetupName.setSelection(meetupName.getText().length());

    // disable form
    groupDropDown.setAdapter(new ArrayAdapter<String>(getActivity(),
        R.layout.layout_group_spinner_item, new ArrayList<String>()));
    groupDropDown.setEnabled(false);
    date.setEnabled(false);
    scheduleTypeDropDown.setAdapter(new ArrayAdapter<String>(getActivity(),
        R.layout.layout_group_spinner_item, new ArrayList<String>()));
    from.setEnabled(false);
    to.setEnabled(false);

    return v;
  }

  @Override
  public void resetAllError() {
    meetupNameText.setErrorEnabled(false);
  }

  @Override
  public void setMeetUpNameError(String error) {
    meetupNameText.setError(error);
  }

  @Override
  public void onFinishPostMeetUp(String msg) {
    Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    removeAllFormData();
    view.onFinishCreateMeetUp();
  }

  @Override
  public void onErrorPostMeetUp(String error) {
    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
  }

  private void removeAllFormData() {
    List<String> keys = Collections
        .unmodifiableList(Arrays.asList(SharedPrefManager.MEETUP_FORM_NAME_KEY,
            SharedPrefManager.MEETUP_FORM_GROUP_ID_KEY,
            SharedPrefManager.MEETUP_FORM_GROUP_OWNER_KEY,
            SharedPrefManager.MEETUP_FORM_GROUP_TITLE_KEY,
            SharedPrefManager.MEETUP_FORM_START_DATE_KEY,
            SharedPrefManager.MEETUP_FORM_END_DATE_KEY,
            SharedPrefManager.MEETUP_FORM_REPEAT_TYPE_KEY,
            SharedPrefManager.MEETUP_FORM_REPEAT_COUNT_KEY,
            SharedPrefManager.MEETUP_FORM_CHOSEN_START_KEY,
            SharedPrefManager.MEETUP_FORM_END_TIME_KEY,
            SharedPrefManager.MEETUP_FORM_START_TIME_KEY));

    SharedPrefManager manager = SharedPrefManager.getInstance(null);
    for (String key : keys) {
      manager.remove(key);
    }
  }

  @OnClick(R.id.continue_button_add_meetup_confirmation_form)
  public void onClickContinueButtonAddMeetupConfirmationForm(View v) {
    MeetUpFormData data = new MeetUpFormData.Builder()
        .setMeetUpName(meetupName.getText().toString().trim())
        .setGroupId(groupId)
        .setGroupOwner(groupOwner)
        .setGroupName(groupDropDown.getText().toString().trim())
        .setRepeatType(repeatType)
        .setRepeatCount(repeatCount)
        .setStartDate(DateUtil.parseDateString(date.getText().toString().trim()))
        .setStartTime(DateUtil.parseHourMinuteString(from.getText().toString().trim(), ":"))
        .setEndTime(DateUtil.parseHourMinuteString(to.getText().toString().trim(), ":"))
        .create();

    presenter.submit(data);
  }

  @OnClick(R.id.retry_button_add_meetup_confirmation_form)
  public void onClickRetryButtonAddMeetupConfirmationForm(View v) {
    removeAllFormData();
    view.onRetryCreateMeetUp();
  }
}

