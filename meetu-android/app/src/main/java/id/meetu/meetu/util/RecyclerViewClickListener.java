package id.meetu.meetu.util;

import android.view.View;

/**
 * Created by ayaz97 on 19/04/18.
 */

public interface RecyclerViewClickListener {
  void onItemClick(View v, int position);
}
