package id.meetu.meetu.module.profile.model;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordRequest {

  @SerializedName("password")
  private String password;

  @SerializedName("confirm_password")
  private String confirmPassword;

  public ChangePasswordRequest(String password, String confirmPassword) {
    this.password = password;
    this.confirmPassword = confirmPassword;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getConfirmPassword() {
    return confirmPassword;
  }

  public void setConfirmPassword(String confirmPassword) {
    this.confirmPassword = confirmPassword;
  }
}
