package id.meetu.meetu.module.group.service;

import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.model.UserSearchRequest;
import id.meetu.meetu.module.group.model.AddMemberRequest;
import id.meetu.meetu.module.group.model.CreateGroupRequest;
import id.meetu.meetu.module.group.model.GroupMemberResponse;
import id.meetu.meetu.module.group.model.GroupResponse;
import id.meetu.meetu.service.GroupApi;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;


/**
 * Created by Andre on 4/11/2018.
 */


public class GroupService {

  private GroupApi.Service service;

  public GroupService() {
    this.service = new GroupApi().create();
  }

  public GroupService(GroupApi.Service service) {
    this.service = service;
  }

  public GroupApi.Service getService() {
    return service;
  }

  public void getGroups(Callback<List<GroupResponse>> callback) {
    Call<List<GroupResponse>> call = service.getGroups();
    call.enqueue(callback);
  }

  public void searchUser(Callback<User> callback, UserSearchRequest request) {
    Call<User> call = service.searchUser(request);
    call.enqueue(callback);
  }

  public void getGroupMember(Callback<GroupMemberResponse> callback, int groupId) {
    Call<GroupMemberResponse> call = service.getGroupMember(groupId);
    call.enqueue(callback);
  }

  public void createGroup(Callback<GroupMemberResponse> callback, CreateGroupRequest request) {
    Call<GroupMemberResponse> call = service.createGroup(request);
    call.enqueue(callback);
  }

  public void addMember(Callback<GroupMemberResponse> callback, AddMemberRequest request) {
    Call<GroupMemberResponse> call = service.addMember(request);
    call.enqueue(callback);
  }
}