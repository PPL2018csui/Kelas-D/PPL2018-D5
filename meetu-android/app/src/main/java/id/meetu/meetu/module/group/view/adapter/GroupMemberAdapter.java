package id.meetu.meetu.module.group.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import id.meetu.meetu.R;
import id.meetu.meetu.module.group.model.GroupMemberDetail;
import id.meetu.meetu.util.RecyclerViewClickListener;
import java.util.List;

public class GroupMemberAdapter extends
    RecyclerView.Adapter<GroupMemberAdapter.GroupMemberViewHolder> {

  private Context mCtx;
  private List<GroupMemberDetail> groupMemberLists;
  private RecyclerViewClickListener listener;

  public GroupMemberAdapter(Context mCtx, List<GroupMemberDetail> groupMemberLists,
      RecyclerViewClickListener listener) {
    this.mCtx = mCtx;
    if (groupMemberLists.isEmpty() || groupMemberLists.get(0).getId() != 0) {
      groupMemberLists.add(0, new GroupMemberDetail(0, "Add Member", ""));
    }
    this.groupMemberLists = groupMemberLists;
    this.listener = listener;
  }

  @Override
  public GroupMemberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(mCtx);
    View view = inflater.inflate(R.layout.layout_cardview_member, null);
    final GroupMemberViewHolder holder = new GroupMemberViewHolder(view);
    view.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        listener.onItemClick(v, holder.getAdapterPosition());
      }
    });

    return holder;
  }

  @Override
  public void onBindViewHolder(GroupMemberViewHolder holder, int position) {
    GroupMemberDetail groupMember = groupMemberLists.get(position);
    holder.textViewName.setText(groupMember.getName());
    holder.cardView.setCardBackgroundColor(Color.TRANSPARENT);
    holder.cardView.setCardElevation(0);

    if (position == 0) {
      holder.image.setImageDrawable(
          holder.itemView.getContext().getResources().getDrawable(R.drawable.ic_plus));
    } else {
      String url = groupMember.getProfileImageUrl();

      Picasso.with(mCtx).load(url)
          .placeholder(R.drawable.profile_placeholder).error(R.drawable.profile_placeholder)
          .fit().centerInside()
          .into(holder.image);
    }

  }

  public void setGroupMemberLists(List<GroupMemberDetail> newMembers) {
    groupMemberLists = newMembers;
    if (groupMemberLists.isEmpty() || groupMemberLists.get(0).getId() != 0) {
      groupMemberLists.add(0, new GroupMemberDetail(0, "Add Member", ""));
    }
    notifyDataSetChanged();
  }

  public void setListener(RecyclerViewClickListener listener) {
    this.listener = listener;
  }

  @Override
  public int getItemCount() {
    return groupMemberLists.size();
  }

  public class GroupMemberViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.profile_name)
    TextView textViewName;

    @BindView(R.id.profile_image)
    CircleImageView image;

    @BindView(R.id.id_group)
    CardView cardView;

    public GroupMemberViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
