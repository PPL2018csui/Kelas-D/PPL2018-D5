package id.meetu.meetu.module.group.view.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.group.contract.GroupActivityContract;
import id.meetu.meetu.module.group.contract.GroupMembersContract;
import id.meetu.meetu.module.group.model.GroupMemberDetail;
import id.meetu.meetu.module.group.model.GroupMembers;
import id.meetu.meetu.module.group.presenter.GroupMemberPresenter;
import id.meetu.meetu.module.group.view.adapter.GroupMemberAdapter;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.RecyclerViewClickListener;
import java.util.ArrayList;
import org.joda.time.format.DateTimeFormat;

public class GroupMemberFragment extends BaseFragment implements GroupMembersContract.View {

  @BindView(R.id.recyclerView)
  RecyclerView recyclerView;
  @BindView(R.id.group_title)
  TextView groupTitleText;
  @BindView(R.id.fragment_create_group)
  FrameLayout contentView;

  @BindView(R.id.group_created)
  TextView groupCreatedText;

  private GroupActivityContract.View activity;
  private GroupMembersContract.Presenter presenter;
  private GroupMemberAdapter adapter;
  private GroupMembers groupMembers;
  private int groupId;

  public static GroupMemberFragment newInstance(GroupActivityContract.View activity, int groupId) {
    GroupMemberFragment fragment = new GroupMemberFragment();
    Bundle args = new Bundle();

    fragment.setArguments(args);
    fragment.activity = activity;
    fragment.groupId = groupId;
    fragment.presenter = new GroupMemberPresenter(fragment);
    fragment.groupMembers = new GroupMembers(groupId, "Group", null,
        new ArrayList<GroupMemberDetail>());

    return fragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_group_detail, container, false);

    ButterKnife.bind(this, v);
    activity.changeActionText("Group Detail");
    progressBarView.setProgressBarHideView(contentView);

    recyclerView.setLayoutManager(new GridLayoutManager(v.getContext(), 3));
    recyclerView.setHasFixedSize(true);

    adapter = new GroupMemberAdapter(v.getContext(), groupMembers.getGroupMember(),
        new RecyclerViewClickListener() {
          @Override
          public void onItemClick(View v, int position) {

          }
        });
    recyclerView.setAdapter(adapter);
    presenter.getMembersInGroup(groupId);

    return v;
  }

  @Override
  public void onGetMembersSuccess(GroupMembers groupWithMembers) {
    groupMembers = groupWithMembers;

    String createdDate = DateTimeFormat.forPattern("dd MMMM yyyy")
        .print(groupMembers.getCreatedDate());
    groupTitleText.setText(groupMembers.getTitle());
    groupCreatedText.setText("Established on " + createdDate);

    adapter.setGroupMemberLists(groupMembers.getGroupMember());
    adapter.setListener(new RecyclerViewClickListener() {
      @Override
      public void onItemClick(View v, int position) {
        if (position == 0) {
          onClickAddPeople();
        }
      }
    });
  }

  @Override
  public void onGetMembersError(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
  }

  public void onClickAddPeople() {
    activity.onShowAddMember(groupMembers, Constant.GROUP_ADD_MEMBER_MODE);
  }
}
