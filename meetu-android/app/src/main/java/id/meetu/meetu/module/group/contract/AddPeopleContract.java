package id.meetu.meetu.module.group.contract;

import id.meetu.meetu.base.BaseContract;
import id.meetu.meetu.module.group.model.GroupMemberDetail;
import id.meetu.meetu.module.group.model.GroupMembers;

public interface AddPeopleContract {

  interface View extends BaseContract.View {

    void onGetPeopleSuccess(GroupMemberDetail groupMemberDetail);

    void onGetPeopleNotExist();

    void onGetPeopleError(String message);

    void onSaveError(String message);

    void onSaveSuccess(String message, GroupMembers groupMembers);

    void hideLayoutsAndErrors();

    void setUsernameError(String error);
  }

  interface Presenter {

    void searchPeople(String username);

    void onAdd(GroupMembers members, GroupMemberDetail newMember, int mode);
  }
}
