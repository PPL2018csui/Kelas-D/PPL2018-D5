package id.meetu.meetu.module.meetup.contract;

import id.meetu.meetu.module.meetup.model.TimeRecommendationDetail;
import id.meetu.meetu.util.DateUtil.RepeatType;
import java.util.List;
import org.joda.time.DateTime;

/**
 * Created by ayaz97 on 18/04/18.
 */

public interface AddMeetUpActivityContract {

  interface View {

    void onFinishFillForm();

    void onFinishGetRecommendation(List<List<TimeRecommendationDetail>> recommendation);

    void onFinishEmptyRecommendation();

    void onError(String error);

    void onFinishPickInterval();

    void onFinishCreateMeetUp();

    void onRetryCreateMeetUp();
  }

  interface Presenter {

    void fetchTimeRecommendation(int groupId, DateTime start, DateTime end, RepeatType repeatType,
        int repeatCount);
  }
}
