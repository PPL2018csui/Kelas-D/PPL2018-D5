package id.meetu.meetu.service;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.base.BaseApi;
import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.meetup.model.MeetUpResponse;
import id.meetu.meetu.module.meetup.model.TimeRecommendationRequest;
import id.meetu.meetu.module.meetup.model.TimeRecommendationResponse;
import java.util.List;
import java.util.Map;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;

/**
 * Created by ayaz97 on 27/03/18.
 */

public class MeetUpApi extends BaseApi<MeetUpApi.Service> {

  @Override
  public String findUrl() {
    return BuildConfig.BACKEND_URL;
  }

  @Override
  public Service create() {
    return build().create(Service.class);
  }

  public interface Service {

    @GET("api/meetups/")
    Call<List<MeetUpResponse>> getMeetUps();

    @POST("api/meetups/")
    Call<BaseResponse> postMeetUps(@Body List<MeetUpResponse> meetUpResponse);

    @HTTP(method = "DELETE", path = "/api/meetups/", hasBody = true)
    Call<MeetUpResponse> deleteMeetUps(@Body Map<String, String> request);
  }
}