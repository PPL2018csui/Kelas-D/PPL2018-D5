package id.meetu.meetu.module.meetup.contract;

import id.meetu.meetu.base.BaseContract;
import id.meetu.meetu.module.meetup.model.MeetUpFormData;

/**
 * Created by ayaz97 on 19/04/18.
 */

public interface AddMeetUpConfirmationContract {

  interface View extends BaseContract.View {

    void resetAllError();

    void setMeetUpNameError(String error);

    void onFinishPostMeetUp(String msg);

    void onErrorPostMeetUp(String error);
  }

  interface Presenter {

    void submit(MeetUpFormData data);
  }
}
