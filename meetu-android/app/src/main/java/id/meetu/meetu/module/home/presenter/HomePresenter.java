package id.meetu.meetu.module.home.presenter;

import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.meetup.model.MeetUpDetail;
import id.meetu.meetu.module.meetup.model.MeetUpResponse;
import id.meetu.meetu.module.meetup.service.MeetUpService;
import id.meetu.meetu.module.profile.service.ProfileService;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 27/03/18.
 */

public class HomePresenter {

  private HomePresenterListener listener;
  private MeetUpService meetUpService;
  private ProfileService profileService;

  public HomePresenter(HomePresenterListener listener) {
    this.listener = listener;
    this.meetUpService = new MeetUpService();
    this.profileService = new ProfileService();
  }

  public HomePresenter(HomePresenterListener listener, MeetUpService meetUpService,
      ProfileService profileService) {
    this.listener = listener;
    this.meetUpService = meetUpService;
    this.profileService = profileService;
  }

  public HomePresenterListener getListener() {
    return listener;
  }

  public MeetUpService getMeetUpService() {
    return meetUpService;
  }

  public void getMeetUps() {
    listener.onLoading();
    meetUpService.getMeetUps(new Callback<List<MeetUpResponse>>() {
      @Override
      public void onResponse(Call<List<MeetUpResponse>> call,
          Response<List<MeetUpResponse>> response) {
        if (response.isSuccessful()) {
          List<MeetUpDetail> meetUps = new ArrayList<>();
          int idx = 1;

          for (MeetUpResponse meetUp : response.body()) {
            String title = meetUp.getTitle();
            String group = meetUp.getGroupResponse().getTitle();
            DateTime startTime = meetUp.getStartTime();
            DateTime endTime = meetUp.getEndTime();

            String date = DateTimeFormat.forPattern("EEEE, dd MMMM").print(startTime);
            String startDetail = DateTimeFormat.forPattern("HH.mm").print(startTime);
            String endDetail = DateTimeFormat.forPattern("HH.mm").print(endTime);
            String locationTime = startDetail + " - " + endDetail;

            MeetUpDetail detail = new MeetUpDetail(idx, title, group, date, locationTime, endTime);
            meetUps.add(detail);

            idx += 1;
          }
          listener.onFinishLoading();
          listener.onGetMeetUpSuccess(meetUps);
        } else {
          listener.onFinishLoading();
          listener.onError("Failed to get your MeetUps");
        }
      }

      @Override
      public void onFailure(Call<List<MeetUpResponse>> call, Throwable t) {
        listener.onFinishLoading();
        listener.onError("Something is wrong with your connection");
      }
    });
  }

  public void getProfile() {
    listener.onLoading();

    profileService.getProfile(new Callback<User>() {

      @Override
      public void onResponse(Call<User> call, Response<User> response) {
        if (response.isSuccessful()) {
          listener.onFinishLoading();
          listener.onGetProfileSuccess(response.body());
        } else {
          listener.onFinishLoading();
          listener.onError("Failed to get your profile data");
        }
      }

      @Override
      public void onFailure(Call<User> call, Throwable t) {
        listener.onFinishLoading();
        listener.onError("Something is wrong with your connection");
      }
    });
  }
}
