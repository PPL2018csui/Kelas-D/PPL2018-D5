package id.meetu.meetu.util;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.Menu;
import android.view.MenuItem;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import id.meetu.meetu.R;
import id.meetu.meetu.module.home.view.activity.HomeActivity;
import id.meetu.meetu.module.notification.view.activity.NotificationActivity;
import id.meetu.meetu.module.profile.view.activity.ProfileActivity;

/**
 * Created by luthfi on 04/03/18.
 */

public class BottomNavigationViewHelper {

  public static void setupBottomNavigationView(final Context context,
      BottomNavigationViewEx bottomNav, final int ACTIVITY_NUM) {
    bottomNav.enableAnimation(false);
    bottomNav.enableItemShiftingMode(false);
    bottomNav.enableShiftingMode(false);
    bottomNav.setTextVisibility(false);
    enableNavigation(context, bottomNav, ACTIVITY_NUM);
    Menu menu = bottomNav.getMenu();
    MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
    menuItem.setChecked(true);
  }

  public static void enableNavigation(final Context context, BottomNavigationViewEx view,
      final int ACTIVITY_NUM) {
    view.setOnNavigationItemSelectedListener(
        new BottomNavigationView.OnNavigationItemSelectedListener() {
          @Override
          public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
              case R.id.ic_profile:
                if (ACTIVITY_NUM != ProfileActivity.ACTIVITY_NUM) {
                  Intent intent3 = new Intent(context,
                      ProfileActivity.class); // ACTIVITY_NUM = 0
                  context
                      .startActivity(intent3.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                }
                break;

              case R.id.ic_home:
                if (ACTIVITY_NUM != HomeActivity.ACTIVITY_NUM) {
                  Intent intent1 = new Intent(context,
                      HomeActivity.class); // ACTIVITY_NUM = 1
                  context
                      .startActivity(intent1.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                }
                break;

              case R.id.ic_notification:
                if (ACTIVITY_NUM != NotificationActivity.ACTIVITY_NUM) {
                  Intent intent2 = new Intent(context,
                      NotificationActivity.class); // ACTIVITY_NUM = 2
                  context
                      .startActivity(intent2.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION));
                }
                break;
            }
            return false;
          }
        });
  }
}
