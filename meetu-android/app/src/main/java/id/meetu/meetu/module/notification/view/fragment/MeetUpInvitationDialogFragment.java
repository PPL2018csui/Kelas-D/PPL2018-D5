package id.meetu.meetu.module.notification.view.fragment;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.util.DateUtil;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

/**
 * Created by ayaz97 on 10/04/18.
 */

public class MeetUpInvitationDialogFragment extends DialogFragment {

  @BindView(R.id.meetup_title)
  TextView meetUpTitle;

  @BindView(R.id.meetup_group)
  TextView meetUpGroup;

  @BindView(R.id.meetup_date)
  TextView meetUpDate;

  @BindView(R.id.meetup_time)
  TextView meetUpTime;

  @BindView(R.id.meetup_repetition)
  TextView meetUpRepetition;

  @BindView(R.id.ok_button)
  Button okButton;

  public static MeetUpInvitationDialogFragment newInstance(String title, String group, String startDate, String endDate, String repetition) {
    MeetUpInvitationDialogFragment fragment = new MeetUpInvitationDialogFragment();

    DateTimeFormatter parseDtf = ISODateTimeFormat.dateTimeParser();
    DateTimeFormatter printDateDtf = DateTimeFormat.forPattern("EEEE, dd MMMM yyyy");

    DateTime start = parseDtf.parseDateTime(startDate);
    DateTime end = parseDtf.parseDateTime(endDate);

    String date = printDateDtf.print(start);
    String time = DateUtil.getHourMinuteString(start.getHourOfDay(), start.getMinuteOfHour(), ".");
    time += " - ";
    time += DateUtil.getHourMinuteString(end.getHourOfDay(), end.getMinuteOfHour(), ".");

    Bundle args = new Bundle();
    args.putString("title", title);
    args.putString("group", "with " + group);
    args.putString("date", "From " + date);
    args.putString("time", time);
    args.putString("repetition", repetition);

    fragment.setArguments(args);

    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_meetup_confirmation, container, false);
    ButterKnife.bind(this, v);

    meetUpTitle.setText(getArguments().getString("title"));
    meetUpGroup.setText(getArguments().getString("group"));
    meetUpDate.setText(getArguments().getString("date"));
    meetUpTime.setText(getArguments().getString("time"));
    meetUpRepetition.setText(getArguments().getString("repetition"));

    getDialog().setCanceledOnTouchOutside(false);
    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    return v;
  }

  @OnClick(R.id.ok_button)
  public void onOkButtonClick() {
    getDialog().dismiss();
  }
}
