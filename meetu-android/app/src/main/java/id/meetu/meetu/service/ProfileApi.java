package id.meetu.meetu.service;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.base.BaseApi;
import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.profile.model.ChangePasswordRequest;
import id.meetu.meetu.module.profile.model.EditProfileRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public class ProfileApi extends BaseApi<ProfileApi.Service> {

  @Override
  public String findUrl() {
    return BuildConfig.BACKEND_URL;
  }

  @Override
  public ProfileApi.Service create() {
    return build().create(ProfileApi.Service.class);
  }

  public interface Service {

    @POST("api/change-password/")
    Call<Void> changePassword(@Body ChangePasswordRequest request);

    @POST("api/edit-profile/")
    Call<AuthResponse> editProfile(@Body EditProfileRequest request);

    @GET("api/profile/")
    Call<User> getProfile();
  }
}
