package id.meetu.meetu.module.notification.model;

import id.meetu.meetu.module.meetup.model.MeetUpDetail;
import org.joda.time.DateTime;

/**
 * Created by luthfi on 09/05/18.
 */

public class NotificationDetail extends MeetUpDetail {

  private final int MAX_ID = Integer.MAX_VALUE;
  private String header;
  private String person;
  private int notificationType;
  private String repetition;
  private DateTime dateTime;

  public NotificationDetail() {
    super();
  }

  public NotificationDetail(int id, int notificationType, String header, String event, String group,
      String date,
      String locationTime, String repetition, DateTime dateTime) {
    super(id, event, group, date, locationTime, dateTime);
    this.header = header;
    this.person = "";
    this.notificationType = notificationType;
    this.repetition = repetition;
    this.dateTime = dateTime;
  }

  public NotificationDetail(int id, int notificationType, String header, String group,
      String person) {
    super(id, "", group, "", "", null);
    this.header = header;
    this.person = person;
    this.notificationType = notificationType;
  }


  public String getHeader() {
    return this.header;
  }

  public void setHeader(String header) {
    this.header = header;
  }

  public int getNotificationType() {
    return this.notificationType;
  }

  public void setNotificationType(int notificationType) {
    this.notificationType = notificationType;
  }

  public String getPerson() {
    return this.person;
  }

  public void setPerson(String person) {
    this.person = person;
  }

  public String getRepetition() {
    return repetition;
  }

  public void setRepetition(String repetition) {
    this.repetition = repetition;
  }
}
