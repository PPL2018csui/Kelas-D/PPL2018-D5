package id.meetu.meetu.module.group.view.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.group.contract.CreateGroupContract;
import id.meetu.meetu.module.group.contract.GroupActivityContract;
import id.meetu.meetu.module.group.model.GroupMemberDetail;
import id.meetu.meetu.module.group.model.GroupMembers;
import id.meetu.meetu.module.group.presenter.CreateGroupPresenter;
import id.meetu.meetu.module.group.view.adapter.GroupMemberAdapter;
import id.meetu.meetu.util.Constant;
import id.meetu.meetu.util.RecyclerViewClickListener;
import id.meetu.meetu.util.SharedPrefManager;
import java.util.ArrayList;
import java.util.List;

public class CreateGroupFragment extends BaseFragment implements CreateGroupContract.View {

  @BindView(R.id.recyclerView)
  RecyclerView recyclerView;

  @BindView(R.id.group_name_text)
  TextInputLayout groupNameText;

  @BindView(R.id.group_name)
  EditText groupName;

  private GroupMemberAdapter adapter;
  private GroupMembers groupMembers;
  private GroupActivityContract.View activity;
  private CreateGroupContract.Presenter presenter;

  public static CreateGroupFragment newInstance(GroupMembers groupMembers,
      GroupActivityContract.View activity) {
    CreateGroupFragment fragment = new CreateGroupFragment();
    SharedPrefManager manager = SharedPrefManager.getInstance(null);

    if (groupMembers == null) {
      List<GroupMemberDetail> members = new ArrayList<>();
      int curUser = manager.readInt(SharedPrefManager.USER_PROFILE_ID_KEY, -1);
      String curName = manager.readString(SharedPrefManager.USER_FIRST_NAME, "");
      curName += " ";
      curName += manager.readString(SharedPrefManager.USER_LAST_NAME, "");
      String profileImage = manager.readString(SharedPrefManager.USER_PROFILE_IMAGE_URL, null);

      members.add(new GroupMemberDetail(curUser, curName, profileImage));
      groupMembers = new GroupMembers(-1, "", null, members);
    }

    fragment.groupMembers = groupMembers;
    fragment.activity = activity;
    fragment.presenter = new CreateGroupPresenter(fragment);

    return fragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_create_group, container, false);

    ButterKnife.bind(this, v);
    progressBarView.setWithOverlay(true);

    recyclerView.setLayoutManager(new GridLayoutManager(v.getContext(), 3));
    recyclerView.setHasFixedSize(true);

    groupName.setText(groupMembers.getTitle());

    adapter = new GroupMemberAdapter(v.getContext(), groupMembers.getGroupMember(),
        new RecyclerViewClickListener() {
          @Override
          public void onItemClick(View v, int position) {
            if (position == 0) {
              onClickAddPeople();
            }
          }
        });
    recyclerView.setAdapter(adapter);

    activity.changeActionText("Create Group");

    return v;
  }

  private GroupMembers getCurrentGroupMembers() {
    String title = groupName.getText().toString().trim();
    int groupId = groupMembers.getGroupId();

    List<GroupMemberDetail> members = new ArrayList<>();
    for (GroupMemberDetail member : groupMembers.getGroupMember()) {
      members.add(member);
    }

    members.remove(0);
    GroupMembers groupMembers = new GroupMembers(groupId, title, null, members);

    return groupMembers;
  }

  public void onClickAddPeople() {
    GroupMembers groupMembers = getCurrentGroupMembers();

    activity.onShowAddMember(groupMembers, Constant.GROUP_CREATE_MODE);
  }

  @OnClick(R.id.button_create_group)
  public void onClickCreateGroup() {
    GroupMembers groupMembers = getCurrentGroupMembers();
    presenter.submit(groupMembers);
  }

  @Override
  public void resetAllError() {
    groupNameText.setErrorEnabled(false);
  }

  @Override
  public void setGroupNameError(String error) {
    groupNameText.setError(error);
  }

  @Override
  public void onCreateSuccess(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    activity.onSaveGroup();
  }

  @Override
  public void onCreateError(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
  }
}
