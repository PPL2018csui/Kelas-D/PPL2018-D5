package id.meetu.meetu.module.schedule.model;

import com.google.gson.annotations.SerializedName;
import id.meetu.meetu.base.BaseResponse;
import org.joda.time.DateTime;

/**
 * Created by ayaz97 on 12/03/18.
 */

public class ScheduleResponse extends BaseResponse {

  @SerializedName("title")
  private String title;

  @SerializedName("start_time")
  private DateTime startTime;

  @SerializedName("end_time")
  private DateTime endTime;

  public ScheduleResponse(String title, DateTime startTime, DateTime endTime) {
    this.title = title;
    this.startTime = startTime;
    this.endTime = endTime;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public DateTime getStartTime() {
    return startTime;
  }

  public void setStartTime(DateTime startTime) {
    this.startTime = startTime;
  }

  public DateTime getEndTime() {
    return endTime;
  }

  public void setEndTime(DateTime endTime) {
    this.endTime = endTime;
  }
}
