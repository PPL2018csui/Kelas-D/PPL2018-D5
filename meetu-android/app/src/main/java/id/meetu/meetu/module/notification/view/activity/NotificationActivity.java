package id.meetu.meetu.module.notification.view.activity;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.module.group.view.activity.GroupActivity;
import id.meetu.meetu.module.notification.view.fragment.NotificationListFragment;
import id.meetu.meetu.module.schedule.view.ScheduleActivity;
import id.meetu.meetu.util.BottomNavigationViewHelper;

/**
 * Created by luthfi on 04/03/18.
 */

public class NotificationActivity extends BaseActivity {

  public static final int ACTIVITY_NUM = 2;

  @BindView(R.id.navbar_bottom)
  BottomNavigationViewEx bottomNav;

  @OnClick(R.id.add_meetup_calendar_button)
  public void onClickCalendar(View v) {
    startActivity(new Intent(this, ScheduleActivity.class));
  }

  @OnClick(R.id.group_list_member_button)
  public void onClickGroup(View v) {
    startActivity(new Intent(this, GroupActivity.class));
  }

  @Override
  public void onBackPressed() {
    if (getFragmentManager().getBackStackEntryCount() > 0) {
      getFragmentManager().popBackStack();
    } else {
      super.onBackPressed();
      overridePendingTransition(0, 0);
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ButterKnife.bind(this);

    showNotifications();
  }

  @Override
  protected void onResume() {
    super.onResume();

    BottomNavigationViewHelper
        .setupBottomNavigationView(NotificationActivity.this, bottomNav, ACTIVITY_NUM);
  }

  @Override
  public int findLayout() {
    return R.layout.activity_notifications;
  }

  public void showNotifications() {
    NotificationListFragment fragment = NotificationListFragment.newInstance(this);
    getFragmentManager().beginTransaction().replace(R.id.body, fragment).commit();
  }
}
