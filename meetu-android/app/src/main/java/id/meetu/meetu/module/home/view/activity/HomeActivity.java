package id.meetu.meetu.module.home.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.google.firebase.iid.FirebaseInstanceId;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseActivity;
import id.meetu.meetu.module.group.view.activity.GroupActivity;
import id.meetu.meetu.module.home.view.activity.fragment.HomeFragment;
import id.meetu.meetu.module.schedule.view.ScheduleActivity;
import id.meetu.meetu.util.Constant;

public class HomeActivity extends BaseActivity {

  public static final int ACTIVITY_NUM = 1;

  @Override
  public void onBackPressed() {
    if (getFragmentManager().getBackStackEntryCount() > 0) {
      getFragmentManager().popBackStack();
    } else {
      super.onBackPressed();
      overridePendingTransition(0, 0);
    }
  }

  @OnClick(R.id.add_meetup_calendar_button)
  public void onClickCalendar(View v) {
    startActivity(new Intent(HomeActivity.this, ScheduleActivity.class));
  }

  @OnClick(R.id.group_list_member_button)
  public void onClickGroup(View v) {
    startActivity(new Intent(HomeActivity.this, GroupActivity.class));
  }

  @Override
  public int findLayout() {
    return R.layout.activity_home;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    ButterKnife.bind(this);

    if (getIntent().getExtras() != null) {
      Bundle extra = getIntent().getExtras();

      if (extra.keySet().contains(Constant.FIRST_TIME_HOME)) {
        updateFirebaseToken(FirebaseInstanceId.getInstance().getToken());
      }

      if (extra.keySet().contains("type")) {
        Intent intent = getIntent();
        if (extra.getString("type").equals(Constant.FIREBASE_EVENT_GROUP)) {
          showGroupDialog(intent);
        } else {
          showMeetUpConfirmationDialog(intent);
        }
      }
    }

    showHome();
  }

  private void showHome() {
    getFragmentManager().beginTransaction().replace(R.id.home, new HomeFragment())
        .commit();
  }
}
