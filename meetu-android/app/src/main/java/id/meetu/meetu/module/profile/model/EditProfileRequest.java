package id.meetu.meetu.module.profile.model;

import com.google.gson.annotations.SerializedName;

public class EditProfileRequest {

  @SerializedName("first_name")
  private String firstName;

  @SerializedName("last_name")
  private String lastName;

  @SerializedName("phone_number")
  private String phoneNumber;

  @SerializedName("location")
  private String location;

  @SerializedName("profile_image_url")
  private String profileImageUrl;

  public EditProfileRequest(String firstName, String lastName, String phoneNumber, String location,
      String profileImageUrl) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.phoneNumber = phoneNumber;
    this.location = location;
    this.profileImageUrl = profileImageUrl;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public String getProfileImageUrl() {
    return profileImageUrl;
  }

  public void setProfileImageUrl(String profileImageUrl) {
    this.profileImageUrl = profileImageUrl;
  }
}
