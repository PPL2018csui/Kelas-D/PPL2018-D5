package id.meetu.meetu.module.notification.view.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.notification.contract.NotificationListContract;
import id.meetu.meetu.module.notification.model.NotificationDetail;
import id.meetu.meetu.module.notification.presenter.NotificationListPresenter;
import id.meetu.meetu.module.notification.view.activity.NotificationActivity;
import id.meetu.meetu.module.notification.view.adapter.NotificationAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by luthfi on 09/05/18.
 */

public class NotificationListFragment extends BaseFragment implements
    NotificationListContract.View {

  @BindView(R.id.card_list)
  RecyclerView recyclerView;
  @BindView(R.id.no_notifications)
  TextView noNotifications;
  @BindView(R.id.fragment_notifications)
  RelativeLayout relativeLayout;

  private NotificationAdapter adapter;
  private NotificationActivity activity;
  private NotificationListContract.Presenter presenter;

  public static NotificationListFragment newInstance(NotificationActivity notificationActivity) {
    NotificationListFragment fragment = new NotificationListFragment();

    Bundle args = new Bundle();

    fragment.setArguments(args);
    fragment.activity = notificationActivity;
    fragment.presenter = new NotificationListPresenter(fragment);

    return fragment;
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_notifications, container, false);
    ButterKnife.bind(this, view);
    progressBarView.setProgressBarBaseView(relativeLayout);

    adapter = new NotificationAdapter(view.getContext(), new ArrayList<NotificationDetail>());

    recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
    recyclerView.setHasFixedSize(true);
    recyclerView.setAdapter(adapter);

    noNotifications.setVisibility(View.INVISIBLE);
    presenter.getNotifications();

    return view;
  }

  @Override
  public void onGetNotificationsSuccess(List<NotificationDetail> notifications) {
    adapter.setNotificationList(notifications);

    if (notifications.isEmpty()) {
      setNotificationsNotVisible();
    } else {
      setNotificationsVisible();
    }
  }

  @Override
  public void onGetNotificationsError(String error) {
    Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
  }

  void setNotificationsNotVisible() {
    recyclerView.setVisibility(View.GONE);
    noNotifications.setVisibility(View.VISIBLE);
  }

  void setNotificationsVisible() {
    recyclerView.setVisibility(View.VISIBLE);
    noNotifications.setVisibility(View.GONE);
  }

  @Override
  public void onLoading() {
    View view = getView();
    if (view != null) {
      getView().setClickable(false);
    }
    progressBarView.show();
  }

  @Override
  public void onFinishLoading() {
    View view = getView();
    if (view != null) {
      getView().setClickable(true);
    }
    if (getActivity() != null) {
      getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          Handler handler = new Handler();
          handler.postDelayed(new Runnable() {
            @Override
            public void run() {
              progressBarView.hide();
            }
          }, 500);
        }
      });
    }
  }
}
