package id.meetu.meetu.util;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.meetu.meetu.R;
import id.meetu.meetu.util.DateUtil.RepeatType;
import java.util.List;

/**
 * Created by ayaz97 on 12/04/18.
 */

public class SelectRepetitionFragment extends DialogFragment {

  @BindView(R.id.repeat_count_dropdown)
  AutoCompleteDropDownView repeatCount;

  @BindView(R.id.repeat_type_dropdown)
  AutoCompleteDropDownView repeatType;

  @BindView(R.id.continue_button)
  Button continueButton;

  @BindView(R.id.cancel_button)
  Button cancelButton;

  SelectRepetitionFragment.Listener listener;

  ArrayAdapter<String> repeatCountAdapter;
  ArrayAdapter<String> repeatTypeAdapter;

  int selectedCountIndex = 0;
  int selectedTypeIndex = 0;

  public static SelectRepetitionFragment newInstance(RepeatType repeatType, int repeatCount,
      SelectRepetitionFragment.Listener listener) {
    SelectRepetitionFragment fragment = new SelectRepetitionFragment();
    fragment.listener = listener;

    if (repeatCount != -1) {
      fragment.selectedCountIndex = repeatCount - 1;

      if (repeatType == RepeatType.DAILY) {
        fragment.selectedTypeIndex = 0;
      } else if (repeatType == RepeatType.WEEKLY) {
        fragment.selectedTypeIndex = 1;
      } else {
        fragment.selectedTypeIndex = 2;
      }
    }

    return fragment;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    RepeatType mode;
    if (selectedTypeIndex == 0) {
      mode = RepeatType.DAILY;
    } else if (selectedTypeIndex == 1) {
      mode = RepeatType.WEEKLY;
    } else {
      mode = RepeatType.MONTHLY;
    }

    repeatCountAdapter = new ArrayAdapter<>(getActivity(), R.layout.layout_group_spinner_item,
        DateUtil.getListForTimeDropdown(mode));
    repeatTypeAdapter = new ArrayAdapter<>(getActivity(), R.layout.layout_group_spinner_item,
        Constant.REPETITION_TYPE_FORM);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_select_repetition, container, false);
    ButterKnife.bind(this, v);

    repeatCount.setAdapter(repeatCountAdapter);
    repeatCount.setText(repeatCountAdapter.getItem(selectedCountIndex).toString(), false);
    repeatCount.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        selectedCountIndex = position;
      }
    });

    repeatType.setAdapter(repeatTypeAdapter);
    repeatType.setText(repeatTypeAdapter.getItem(selectedTypeIndex).toString(), false);
    repeatType.setOnItemClickListener(new OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (selectedTypeIndex != position) {
          RepeatType mode;

          if (position == 0) {
            mode = RepeatType.DAILY;
          } else if (position == 1) {
            mode = RepeatType.WEEKLY;
          } else {
            mode = RepeatType.MONTHLY;
          }

          List<String> timeList = DateUtil.getListForTimeDropdown(mode);

          repeatCountAdapter.clear();
          repeatCountAdapter.addAll(timeList);
          repeatCountAdapter.notifyDataSetChanged();

          selectedCountIndex = Math.min(selectedCountIndex, timeList.size() - 1);
          repeatCount.setText(timeList.get(selectedCountIndex), false);

          selectedTypeIndex = position;
        }
      }
    });

    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    getDialog().setCanceledOnTouchOutside(false);
    getDialog().setOnKeyListener(new OnKeyListener() {
      @Override
      public boolean onKey(DialogInterface dialog, int keyCode,
          KeyEvent event) {

        // back button has to call onCancel, to maintain state
        if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
          listener.onCancelRepetition();
        }

        return false;
      }
    });

    return v;
  }

  @OnClick(R.id.cancel_button)
  public void onClickCancel() {
    listener.onCancelRepetition();
    getDialog().dismiss();
  }

  @OnClick(R.id.continue_button)
  public void onClickContinue() {
    RepeatType repeatType = RepeatType.DAILY;
    if (selectedTypeIndex == 1) {
      repeatType = RepeatType.WEEKLY;
    } else if (selectedTypeIndex == 2) {
      repeatType = RepeatType.MONTHLY;
    }

    int repeatCount = selectedCountIndex + 1;

    listener.onConfirmRepetition(repeatType, repeatCount);
    getDialog().dismiss();
  }

  public interface Listener {

    public void onConfirmRepetition(RepeatType repeatType, int repeatCount);

    public void onCancelRepetition();
  }
}
