package id.meetu.meetu.module.schedule.presenter;

import id.meetu.meetu.base.BasePresenterListener;
import id.meetu.meetu.module.schedule.model.Schedule;
import id.meetu.meetu.module.schedule.model.ScheduleInfo;
import java.util.List;

/**
 * Created by ayaz97 on 12/03/18.
 */

public interface SchedulePresenterListener extends BasePresenterListener {

  void onGetSuccess(List<ScheduleInfo> schedules);

  void onPostSuccess(String msg);

  void onPostSuccess(List<ScheduleInfo> schedules);

  void onDeleteSuccess(String message);
}
