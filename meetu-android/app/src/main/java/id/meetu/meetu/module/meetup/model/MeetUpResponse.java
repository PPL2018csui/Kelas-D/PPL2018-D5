package id.meetu.meetu.module.meetup.model;

import com.google.gson.annotations.SerializedName;
import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.group.model.GroupResponse;
import org.joda.time.DateTime;

/**
 * Created by ayaz97 on 27/03/18.
 */

public class MeetUpResponse extends BaseResponse {


  @SerializedName("title")
  private String title;

  @SerializedName("group")
  private GroupResponse groupResponse;

  @SerializedName("start_time")
  private DateTime startTime;

  @SerializedName("end_time")
  private DateTime endTime;

  public MeetUpResponse(String title, GroupResponse groupResponse, DateTime startTime,
      DateTime endTime) {
    this.title = title;
    this.groupResponse = groupResponse;
    this.startTime = startTime;
    this.endTime = endTime;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public GroupResponse getGroupResponse() {
    return groupResponse;
  }

  public void setGroupResponse(GroupResponse groupResponse) {
    this.groupResponse = groupResponse;
  }

  public DateTime getStartTime() {
    return startTime;
  }

  public void setStartTime(DateTime startTime) {
    this.startTime = startTime;
  }

  public DateTime getEndTime() {
    return endTime;
  }

  public void setEndTime(DateTime endTime) {
    this.endTime = endTime;
  }
}
