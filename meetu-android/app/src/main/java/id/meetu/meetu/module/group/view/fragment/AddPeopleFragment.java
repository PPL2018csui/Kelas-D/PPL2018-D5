package id.meetu.meetu.module.group.view.fragment;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.squareup.picasso.Picasso;
import de.hdodenhof.circleimageview.CircleImageView;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.group.contract.AddPeopleContract;
import id.meetu.meetu.module.group.contract.GroupActivityContract;
import id.meetu.meetu.module.group.model.GroupMemberDetail;
import id.meetu.meetu.module.group.model.GroupMembers;
import id.meetu.meetu.module.group.presenter.AddPeoplePresenter;
import id.meetu.meetu.util.Constant;

public class AddPeopleFragment extends BaseFragment implements AddPeopleContract.View {

  @BindView(R.id.add_people_button)
  Button addPeopleButton;

  @BindView(R.id.content)
  LinearLayout contentLayout;

  @BindView(R.id.found_layout)
  LinearLayout foundLayout;

  @BindView(R.id.not_found_layout)
  LinearLayout notFoundLayout;

  @BindView(R.id.people_name)
  TextView peopleName;

  @BindView(R.id.search_username)
  EditText searchUsername;

  @BindView(R.id.search_username_text)
  TextInputLayout searchUsernameText;

  @BindView(R.id.profile_image)
  CircleImageView profileImage;

  private AddPeopleContract.Presenter presenter;
  private GroupActivityContract.View activity;
  private GroupMemberDetail searchedMember;
  private GroupMembers members;
  private int mode;

  public static AddPeopleFragment newInstance(GroupActivityContract.View activity,
      GroupMembers members, int mode) {
    AddPeopleFragment fragment = new AddPeopleFragment();

    fragment.presenter = new AddPeoplePresenter(fragment);
    fragment.members = members;
    fragment.activity = activity;
    fragment.mode = mode;
    fragment.searchedMember = null;

    return fragment;
  }

  @OnClick(R.id.search_people_button)
  public void onSearchPeopleClicked(View v) {
    String username = searchUsername.getText().toString().trim();
    searchedMember = null;

    presenter.searchPeople(username);
  }

  @OnClick(R.id.add_people_button)
  public void onClickAddPeople(View v) {
    progressBarView.setWithOverlay(true);
    presenter.onAdd(members, searchedMember, mode);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_add_people, container, false);
    ButterKnife.bind(this, v);

    activity.changeActionText("Add People");
    progressBarView.setProgressBarHideView(contentLayout);

    hideLayoutsAndErrors();

    return v;
  }

  @Override
  public void onGetPeopleSuccess(GroupMemberDetail groupMemberDetail) {
    searchedMember = groupMemberDetail;
    peopleName.setText(searchedMember.getName());
    Picasso.with(getActivity()).load(groupMemberDetail.getProfileImageUrl())
        .placeholder(R.drawable.profile_placeholder).error(R.drawable.profile_placeholder)
        .into(profileImage);
    foundLayout.setVisibility(View.VISIBLE);
  }

  @Override
  public void onGetPeopleNotExist() {
    notFoundLayout.setVisibility(View.VISIBLE);
  }

  @Override
  public void onGetPeopleError(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
  }

  @Override
  public void onSaveError(String message) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
  }

  @Override
  public void onSaveSuccess(String message, GroupMembers members) {
    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();

    if (mode == Constant.GROUP_CREATE_MODE) {
      activity.onAddPeopleCreateGroup(members);
    } else {
      activity.onAddPeopleMemberDetail(members.getGroupId());
    }
  }

  @Override
  public void hideLayoutsAndErrors() {
    searchUsernameText.setErrorEnabled(false);
    notFoundLayout.setVisibility(View.GONE);
    foundLayout.setVisibility(View.GONE);
  }

  @Override
  public void setUsernameError(String error) {
    searchUsernameText.setError(error);
  }
}
