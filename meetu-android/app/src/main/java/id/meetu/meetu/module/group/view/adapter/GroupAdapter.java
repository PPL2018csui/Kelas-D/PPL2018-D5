package id.meetu.meetu.module.group.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.meetu.meetu.R;
import id.meetu.meetu.module.group.model.GroupDetail;
import id.meetu.meetu.util.RecyclerViewClickListener;
import java.util.List;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.GroupViewHolder> {

  private Context mCtx;
  private List<GroupDetail> groupList;
  private RecyclerViewClickListener listener;

  public GroupAdapter(Context mCtx, List<GroupDetail> groupList,
      RecyclerViewClickListener listener) {
    this.mCtx = mCtx;
    this.groupList = groupList;
    this.listener = listener;
  }

  @Override
  public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(mCtx);
    View view = inflater.inflate(R.layout.layout_cardview_group, null);
    final GroupViewHolder itemViewHolder = new GroupViewHolder(view);
    view.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View view) {
        listener.onItemClick(view, itemViewHolder.getAdapterPosition());
      }
    });

    return itemViewHolder;
  }

  @Override
  public void onBindViewHolder(GroupViewHolder holder, int position) {
    GroupDetail groupDetail = groupList.get(position);
    holder.textViewName.setText(groupDetail.getTitle());
  }

  @Override
  public int getItemCount() {
    return groupList.size();
  }

  public List<GroupDetail> getGroupList() {
    return groupList;
  }

  public void setGroupList(List<GroupDetail> groupList) {
    this.groupList = groupList;
    notifyDataSetChanged();
  }

  class GroupViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.group_name)
    TextView textViewName;

    public GroupViewHolder(View v) {
      super(v);
      ButterKnife.bind(this, v);
    }
  }
}
