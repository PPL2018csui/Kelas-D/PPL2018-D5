package id.meetu.meetu.module.meetup.view.fragment;

import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import id.meetu.meetu.R;
import id.meetu.meetu.util.DateUtil;
import org.joda.time.DateTime;

/**
 * Created by ayaz97 on 07/04/18.
 */

public class IntervalPickerFragment extends DialogFragment {

  @BindView(R.id.interval_text)
  TextView intervalText;

  @BindView(R.id.start_interval_text)
  TextInputLayout startIntervalText;

  @BindView(R.id.start_interval)
  EditText startInterval;

  @BindView(R.id.end_interval_text)
  TextInputLayout endIntervalText;

  @BindView(R.id.end_interval)
  EditText endInterval;

  @BindView(R.id.cancel_button)
  Button cancelButton;

  @BindView(R.id.continue_button)
  Button continueButton;

  Listener listener;

  int actualStartHour;
  int actualStartMinute;
  int actualEndHour;
  int actualEndMinute;

  int startHour;
  int startMinute;
  int endHour;
  int endMinute;

  TimePickerDialog.OnTimeSetListener startListener;
  TimePickerDialog.OnTimeSetListener endListener;

  public static IntervalPickerFragment newInstance(DateTime start, DateTime end,
      Listener listener) {
    IntervalPickerFragment ipf = new IntervalPickerFragment();

    int startHour = start.getHourOfDay();
    int startMinute = start.getMinuteOfHour();
    int endHour = end.getHourOfDay();
    int endMinute = end.getMinuteOfHour();

    Bundle args = new Bundle();
    args.putInt("startHour", startHour);
    args.putInt("startMinute", startMinute);
    args.putInt("endHour", endHour);
    args.putInt("endMinute", endMinute);

    ipf.setArguments(args);
    ipf.setListener(listener);

    return ipf;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    startHour = getArguments().getInt("startHour");
    startMinute = getArguments().getInt("startMinute");

    endHour = getArguments().getInt("endHour");
    endMinute = getArguments().getInt("endMinute");

    actualStartHour = startHour;
    actualStartMinute = startMinute;
    actualEndHour = endHour;
    actualEndMinute = endMinute;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_set_interval, container, false);
    ButterKnife.bind(this, v);

    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

    String intervalText = "";
    intervalText += "You have chosen\n";
    intervalText += DateUtil.getHourMinuteString(actualStartHour, actualStartMinute, ".");
    intervalText += " - ";
    intervalText += DateUtil.getHourMinuteString(actualEndHour, actualEndMinute, ".") + "\n";
    intervalText += "Interval";

    this.intervalText.setText(intervalText);
    startInterval.setText(DateUtil.getHourMinuteString(actualStartHour, actualStartMinute, ":"));
    endInterval.setText(DateUtil.getHourMinuteString(actualEndHour, actualEndMinute, ":"));

    startInterval.setInputType(InputType.TYPE_NULL);
    endInterval.setInputType(InputType.TYPE_NULL);

    prepareTimeListener();
    getDialog().setCanceledOnTouchOutside(false);

    return v;
  }

  public void setListener(Listener listener) {
    this.listener = listener;
  }

  void prepareTimeListener() {
    startListener = new TimePickerDialog.OnTimeSetListener() {
      @Override
      public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        startHour = hourOfDay;
        startMinute = minute;

        startInterval.setText(DateUtil.getHourMinuteString(startHour, startMinute, ":"));
      }
    };

    endListener = new TimePickerDialog.OnTimeSetListener() {
      @Override
      public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        endHour = hourOfDay;
        endMinute = minute;

        endInterval.setText(DateUtil.getHourMinuteString(endHour, endMinute, ":"));
      }
    };
  }

  @OnClick(R.id.start_interval)
  public void onClickStartInterval(View view) {
    new TimePickerDialog(getActivity(), R.style.DialogTheme, startListener, startHour, startMinute,
        true).show();
  }

  @OnFocusChange(R.id.start_interval)
  public void onFocusChangeStartInterval(View view, boolean hasFocus) {
    if (hasFocus) {
      new TimePickerDialog(getActivity(), R.style.DialogTheme, startListener, startHour,
          startMinute,
          true).show();
    }
  }

  @OnClick(R.id.end_interval)
  public void onClickEndInterval(View view) {
    new TimePickerDialog(getActivity(), R.style.DialogTheme, endListener, endHour, endMinute,
        true).show();
  }

  @OnFocusChange(R.id.end_interval)
  public void onFocusChangeEndInterval(View view, boolean hasFocus) {
    if (hasFocus) {
      new TimePickerDialog(getActivity(), R.style.DialogTheme, endListener, endHour, endMinute,
          true).show();
    }
  }

  private void resetAllError() {
    startIntervalText.setErrorEnabled(false);
    endIntervalText.setErrorEnabled(false);
  }

  private boolean isValidInput() {
    boolean isValid = true;

    String rangeError = "This field must be in range of " + DateUtil
        .getHourMinuteString(actualStartHour, actualStartMinute,
            ".") + " and " + DateUtil.getHourMinuteString(actualEndHour, actualEndMinute, ".");

    DateTime actualStart = new DateTime(1970, 1, 1, actualStartHour, actualStartMinute);
    DateTime actualEnd = new DateTime(1970, 1, 1, actualEndHour, actualEndMinute);
    DateTime start = new DateTime(1970, 1, 1, startHour, startMinute);
    DateTime end = new DateTime(1970, 1, 1, endHour, endMinute);

    // start not in range
    if (start.compareTo(actualStart) < 0 || start.compareTo(actualEnd) > 0) {
      startIntervalText.setError(rangeError);
      isValid = false;
    }

    // end not in range
    if (end.compareTo(actualStart) < 0 || end.compareTo(actualEnd) > 0) {
      endIntervalText.setError(rangeError);
      isValid = false;
    }

    // to < from
    if (start.compareTo(end) > 0) {
      String error = "'To' time must be higher than 'From' time";
      endIntervalText.setError(error);
      isValid = false;
    }

    return isValid;
  }

  @OnClick(R.id.cancel_button)
  public void onClickCancelButton(View view) {
    getDialog().dismiss();
  }

  @OnClick(R.id.continue_button)
  public void onClickContinueButton(View view) {
    resetAllError();

    if (isValidInput()) {
      DateTime start = new DateTime(1970, 1, 1, startHour, startMinute);
      DateTime end = new DateTime(1970, 1, 1, endHour, endMinute);

      listener.onConfirm(start, end);
      getDialog().dismiss();
    }
  }

  public interface Listener {

    public void onConfirm(DateTime start, DateTime end);
  }
}
