package id.meetu.meetu.module.schedule.view.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import id.meetu.meetu.R;
import id.meetu.meetu.base.BaseFragment;
import id.meetu.meetu.module.schedule.model.ScheduleInfo;
import id.meetu.meetu.module.schedule.presenter.SchedulePresenter;
import id.meetu.meetu.module.schedule.presenter.SchedulePresenterListener;
import id.meetu.meetu.module.schedule.view.adapter.ScheduleAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class ScheduleFragment extends BaseFragment implements SchedulePresenterListener {

  protected ScheduleAdapter adapter;
  @BindView(R.id.fragment_schedule)
  FrameLayout baseView;
  @BindView(R.id.layout_schedules)
  FrameLayout hideView;
  @BindView(R.id.card_list)
  RecyclerView recyclerView;
  @BindView(R.id.no_events)
  TextView noEventsTxtView;
  Map<String, List<ScheduleInfo>> dateMap;
  Set<DateTime> scheduleDays;
  DateTime currentDate;
  private SchedulePresenter presenter = new SchedulePresenter(this);

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    dateMap = new TreeMap<>();
    scheduleDays = new TreeSet<>();
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_schedule, container, false);
    ButterKnife.bind(this, view);
    progressBarView.setProgressBarBaseView(baseView);
    progressBarView.setProgressBarHideView(hideView);

    LinearLayoutManager llm = new LinearLayoutManager(getActivity());
    llm.setOrientation(LinearLayoutManager.VERTICAL);
    recyclerView.setLayoutManager(llm);
    recyclerView.setHasFixedSize(true);

    List<ScheduleInfo> scheduleInfo = new ArrayList<>();
    adapter = new ScheduleAdapter(getActivity(), scheduleInfo);

    onLoading();
    presenter.getSchedules();
    presenter.getMeetUps();

    return view;
  }

  public void checkEmptyList() {
    if (((ScheduleAdapter) recyclerView.getAdapter()).getScheduleList().isEmpty()) {
      recyclerView.setVisibility(View.GONE);
      noEventsTxtView.setVisibility(View.VISIBLE);
    } else {
      recyclerView.setVisibility(View.VISIBLE);
      noEventsTxtView.setVisibility(View.GONE);
    }
  }

  public void setScheduleByDate(DateTime date) {
    currentDate = date;
    String dateSelected = DateTimeFormat.forPattern("EEEE, dd MMMM").print(date);

    List<ScheduleInfo> scheduleOnDate = dateMap.get(dateSelected);

    adapter = new ScheduleAdapter(getActivity(),
        scheduleOnDate != null ? scheduleOnDate : new ArrayList<ScheduleInfo>());
    recyclerView.setAdapter(adapter);
    checkEmptyList();
  }

  public void updateDateMap(List<ScheduleInfo> schedules) {
    Set<String> seenDate = new TreeSet<>();

    for (ScheduleInfo schedule : schedules) {
      String date = DateTimeFormat.forPattern("EEEE, dd MMMM").print(schedule.getDate());
      List<ScheduleInfo> scheduleDate =
          dateMap.containsKey(date) ? dateMap.get(date) : new ArrayList<ScheduleInfo>();
      scheduleDate.add(schedule);
      seenDate.add(date);
      dateMap.put(date, scheduleDate);
    }

    for (String date : seenDate) {
      List<ScheduleInfo> scheduleInfos = dateMap.get(date);
      Collections.sort(scheduleInfos, new Comparator<ScheduleInfo>() {
        @Override
        public int compare(ScheduleInfo o1, ScheduleInfo o2) {
          return o1.getTime().compareTo(o2.getTime());
        }
      });
    }
  }

  @Override
  public void onError(String error) {
    onFinishLoading();
    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onGetSuccess(List<ScheduleInfo> schedules) {
    onFinishLoading();
    updateDateMap(schedules);
    setScheduleByDate(new DateTime());

    for (ScheduleInfo schedule : schedules) {
      scheduleDays.add(schedule.getDate());
    }
    CalendarFragment calendarFrag = (CalendarFragment) getFragmentManager()
        .findFragmentById(R.id.calendar);
    calendarFrag.updateCalendar(scheduleDays);
  }

  @Override
  public void onPostSuccess(String msg) {
  }

  @Override
  public void onPostSuccess(List<ScheduleInfo> schedules) {
  }

  @Override
  public void onDeleteSuccess(String message) {

  }

  @Override
  public void onLoading() {
    View view = getView();
    if (view != null) {
      getView().setClickable(false);
    }
    progressBarView.show();
  }

  @Override
  public void onFinishLoading() {
    View view = getView();
    if (view != null) {
      getView().setClickable(true);
    }
    if (getActivity() != null) {
      getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          Handler handler = new Handler();
          handler.postDelayed(new Runnable() {
            @Override
            public void run() {
              progressBarView.hide();
            }
          }, 500);
        }
      });
    }
  }
}
