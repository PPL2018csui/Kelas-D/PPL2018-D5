package id.meetu.meetu.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.service.ProfileApi.Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
public class ProfileApiTest {

  @Test
  public void testFindUrl() {
    String url = new ProfileApi().findUrl();

    assertEquals(BuildConfig.BACKEND_URL, url);
  }

  @Test
  public void testCreate() {
    Service service = new ProfileApi().create();

    assertNotNull(service);
    assertTrue(service instanceof Service);
  }
}

