package id.meetu.meetu.module.meetup.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class TimeRecommendationRequestTest {

  private TimeRecommendationRequest request;

  @Before
  public void setUp() {
    int groupId = 666;
    DateTime timeZone = new DateTime("1899-12-31T17:00:00Z");
    DateTime startDate = new DateTime("2018-04-11T17:00:00Z");
    DateTime endDate = new DateTime("2018-04-21T17:00:00Z");
    String repeatType = "ONCE";
    int repeatCount = 1;

    request = new TimeRecommendationRequest(groupId, timeZone, startDate, endDate, repeatType,
        repeatCount);
  }

  @Test
  public void testConstructModelWithParam() {
    int groupId = 666;
    DateTime timeZone = new DateTime("1899-12-31T17:00:00Z");
    DateTime startDate = new DateTime("2018-04-11T17:00:00Z");
    DateTime endDate = new DateTime("2018-04-21T17:00:00Z");
    String repeatType = "ONCE";
    int repeatCount = 1;

    TimeRecommendationRequest timeRecommendationRequest = new TimeRecommendationRequest(groupId,
        timeZone,
        startDate, endDate, repeatType, repeatCount);
    assertNotNull(timeRecommendationRequest);
  }

  @Test
  public void setterGetterGroupId() {
    int groupId = 777;
    request.setGroupId(groupId);

    assertEquals(groupId, request.getGroupId());
  }

  @Test
  public void setterGetterTimeZone() {
    DateTime timeZoneTest = new DateTime(1900, 1, 1, 0, 0);
    request.setTimeZone(timeZoneTest);

    assertEquals(timeZoneTest, request.getTimeZone());
  }


  @Test
  public void setterGetterStartDate() {
    DateTime startDateTest = new DateTime(2018, 4, 5, 1, 10, 0);
    request.setStartDate(startDateTest);

    assertEquals(startDateTest, request.getStartDate());
  }

  @Test
  public void setterGetterEndDate() {
    DateTime endDateTest = new DateTime(2018, 4, 5, 1, 10, 30);
    request.setEndDate(endDateTest);

    assertEquals(endDateTest, request.getEndDate());
  }

  @Test
  public void setterGetterRepeatType() {
    request.setRepeatType("DAILY");

    assertEquals("DAILY", request.getRepeatType());
  }

  @Test
  public void setterGetterRepeatCount() {
    request.setRepeatCount(2);

    assertEquals(2, request.getRepeatCount());
  }
}