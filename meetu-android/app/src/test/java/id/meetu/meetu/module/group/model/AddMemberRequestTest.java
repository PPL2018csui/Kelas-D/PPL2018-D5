package id.meetu.meetu.module.group.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class AddMemberRequestTest {

  private AddMemberRequest request;

  @Before
  public void setUp() {
    request = new AddMemberRequest(1, 2);
  }

  @Test
  public void testCreate() {
    request = new AddMemberRequest(3, 5);

    assertTrue(request instanceof AddMemberRequest);
    assertEquals(3, request.getUserId());
    assertEquals(5, request.getGroupId());
  }

  @Test
  public void testSetterGetterUserId() {
    request.setUserId(2);

    assertEquals(2, request.getUserId());
  }

  @Test
  public void testSetterGetterGroupId() {
    request.setGroupId(2);

    assertEquals(2, request.getGroupId());
  }
}
