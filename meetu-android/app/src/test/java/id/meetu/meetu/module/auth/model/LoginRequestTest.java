package id.meetu.meetu.module.auth.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 16/04/18.
 */
@RunWith(PowerMockRunner.class)
public class LoginRequestTest {

  @Test
  public void testConstructor() {
    LoginRequest request = new LoginRequest("aku", "ganteng");

    assertTrue(request instanceof LoginRequest);
    assertEquals("aku", request.getUsername());
    assertEquals("ganteng", request.getPassword());
  }

  @Test
  public void testSetterGetterEmail() {
    LoginRequest request = new LoginRequest("aku", "ganteng");
    request.setUsername("asd");

    assertEquals("asd", request.getUsername());
  }

  @Test
  public void testSetterGetterPassword() {
    LoginRequest request = new LoginRequest("aku", "ganteng");
    request.setPassword("asd");

    assertEquals("asd", request.getPassword());
  }
}
