package id.meetu.meetu.module.notifications.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import id.meetu.meetu.module.notification.model.NotificationResponse;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class NotificationResponseTest {

  private NotificationResponse response;

  @Before
  public void setUp() {
    response = new NotificationResponse();
  }

  @Test
  public void testConstruct() {
    response = new NotificationResponse();

    assertNotNull(response);
    assertTrue(response instanceof NotificationResponse);
  }

  @Test
  public void testSetterGetterTitle() {
    response.setTitle("makan batu");

    assertEquals("makan batu", response.getTitle());
  }

  @Test
  public void testSetterGetterNotificationType() {
    response.setNotificationType("grup");

    assertEquals("grup", response.getNotificationType());
  }

  @Test
  public void testSetterGetterInviter() {
    response.setInviter("ricky bau");

    assertEquals("ricky bau", response.getInviter());
  }

  @Test
  public void testSetterGetterRepetition() {
    response.setRepetition("setiap hari");

    assertEquals("setiap hari", response.getRepetition());
  }

  @Test
  public void testSetterGetterStartTime() {
    DateTime time = new DateTime();
    response.setStartTime(time);

    assertEquals(time, response.getStartTime());
  }

  @Test
  public void testSetterGetterEndTime() {
    DateTime time = new DateTime();
    response.setEndTime(time);

    assertEquals(time, response.getEndTime());
  }
}
