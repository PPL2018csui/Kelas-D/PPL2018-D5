package id.meetu.meetu.module.auth.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class GoogleSignInRequestTest {

  private GoogleSignInRequest request;

  @Before
  public void setUp() {
    request = new GoogleSignInRequest("123", "ayaz@gmail.com", "ayaz", "dz");
  }

  @Test
  public void testSetterGetterIdToken() {
    request.setIdToken("kentut");

    assertEquals("kentut", request.getIdToken());
  }
}
