package id.meetu.meetu.module.meetup.presenter;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.group.model.GroupDetail;
import id.meetu.meetu.module.group.model.GroupResponse;
import id.meetu.meetu.module.group.service.GroupService;
import id.meetu.meetu.module.meetup.contract.AddMeetUpFormContract;
import id.meetu.meetu.module.meetup.model.MeetUpFormData;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 17/04/18.
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class AddMeetupFormPresenterTest {

  private AddMeetUpFormContract.View viewMock;
  private AddMeetupFormPresenter presenter;
  private MeetUpFormData dataMock;
  private GroupService serviceMock;

  @Before
  public void setUp() {
    viewMock = PowerMockito.mock(AddMeetUpFormContract.View.class);
    serviceMock = PowerMockito.mock(GroupService.class);
    presenter = new AddMeetupFormPresenter(viewMock, serviceMock);
    dataMock = PowerMockito.mock(MeetUpFormData.class);

    when(dataMock.getMeetUpName()).thenReturn("makan");
    when(dataMock.getGroupName()).thenReturn("grup wibu");
    when(dataMock.getStartDate()).thenReturn(new DateTime(2018, 1, 1, 1, 1));
    when(dataMock.getEndDate()).thenReturn(new DateTime(2018, 1, 2, 1, 1, 2));
  }

  @Test
  public void testConstructor() {
    presenter = new AddMeetupFormPresenter(viewMock);

    assertTrue(presenter instanceof AddMeetupFormPresenter);
  }

  @Test
  public void testSuccessSubmit() {
    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(1)).onFinishSubmit(any(MeetUpFormData.class));
  }

  @Test
  public void testFailName() {
    when(dataMock.getMeetUpName()).thenReturn("");
    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).onFinishSubmit(any(MeetUpFormData.class));
    verify(viewMock, times(1)).setMeetupNameError(anyString());
  }

  @Test
  public void testFailGroup() {
    when(dataMock.getGroupName()).thenReturn("");
    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).onFinishSubmit(any(MeetUpFormData.class));
    verify(viewMock, times(1)).setGroupError(anyString());
  }

  @Test
  public void testFailStartDate() {
    when(dataMock.getStartDate()).thenReturn(null);
    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).onFinishSubmit(any(MeetUpFormData.class));
    verify(viewMock, times(1)).setStartDateError(anyString());
  }

  @Test
  public void testFailEndDate1() {
    when(dataMock.getEndDate()).thenReturn(null);
    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).onFinishSubmit(any(MeetUpFormData.class));
    verify(viewMock, times(1)).setEndDateError(anyString());

  }

  @Test
  public void testFailEndDate2() {
    when(dataMock.getStartDate()).thenReturn(new DateTime(2019, 1, 1, 1, 1, 1));
    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).onFinishSubmit(any(MeetUpFormData.class));
    verify(viewMock, times(1)).setEndDateError(anyString());
  }

  @Test
  public void testFetchGroupsSuccess() {
    final Call<List<GroupResponse>> callGroupMock = mock(Call.class);

    GroupResponse groupResponse = new GroupResponse(2, 1, "meetu peeps");
    final List<GroupResponse> responseBody = new ArrayList<>();
    responseBody.add(groupResponse);

    GroupDetail groupDetail = new GroupDetail(2, "meetu peeps", 1);

    final Response<List<GroupResponse>> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(responseBody);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<GroupResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callGroupMock, responseMock);

        return null;
      }
    }).when(serviceMock).getGroups(any(Callback.class));

    presenter.fetchGroups();

    ArgumentCaptor<List> argument = ArgumentCaptor.forClass(List.class);
    verify(viewMock, times(1)).populateGroup(argument.capture());

    GroupDetail resultDetail = (GroupDetail) argument.getValue().get(0);

    assertEquals(groupDetail.getId(), resultDetail.getId());
    assertEquals(groupDetail.getTitle(), resultDetail.getTitle());
    assertEquals(groupDetail.getOwner(), resultDetail.getOwner());
  }

  @Test
  public void testFetchGroupsNotSuccess() {
    final Call<List<GroupResponse>> callGroupMock = mock(Call.class);
    final Response<List<GroupResponse>> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<GroupResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callGroupMock, responseMock);

        return null;
      }
    }).when(serviceMock).getGroups(any(Callback.class));

    presenter.fetchGroups();

    verify(viewMock, times(1)).onFetchGroupError("Something is wrong");
  }

  @Test
  public void testFetchGroupsFailure() {
    final Call<List<GroupResponse>> callGroupMock = mock(Call.class);
    final Throwable throwableMock = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<GroupResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callGroupMock, throwableMock);

        return null;
      }
    }).when(serviceMock).getGroups(any(Callback.class));

    presenter.fetchGroups();

    verify(viewMock, times(1)).onFetchGroupError("Failed to retrieve your groups");
  }
}
