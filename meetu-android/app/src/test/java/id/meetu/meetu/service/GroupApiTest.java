package id.meetu.meetu.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.service.GroupApi.Service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;


/**
 * Created by Andre on 4/11/2018.
 */

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
public class GroupApiTest {

  @Test
  public void testFindUrl() {
    String url = new GroupApi().findUrl();

    assertEquals(BuildConfig.BACKEND_URL, url);
  }

  @Test
  public void testCreate() {
    Service service = new GroupApi().create();

    assertNotNull(service);
    assertTrue(service instanceof Service);
  }
}

