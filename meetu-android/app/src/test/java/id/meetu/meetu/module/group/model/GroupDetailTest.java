package id.meetu.meetu.module.group.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import id.meetu.meetu.module.group.model.GroupDetail;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by Andre on 4/11/2018.
 */

@RunWith(PowerMockRunner.class)
public class GroupDetailTest {

  @Test
  public void testConstructGroupDetail() {
    GroupDetail detail = new GroupDetail();
    assertNotNull(detail);
  }

  @Test
  public void testConstructMeetUpDetaiWithParaml() {
    int id = 0;
    String title = "Title";
    int owner = 1;

    GroupDetail detail = new GroupDetail(id, title, owner);
    assertNotNull(detail);
  }

  @Test
  public void testSetterGetterId() {
    GroupDetail detail = new GroupDetail();
    detail.setId(1);
    assertEquals(detail.getId(), 1);
  }

  @Test
  public void testSetterGetterTitle() {
    GroupDetail detail = new GroupDetail();
    detail.setTitle("Title1");
    assertEquals(detail.getTitle(), "Title1");
  }

  @Test
  public void testSetterGetterOwner() {
    GroupDetail detail = new GroupDetail();
    detail.setOwner(1);
    assertEquals(detail.getOwner(), 1);
  }
}
