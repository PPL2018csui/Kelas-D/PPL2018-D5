package id.meetu.meetu.module.group.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

import id.meetu.meetu.module.group.model.GroupDetail;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

@RunWith(PowerMockRunner.class)
public class GroupMemberDetailTest {

  @Test
  public void testConstructGroupMemberDetail() {
    GroupMemberDetail detail = new GroupMemberDetail();
    assertNotNull(detail);
  }

  @Test
  public void testConstructGroupMemberDetailWithParam() {
    GroupMemberDetail detail = new GroupMemberDetail(1, "Luthfi Sulaiman", "asd");
    assertNotNull(detail);
  }

  @Test
  public void testSetterGetterId() {
    GroupMemberDetail detail = new GroupMemberDetail();
    detail.setId(1);
    assertEquals(detail.getId(), 1);
  }

  @Test
  public void testSetterGetterName() {
    GroupMemberDetail detail = new GroupMemberDetail();
    detail.setName("Luthfi");
    assertEquals(detail.getName(), "Luthfi");
  }

  @Test
  public void testSetterGetterProfileImageUrl() {
    GroupMemberDetail detail = new GroupMemberDetail();
    detail.setProfileImageUrl("asdasd");
    assertEquals("asdasd", detail.getProfileImageUrl());
  }
}
