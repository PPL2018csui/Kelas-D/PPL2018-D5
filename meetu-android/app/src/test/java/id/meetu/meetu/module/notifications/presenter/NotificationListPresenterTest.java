package id.meetu.meetu.module.notifications.presenter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.notification.contract.NotificationListContract;
import id.meetu.meetu.module.notification.model.NotificationDetail;
import id.meetu.meetu.module.notification.model.NotificationResponse;
import id.meetu.meetu.module.notification.presenter.NotificationListPresenter;
import id.meetu.meetu.module.notification.service.NotificationService;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class NotificationListPresenterTest {

  private NotificationResponse groupNotifMock;
  private NotificationResponse meetupNotifMock;
  private NotificationListContract.View viewMock;
  private NotificationService notificationServiceMock;

  private NotificationListPresenter presenter;

  @Before
  public void setUp() {
    viewMock = mock(NotificationListContract.View.class);
    notificationServiceMock = mock(NotificationService.class);
    groupNotifMock = mock(NotificationResponse.class);
    meetupNotifMock = mock(NotificationResponse.class);

    when(groupNotifMock.getNotificationType()).thenReturn("Group");
    when(groupNotifMock.getTitle()).thenReturn("asosiasi badak");
    when(groupNotifMock.getInviter()).thenReturn("badak terbang");

    when(meetupNotifMock.getNotificationType()).thenReturn("MeetUp");
    when(meetupNotifMock.getTitle()).thenReturn("rapat badak");
    when(meetupNotifMock.getInviter()).thenReturn("PI badak");
    when(meetupNotifMock.getStartTime()).thenReturn(new DateTime(2018, 5, 15, 20, 0));
    when(meetupNotifMock.getEndTime()).thenReturn(new DateTime(2018, 5, 15, 23, 55));
    when(meetupNotifMock.getRepetition()).thenReturn("tiap hari badak");

    presenter = new NotificationListPresenter(viewMock, notificationServiceMock);
  }

  @Test
  public void testConstruct() {
    presenter = new NotificationListPresenter(viewMock);

    assertNotNull(presenter);
    assertTrue(presenter instanceof NotificationListPresenter);
  }

  @Test
  public void testCreateMeetUpDetail() {
    NotificationDetail meetup = presenter.createMeetUpDetail(1, meetupNotifMock);

    assertEquals(1, meetup.getId());
    assertEquals("You Are Invited!", meetup.getHeader());
    assertEquals("rapat badak", meetup.getTitle());
    assertEquals("PI badak", meetup.getGroups());
    assertEquals("From Tuesday, 15 May 2018", meetup.getDate());
    assertEquals("20.00 - 23.55", meetup.getLocationTime());
    assertEquals("tiap hari badak", meetup.getRepetition());
  }

  @Test
  public void testCreateGroupDetail() {
    NotificationDetail group = presenter.createGroupDetail(2, groupNotifMock);

    assertEquals(2, group.getId());
    assertEquals("You Have Been Invited!", group.getHeader());
    assertEquals("asosiasi badak", group.getGroups());
    assertEquals("badak terbang", group.getPerson());
  }
  @Test
  public void testGetNotificationsSuccess() {
    final Call<List<NotificationResponse>> callMock = mock(Call.class);
    final Response<List<NotificationResponse>> responseMock = mock(Response.class);
    final List<NotificationResponse> body = new ArrayList<>();
    body.add(meetupNotifMock);
    body.add(groupNotifMock);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(body);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<NotificationResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(notificationServiceMock).getNotifications(any(Callback.class));

    presenter.getNotifications();

    verify(viewMock, times(1)).onGetNotificationsSuccess(anyList());

  }


  @Test
  public void testGetNotificationsNotSuccess() {
    final Call<List<NotificationResponse>> callMock = mock(Call.class);
    final Response<List<NotificationResponse>> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<NotificationResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(notificationServiceMock).getNotifications(any(Callback.class));

    presenter.getNotifications();

    verify(viewMock, times(1)).onGetNotificationsError("Failed to get notifications");
  }

  @Test
  public void testGetNotificationsFailure() {
    final Call<List<NotificationResponse>> callMock = mock(Call.class);
    final Throwable throwableMock = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<NotificationResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callMock, throwableMock);

        return null;
      }
    }).when(notificationServiceMock).getNotifications(any(Callback.class));

    presenter.getNotifications();

    verify(viewMock, times(1)).onGetNotificationsError("Something is wrong with your connection");
  }
}
