package id.meetu.meetu.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import id.meetu.meetu.BuildConfig;
import id.meetu.meetu.service.NotificationApi.Service;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
public class NotificationApiTest {

  public void testFindUrl() {
    String url = new NotificationApi().findUrl();

    assertEquals(BuildConfig.BACKEND_URL, url);
  }

  @Test
  public void testCreate() {
    Service service = new NotificationApi().create();

    assertNotNull(service);
    assertTrue(service instanceof Service);
  }
}
