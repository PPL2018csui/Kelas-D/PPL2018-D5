package id.meetu.meetu.module.schedule.model;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 12/03/18.
 */

@RunWith(PowerMockRunner.class)
public class ScheduleResponseTest {

  private ScheduleResponse scheduleResponse;

  @Before
  public void setup() {
    String title = "asd";
    DateTime startTime = new DateTime(2018, 3, 1, 0, 0);
    DateTime endTime = new DateTime(2018, 3, 2, 0, 0);

    scheduleResponse = new ScheduleResponse(title, startTime, endTime);
  }

  @Test
  public void testConstructScheduleResponse() {
    String title = "asd";
    DateTime startTime = new DateTime(2018, 3, 1, 0, 0);
    DateTime endTime = new DateTime(2018, 3, 2, 0, 0);

    ScheduleResponse response = new ScheduleResponse(title, startTime, endTime);

    assertEquals(title, response.getTitle());
    assertEquals(startTime, response.getStartTime());
    assertEquals(endTime, response.getEndTime());
  }

  @Test
  public void testSetterGetterTitle() {
    String title = "lol";

    scheduleResponse.setTitle(title);
    assertEquals(title, scheduleResponse.getTitle());
  }

  @Test
  public void testSetterGetterStartTime() {
    DateTime startTime = new DateTime(2018, 3, 3, 0, 0);

    scheduleResponse.setStartTime(startTime);
    assertEquals(startTime, scheduleResponse.getStartTime());
  }

  @Test
  public void testSetterGetterEndTime() {
    DateTime endTime = new DateTime(2018, 3, 3, 0, 0);

    scheduleResponse.setEndTime(endTime);
    assertEquals(endTime, scheduleResponse.getEndTime());
  }

}
