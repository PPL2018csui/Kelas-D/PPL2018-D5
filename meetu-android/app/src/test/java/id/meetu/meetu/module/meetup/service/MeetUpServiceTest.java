package id.meetu.meetu.module.meetup.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.group.model.GroupResponse;
import id.meetu.meetu.module.meetup.model.MeetUpResponse;
import id.meetu.meetu.service.MeetUpApi;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 27/03/18.
 */

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
public class MeetUpServiceTest {

  @Mock
  private MeetUpApi.Service serviceMock;

  private MeetUpService meetUpService;

  @Before
  public void setUp() {
    serviceMock = mock(MeetUpApi.Service.class);
    meetUpService = new MeetUpService(serviceMock);
  }

  @Test
  public void testConstructMeetUpService1() {
    meetUpService = new MeetUpService();

    assertTrue(meetUpService instanceof MeetUpService);
  }

  @Test
  public void testConstructMeetUpService2() {
    meetUpService = new MeetUpService(serviceMock);

    assertTrue(meetUpService instanceof MeetUpService);
    assertEquals(serviceMock, meetUpService.getService());
  }

  @Test
  public void testGetMeetUps() {
    final Call<List<MeetUpResponse>> callMeetUpMock = mock(Call.class);
    final Callback<List<MeetUpResponse>> callbackMeetUpMock = mock(Callback.class);
    final List<MeetUpResponse> response = new ArrayList<>();

    when(serviceMock.getMeetUps()).thenReturn(callMeetUpMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<MeetUpResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMeetUpMock, Response.success(response));

        return null;
      }
    }).when(callMeetUpMock).enqueue(any(Callback.class));

    meetUpService.getMeetUps(callbackMeetUpMock);

    verify(callbackMeetUpMock, times(1)).onResponse(any(Call.class), any(Response.class));
  }

  @Test
  public void testPostMeetUps() {
    final Call<BaseResponse> callMock = mock(Call.class);
    final Callback<BaseResponse> callbackMock = mock(Callback.class);
    final BaseResponse response = new BaseResponse();

    when(serviceMock.postMeetUps(any(List.class))).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, Response.success(response));

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    meetUpService.postMeetUps(any(List.class), callbackMock);

    verify(callbackMock, times(1)).onResponse(any(Call.class), any(Response.class));
  }

  @Test
  public void testDeleteMeetUps() {
    final Call<MeetUpResponse> callMeetUpMock = mock(Call.class);
    final Callback<MeetUpResponse> callbackMeetUpMock = mock(Callback.class);
    when(serviceMock.deleteMeetUps(anyMap())).thenReturn(callMeetUpMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<MeetUpResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMeetUpMock,
            Response
                .success(new MeetUpResponse("ppl", new GroupResponse(1, 2, "meetu"), new DateTime(),
                    new DateTime())));

        return null;
      }
    }).when(callMeetUpMock).enqueue(any(Callback.class));

    meetUpService.deleteMeetUps("123456789", callbackMeetUpMock);

    verify(callbackMeetUpMock, times(1)).onResponse(any(Call.class), any(Response.class));
  }
}
