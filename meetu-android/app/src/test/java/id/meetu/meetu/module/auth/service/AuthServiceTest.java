package id.meetu.meetu.module.auth.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.GoogleSignInRequest;
import id.meetu.meetu.module.auth.model.LoginRequest;
import id.meetu.meetu.module.auth.model.RegisterRequest;
import id.meetu.meetu.module.auth.model.UpdateTokenRequest;
import id.meetu.meetu.service.AuthApi;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 16/04/18.
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class AuthServiceTest {

  @Mock
  private AuthApi.Service serviceMock;

  private AuthService authService;

  @Before
  public void setUp() {
    serviceMock = mock(AuthApi.Service.class);
    authService = new AuthService(serviceMock);
  }

  @Test
  public void testConstruct1() {
    authService = new AuthService();

    assertTrue(authService instanceof AuthService);
  }

  @Test
  public void testConstruct2() {
    authService = new AuthService(serviceMock);

    assertTrue(authService instanceof AuthService);
    assertEquals(serviceMock, authService.getService());
  }

  @Test
  public void testPostLogin() {
    final Call<AuthResponse> callMock = mock(Call.class);
    Callback<AuthResponse> callbackMock = mock(Callback.class);
    final Response responseMock = mock(Response.class);
    LoginRequest request = mock(LoginRequest.class);

    when(serviceMock.postLogin(any(LoginRequest.class))).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    authService.postLogin(callbackMock, request);

    verify(callbackMock, times(1)).onResponse(callMock, responseMock);
  }

  @Test
  public void testPostRegister() {
    final Call<AuthResponse> callMock = mock(Call.class);
    Callback<AuthResponse> callbackMock = mock(Callback.class);
    final Response responseMock = mock(Response.class);
    RegisterRequest request = mock(RegisterRequest.class);

    when(serviceMock.postRegister(any(RegisterRequest.class))).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    authService.postRegister(callbackMock, request);

    verify(callbackMock, times(1)).onResponse(callMock, responseMock);
  }

  @Test
  public void testPostGoogleSignIn() {
    final Call<AuthResponse> callMock = mock(Call.class);
    Callback<AuthResponse> callbackMock = mock(Callback.class);
    final Response responseMock = mock(Response.class);
    GoogleSignInRequest request = mock(GoogleSignInRequest.class);

    when(serviceMock.postGoogleSignIn(any(GoogleSignInRequest.class))).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    authService.postGoogleSignIn(callbackMock, request);

    verify(callbackMock, times(1)).onResponse(callMock, responseMock);
  }

  @Test
  public void testUpdateFirebaseToken() {
    final Call<Void> callMock = mock(Call.class);
    Callback<Void> callbackMock = mock(Callback.class);
    final Response responseMock = mock(Response.class);
    UpdateTokenRequest request = mock(UpdateTokenRequest.class);

    when(serviceMock.updateFirebaseToken(any(UpdateTokenRequest.class))).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<Void> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    authService.updateFirebaseToken(callbackMock, request);

    verify(callbackMock, times(1)).onResponse(callMock, responseMock);
  }
}
