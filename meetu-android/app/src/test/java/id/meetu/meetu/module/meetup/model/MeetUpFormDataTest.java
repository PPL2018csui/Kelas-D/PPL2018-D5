package id.meetu.meetu.module.meetup.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertTrue;

import id.meetu.meetu.util.DateUtil.RepeatType;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 17/04/18.
 */
@RunWith(PowerMockRunner.class)
public class MeetUpFormDataTest {

  @Test
  public void testConstructor() {
    MeetUpFormData data = new MeetUpFormData();

    assertTrue(data instanceof MeetUpFormData);
  }

  @Test
  public void testBuilder() {
    String name = "aku";
    int groupId = 1;
    String groupName = "petani meme";
    int groupOwner = 2;
    DateTime startDate = new DateTime();
    DateTime endDate = new DateTime();
    RepeatType repeatType = RepeatType.ONCE;
    int repeatCount = 1;
    DateTime startTime = new DateTime();
    DateTime endTime = new DateTime();

    MeetUpFormData data = new MeetUpFormData.Builder().setMeetUpName(name).setGroupId(groupId)
        .setGroupName(groupName).setGroupOwner(groupOwner)
        .setStartDate(startDate).setEndDate(endDate).setRepeatType(repeatType)
        .setRepeatCount(repeatCount).setStartTime(startTime).setEndTime(endTime).create();

    assertEquals(name, data.getMeetUpName());
    assertEquals(groupName, data.getGroupName());
    assertEquals(groupId, data.getGroupId());
    assertEquals(startDate, data.getStartDate());
    assertEquals(endDate, data.getEndDate());
    assertEquals(repeatType, data.getRepeatType());
    assertEquals(repeatCount, data.getRepeatCount());
    assertEquals(startTime, data.getStartTime());
    assertEquals(endTime, data.getEndTime());
  }

  @Test
  public void testSetterGetterName() {
    MeetUpFormData data = new MeetUpFormData();
    data.setMeetUpName("nama");

    assertEquals("nama", data.getMeetUpName());
  }

  @Test
  public void testSetterGetterGroupId() {
    MeetUpFormData data = new MeetUpFormData();
    data.setGroupId(3);

    assertEquals(3, data.getGroupId());
  }

  @Test
  public void testSetterGetterGroupName() {
    MeetUpFormData data = new MeetUpFormData();
    data.setGroupName("grup");

    assertEquals("grup", data.getGroupName());
  }

  @Test
  public void testSetterGetterGroupOwner() {
    MeetUpFormData data = new MeetUpFormData();
    data.setGroupOwner(3);

    assertEquals(3, data.getGroupOwner());
  }

  @Test
  public void testSetterGetterStartDate() {
    MeetUpFormData data = new MeetUpFormData();
    DateTime date = new DateTime(2018, 1, 1, 1, 1);
    data.setStartDate(date);

    assertEquals(date, data.getStartDate());
  }

  @Test
  public void testSetterGetterEndDate() {
    MeetUpFormData data = new MeetUpFormData();
    DateTime date = new DateTime(2018, 1, 1, 1, 1);
    data.setEndDate(date);

    assertEquals(date, data.getEndDate());
  }

  @Test
  public void testSetterGetterRepeatType() {
    MeetUpFormData data = new MeetUpFormData();
    RepeatType type = RepeatType.ONCE;
    data.setRepeatType(type);

    assertEquals(RepeatType.ONCE, data.getRepeatType());
  }

  @Test
  public void testSetterGetterRepeatCount() {
    MeetUpFormData data = new MeetUpFormData();
    data.setRepeatCount(1);

    assertEquals(1, data.getRepeatCount());
  }

  @Test
  public void testSetterGetterStartTime() {
    MeetUpFormData data = new MeetUpFormData();
    DateTime date = new DateTime(2018, 1, 1, 1, 1);
    data.setStartTime(date);

    assertEquals(date, data.getStartTime());
  }

  @Test
  public void testSetterGetterEndTime() {
    MeetUpFormData data = new MeetUpFormData();
    DateTime date = new DateTime(2018, 1, 1, 1, 1);
    data.setEndTime(date);

    assertEquals(date, data.getEndTime());
  }
}
