package id.meetu.meetu.module.auth.presenter;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.auth.contract.AuthContract;
import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.GoogleSignInRequest;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.service.AuthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class AuthPresenterTest {

  private AuthContract.View viewMock;
  private AuthService serviceMock;
  private AuthPresenter presenter;

  @Before
  public void setUp() {
    viewMock = mock(AuthContract.View.class);
    serviceMock = mock(AuthService.class);
    presenter = new AuthPresenter(viewMock, serviceMock);
  }

  @Test
  public void testConstruct() {
    presenter = new AuthPresenter(viewMock);

    assertTrue(presenter instanceof AuthPresenter);
  }

  @Test
  public void testGoogleSignInSuccess() {
    final Call<AuthResponse> callMock = mock(Call.class);
    final Response<AuthResponse> responseMock = mock(Response.class);
    AuthResponse bodyMock = mock(AuthResponse.class);
    User userMock = mock(User.class);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(bodyMock);

    when(bodyMock.getToken()).thenReturn("asd");
    when(bodyMock.getUser()).thenReturn(userMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(serviceMock).postGoogleSignIn(any(Callback.class), any(GoogleSignInRequest.class));

    presenter.googleSignIn("token", "email", "first", "last");

    verify(viewMock, times(1)).onFinishGoogleSignIn(anyString(), any(User.class));
  }

  @Test
  public void testGoogleSignInSuccessNullName() {
    final Call<AuthResponse> callMock = mock(Call.class);
    final Response<AuthResponse> responseMock = mock(Response.class);
    AuthResponse bodyMock = mock(AuthResponse.class);
    User userMock = mock(User.class);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(bodyMock);

    when(bodyMock.getToken()).thenReturn("asd");
    when(bodyMock.getUser()).thenReturn(userMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(serviceMock).postGoogleSignIn(any(Callback.class), any(GoogleSignInRequest.class));

    presenter.googleSignIn("token", "email", null, null);

    verify(viewMock, times(1)).onFinishGoogleSignIn(anyString(), any(User.class));
  }

  @Test
  public void testGoogleSignInNotSuccess() {
    final Call<AuthResponse> callMock = mock(Call.class);
    final Response<AuthResponse> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(serviceMock).postGoogleSignIn(any(Callback.class), any(GoogleSignInRequest.class));

    presenter.googleSignIn("token", "email", "first", "last");

    verify(viewMock, times(1)).onErrorGoogleSignIn("Failed to sign in via Google");
  }

  @Test
  public void testGoogleSignInFailure() {
    final Call<AuthResponse> callMock = mock(Call.class);
    final Throwable throwableMock = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callMock, throwableMock);

        return null;
      }
    }).when(serviceMock).postGoogleSignIn(any(Callback.class), any(GoogleSignInRequest.class));

    presenter.googleSignIn("token", "email", "first", "last");

    verify(viewMock, times(1)).onErrorGoogleSignIn("Something is wrong with your connection");
  }
}
