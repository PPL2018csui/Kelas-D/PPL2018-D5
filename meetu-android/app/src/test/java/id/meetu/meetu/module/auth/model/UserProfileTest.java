package id.meetu.meetu.module.auth.model;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import id.meetu.meetu.module.group.model.Group;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ricky on 12/03/18.
 */

@RunWith(PowerMockRunner.class)
public class UserProfileTest {

  @Test
  public void testConstructModel() {
    UserProfile userProfile = new UserProfile();
    assertNotNull(userProfile);
  }

  @Test
  public void testSetterGetterId() {
    UserProfile userProfile = new UserProfile();
    userProfile.setId(1);
    assertEquals(1, userProfile.getId());
  }

  @Test
  public void testSetterGetterUser() {
    UserProfile userProfile = new UserProfile();
    userProfile.setUser(1);
    assertEquals(1, userProfile.getUser());
  }

  @Test
  public void testSetterGetterPhoneNumber() {
    UserProfile userProfile = new UserProfile();
    userProfile.setPhoneNumber("08123123123");
    assertEquals(userProfile.getPhoneNumber(), "08123123123");
  }

  @Test
  public void testSetterGetterLocation() {
    UserProfile userProfile = new UserProfile();
    userProfile.setLocation("Jakarta Pusat");
    assertEquals(userProfile.getLocation(), "Jakarta Pusat");
  }

  @Test
  public void testSetterGetterGroups() {
    UserProfile userProfile = new UserProfile();
    List<Group> groups = new ArrayList<>();
    userProfile.setGroups(groups);
    assertEquals(userProfile.getGroups(), groups);
  }

  @Test
  public void testSetterGetterImageUrl() {
    UserProfile userProfile = new UserProfile();
    String url = "www.google.com";

    userProfile.setProfileImageUrl(url);
    assertEquals(url, userProfile.getProfileImageUrl());
  }
}
