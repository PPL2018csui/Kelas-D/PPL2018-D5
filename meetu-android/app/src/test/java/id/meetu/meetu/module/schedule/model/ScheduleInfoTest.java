package id.meetu.meetu.module.schedule.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by angelica on 21/03/18.
 */

@RunWith(PowerMockRunner.class)
public class ScheduleInfoTest {

  @Test
  public void testConstructScheduleInfo() {
    ScheduleInfo detail = new ScheduleInfo();
    assertNotNull(detail);
  }

  @Test
  public void testConstructScheduleInfoWithParam() {
    int id = 0;
    String title = "Title";
    DateTime date = null;
    String time = "Time";

    ScheduleInfo detail = new ScheduleInfo(id, title, date, time);
    ScheduleInfo detail2 = new ScheduleInfo(id, title, date, time, false);

    assertNotNull(detail);
    assertNotNull(detail2);
  }

  @Test
  public void testSetterGetterId() {
    ScheduleInfo detail = new ScheduleInfo();
    detail.setId(1);
    assertEquals(detail.getId(), 1);
  }

  @Test
  public void testSetterGetterTitle() {
    ScheduleInfo detail = new ScheduleInfo();
    detail.setTitle("Title1");
    assertEquals(detail.getTitle(), "Title1");
  }

  @Test
  public void testSetterGetterDate() {
    ScheduleInfo detail = new ScheduleInfo();
    DateTime date = new DateTime(2018, 4, 1, 0, 0);
    detail.setDate(date);
    assertNotNull(detail.getDate());
  }

  @Test
  public void testSetterGetterTime() {
    ScheduleInfo detail = new ScheduleInfo();
    detail.setTime("Time1");
    assertEquals(detail.getTime(), "Time1");
  }

  @Test
  public void testSetterGetterIsMeetup() {
    ScheduleInfo detail = new ScheduleInfo();
    detail.setMeetup(false);
    assertEquals(false, detail.isMeetup());
  }
}