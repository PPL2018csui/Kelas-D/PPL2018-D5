package id.meetu.meetu.module.group.presenter;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.group.contract.CreateGroupContract;
import id.meetu.meetu.module.group.model.CreateGroupRequest;
import id.meetu.meetu.module.group.model.GroupMemberDetail;
import id.meetu.meetu.module.group.model.GroupMemberResponse;
import id.meetu.meetu.module.group.model.GroupMembers;
import id.meetu.meetu.module.group.service.GroupService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class CreateGroupPresenterTest {

  private CreateGroupPresenter presenter;
  private GroupService groupServiceMock;
  private CreateGroupContract.View viewMock;

  @Before
  public void setUp() {
    viewMock = mock(CreateGroupContract.View.class);
    groupServiceMock = mock(GroupService.class);

    presenter = new CreateGroupPresenter(viewMock, groupServiceMock);
  }

  @Test
  public void testConstruct() {
    presenter = new CreateGroupPresenter(viewMock);

    assertNotNull(presenter);
  }

  @Test
  public void testEmptyName() {
    GroupMembers members = mock(GroupMembers.class);
    when(members.getTitle()).thenReturn("");

    presenter.submit(members);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(1)).setGroupNameError("This field can't be empty");
  }

  @Test
  public void testCreateGroupSuccess() {
    final Call<GroupMemberResponse> callMock = mock(Call.class);
    final Response<GroupMemberResponse> responseMock = mock(Response.class);
    String title = "budak PPL";
    List<GroupMemberDetail> member = new ArrayList<>();
    member.add(new GroupMemberDetail(1, "ayaze", "ganteng"));

    GroupMembers memberMock = mock(GroupMembers.class);
    when(memberMock.getTitle()).thenReturn(title);
    when(memberMock.getGroupMember()).thenReturn(member);

    when(responseMock.isSuccessful()).thenReturn(true);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(groupServiceMock).createGroup(any(Callback.class), any(CreateGroupRequest.class));

    presenter.submit(memberMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).setGroupNameError(anyString());
    verify(viewMock, times(1)).onCreateSuccess("Group " + title + " created");
  }

  @Test
  public void testCreateGroupNotSuccess() {
    final Call<GroupMemberResponse> callMock = mock(Call.class);
    final Response<GroupMemberResponse> responseMock = mock(Response.class);
    String title = "budak PPL";

    GroupMembers memberMock = mock(GroupMembers.class);
    when(memberMock.getTitle()).thenReturn(title);
    when(memberMock.getGroupMember()).thenReturn(new ArrayList<GroupMemberDetail>());

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(groupServiceMock).createGroup(any(Callback.class), any(CreateGroupRequest.class));

    presenter.submit(memberMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).setGroupNameError(anyString());
    verify(viewMock, times(1)).onCreateError("Failed to create group");
  }

  @Test
  public void testCreateGroupFailure() {
    final Call<GroupMemberResponse> callMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);
    String title = "budak PPL";

    GroupMembers memberMock = mock(GroupMembers.class);
    when(memberMock.getTitle()).thenReturn(title);
    when(memberMock.getGroupMember()).thenReturn(new ArrayList<GroupMemberDetail>());

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callMock, throwable);

        return null;
      }
    }).when(groupServiceMock).createGroup(any(Callback.class), any(CreateGroupRequest.class));

    presenter.submit(memberMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).setGroupNameError(anyString());
    verify(viewMock, times(1)).onCreateError("Something is wrong with your connection");
  }
}
