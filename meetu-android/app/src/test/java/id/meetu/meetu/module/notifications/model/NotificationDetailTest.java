package id.meetu.meetu.module.notifications.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import id.meetu.meetu.module.notification.model.NotificationDetail;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by luthfi on 09/05/18.
 */

@RunWith(PowerMockRunner.class)
public class NotificationDetailTest {

  @Test
  public void testConstructNotificationDetail() {
    NotificationDetail detail = new NotificationDetail();
    assertNotNull(detail);
  }

  @Test
  public void testConstructNotificationDetailWithParam1() {
    int id = 0;
    int notificationType = 0;
    String header = "Header";
    String title = "Title";
    String groups = "Groups";
    String date = "Date";
    String locationTime = "Location Time";
    String repetition = "ini repetition";

    NotificationDetail detail = new NotificationDetail(id, notificationType, header, title, groups, date, locationTime, repetition, new DateTime());
    assertNotNull(detail);
  }

  @Test
  public void testConstructNotificationDetailWithParam2() {
    int id = 0;
    int notificationType = 1;
    String header = "Header";
    String groups = "Groups";
    String person = "Luthfi";

    NotificationDetail detail = new NotificationDetail(id, notificationType, header, groups, person);
    assertNotNull(detail);
  }

  @Test
  public void testSetterGetterHeader() {
    NotificationDetail detail = new NotificationDetail();
    detail.setHeader("Header");
    assertEquals(detail.getHeader(), "Header");
  }

  @Test
  public void testSetterGetterNotificationType() {
    NotificationDetail detail = new NotificationDetail();
    detail.setNotificationType(1);
    assertEquals(detail.getNotificationType(), 1);
  }

  @Test
  public void testSetterGetterPerson() {
    NotificationDetail detail = new NotificationDetail();
    detail.setPerson("Luthfi");
    assertEquals(detail.getPerson(), "Luthfi");
  }

  @Test
  public void testSetterGetterRepetition() {
    NotificationDetail detail = new NotificationDetail();
    detail.setRepetition("ngulang aja terus");

    assertEquals("ngulang aja terus", detail.getRepetition());
  }

  @Test
  public void testSetterGetterDateTime() {
    NotificationDetail detail = new NotificationDetail();
    detail.setDateTime(new DateTime());

    assertNotNull(detail.getDateTime());
  }
}
