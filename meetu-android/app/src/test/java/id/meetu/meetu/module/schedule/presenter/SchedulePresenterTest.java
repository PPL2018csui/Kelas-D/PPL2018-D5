package id.meetu.meetu.module.schedule.presenter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import edu.emory.mathcs.backport.java.util.Arrays;
import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.meetup.model.MeetUpResponse;
import id.meetu.meetu.module.meetup.service.MeetUpService;
import id.meetu.meetu.module.schedule.model.ScheduleInfo;
import id.meetu.meetu.module.schedule.model.ScheduleResponse;
import id.meetu.meetu.module.schedule.service.ScheduleService;
import id.meetu.meetu.util.DateInterval;
import id.meetu.meetu.util.DateUtil;
import id.meetu.meetu.util.DateUtil.RepeatType;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 14/03/18.
 */

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest({DateUtil.class, Response.class})
public class SchedulePresenterTest {

  @Mock
  private SchedulePresenterListener listenerMock;

  @Mock
  private ScheduleService scheduleServiceMock;

  @Mock
  private MeetUpService meetUpServiceMock;

  private SchedulePresenter schedulePresenter;

  @Before
  public void setUp() {
    mockStatic(ScheduleService.class);
    mockStatic(DateUtil.class);

    listenerMock = mock(SchedulePresenterListener.class);
    scheduleServiceMock = mock(ScheduleService.class);
    meetUpServiceMock = mock(MeetUpService.class);
    schedulePresenter = new SchedulePresenter(listenerMock, scheduleServiceMock, meetUpServiceMock);

    DateTime start = new DateTime(2018, 3, 1, 1, 0);
    DateTime end = new DateTime(2018, 3, 1, 2, 0);
    DateInterval interval = new DateInterval(start, end);

    List<DateInterval> dummyIntervals = new ArrayList<>();
    dummyIntervals.add(interval);

    PowerMockito.when(DateUtil
        .getEventRepetition(any(DateTime.class), any(DateTime.class), any(RepeatType.class),
            anyInt()))
        .thenReturn(dummyIntervals);

    PowerMockito.when(DateUtil
        .getHourMinuteString(anyInt(), anyInt(), anyString()))
        .thenReturn("02.34");
  }

  @Test
  public void testConstructSchedulePresenter1() {
    schedulePresenter = new SchedulePresenter(listenerMock);

    assertTrue(schedulePresenter instanceof SchedulePresenter);
    assertEquals(schedulePresenter.getListener(), listenerMock);
  }

  @Test
  public void testConstructSchedulePresenter2() {
    schedulePresenter = new SchedulePresenter(listenerMock, scheduleServiceMock, meetUpServiceMock);

    assertTrue(schedulePresenter instanceof SchedulePresenter);
    assertEquals(schedulePresenter.getListener(), listenerMock);
  }

  @Test
  public void testGetSchedulesInfo() {
    ScheduleInfo[] scheduleInfoArr = {
        new ScheduleInfo(1, "Test1", new DateTime(1, 1, 1, 14, 15), "02.34 - 02.34")};
    List<ScheduleInfo> scheduleInfos = Arrays.asList(scheduleInfoArr);

    ScheduleResponse[] schedResponseArr = {
        new ScheduleResponse("Test1", new DateTime(1, 1, 1, 14, 15),
            new DateTime(1, 1, 1, 14, 30))};
    List<ScheduleResponse> scheduleResponses = Arrays.asList(schedResponseArr);

    List<ScheduleInfo> schedules = schedulePresenter.getSchedulesInfo(scheduleResponses, false);
    assertEquals(scheduleInfos.get(0).getId(), schedules.get(0).getId());
    assertEquals(DateUtil.getDateString(scheduleInfos.get(0).getDate()),
        DateUtil.getDateString(schedules.get(0).getDate()));
    assertEquals(scheduleInfos.get(0).getTitle(), schedules.get(0).getTitle());
    assertEquals(scheduleInfos.get(0).getTime(), schedules.get(0).getTime());
  }

  @Test
  public void testGetSchedulesSuccess() {
    final Call<List<ScheduleResponse>> callScheduleMock = mock(Call.class);

    final DateTime dateTime = new DateTime(2018, 3, 1, 0, 0);
    ScheduleResponse scheduleResponse = new ScheduleResponse("makan",
        dateTime, new DateTime(2018, 3, 1, 1, 15));
    final List<ScheduleResponse> responseBody = new ArrayList<>();
    responseBody.add(scheduleResponse);

    ScheduleInfo scheduleInfo = new ScheduleInfo(1, "makan", dateTime, "02.34 - 02.34");

    final Response<List<ScheduleResponse>> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(responseBody);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<ScheduleResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callScheduleMock, responseMock);

        return null;
      }
    }).when(scheduleServiceMock).getSchedules(any(Callback.class));

    schedulePresenter.getSchedules();

    ArgumentCaptor<List> argument = ArgumentCaptor.forClass(List.class);
    verify(listenerMock, times(1)).onGetSuccess(argument.capture());

    ScheduleInfo resultDetail = (ScheduleInfo) argument.getValue().get(0);

    assertEquals(scheduleInfo.getId(), resultDetail.getId());
    assertEquals(scheduleInfo.getDate(), resultDetail.getDate());
    assertEquals(scheduleInfo.getTitle(), resultDetail.getTitle());
    assertEquals(scheduleInfo.getTime(), resultDetail.getTime());
  }


  @Test
  public void testGetSchedulesNotSuccess() {
    final Call<ScheduleResponse> callScheduleMock = mock(Call.class);
    final Response<ScheduleResponse> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<ScheduleResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callScheduleMock, responseMock);

        return null;
      }
    }).when(scheduleServiceMock).getSchedules(any(Callback.class));

    schedulePresenter.getSchedules();

    verify(listenerMock, times(1)).onError("Failed to get Schedules");
  }

  @Test
  public void testGetSchedulesFailure() {
    final Call<ScheduleResponse> callScheduleMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);
    String message = "mi mie meet u";

    when(throwable.getMessage()).thenReturn(message);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<ScheduleResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callScheduleMock, throwable);

        return null;
      }
    }).when(scheduleServiceMock).getSchedules(any(Callback.class));

    schedulePresenter.getSchedules();

    verify(listenerMock, times(1)).onError(message);
  }

  @Test
  public void testPostSchedulesSuccess() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Response<BaseResponse> responseMock = mock(Response.class);
    String scheduleTitle = "kuliah PPL";

    when(responseMock.isSuccessful()).thenReturn(true);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(1, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(scheduleServiceMock).postSchedules(any(List.class), any(Callback.class));

    schedulePresenter
        .postSchedules(scheduleTitle, new DateTime(), new DateTime(), RepeatType.ONCE, 1);

    verify(listenerMock, times(1)).onPostSuccess("Successfully created " + scheduleTitle);
  }

  @Test
  public void testPostSchedulesNotSuccess() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Response<BaseResponse> responseMock = mock(Response.class);
    String scheduleTitle = "kuliah PPL";

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(1, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(scheduleServiceMock).postSchedules(any(List.class), any(Callback.class));

    schedulePresenter
        .postSchedules(scheduleTitle, new DateTime(), new DateTime(), RepeatType.ONCE, 1);

    verify(listenerMock, times(1)).onError("Conflict exists with current schedules");
  }

  @Test
  public void testPostSchedulesFailure() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);
    String scheduleTitle = "kuliah PPL";

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(1, Callback.class);
        callback.onFailure(callBaseMock, throwable);

        return null;
      }
    }).when(scheduleServiceMock).postSchedules(any(List.class), any(Callback.class));

    schedulePresenter
        .postSchedules(scheduleTitle, new DateTime(), new DateTime(), RepeatType.ONCE, 1);

    verify(listenerMock, times(1)).onError("There is problem with your connection");
  }

  @Test
  public void testDeleteSchedulesSuccess() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Response<BaseResponse> responseMock = mock(Response.class);
    final String parentId = "123456";

    when(responseMock.isSuccessful()).thenReturn(true);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(1, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(scheduleServiceMock).deleteSchedules(anyString(), any(Callback.class));

    schedulePresenter.deleteSchedules(parentId);

    verify(listenerMock, times(1)).onDeleteSuccess("Delete schedule successful");
  }

  @Test
  public void testDeleteSchedulesNotSuccess() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Response<BaseResponse> responseMock = mock(Response.class);
    final String parentId = "123456";

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(1, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(scheduleServiceMock).deleteSchedules(anyString(), any(Callback.class));

    schedulePresenter.deleteSchedules(parentId);

    verify(listenerMock, times(1)).onError("Something is wrong");
  }

  @Test
  public void testDeleteSchedulesFailure() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);
    final String message = "mi mie apa";
    final String parentId = "123456";

    when(throwable.getMessage()).thenReturn(message);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(1, Callback.class);
        callback.onFailure(callBaseMock, throwable);

        return null;
      }
    }).when(scheduleServiceMock).deleteSchedules(anyString(), any(Callback.class));

    schedulePresenter.deleteSchedules(parentId);

    verify(listenerMock, times(1)).onError(message);
  }

  @Test
  public void testGetMeetUpsSuccess() {
    final Call<List<MeetUpResponse>> callMeetUpMock = mock(Call.class);

    final DateTime dateTime = new DateTime(2018, 3, 1, 0, 0);
    MeetUpResponse meetUpResponse = new MeetUpResponse("makan", null,
        dateTime, new DateTime(2018, 3, 1, 1, 15));
    final List<MeetUpResponse> responseBody = new ArrayList<>();
    responseBody.add(meetUpResponse);

    ScheduleInfo scheduleInfo = new ScheduleInfo(1, "makan", dateTime, "02.34 - 02.34");

    final Response<List<MeetUpResponse>> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(responseBody);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<MeetUpResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMeetUpMock, responseMock);

        return null;
      }
    }).when(meetUpServiceMock).getMeetUps(any(Callback.class));

    schedulePresenter.getMeetUps();

    ArgumentCaptor<List> argument = ArgumentCaptor.forClass(List.class);
    verify(listenerMock, times(1)).onGetSuccess(argument.capture());

    ScheduleInfo resultDetail = (ScheduleInfo) argument.getValue().get(0);

    assertEquals(scheduleInfo.getId(), resultDetail.getId());
    assertEquals(scheduleInfo.getDate(), resultDetail.getDate());
    assertEquals(scheduleInfo.getTitle(), resultDetail.getTitle());
    assertEquals(scheduleInfo.getTime(), resultDetail.getTime());
  }


  @Test
  public void testGetMeetUpsNotSuccess() {
    final Call<List<MeetUpResponse>> callMeetUpMock = mock(Call.class);
    final Response<List<MeetUpResponse>> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<MeetUpResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMeetUpMock, responseMock);

        return null;
      }
    }).when(meetUpServiceMock).getMeetUps(any(Callback.class));

    schedulePresenter.getMeetUps();

    verify(listenerMock, times(1)).onError("Failed to get MeetUps");
  }

  @Test
  public void testGetMeetUpsFailure() {
    final Call<List<MeetUpResponse>> callMeetUpMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);
    String message = "mi mie meet u";

    when(throwable.getMessage()).thenReturn(message);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<MeetUpResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callMeetUpMock, throwable);

        return null;
      }
    }).when(meetUpServiceMock).getMeetUps(any(Callback.class));

    schedulePresenter.getMeetUps();

    verify(listenerMock, times(1)).onError("Something is wrong with your connection");
  }
}
