package id.meetu.meetu.module.auth.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 16/04/18.
 */
@RunWith(PowerMockRunner.class)
public class AuthResponseTest {

  private AuthResponse authResponse;

  @Before
  public void setUp() {
    User userMock = PowerMockito.mock(User.class);
    String token = "token";

    authResponse = new AuthResponse(token, userMock);
  }

  @Test
  public void testConstruct() {
    User userMock = PowerMockito.mock(User.class);
    String token = "token";

    authResponse = new AuthResponse(token, userMock);

    assertTrue(authResponse instanceof AuthResponse);
    assertEquals(token, authResponse.getToken());
    assertEquals(userMock, authResponse.getUser());
  }

  @Test
  public void testSetterGetterUser() {
    User dummy = PowerMockito.mock(User.class);

    authResponse.setUser(dummy);
    assertEquals(dummy, authResponse.getUser());
  }

  @Test
  public void testSetterGetterToken() {
    authResponse.setToken("dummy");

    assertEquals("dummy", authResponse.getToken());
  }
}
