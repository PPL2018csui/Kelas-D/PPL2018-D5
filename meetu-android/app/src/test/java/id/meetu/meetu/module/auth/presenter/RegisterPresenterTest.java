package id.meetu.meetu.module.auth.presenter;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.auth.contract.RegisterContract;
import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.RegisterRequest;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.model.UserProfile;
import id.meetu.meetu.module.auth.service.AuthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 17/04/18.
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class RegisterPresenterTest {

  private RegisterContract.View viewMock;
  private RegisterPresenter presenter;
  private RegisterPresenter presenterWithService;
  private AuthService authServiceMock;

  @Before
  public void setUp() {
    viewMock = PowerMockito.mock(RegisterContract.View.class);
    authServiceMock = PowerMockito.mock(AuthService.class);
    presenterWithService = new RegisterPresenter(viewMock, authServiceMock);
    presenter = new RegisterPresenter(viewMock);
  }

  @Test
  public void testCancel() {
    presenter.cancel();

    verify(viewMock, times(1)).onCancel();
  }

  @Test
  public void testSubmitNonEmpty() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Response<BaseResponse> responseMock = mock(Response.class);
    AuthResponse responseMockBody = mock(AuthResponse.class);
    User userMock = mock(User.class);
    UserProfile profileMock = mock(UserProfile.class);

    when(responseMockBody.getToken()).thenReturn("token");
    when(responseMockBody.getUser()).thenReturn(userMock);

    when(userMock.getId()).thenReturn(777);
    when(userMock.getFirstName()).thenReturn("ayaze");
    when(userMock.getLastName()).thenReturn("ayaze");
    when(userMock.getUserProfile()).thenReturn(profileMock);
    when(profileMock.getId()).thenReturn(666);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(responseMockBody);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(authServiceMock).postRegister(any(Callback.class), any(RegisterRequest.class));

    presenterWithService.submit("cece@cece.com", "Grace", "Angelica", "gres", "gres");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setFirstNameError(anyString());
    verify(viewMock, times(0)).setLastNameError(anyString());
    verify(viewMock, times(0)).setEmailError(anyString());
    verify(viewMock, times(0)).setPasswordError(anyString());
    verify(viewMock, times(0)).setVerifyPasswordError(anyString());
    verify(viewMock, times(1)).onRegisterSuccess(anyString(), any(User.class));
  }

  @Test
  public void testRegisterNotSuccess() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Response<BaseResponse> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(authServiceMock).postRegister(any(Callback.class), any(RegisterRequest.class));

    presenterWithService.submit("cece@cece.com", "Grace", "Angelica", "gres", "gres");

    verify(viewMock, times(1)).onRegisterFail();
  }

  @Test
  public void testRegisterFailure() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callBaseMock, throwable);

        return null;
      }
    }).when(authServiceMock).postRegister(any(Callback.class), any(RegisterRequest.class));

    presenterWithService.submit("cece@cece.com", "Grace", "Angelica", "gres", "gres");

    verify(viewMock, times(1)).onRegisterError();
  }

  @Test
  public void testSubmitPasswordEmpty() {
    presenter.submit("cece@cece.com", "Grace", "Angelica", "", "gres");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setFirstNameError(anyString());
    verify(viewMock, times(0)).setLastNameError(anyString());
    verify(viewMock, times(0)).setEmailError(anyString());
    verify(viewMock, times(1)).setPasswordError(anyString());
    verify(viewMock, times(0)).setVerifyPasswordError(anyString());
    verify(viewMock, times(0)).onRegisterSuccess(anyString(), any(User.class));
  }

  @Test
  public void testSubmitFirstNameEmpty() {
    presenter.submit("cece@cece.com", "", "Angelica", "gres", "gres");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(1)).setFirstNameError(anyString());
    verify(viewMock, times(0)).setLastNameError(anyString());
    verify(viewMock, times(0)).setEmailError(anyString());
    verify(viewMock, times(0)).setPasswordError(anyString());
    verify(viewMock, times(0)).setVerifyPasswordError(anyString());
    verify(viewMock, times(0)).onRegisterSuccess(anyString(), any(User.class));
  }

  @Test
  public void testSubmitLastNameEmpty() {
    presenter.submit("cece@cece.com", "Grace", "", "gres", "gres");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setFirstNameError(anyString());
    verify(viewMock, times(1)).setLastNameError(anyString());
    verify(viewMock, times(0)).setEmailError(anyString());
    verify(viewMock, times(0)).setPasswordError(anyString());
    verify(viewMock, times(0)).setVerifyPasswordError(anyString());
    verify(viewMock, times(0)).onRegisterSuccess(anyString(), any(User.class));
  }

  @Test
  public void testSubmitEmailEmpty() {
    presenter.submit("", "Grace", "Angelica", "gres", "gres");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setFirstNameError(anyString());
    verify(viewMock, times(0)).setLastNameError(anyString());
    verify(viewMock, times(1)).setEmailError(anyString());
    verify(viewMock, times(0)).setPasswordError(anyString());
    verify(viewMock, times(0)).setVerifyPasswordError(anyString());
    verify(viewMock, times(0)).onRegisterSuccess(anyString(), any(User.class));
  }

  @Test
  public void testSubmitVerifyPasswordEmpty() {
    presenter.submit("cece@cece.com", "Grace", "Angelica", "gres", "");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setFirstNameError(anyString());
    verify(viewMock, times(0)).setLastNameError(anyString());
    verify(viewMock, times(0)).setEmailError(anyString());
    verify(viewMock, times(0)).setPasswordError(anyString());
    verify(viewMock, times(1)).setVerifyPasswordError(anyString());
    verify(viewMock, times(0)).onRegisterSuccess(anyString(), any(User.class));
  }

  @Test
  public void testSubmitInvalidEmail() {
    presenter.submit("cece", "Grace", "Angelica", "gres", "gres");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setFirstNameError(anyString());
    verify(viewMock, times(0)).setLastNameError(anyString());
    verify(viewMock, times(1)).setEmailError(anyString());
    verify(viewMock, times(0)).setPasswordError(anyString());
    verify(viewMock, times(0)).setVerifyPasswordError(anyString());
    verify(viewMock, times(0)).onRegisterSuccess(anyString(), any(User.class));
  }

  @Test
  public void testSubmitPasswordNotMatch() {
    presenter.submit("cece@cece.com", "Grace", "Angelica", "gres", "grace");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setFirstNameError(anyString());
    verify(viewMock, times(0)).setLastNameError(anyString());
    verify(viewMock, times(0)).setEmailError(anyString());
    verify(viewMock, times(0)).setPasswordError(anyString());
    verify(viewMock, times(1)).setVerifyPasswordError(anyString());
    verify(viewMock, times(0)).onRegisterSuccess(anyString(), any(User.class));
  }

  @Test
  public void testSubmitAllEmpty() {
    presenter.submit("", "", "", "", "");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(1)).setFirstNameError(anyString());
    verify(viewMock, times(1)).setLastNameError(anyString());
    verify(viewMock, times(1)).setEmailError(anyString());
    verify(viewMock, times(1)).setPasswordError(anyString());
    verify(viewMock, times(1)).setVerifyPasswordError(anyString());
    verify(viewMock, times(0)).onRegisterSuccess(anyString(), any(User.class));
  }
}
