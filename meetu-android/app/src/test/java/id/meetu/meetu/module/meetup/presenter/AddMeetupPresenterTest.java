package id.meetu.meetu.module.meetup.presenter;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.meetup.contract.AddMeetUpActivityContract;
import id.meetu.meetu.module.meetup.model.TimeRecommendationRequest;
import id.meetu.meetu.module.meetup.model.TimeRecommendationResponse;
import id.meetu.meetu.module.meetup.service.TimeRecommendationService;
import id.meetu.meetu.util.DateUtil.RepeatType;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 19/04/18.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(Response.class)
@PowerMockIgnore("javax.net.ssl.*")
public class AddMeetupPresenterTest {

  private AddMeetUpActivityContract.View viewMock;
  private TimeRecommendationService serviceMock;
  private AddMeetupPresenter presenter;

  private Call<List<List<TimeRecommendationResponse>>> callMock;

  @Before
  public void setUp() {
    viewMock = PowerMockito.mock(AddMeetUpActivityContract.View.class);
    serviceMock = PowerMockito.mock(TimeRecommendationService.class);
    callMock = PowerMockito.mock(Call.class);

    presenter = new AddMeetupPresenter(viewMock, serviceMock);
  }

  @Test
  public void testConstruct() {
    presenter = new AddMeetupPresenter(viewMock);

    assertTrue(presenter instanceof AddMeetupPresenter);
  }

  @Test
  public void testFetchTimeRecommendationSuccess() {
    final Call<List<List<TimeRecommendationResponse>>> callMock = (Call<List<List<TimeRecommendationResponse>>>) mock(
        Call.class);
    final Response<List<List<TimeRecommendationResponse>>> responseMock = (Response<List<List<TimeRecommendationResponse>>>) (mock(
        Response.class));

    final List<List<TimeRecommendationResponse>> response = new ArrayList<>();
    response.add(new ArrayList<TimeRecommendationResponse>());
    response.get(0).add(new TimeRecommendationResponse(new DateTime(), new DateTime()));

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(response);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<List<TimeRecommendationResponse>>> callback = invocation
            .getArgumentAt(1, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(serviceMock)
        .postTimeRecommendations(any(TimeRecommendationRequest.class), any(Callback.class));

    presenter
        .fetchTimeRecommendation(111, new DateTime(), new DateTime(), RepeatType.ONCE, 1);

    verify(viewMock, times(1)).onFinishGetRecommendation(any(List.class));
  }

  @Test
  public void testFetchTimeRecommendationSuccessEmpty() {
    final Call<List<List<TimeRecommendationResponse>>> callMock = (Call<List<List<TimeRecommendationResponse>>>) mock(
        Call.class);
    final Response<List<List<TimeRecommendationResponse>>> responseMock = (Response<List<List<TimeRecommendationResponse>>>) (mock(
        Response.class));

    final List<List<TimeRecommendationResponse>> response = new ArrayList<>();

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(response);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<List<TimeRecommendationResponse>>> callback = invocation
            .getArgumentAt(1, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(serviceMock)
        .postTimeRecommendations(any(TimeRecommendationRequest.class), any(Callback.class));

    presenter
        .fetchTimeRecommendation(111, new DateTime(), new DateTime(), RepeatType.ONCE, 1);

    verify(viewMock, times(1)).onFinishEmptyRecommendation();
  }

  @Test
  public void testFetchRecommendationNotSuccess() {
    final Call<List<List<TimeRecommendationResponse>>> callMock = mock(Call.class);
    final Response<List<List<TimeRecommendationResponse>>> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<List<TimeRecommendationResponse>>> callback = invocation
            .getArgumentAt(1, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(serviceMock)
        .postTimeRecommendations(any(TimeRecommendationRequest.class), any(Callback.class));

    presenter
        .fetchTimeRecommendation(111, new DateTime(), new DateTime(), RepeatType.ONCE, 1);

    verify(viewMock, times(1)).onError("Something is wrong");
  }

  @Test
  public void testFetchRecommendationFailure() {
    final Call<List<List<TimeRecommendationResponse>>> callMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<List<TimeRecommendationResponse>>> callback = invocation
            .getArgumentAt(1, Callback.class);
        callback.onFailure(callMock, throwable);

        return null;
      }
    }).when(serviceMock)
        .postTimeRecommendations(any(TimeRecommendationRequest.class), any(Callback.class));

    presenter
        .fetchTimeRecommendation(111, new DateTime(), new DateTime(), RepeatType.ONCE, 1);

    verify(viewMock, times(1)).onError("Something is wrong with your connection");

  }
}
