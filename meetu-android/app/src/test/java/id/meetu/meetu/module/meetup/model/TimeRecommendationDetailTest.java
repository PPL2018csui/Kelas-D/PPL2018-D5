package id.meetu.meetu.module.meetup.model;


import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

import java.util.Date;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class TimeRecommendationDetailTest {

  @Test
  public void testConstructMeetUpDetail() {
    TimeRecommendationDetail detail = new TimeRecommendationDetail();
    assertNotNull(detail);
  }

  @Test
  public void testConstructMeetUpDetaiWithParaml() {
    int id = 0;
    DateTime startTime = new DateTime(2018, 4, 4, 10, 15);
    DateTime endTime = new DateTime(2018, 4, 4, 10, 45);
    int members = 10;
    int availableMembers = 10;

    TimeRecommendationDetail timeRecommendationDetail = new TimeRecommendationDetail(id, startTime, endTime,
        members, availableMembers);
    assertNotNull(timeRecommendationDetail);
  }


  @Test
  public void testSetterGetterId() {
    TimeRecommendationDetail timeDetail = new TimeRecommendationDetail();
    timeDetail.setId(1);
    assertEquals(1, timeDetail.getId());
  }

  @Test
  public void testSetterGetterStartTime() {
    TimeRecommendationDetail timeDetail = new TimeRecommendationDetail();
    timeDetail.setStartTime(new DateTime(1, 1, 1, 1, 1));
    assertNotNull(timeDetail.getStartTime());
  }

  @Test
  public void testSetterGetterEndTime() {
    TimeRecommendationDetail timeDetail = new TimeRecommendationDetail();
    timeDetail.setEndTime(new DateTime(1, 1, 1, 1, 1));
    assertNotNull(timeDetail.getEndTime());
  }

  @Test
  public void testSetterGetterMembers() {
    TimeRecommendationDetail timeDetail = new TimeRecommendationDetail();
    timeDetail.setMembers(6);
    assertEquals(6, timeDetail.getMembers());
  }

  @Test
  public void testSetterGetterAvailableMembers() {
    TimeRecommendationDetail timeDetail = new TimeRecommendationDetail();
    timeDetail.setAvailableMembers(5);
    assertEquals(5, timeDetail.getAvailableMembers());
  }
}

