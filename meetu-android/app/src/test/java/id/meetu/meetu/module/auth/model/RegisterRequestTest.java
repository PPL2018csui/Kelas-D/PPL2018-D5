package id.meetu.meetu.module.auth.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 16/04/18.
 */
@RunWith(PowerMockRunner.class)
public class RegisterRequestTest {

  @Test
  public void testConstructor() {
    RegisterRequest request = new RegisterRequest("ayaze@meetu.com", "ayaz", "dz", "password");

    assertTrue(request instanceof RegisterRequest);
    assertEquals("ayaze@meetu.com", request.getUsername());
    assertEquals("ayaz", request.getFirstName());
    assertEquals("dz", request.getLastName());
    assertEquals("password", request.getPassword());
  }

  @Test
  public void testSetterGetterEmail() {
    RegisterRequest request = new RegisterRequest("ayaze@meetu.com", "ayaz", "dz", "password");
    request.setUsername("dummy");

    assertEquals("dummy", request.getUsername());
  }

  @Test
  public void testSetterGetterFirstName() {
    RegisterRequest request = new RegisterRequest("ayaze@meetu.com", "ayaz", "dz", "password");
    request.setFirstName("dummy");

    assertEquals("dummy", request.getFirstName());
  }

  @Test
  public void testSetterGetterLastName() {
    RegisterRequest request = new RegisterRequest("ayaze@meetu.com", "ayaz", "dz", "password");
    request.setLastName("dummy");

    assertEquals("dummy", request.getLastName());
  }

  @Test
  public void testSetterGetterPassword() {
    RegisterRequest request = new RegisterRequest("ayaze@meetu.com", "ayaz", "dz", "password");
    request.setPassword("dummy");

    assertEquals("dummy", request.getPassword());
  }
}
