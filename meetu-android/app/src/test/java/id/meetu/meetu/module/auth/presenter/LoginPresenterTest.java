package id.meetu.meetu.module.auth.presenter;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.auth.contract.LoginContract;
import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.LoginRequest;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.model.UserProfile;
import id.meetu.meetu.module.auth.service.AuthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 17/04/18.
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class LoginPresenterTest {

  private LoginContract.View viewMock;
  private LoginPresenter presenter;
  private LoginPresenter presenterWithoutService;
  private AuthService authServiceMock;

  @Before
  public void setUp() {
    viewMock = PowerMockito.mock(LoginContract.View.class);
    authServiceMock = PowerMockito.mock(AuthService.class);
    presenterWithoutService = new LoginPresenter(viewMock);
    presenter = new LoginPresenter(viewMock, authServiceMock);
  }

  @Test
  public void testCancel() {
    presenterWithoutService.cancel();

    verify(viewMock, times(1)).onCancel();
  }

  @Test
  public void testSubmitNonEmptySuccess() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Response<BaseResponse> responseMock = mock(Response.class);
    AuthResponse responseMockBody = mock(AuthResponse.class);
    User userMock = mock(User.class);
    UserProfile profileMock = mock(UserProfile.class);

    when(responseMockBody.getToken()).thenReturn("token");
    when(responseMockBody.getUser()).thenReturn(userMock);

    when(userMock.getId()).thenReturn(777);
    when(userMock.getFirstName()).thenReturn("ayaze");
    when(userMock.getLastName()).thenReturn("ayaze");
    when(userMock.getUserProfile()).thenReturn(profileMock);
    when(profileMock.getId()).thenReturn(666);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(responseMockBody);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(authServiceMock).postLogin(any(Callback.class), any(LoginRequest.class));

    presenter.submit("ayaz", "ganteng");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setUsernameError(anyString());
    verify(viewMock, times(0)).setPasswordError(anyString());
    verify(viewMock, times(1)).onLoginSuccess(anyString(), any(User.class));
  }

  @Test
  public void testSubmitNonEmptyNotSuccess() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Response<BaseResponse> responseMock = mock(Response.class);
    AuthResponse responseMockBody = mock(AuthResponse.class);

    when(responseMock.isSuccessful()).thenReturn(false);
    when(responseMock.body()).thenReturn(responseMockBody);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(authServiceMock).postLogin(any(Callback.class), any(LoginRequest.class));

    presenter.submit("ayaz", "ganteng");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setUsernameError(anyString());
    verify(viewMock, times(0)).setPasswordError(anyString());
    verify(viewMock, times(1)).onLoginFail();
  }

  @Test
  public void testLoginOnFailure() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callBaseMock, throwable);

        return null;
      }
    }).when(authServiceMock).postLogin(any(Callback.class), any(LoginRequest.class));

    presenter.submit("ricky", "lebih ganteng");

    verify(viewMock, times(1)).onLoginError();
  }

  @Test
  public void testSubmitPasswordEmpty() {
    presenterWithoutService.submit("ayaz", "");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setUsernameError(anyString());
    verify(viewMock, times(1)).setPasswordError(anyString());
    verify(viewMock, times(0)).onLoginSuccess(anyString(), any(User.class));
  }

  @Test
  public void testSubmitUsernameEmpty() {
    presenterWithoutService.submit("", "ganteng");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(1)).setUsernameError(anyString());
    verify(viewMock, times(0)).setPasswordError(anyString());
    verify(viewMock, times(0)).onLoginSuccess(anyString(), any(User.class));
  }

  @Test
  public void testSubmitAllEmpty() {
    presenterWithoutService.submit("", "");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(1)).setUsernameError(anyString());
    verify(viewMock, times(1)).setPasswordError(anyString());
    verify(viewMock, times(0)).onLoginSuccess(anyString(), any(User.class));
  }
}
