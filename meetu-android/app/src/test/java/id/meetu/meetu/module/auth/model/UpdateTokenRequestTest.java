package id.meetu.meetu.module.auth.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class UpdateTokenRequestTest {

  @Test
  public void testConstruct() {
    UpdateTokenRequest request = new UpdateTokenRequest("assdadsada");

    assertNotNull(request);
  }

  @Test
  public void testSetterGetterToken() {
    UpdateTokenRequest request = new UpdateTokenRequest("assdadsada");
    request.setToken("aa:1234");

    assertEquals("aa:1234", request.getToken());
  }
}
