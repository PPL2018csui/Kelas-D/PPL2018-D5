package id.meetu.meetu.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import id.meetu.meetu.util.DateUtil.RepeatType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 12/03/18.
 */

@RunWith(PowerMockRunner.class)
public class DateUtilTest {


  @Test
  public void testInstantiateDateUtil() {
    DateUtil dateUtil = new DateUtil();

    assertTrue(dateUtil instanceof DateUtil);
  }

  @Test
  public void testGetRepetitionText() {
    String daily = "Every day for 1 day(s)";
    String weekly = "Every week for 1 week(s)";
    String monthly = "Every month for 1 month(s)";

    assertEquals(daily, DateUtil.getRepetitionText(RepeatType.DAILY, 1));
    assertEquals(weekly, DateUtil.getRepetitionText(RepeatType.WEEKLY, 1));
    assertEquals(monthly, DateUtil.getRepetitionText(RepeatType.MONTHLY, 1));
  }

  @Test
  public void testGetListForTimeDropdownOnce() {
    List<String> expected = new ArrayList<>();
    expected.add("1");
    List<String> result = DateUtil.getListForTimeDropdown(RepeatType.ONCE);

    assertEquals(expected, result);
  }

  @Test
  public void testGetListForTimeDropdownDaily() {
    List<String> expected = new ArrayList<>();
    for (int i = 1; i <= Constant.MAX_DAY_REPETITION; i++) {
      expected.add(Integer.toString(i));
    }

    List<String> result = DateUtil.getListForTimeDropdown(RepeatType.DAILY);

    assertEquals(expected, result);
  }

  @Test
  public void testGetListForTimeDropdownWeekly() {
    List<String> expected = new ArrayList<>();
    for (int i = 1; i <= Constant.MAX_WEEK_REPETITION; i++) {
      expected.add(Integer.toString(i));
    }

    List<String> result = DateUtil.getListForTimeDropdown(RepeatType.WEEKLY);

    assertEquals(expected, result);
  }

  @Test
  public void testGetListForTimeDropdownMonthly() {
    List<String> expected = new ArrayList<>();
    for (int i = 1; i <= Constant.MAX_MONTH_REPETITION; i++) {
      expected.add(Integer.toString(i));
    }

    List<String> result = DateUtil.getListForTimeDropdown(RepeatType.MONTHLY);

    assertEquals(expected, result);
  }

  @Test
  public void testGetHourMinuteString1() {
    String expected = "01:59";
    String result = DateUtil.getHourMinuteString(1, 59, ":");

    assertEquals(expected, result);
  }

  @Test
  public void testGetHourMinuteString2() {
    String expected = "23.02";
    String result = DateUtil.getHourMinuteString(23, 2, ".");

    assertEquals(expected, result);
  }

  @Test
  public void testParseHourMinuteStringOk() {
    String date = "23:02";
    DateTime result = DateUtil.parseHourMinuteString(date, ":");

    assertEquals(23, result.getHourOfDay());
    assertEquals(2, result.getMinuteOfHour());
  }

  @Test
  public void testParseHourMinuteStringWrongSeparator() {
    String date = "23:02";
    DateTime result = DateUtil.parseHourMinuteString(date, ".");

    assertNull(result);
  }

  @Test
  public void testParseHourMinuteStringWrongFormat() {
    String date = "23asd02";
    DateTime result = DateUtil.parseHourMinuteString(date, ":");

    assertNull(result);
  }

  @Test
  public void testGetDateString() {
    DateTime date = new DateTime(1997, 8, 28, 0, 0);
    String expected = "28/08/1997";
    String result = DateUtil.getDateString(date);

    assertEquals(expected, result);
  }

  @Test
  public void testParseDateStringOk() {
    String dateString = "28/08/1997";
    DateTime result = DateUtil.parseDateString(dateString);

    assertEquals(1997, result.getYear());
    assertEquals(8, result.getMonthOfYear());
    assertEquals(28, result.getDayOfMonth());
  }

  @Test
  public void testParseDateStringFail() {
    String dateString = "akuganteng";
    DateTime result = DateUtil.parseDateString(dateString);

    assertNull(result);
  }

  @Test
  public void testRepeatTypeValues() {
    String[] values = {"ONCE", "DAILY", "WEEKLY", "MONTHLY"};
    Set<String> expectedvalues = new HashSet<>(Arrays.asList(values));
    Set<String> actualValues = new HashSet<>();

    for (RepeatType repeatType : RepeatType.values()) {
      actualValues.add(repeatType.name());
    }

    assertEquals(expectedvalues, actualValues);
  }

  @Test
  public void testRepeatTypeValueOf() {
    String[] values = {"ONCE", "DAILY", "WEEKLY", "MONTHLY"};
    RepeatType[] repeatTypes = {RepeatType.ONCE, RepeatType.DAILY, RepeatType.WEEKLY,
        RepeatType.MONTHLY};

    for (int i = 0; i < values.length; i++) {
      assertEquals(repeatTypes[i], RepeatType.valueOf(values[i]));
    }
  }

  @Test
  public void testGetNextRepetitionNull() {
    DateTime current = new DateTime(2018, 3, 1, 1, 0);

    DateTime next = DateUtil.getNextRepetition(current, 1, null);
    assertNull(next);
  }

  @Test
  public void testGetNextRepetitionOnce() {
    DateTime current = new DateTime(2018, 3, 1, 1, 0);

    DateTime next = DateUtil.getNextRepetition(current, 1, RepeatType.ONCE);
    assertNull(next);
  }

  @Test
  public void testGetNextRepetitionDaily() {
    DateTime current = new DateTime(2018, 1, 31, 1, 0);
    DateTime expected = new DateTime(2018, 2, 1, 1, 0);

    DateTime next = DateUtil.getNextRepetition(current, 1, RepeatType.DAILY);
    assertEquals(expected, next);
  }

  @Test
  public void testGetNextRepetitionWeekly() {
    DateTime current = new DateTime(2018, 1, 31, 1, 0);
    DateTime expected = new DateTime(2018, 2, 7, 1, 0);

    DateTime next = DateUtil.getNextRepetition(current, 1, RepeatType.WEEKLY);
    assertEquals(expected, next);
  }

  @Test
  public void testGetNextRepetitionMonthly() {
    DateTime current = new DateTime(2018, 1, 31, 1, 0);
    DateTime expected1 = new DateTime(2018, 2, 28, 1, 0);
    DateTime expected2 = new DateTime(2018, 3, 31, 1, 0);

    DateTime next1 = DateUtil.getNextRepetition(current, 1, RepeatType.MONTHLY);
    DateTime next2 = DateUtil.getNextRepetition(current, 2, RepeatType.MONTHLY);

    assertEquals(expected1, next1);
    assertEquals(expected2, next2);
  }

  @Test
  public void testGetEventRepetitionOnce() {
    DateTime start = new DateTime(2018, 1, 31, 1, 0);
    DateTime end = new DateTime(2018, 1, 31, 2, 30);
    List<DateInterval> expectedResult = new ArrayList<>();
    expectedResult.add(new DateInterval(start, end));

    List<DateInterval> result = DateUtil
        .getEventRepetition(start, end, RepeatType.ONCE, 0);
    assertEquals(expectedResult, result);
  }

  @Test
  public void testGetEventRepetitionDaily() {
    DateTime start = new DateTime(2018, 1, 31, 1, 0);
    DateTime end = new DateTime(2018, 1, 31, 2, 30);
    List<DateInterval> expectedResult = new ArrayList<>();

    expectedResult.add(new DateInterval(start, end));
    for (int i = 1; i <= 6; i++) {
      expectedResult
          .add(new DateInterval(new DateTime(2018, 2, i, 1, 0), new DateTime(2018, 2, i, 2, 30)));
    }

    List<DateInterval> result = DateUtil
        .getEventRepetition(start, end, RepeatType.DAILY, 7);
    assertEquals(expectedResult, result);
  }

  @Test
  public void testGetEventRepetitionWeekly() {
    DateTime start = new DateTime(2018, 1, 31, 1, 0);
    DateTime end = new DateTime(2018, 1, 31, 2, 30);
    List<DateInterval> expectedResult = new ArrayList<>();

    expectedResult.add(new DateInterval(start, end));
    expectedResult
        .add(new DateInterval(new DateTime(2018, 2, 7, 1, 0), new DateTime(2018, 2, 7, 2, 30)));
    expectedResult
        .add(new DateInterval(new DateTime(2018, 2, 14, 1, 0), new DateTime(2018, 2, 14, 2, 30)));

    List<DateInterval> result = DateUtil
        .getEventRepetition(start, end, RepeatType.WEEKLY, 3);
    assertEquals(expectedResult, result);
  }

  @Test
  public void testGetEventRepetitionMonthly() {
    DateTime start = new DateTime(2018, 1, 31, 1, 0);
    DateTime end = new DateTime(2018, 1, 31, 2, 30);
    List<DateInterval> expectedResult = new ArrayList<>();

    expectedResult.add(new DateInterval(start, end));
    expectedResult
        .add(new DateInterval(new DateTime(2018, 2, 28, 1, 0), new DateTime(2018, 2, 28, 2, 30)));
    expectedResult
        .add(new DateInterval(new DateTime(2018, 3, 31, 1, 0), new DateTime(2018, 3, 31, 2, 30)));
    expectedResult
        .add(new DateInterval(new DateTime(2018, 4, 30, 1, 0), new DateTime(2018, 4, 30, 2, 30)));

    List<DateInterval> result = DateUtil
        .getEventRepetition(start, end, RepeatType.MONTHLY, 4);
    assertEquals(expectedResult, result);
  }
}
