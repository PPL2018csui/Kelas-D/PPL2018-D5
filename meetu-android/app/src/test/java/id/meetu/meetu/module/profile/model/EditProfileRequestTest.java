package id.meetu.meetu.module.profile.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class EditProfileRequestTest {

  private EditProfileRequest request;

  @Before
  public void setUp() {
    request = new EditProfileRequest("ayaz", "tampan", "123", "bontang", "www.google.com");
  }

  @Test
  public void testSetterGetterFirstName() {
    request.setFirstName("babon");
    assertEquals("babon", request.getFirstName());
  }

  @Test
  public void testSetterGetterLastName() {
    request.setLastName("babon");
    assertEquals("babon", request.getLastName());
  }

  @Test
  public void testSetterGetterPhoneNumber() {
    request.setPhoneNumber("321");
    assertEquals("321", request.getPhoneNumber());
  }

  @Test
  public void testSetterGetterLocation() {
    request.setLocation("hutan");
    assertEquals("hutan", request.getLocation());
  }

  @Test
  public void testSetterGetterImageUrl() {
    request.setProfileImageUrl("www.facebook.com");
    assertEquals("www.facebook.com", request.getProfileImageUrl());
  }

}
