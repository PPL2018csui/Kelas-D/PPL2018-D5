package id.meetu.meetu.module.schedule.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.schedule.model.ScheduleResponse;
import id.meetu.meetu.service.ScheduleApi;
import id.meetu.meetu.service.ScheduleApi.Service;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 12/03/18.
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
public class ScheduleServiceTest {

  @Mock
  private ScheduleApi.Service serviceMock;

  private ScheduleService scheduleService;

  @Before
  public void setUp() {
    serviceMock = mock(Service.class);
    scheduleService = new ScheduleService(serviceMock);
  }

  @Test
  public void testConstructScheduleService1() {
    scheduleService = new ScheduleService();

    assertTrue(scheduleService instanceof ScheduleService);
  }

  @Test
  public void testConstructScheduleService2() {
    scheduleService = new ScheduleService(serviceMock);

    assertTrue(scheduleService instanceof ScheduleService);
    assertEquals(serviceMock, scheduleService.getService());
  }

  @Test
  public void testGetSchedules() {
    final Call<List<ScheduleResponse>> callScheduleMock = mock(Call.class);
    final Callback<List<ScheduleResponse>> callbackScheduleMock = mock(Callback.class);
    when(serviceMock.getSchedules()).thenReturn(callScheduleMock);
    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<ScheduleResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        List<ScheduleResponse> listSchedule = new ArrayList<>();
        callback.onResponse(callScheduleMock, Response.success(listSchedule));

        return null;
      }
    }).when(callScheduleMock).enqueue(any(Callback.class));

    scheduleService.getSchedules(callbackScheduleMock);

    verify(callbackScheduleMock, times(1)).onResponse(any(Call.class), any(Response.class));
  }

  @Test
  public void testPostSchedules() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Callback<BaseResponse> callbackBaseMock = mock(Callback.class);
    when(serviceMock.postSchedules(any(List.class))).thenReturn(callBaseMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callBaseMock, Response.success(new BaseResponse()));

        return null;
      }
    }).when(callBaseMock).enqueue(any(Callback.class));

    scheduleService.postSchedules(new ArrayList<ScheduleResponse>(), callbackBaseMock);

    verify(callbackBaseMock, times(1)).onResponse(any(Call.class), any(Response.class));
  }

  @Test
  public void testDeleteSchedules() {
    final Call<BaseResponse> callBaseMock = mock(Call.class);
    final Callback<BaseResponse> callbackBaseMock = mock(Callback.class);
    when(serviceMock.deleteSchedules(anyMap())).thenReturn(callBaseMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callBaseMock, Response.success(new BaseResponse()));

        return null;
      }
    }).when(callBaseMock).enqueue(any(Callback.class));

    scheduleService.deleteSchedules("123456789", callbackBaseMock);

    verify(callbackBaseMock, times(1)).onResponse(any(Call.class), any(Response.class));
  }
}
