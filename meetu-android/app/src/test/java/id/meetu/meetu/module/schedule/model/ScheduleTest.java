package id.meetu.meetu.module.schedule.model;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import java.util.Date;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ricky on 11/03/18.
 */

@RunWith(PowerMockRunner.class)
public class ScheduleTest {

  @Test
  public void testConstructModel1() {
    Schedule schedule = new Schedule();
    assertNotNull(schedule);
  }

  @Test
  public void testConstructModel2() {
    String title = "makan-makan kintan";
    Date startTime = new Date(2018, 3, 12);
    Date endTime = new Date(2018, 3, 12);

    Schedule schedule = new Schedule(title, startTime, endTime);

    assertEquals(title, schedule.getTitle());
    assertEquals(startTime, schedule.getStartTime());
    assertEquals(endTime, schedule.getEndTime());
  }

  @Test
  public void testSetterGetterId() {
    Schedule schedule = new Schedule();
    schedule.setId(1);
    assertEquals(schedule.getId(), 1);
  }

  @Test
  public void testSetterGetterTitle() {
    Schedule schedule = new Schedule();
    schedule.setTitle("Schedule Ricky");
    assertEquals(schedule.getTitle(), "Schedule Ricky");
  }

  @Test
  public void testSetterGetterGroupId() {
    Schedule schedule = new Schedule();
    schedule.setParentId("parent1");
    assertEquals(schedule.getParentId(), "parent1");
  }

  @Test
  public void testSetterGetterStartTime() {
    Schedule schedule = new Schedule();
    Date currDate = new Date();
    schedule.setStartTime(currDate);
    assertEquals(schedule.getStartTime(), currDate);
  }

  @Test
  public void testSetterGetterEndTime() {
    Schedule schedule = new Schedule();
    Date currDate = new Date();
    schedule.setEndTime(currDate);
    assertEquals(schedule.getEndTime(), currDate);
  }
}
