package id.meetu.meetu.module.group.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.*;

import java.util.ArrayList;
import org.joda.time.DateTime;
import org.junit.Test;

public class GroupMembersTest {
  @Test
  public void testConstructGroupMemberDetail() {
    GroupMembers detail = new GroupMembers(1, "PPL", new DateTime(), new ArrayList<GroupMemberDetail>());
    assertNotNull(detail);
  }

  @Test
  public void testSetterGetterTitle() {
    GroupMembers detail = new GroupMembers(1, "PPL", new DateTime(), new ArrayList<GroupMemberDetail>());
    detail.setTitle("BEM");
    assertEquals(detail.getTitle(), "BEM");
  }

  @Test
  public void testSetterGetterId() {
    GroupMembers detail = new GroupMembers(1, "PPL", new DateTime(), new ArrayList<GroupMemberDetail>());
    detail.setGroupId(-1);
    assertEquals(-1, detail.getGroupId());
  }

  @Test
  public void testSetterGetterDate() {
    GroupMembers detail = new GroupMembers(1, "PPL", new DateTime(), new ArrayList<GroupMemberDetail>());
    detail.setCreatedDate(new DateTime());
    assertNotNull(detail.getCreatedDate());
  }

  @Test
  public void testSetterGetterMembers() {
    GroupMembers detail = new GroupMembers(1, "PPL", new DateTime(), new ArrayList<GroupMemberDetail>());
    detail.setGroupMember(new ArrayList<GroupMemberDetail>());
    assertNotNull(detail.getGroupMember());
  }
}