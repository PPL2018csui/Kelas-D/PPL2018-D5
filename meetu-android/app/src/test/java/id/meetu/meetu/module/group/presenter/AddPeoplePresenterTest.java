package id.meetu.meetu.module.group.presenter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.model.UserProfile;
import id.meetu.meetu.module.auth.model.UserSearchRequest;
import id.meetu.meetu.module.group.contract.AddPeopleContract;
import id.meetu.meetu.module.group.model.AddMemberRequest;
import id.meetu.meetu.module.group.model.GroupMemberDetail;
import id.meetu.meetu.module.group.model.GroupMemberResponse;
import id.meetu.meetu.module.group.model.GroupMembers;
import id.meetu.meetu.module.group.model.Member;
import id.meetu.meetu.module.group.service.GroupService;
import id.meetu.meetu.util.Constant;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class AddPeoplePresenterTest {

  private AddPeopleContract.View viewMock;
  private AddPeoplePresenter presenter;
  private AddPeoplePresenter presenterWithoutService;
  private GroupService groupServiceMock;
  private GroupMembers membersMock;

  @Before
  public void setUp() {
    viewMock = PowerMockito.mock(AddPeopleContract.View.class);
    groupServiceMock = PowerMockito.mock(GroupService.class);
    presenterWithoutService = new AddPeoplePresenter(viewMock);
    presenter = new AddPeoplePresenter(viewMock, groupServiceMock);

    List<GroupMemberDetail> member = new ArrayList<>();
    member.add(new GroupMemberDetail(1, "ayaze", "asd"));
    member.add(new GroupMemberDetail(2, "evolt", "tlove"));

    membersMock = mock(GroupMembers.class);
    when(membersMock.getGroupMember()).thenReturn(member);
  }

  @Test
  public void testSearchPeopleEmpty() {
    presenter.searchPeople("");

    verify(viewMock, times(1)).hideLayoutsAndErrors();
    verify(viewMock, times(1)).setUsernameError("This field can't be empty");

    verify(viewMock, times(0)).onGetPeopleSuccess(any(GroupMemberDetail.class));
  }

  @Test
  public void testSearchPeopleSuccess() {
    final Call<User> callMock = mock(Call.class);
    final Response<User> responseMock = mock(Response.class);
    User userMock = mock(User.class);
    UserProfile profileMock = mock(UserProfile.class);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(userMock);

    when(userMock.getUserProfile()).thenReturn(profileMock);
    when(profileMock.getId()).thenReturn(1);
    when(userMock.getFirstName()).thenReturn("aaa");
    when(userMock.getLastName()).thenReturn("bbb");

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<User> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(groupServiceMock).searchUser(any(Callback.class), any(UserSearchRequest.class));

    presenter.searchPeople("graceanglc@gmail.com");

    verify(viewMock, times(1)).hideLayoutsAndErrors();

    ArgumentCaptor<GroupMemberDetail> argument = ArgumentCaptor.forClass(GroupMemberDetail.class);
    verify(viewMock, times(1)).onGetPeopleSuccess(argument.capture());

    GroupMemberDetail member = argument.getValue();
    assertEquals(1, member.getId());
    assertEquals("aaa bbb", member.getName());
  }

  @Test
  public void testSearchPeopleNotSuccess() {
    final Call<User> callMock = mock(Call.class);
    final Response<User> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<User> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(groupServiceMock).searchUser(any(Callback.class), any(UserSearchRequest.class));

    presenter.searchPeople("ayaze@gmail.com");

    verify(viewMock, times(1)).hideLayoutsAndErrors();
    verify(viewMock, times(1)).onGetPeopleNotExist();
  }

  @Test
  public void testSearchPeopleFailure() {
    final Call<User> callMock = mock(Call.class);
    final Throwable throwable = PowerMockito.mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<User> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callMock, throwable);

        return null;
      }
    }).when(groupServiceMock).searchUser(any(Callback.class), any(UserSearchRequest.class));

    presenter.searchPeople("ayaze@gmail.com");

    verify(viewMock, times(1)).hideLayoutsAndErrors();
    verify(viewMock, times(1)).onGetPeopleError("Something is wrong with your connection");
  }

  @Test
  public void testOnAddNull() {
    presenter.onAdd(membersMock, null, Constant.GROUP_CREATE_MODE);

    verify(viewMock, times(1)).onSaveError("No user selected");
  }

  @Test
  public void testOnAddIsMember() {
    presenter
        .onAdd(membersMock, new GroupMemberDetail(1, "ayaze", "ezaya"), Constant.GROUP_CREATE_MODE);

    verify(viewMock, times(1)).onSaveError("This user is already in the group");
  }

  @Test
  public void testOnAddCreate() {
    ArgumentCaptor<GroupMembers> argument = ArgumentCaptor.forClass(GroupMembers.class);

    presenter
        .onAdd(membersMock, new GroupMemberDetail(3, "azure", "eruza"), Constant.GROUP_CREATE_MODE);

    verify(viewMock, times(1)).onSaveSuccess(anyString(), argument.capture());

    GroupMembers capturedMember = argument.getValue();

    assertEquals(3, capturedMember.getGroupMember().size());
    assertEquals(3, capturedMember.getGroupMember().get(2).getId());
    assertEquals("azure", capturedMember.getGroupMember().get(2).getName());
  }

  @Test
  public void testOnAddMemberSuccess() {
    final Call<GroupMemberResponse> callMock = mock(Call.class);
    final Response<GroupMemberResponse> responseMock = mock(Response.class);
    GroupMemberResponse bodyMock = mock(GroupMemberResponse.class);
    ArgumentCaptor<GroupMembers> argument = ArgumentCaptor.forClass(GroupMembers.class);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(bodyMock);

    List<Member> members = new ArrayList<>();

    Member memberMock = mock(Member.class);
    User userMock = mock(User.class);

    when(memberMock.getUserId()).thenReturn(3);
    when(memberMock.getMemberProfile()).thenReturn(userMock);
    when(memberMock.getProfileImageUrl()).thenReturn("aku aku");
    when(userMock.getFirstName()).thenReturn("ayaze");
    when(userMock.getLastName()).thenReturn("dz");

    members.add(memberMock);

    when(bodyMock.getId()).thenReturn(777);
    when(bodyMock.getTitle()).thenReturn("makan babi");
    when(bodyMock.getCreatedDate()).thenReturn(new DateTime(2018, 1, 1, 1, 1, 1));
    when(bodyMock.getMembers()).thenReturn(members);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(groupServiceMock).addMember(any(Callback.class), any(AddMemberRequest.class));

    presenter.onAdd(membersMock, new GroupMemberDetail(3, "azure", "eruza"),
        Constant.GROUP_ADD_MEMBER_MODE);

    verify(viewMock, times(1)).onSaveSuccess(anyString(), argument.capture());

    GroupMembers capturedMember = argument.getValue();

    assertEquals(777, capturedMember.getGroupId());
    assertEquals(1, capturedMember.getGroupMember().size());
    assertEquals(3, capturedMember.getGroupMember().get(0).getId());
    assertEquals("ayaze dz", capturedMember.getGroupMember().get(0).getName());
    assertEquals("aku aku", capturedMember.getGroupMember().get(0).getProfileImageUrl());
  }

  @Test
  public void testOnAddMemberNotSuccess() {
    final Call<GroupMemberResponse> callMock = mock(Call.class);
    final Response<GroupMemberResponse> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(groupServiceMock).addMember(any(Callback.class), any(AddMemberRequest.class));

    presenter.onAdd(membersMock, new GroupMemberDetail(3, "azure", "eruza"),
        Constant.GROUP_ADD_MEMBER_MODE);

    verify(viewMock, times(1)).onSaveError("Failed to add this user");
  }

  @Test
  public void testOnAddMemberFailure() {
    final Call<GroupMemberResponse> callMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callMock, throwable);

        return null;
      }
    }).when(groupServiceMock).addMember(any(Callback.class), any(AddMemberRequest.class));

    presenter.onAdd(membersMock, new GroupMemberDetail(3, "azure", "eruza"),
        Constant.GROUP_ADD_MEMBER_MODE);

    verify(viewMock, times(1)).onSaveError("Something is wrong with your connection");
  }

}
