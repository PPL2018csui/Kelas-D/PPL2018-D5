package id.meetu.meetu.util;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

import android.content.Context;
import android.content.SharedPreferences;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 18/04/18.
 */
@RunWith(PowerMockRunner.class)
public class SharedPrefManagerTest {

  private SharedPreferences sharedPrefMock;
  private Context contextMock;
  private SharedPreferences.Editor editorMock;
  private SharedPrefManager manager;

  @Before
  public void setUp() {
    SharedPrefManager.resetManager();

    contextMock = PowerMockito.mock(Context.class);
    sharedPrefMock = PowerMockito.mock(SharedPreferences.class);
    editorMock = PowerMockito.mock(SharedPreferences.Editor.class);

    when(contextMock.getApplicationContext()).thenReturn(contextMock);
    when(contextMock.getSharedPreferences(anyString(), anyInt())).thenReturn(sharedPrefMock);
    when(sharedPrefMock.edit()).thenReturn(editorMock);
    when(editorMock.clear()).thenReturn(editorMock);

    SharedPrefManager.getInstance(contextMock);
    manager = SharedPrefManager.getInstance(null);

    when(sharedPrefMock.getString(anyString(), anyString())).thenReturn("asd");
    when(sharedPrefMock.getInt(anyString(), anyInt())).thenReturn(666);
    when(sharedPrefMock.getLong(anyString(), anyLong())).thenReturn(666L);
    when(sharedPrefMock.getBoolean(anyString(), anyBoolean())).thenReturn(false);
  }

  @Test
  public void testReadWriteLong() {
    String key = "inibuattestingdoangya";
    int defValue = 777;

    assertEquals(666, manager.readLong(key, defValue));

    manager.writeLong(key, defValue);
    verify(editorMock, times(1)).putLong(key, defValue);
    verify(editorMock, times(1)).apply();
  }

  @Test
  public void testReadWriteBoolean() {
    String key = "inibuattestingdoangya";
    boolean defValue = true;

    assertEquals(false, manager.readBoolean(key, defValue));

    manager.writeBoolean(key, defValue);
    verify(editorMock, times(1)).putBoolean(key, defValue);
    verify(editorMock, times(1)).apply();
  }

  @Test
  public void testReadWriteInt() {
    String key = "inibuattestingdoangya";
    int defValue = 777;

    assertEquals(666, manager.readInt(key, defValue));

    manager.writeInt(key, defValue);
    verify(editorMock, times(1)).putInt(key, defValue);
    verify(editorMock, times(1)).apply();
  }

  @Test
  public void testReadWriteString() {
    String key = "inibuattestingdoangya";
    String defValue = "defval";

    assertEquals("asd", manager.readString(key, defValue));

    manager.writeString(key, defValue);
    verify(editorMock, times(1)).putString(key, defValue);
    verify(editorMock, times(1)).apply();
  }

  @Test
  public void testRemove() {
    String key = "inibuattestingdoangya";
    String defValue = "defval";

    manager.remove(key);
    verify(editorMock, times(1)).remove(key);
    verify(editorMock, times(1)).apply();
  }

  @Test
  public void testClear() {
    manager.clear();

    verify(editorMock, times(1)).clear();
    verify(editorMock, times(1)).apply();
  }
}
