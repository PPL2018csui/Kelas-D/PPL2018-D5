package id.meetu.meetu.module.profile.presenter;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.profile.contract.ChangePasswordContract;
import id.meetu.meetu.module.profile.model.ChangePasswordRequest;
import id.meetu.meetu.module.profile.service.ProfileService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andre on 5/5/2018.
 */


@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class ChangePasswordPresenterTest {

  private ChangePasswordContract.View viewMock;
  private ChangePasswordPresenter presenter;
  private ChangePasswordPresenter presenterWithService;
  private ProfileService profileService;

  @Before
  public void setUp() {
    viewMock = PowerMockito.mock(ChangePasswordContract.View.class);
    presenter = new ChangePasswordPresenter(viewMock);
    profileService = PowerMockito.mock(ProfileService.class);
    presenterWithService = new ChangePasswordPresenter(viewMock, profileService);
  }

  @Test
  public void testConstructor() {
    presenter = new ChangePasswordPresenter(viewMock);

    assertTrue(presenter instanceof ChangePasswordPresenter);
  }

  @Test
  public void testSubmitSuccess() {
    final Call<Void> callBaseMock = mock(Call.class);
    final Response<Void> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(true);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<Void> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(profileService).changePassword(any(Callback.class), any(ChangePasswordRequest.class));

    presenterWithService.submit("abc", "abc");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setNewPasswordError(anyString());
    verify(viewMock, times(0)).setVerifyPasswordError(anyString());
    verify(viewMock, times(1)).onSubmitSuccess(anyString());
  }

  @Test
  public void testSubmitNotSuccess() {

    final Call<Void> callBaseMock = mock(Call.class);
    final Response<Void> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<Void> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(profileService).changePassword(any(Callback.class), any(ChangePasswordRequest.class));

    presenterWithService.submit("abc", "abc");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setNewPasswordError(anyString());
    verify(viewMock, times(0)).setVerifyPasswordError(anyString());
    verify(viewMock, times(1)).onSubmitFail(anyString());
  }

  @Test
  public void testSubmitFailure() {

    final Call<Void> callBaseMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<Void> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callBaseMock, throwable);

        return null;
      }
    }).when(profileService).changePassword(any(Callback.class), any(ChangePasswordRequest.class));

    presenterWithService.submit("abc", "abc");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setNewPasswordError(anyString());
    verify(viewMock, times(0)).setVerifyPasswordError(anyString());
    verify(viewMock, times(1)).onSubmitFail(anyString());
  }

  @Test
  public void testEmptyNewPassword() {
    presenter.submit("", "abc");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(2)).setNewPasswordError(anyString());
    verify(viewMock, times(0)).setVerifyPasswordError(anyString());
  }

  @Test
  public void testEmptyVerifyPassword() {
    presenter.submit("abc", "");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setNewPasswordError(anyString());
    verify(viewMock, times(1)).setVerifyPasswordError(anyString());
  }

  @Test
  public void testNotSamePassword() {
    presenter.submit("abc", "cba");

    verify(viewMock, times(1)).disableAllError();
    verify(viewMock, times(0)).setNewPasswordError(anyString());
    verify(viewMock, times(1)).setVerifyPasswordError(anyString());
  }
}
