package id.meetu.meetu.module.group.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.*;

import id.meetu.meetu.module.auth.model.User;
import java.util.ArrayList;
import org.joda.time.DateTime;
import org.junit.Test;

public class MemberTest {
  @Test
  public void testConstructMember() {
    Member detail = new Member(1, "+999999999", "Fasilkom UI", new User(), "asd");
    assertNotNull(detail);
  }

  @Test
  public void testSetterGetterId() {
    Member detail = new Member(1, "+999999999", "Fasilkom UI", new User(), "asd");
    detail.setUserId(2);
    assertEquals(detail.getUserId(), 2);
  }

  @Test
  public void testSetterGetterPhone() {
    Member detail = new Member(1, "+999999999", "Fasilkom UI", new User(), "asd");
    detail.setPhoneNumber("+9999998");
    assertEquals(detail.getPhoneNumber(), "+9999998");
  }

  @Test
  public void testSetterGetterLocation() {
    Member detail = new Member(1, "+999999999", "Fasilkom UI", new User(), "asd");
    detail.setLocation("Rumah");
    assertEquals(detail.getLocation(), "Rumah");
  }

  @Test
  public void testSetterGetterUser() {
    Member detail = new Member(1, "+999999999", "Fasilkom UI", new User(), "asd");
    detail.setMemberProfile(null);
    assertNull(detail.getMemberProfile());
  }

  @Test
  public void testSetterGetterProfileImageUrl() {
    Member detail = new Member(1, "+999999999", "Fasilkom UI", new User(), "asd");
    detail.setProfileImageUrl("makanan");
    assertEquals("makanan", detail.getProfileImageUrl());
  }
}