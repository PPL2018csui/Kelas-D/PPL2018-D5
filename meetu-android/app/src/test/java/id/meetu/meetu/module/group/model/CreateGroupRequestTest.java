package id.meetu.meetu.module.group.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class CreateGroupRequestTest {

  private CreateGroupRequest request;

  @Before
  public void setUp() {
    request = new CreateGroupRequest("meetu peeps", Arrays.asList(1, 2, 3));
  }

  @Test
  public void testCreate() {
    request = new CreateGroupRequest("meetu peeps", Arrays.asList(1, 2, 3));

    assertTrue(request instanceof CreateGroupRequest);
    assertEquals("meetu peeps", request.getTitle());
    assertEquals(Arrays.asList(1, 2, 3), request.getUserIds());
  }

  @Test
  public void testSetterGetterTitle() {
    request.setTitle("asd");

    assertEquals("asd", request.getTitle());
  }

  @Test
  public void testSetterGetterUserIds() {
    request.setUserIds(Arrays.asList(3, 4));

    assertEquals(Arrays.asList(3, 4), request.getUserIds());
  }
}
