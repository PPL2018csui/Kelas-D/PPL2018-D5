package id.meetu.meetu.module.notifications.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.notification.model.NotificationResponse;
import id.meetu.meetu.module.notification.service.NotificationService;
import id.meetu.meetu.service.NotificationApi;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class NotificationServiceTest {

  private NotificationService notificationService;
  private NotificationApi.Service serviceMock;

  @Before
  public void setUp() {
    serviceMock = mock(NotificationApi.Service.class);
    notificationService = new NotificationService(serviceMock);
  }

  @Test
  public void testConstruct() {
    notificationService = new NotificationService();

    assertNotNull(notificationService);
    assertTrue(notificationService instanceof NotificationService);
  }

  @Test
  public void testGetNotifications() {
    final Call<List<NotificationResponse>> callMock = mock(Call.class);
    final Callback<List<NotificationResponse>> callbackMock = mock(Callback.class);
    final Response<List<NotificationResponse>> responseMock = mock(Response.class);

    when(serviceMock.getNotifications()).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<NotificationResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        List<NotificationResponse> listSchedule = new ArrayList<>();
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    notificationService.getNotifications(callbackMock);

    verify(callbackMock, times(1)).onResponse(callMock, responseMock);

  }
}
