package id.meetu.meetu.module.profile.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.meetup.model.MeetUpResponse;
import id.meetu.meetu.module.profile.model.ChangePasswordRequest;
import id.meetu.meetu.module.profile.model.EditProfileRequest;
import id.meetu.meetu.service.ProfileApi;
import id.meetu.meetu.service.ProfileApi.Service;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
public class ProfileServiceTest {

  private ProfileService profileService;
  private Service serviceMock;

  @Before
  public void setUp() {
    serviceMock = mock(ProfileApi.Service.class);
    profileService = new ProfileService(serviceMock);
  }

  @Test
  public void testConstruct() {
    profileService = new ProfileService();

    assertNotNull(profileService);
    assertTrue(profileService instanceof  ProfileService);
  }

  @Test
  public void testChangePassword() {
    final Call<Void> callMock = mock(Call.class);
    final Callback<Void> callbackMock = mock(Callback.class);
    final Void responseBody = mock(Void.class);
    final Response<Void> response = Response.success(responseBody);
    final ChangePasswordRequest request = mock(ChangePasswordRequest.class);

    when(serviceMock.changePassword(request)).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<Void> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, response);

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    profileService.changePassword(callbackMock, request);

    verify(callbackMock, times(1)).onResponse(callMock, response);
  }

  @Test
  public void testEditProfile() {
    final Call<AuthResponse> callMock = mock(Call.class);
    final Callback<AuthResponse> callbackMock = mock(Callback.class);
    final AuthResponse responseBody = mock(AuthResponse.class);
    final Response<AuthResponse> response = Response.success(responseBody);
    final EditProfileRequest request = mock(EditProfileRequest.class);

    when(serviceMock.editProfile(request)).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, response);

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    profileService.editProfile(callbackMock, request);

    verify(callbackMock, times(1)).onResponse(callMock, response);
  }

  @Test
  public void testGetProfile() {
    final Call<User> callMock = mock(Call.class);
    final Callback<User> callbackMock = mock(Callback.class);
    final User responseBody = mock(User.class);
    final Response<User> response = Response.success(responseBody);

    when(serviceMock.getProfile()).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<User> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, response);

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    profileService.getProfile(callbackMock);

    verify(callbackMock, times(1)).onResponse(callMock, response);
  }
}
