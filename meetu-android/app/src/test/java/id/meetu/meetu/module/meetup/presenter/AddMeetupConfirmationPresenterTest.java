package id.meetu.meetu.module.meetup.presenter;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.base.BaseResponse;
import id.meetu.meetu.module.meetup.contract.AddMeetUpConfirmationContract;
import id.meetu.meetu.module.meetup.model.MeetUpFormData;
import id.meetu.meetu.module.meetup.service.MeetUpService;
import id.meetu.meetu.util.DateInterval;
import id.meetu.meetu.util.DateUtil;
import id.meetu.meetu.util.DateUtil.RepeatType;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ayaz97 on 20/04/18.
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest({DateUtil.class, Response.class})
public class AddMeetupConfirmationPresenterTest {

  private AddMeetUpConfirmationContract.View viewMock;
  private MeetUpService serviceMock;
  private AddMeetupConfirmationPresenter presenter;
  private MeetUpFormData dataMock;

  @Before
  public void setUp() {
    mockStatic(DateUtil.class);

    viewMock = mock(AddMeetUpConfirmationContract.View.class);
    serviceMock = mock(MeetUpService.class);
    dataMock = mock(MeetUpFormData.class);
    presenter = new AddMeetupConfirmationPresenter(viewMock, serviceMock);

    DateTime start = new DateTime(2018, 3, 1, 1, 0);
    DateTime end = new DateTime(2018, 3, 1, 2, 0);
    DateInterval interval = new DateInterval(start, end);

    List<DateInterval> dummyIntervals = new ArrayList<>();
    dummyIntervals.add(interval);

    when(DateUtil
        .getEventRepetition(any(DateTime.class), any(DateTime.class), any(RepeatType.class),
            anyInt()))
        .thenReturn(dummyIntervals);

    when(dataMock.getMeetUpName()).thenReturn("ketemuan");
    when(dataMock.getGroupId()).thenReturn(1);
    when(dataMock.getGroupName()).thenReturn("ayaze");
    when(dataMock.getGroupOwner()).thenReturn(666);
    when(dataMock.getStartDate()).thenReturn(new DateTime(2018, 8, 28, 0, 0));
    when(dataMock.getStartTime()).thenReturn(new DateTime(2018, 8, 28, 8, 0));
    when(dataMock.getEndTime()).thenReturn(new DateTime(2018, 8, 28, 8, 50));
    when(dataMock.getRepeatType()).thenReturn(RepeatType.ONCE);
    when(dataMock.getRepeatCount()).thenReturn(1);
  }

  @Test
  public void testConstruct() {
    presenter = new AddMeetupConfirmationPresenter(viewMock);

    assertTrue(presenter instanceof AddMeetupConfirmationPresenter);
  }

  @Test
  public void testEmptyMeetUpName() {
    when(dataMock.getMeetUpName()).thenReturn("");

    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(1)).setMeetUpNameError("This field can't be empty");
  }

  @Test
  public void testPostMeetUpSuccess() {
    final Call<BaseResponse> callMock = mock(Call.class);
    final Response<BaseResponse> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(true);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(1, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(serviceMock).postMeetUps(any(List.class), any(Callback.class));

    presenter.submit(dataMock);

    verify(viewMock, times(1)).onFinishPostMeetUp(anyString());
  }

  @Test
  public void testPostMeetUpNotSuccess() {
    final Call<BaseResponse> callMock = mock(Call.class);
    final Response<BaseResponse> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(1, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(serviceMock).postMeetUps(any(List.class), any(Callback.class));

    presenter.submit(dataMock);

    verify(viewMock, times(1)).onErrorPostMeetUp("Failed to create MeetUp");
  }

  @Test
  public void testPostMeetUpFailure() {
    final Call<BaseResponse> callMock = mock(Call.class);
    final Throwable t = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<BaseResponse> callback = invocation.getArgumentAt(1, Callback.class);
        callback.onFailure(callMock, t);

        return null;
      }
    }).when(serviceMock).postMeetUps(any(List.class), any(Callback.class));

    presenter.submit(dataMock);

    verify(viewMock, times(1)).onErrorPostMeetUp("Something is wrong with your connection");
  }
}
