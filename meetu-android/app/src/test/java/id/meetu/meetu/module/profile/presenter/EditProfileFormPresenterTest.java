package id.meetu.meetu.module.profile.presenter;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.model.UserProfile;
import id.meetu.meetu.module.profile.contract.EditProfileFormContract;
import id.meetu.meetu.module.profile.model.ChangePasswordRequest;
import id.meetu.meetu.module.profile.model.EditProfileFormData;
import id.meetu.meetu.module.profile.model.EditProfileRequest;
import id.meetu.meetu.module.profile.service.ProfileService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andre on 5/5/2018.
 */


@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class EditProfileFormPresenterTest {

  private EditProfileFormContract.View viewMock;
  private EditProfileFormPresenter presenter;
  private EditProfileFormPresenter presenterWithService;
  private ProfileService profileService;
  private EditProfileFormData dataMock;

  @Before
  public void setUp() {
    viewMock = PowerMockito.mock(EditProfileFormContract.View.class);
    presenter = new EditProfileFormPresenter(viewMock);
    dataMock = PowerMockito.mock(EditProfileFormData.class);
    profileService = PowerMockito.mock(ProfileService.class);
    presenterWithService = new EditProfileFormPresenter(viewMock, profileService);

    when(dataMock.getFirstName()).thenReturn("andre");
    when(dataMock.getLastName()).thenReturn("pratama");
    when(dataMock.getPhoneNumber()).thenReturn("082121212121");
    when(dataMock.getEmail()).thenReturn("andre@gmail.com");
    when(dataMock.getLocation()).thenReturn("jakarta");
  }

  @Test
  public void testConstructor() {
    presenter = new EditProfileFormPresenter(viewMock);

    assertTrue(presenter instanceof EditProfileFormPresenter);
  }

  @Test
  public void testSubmitSuccess() {
    final Call<AuthResponse> callBaseMock = mock(Call.class);
    final Response<AuthResponse> responseMock = mock(Response.class);
    AuthResponse responseMockBody = mock(AuthResponse.class);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(responseMockBody);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(profileService).editProfile(any(Callback.class), any(EditProfileRequest.class));

    presenterWithService.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(1)).onSubmitSuccess(any(EditProfileFormData.class), anyString());
  }

  @Test
  public void testSubmitNotSuccess() {

    final Call<AuthResponse> callBaseMock = mock(Call.class);
    final Response<AuthResponse> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callBaseMock, responseMock);

        return null;
      }
    }).when(profileService).editProfile(any(Callback.class), any(EditProfileRequest.class));

    presenterWithService.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(1)).onSubmitFail(anyString());
  }

  @Test
  public void testSubmitFailure() {

    final Call<AuthResponse> callBaseMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callBaseMock, throwable);

        return null;
      }
    }).when(profileService).editProfile(any(Callback.class), any(EditProfileRequest.class));

    presenterWithService.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(1)).onSubmitFail(anyString());
  }

  @Test
  public void testFailFirstName() {
    when(dataMock.getFirstName()).thenReturn("");
    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).onSubmitSuccess(any(EditProfileFormData.class), any(String.class));
    verify(viewMock, times(1)).setFirstNameError(anyString());
  }

  @Test
  public void testFailLastName() {
    when(dataMock.getLastName()).thenReturn("");
    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).onSubmitSuccess(any(EditProfileFormData.class), any(String.class));
    verify(viewMock, times(1)).setLastNameError(anyString());
  }

  @Test
  public void testFailPhoneNumber() {
    when(dataMock.getPhoneNumber()).thenReturn("");
    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).onSubmitSuccess(any(EditProfileFormData.class), any(String.class));
    verify(viewMock, times(1)).setPhoneNumberError(anyString());
  }

  @Test
  public void testInvalidPhoneNumber() {
    when(dataMock.getPhoneNumber()).thenReturn("hehe");
    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).onSubmitSuccess(any(EditProfileFormData.class), any(String.class));
    verify(viewMock, times(1)).setPhoneNumberError(anyString());
  }

  @Test
  public void testFailLocation() {
    when(dataMock.getLocation()).thenReturn("");
    presenter.submit(dataMock);

    verify(viewMock, times(1)).resetAllError();
    verify(viewMock, times(0)).onSubmitSuccess(any(EditProfileFormData.class), any(String.class));
    verify(viewMock, times(1)).setLocationError(anyString());
  }

}
