package id.meetu.meetu.module.group.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 27/03/18.
 */
@RunWith(PowerMockRunner.class)
public class GroupResponseTest {

  @Test
  public void testConstructGroupResponse() {
    int id = 2;
    int owner = 1;
    String title = "meetu peeps";

    GroupResponse groupResponse = new GroupResponse(id, owner, title);

    assertEquals(id, groupResponse.getId());
    assertEquals(owner, groupResponse.getOwner());
    assertEquals(title, groupResponse.getTitle());
  }

  @Test
  public void testSetterGetterId() {
    int id = 2;
    int owner = 1;
    String title = "meetu peeps";

    GroupResponse groupResponse = new GroupResponse(id, owner, title);

    groupResponse.setId(1);
    assertEquals(1, groupResponse.getId());
  }

  @Test
  public void testSetterGetterOwner() {
    int id = 2;
    int owner = 1;
    String title = "meetu peeps";

    GroupResponse groupResponse = new GroupResponse(id, owner, title);

    groupResponse.setOwner(2);
    assertEquals(2, groupResponse.getOwner());
  }

  @Test
  public void testSetterGetterTitle() {
    int id = 1;
    int owner = 1;
    String title = "meetu peeps";

    GroupResponse groupResponse = new GroupResponse(id, owner, title);

    groupResponse.setTitle("risk tekers");
    assertEquals("risk tekers", groupResponse.getTitle());
  }
}
