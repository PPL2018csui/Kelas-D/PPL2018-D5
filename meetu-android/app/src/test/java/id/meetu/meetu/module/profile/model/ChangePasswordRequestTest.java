package id.meetu.meetu.module.profile.model;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class ChangePasswordRequestTest {

  private ChangePasswordRequest request;

  @Before
  public void setUp() {
    request = new ChangePasswordRequest("a", "b");
  }

  @Test
  public void testSetterGetterPassword() {
    request.setPassword("celana");
    assertEquals("celana", request.getPassword());
  }

  @Test
  public void testSetterGetterConfirmPassword() {
    request.setConfirmPassword("celana");
    assertEquals("celana", request.getConfirmPassword());
  }
}
