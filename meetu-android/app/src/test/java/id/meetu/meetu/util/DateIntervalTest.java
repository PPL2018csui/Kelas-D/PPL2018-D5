package id.meetu.meetu.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import id.meetu.meetu.BuildConfig;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

/**
 * Created by ayaz97 on 07/03/18.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class DateIntervalTest {

  @Test
  public void testCreateDateInterval() {
    DateTime start = new DateTime(2018, 3, 1, 1, 0, DateTimeZone.UTC);

    DateTime end = new DateTime(2018, 3, 1, 2, 0, DateTimeZone.UTC);

    DateInterval interval = new DateInterval(start, end);

    assertEquals(start, interval.getStart());
    assertEquals(end, interval.getEnd());
  }

  @Test
  public void testChangeDateInterval() {
    DateTime start = new DateTime(2018, 3, 1, 1, 0, DateTimeZone.UTC);
    DateTime end = new DateTime(2018, 3, 1, 2, 0, DateTimeZone.UTC);

    DateInterval interval = new DateInterval(start, end);
    interval.setStart(end);
    interval.setEnd(start);

    assertEquals(end, interval.getStart());
    assertEquals(start, interval.getEnd());
  }

  @Test
  public void testNullEquals() {
    DateTime start = new DateTime(2018, 3, 1, 1, 0);
    DateTime end = new DateTime(2018, 3, 1, 2, 0);
    DateInterval interval = new DateInterval(start, end);

    assertNotEquals(interval, null);
  }

  @Test
  public void testDifferentObjectEquals() {
    DateTime start = new DateTime(2018, 3, 1, 1, 0);
    DateTime end = new DateTime(2018, 3, 1, 2, 0);
    DateInterval interval = new DateInterval(start, end);

    assertNotEquals(interval, start);
  }

  @Test
  public void testEqualObject() {
    DateTime start = new DateTime(2018, 3, 1, 1, 0);
    DateTime end = new DateTime(2018, 3, 1, 2, 0);
    DateInterval interval = new DateInterval(start, end);

    assertEquals(interval, interval);
  }

  @Test
  public void testEqualStartAndEnd() {
    DateTime start = new DateTime(2018, 3, 1, 1, 0);
    DateTime end = new DateTime(2018, 3, 1, 2, 0);
    DateInterval interval1 = new DateInterval(start, end);
    DateInterval interval2 = new DateInterval(start, end);

    assertEquals(interval1, interval2);
  }

  @Test
  public void testDifferentStart() {
    DateTime start = new DateTime(2018, 3, 1, 1, 0);
    DateTime end = new DateTime(2018, 3, 1, 2, 0);

    DateInterval interval1 = new DateInterval(start, end);
    DateInterval interval2 = new DateInterval(start, end);
    interval2.setStart(start.plusMinutes(1));

    assertNotEquals(interval1, interval2);
  }

  @Test
  public void testDifferentEnd() {
    DateTime start = new DateTime(2018, 3, 1, 1, 0);
    DateTime end = new DateTime(2018, 3, 1, 2, 0);

    DateInterval interval1 = new DateInterval(start, end);
    DateInterval interval2 = new DateInterval(start, end);
    interval2.setEnd(end.plusMinutes(1));

    assertNotEquals(interval1, interval2);
  }

  @Test
  public void testDifferentStartAndEnd() {
    DateTime start = new DateTime(2018, 3, 1, 1, 0);
    DateTime end = new DateTime(2018, 3, 1, 2, 0);

    DateInterval interval1 = new DateInterval(start, end);
    DateInterval interval2 = new DateInterval(start, end);
    interval2.setStart(start.plusMinutes(1));
    interval2.setEnd(end.plusMinutes(1));

    assertNotEquals(interval1, interval2);
  }
}
