package id.meetu.meetu.module.meetup.model;

import static org.junit.Assert.assertEquals;

import id.meetu.meetu.module.group.model.GroupResponse;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by ayaz97 on 27/03/18.
 */
@RunWith(PowerMockRunner.class)
public class MeetUpResponseTest {

  private MeetUpResponse meetUpResponse;

  @Before
  public void setup() {
    String title = "asd";
    GroupResponse groupResponse = new GroupResponse(2, 1, "dsa");
    DateTime startTime = new DateTime(2018, 3, 1, 0, 0);
    DateTime endTime = new DateTime(2018, 3, 2, 0, 0);

    meetUpResponse = new MeetUpResponse(title, groupResponse, startTime, endTime);
  }

  @Test
  public void testConstructMeetUpResponse() {
    String title = "asd";
    GroupResponse groupResponse = new GroupResponse(2, 1, "dsa");
    DateTime startTime = new DateTime(2018, 3, 1, 0, 0);
    DateTime endTime = new DateTime(2018, 3, 2, 0, 0);

    MeetUpResponse response = new MeetUpResponse(title, groupResponse, startTime, endTime);

    assertEquals(title, response.getTitle());
    assertEquals(groupResponse, response.getGroupResponse());
    assertEquals(startTime, response.getStartTime());
    assertEquals(endTime, response.getEndTime());
  }

  @Test
  public void testSetterGetterTitle() {
    String title = "lol";

    meetUpResponse.setTitle(title);
    assertEquals(title, meetUpResponse.getTitle());
  }

  @Test
  public void testSetterGetterGroupResponse() {
    GroupResponse groupResponse = new GroupResponse(2, 3, "sda");

    meetUpResponse.setGroupResponse(groupResponse);
    assertEquals(groupResponse, meetUpResponse.getGroupResponse());
  }

  @Test
  public void testSetterGetterStartTime() {
    DateTime startTime = new DateTime(2018, 3, 3, 0, 0);

    meetUpResponse.setStartTime(startTime);
    assertEquals(startTime, meetUpResponse.getStartTime());
  }

  @Test
  public void testSetterGetterEndTime() {
    DateTime endTime = new DateTime(2018, 3, 3, 0, 0);

    meetUpResponse.setEndTime(endTime);
    assertEquals(endTime, meetUpResponse.getEndTime());
  }
}
