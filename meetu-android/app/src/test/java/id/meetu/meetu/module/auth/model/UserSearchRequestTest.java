package id.meetu.meetu.module.auth.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class UserSearchRequestTest {

  @Test
  public void testSetterGetter() {
    UserSearchRequest request = new UserSearchRequest("ayaze");
    assertEquals("ayaze", request.getUsername());

    request.setUsername("asd");
    assertEquals("asd", request.getUsername());
  }
}
