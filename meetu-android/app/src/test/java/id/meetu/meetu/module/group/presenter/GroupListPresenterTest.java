package id.meetu.meetu.module.group.presenter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.group.contract.GroupListContract;
import id.meetu.meetu.module.group.model.GroupDetail;
import id.meetu.meetu.module.group.model.GroupResponse;
import id.meetu.meetu.module.group.service.GroupService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andre on 4/12/2018.
 */

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class GroupListPresenterTest {

  private GroupListContract.View viewMock;
  private GroupListPresenter presenter;
  private GroupListPresenter presenterWithoutService;
  private GroupService groupServiceMock;


  @Before
  public void setUp() {
    viewMock = PowerMockito.mock(GroupListContract.View.class);
    groupServiceMock = PowerMockito.mock(GroupService.class);
    presenterWithoutService = new GroupListPresenter(viewMock);
    presenter = new GroupListPresenter(viewMock, groupServiceMock);
  }

  @Test
  public void testGetGroupsSuccess() {
    final Call<List<GroupResponse>> callGroupMock = mock(Call.class);

    GroupResponse groupResponse = new GroupResponse(2, 1, "meetu peeps");
    final List<GroupResponse> responseBody = new ArrayList<>();
    responseBody.add(groupResponse);

    GroupDetail groupDetail = new GroupDetail(2, "meetu peeps", 1);

    final Response<List<GroupResponse>> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(responseBody);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<GroupResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callGroupMock, responseMock);

        return null;
      }
    }).when(groupServiceMock).getGroups(any(Callback.class));

    presenter.getGroups();

    ArgumentCaptor<List> argument = ArgumentCaptor.forClass(List.class);
    verify(viewMock, times(1)).onGetSuccess(argument.capture());

    GroupDetail resultDetail = (GroupDetail) argument.getValue().get(0);

    assertEquals(groupDetail.getId(), resultDetail.getId());
    assertEquals(groupDetail.getTitle(), resultDetail.getTitle());
    assertEquals(groupDetail.getOwner(), resultDetail.getOwner());
  }

  @Test
  public void testGetGroupsNotSuccess() {
    final Call<List<GroupResponse>> callGroupMock = mock(Call.class);
    final Response<List<GroupResponse>> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<GroupResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callGroupMock, responseMock);

        return null;
      }
    }).when(groupServiceMock).getGroups(any(Callback.class));

    presenter.getGroups();

    verify(viewMock, times(1)).onError("Something is wrong");
  }

  @Test
  public void testGetGroupsFailure() {
    final Call<List<GroupResponse>> callGroupMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<GroupResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callGroupMock, throwable);

        return null;
      }
    }).when(groupServiceMock).getGroups(any(Callback.class));

    presenter.getGroups();

    verify(viewMock, times(1)).onError("Something is wrong with your connection");
  }
}
