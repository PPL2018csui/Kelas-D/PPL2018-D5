package id.meetu.meetu.module.profile.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 * Created by Andre on 5/3/2018.
 */


@RunWith(PowerMockRunner.class)
public class EditProfileFormDataTest {

  @Test
  public void testConstructor() {
    EditProfileFormData data = new EditProfileFormData();

    assertTrue(data instanceof EditProfileFormData);
  }

  @Test
  public void testBuilder() {
    String firstName = "andre";
    String lastName = "pratama";
    String phoneNumber = "0818121812181";
    String email = "andre@gmail.com";
    String location = "jakarta";
    String profileImageURL = "https://google.com";

    EditProfileFormData data = new EditProfileFormData.Builder().setFirstName(firstName)
        .setLastName(lastName)
        .setPhoneNumber(phoneNumber)
        .setEmail(email).setLocation(location)
        .setProfileImageURL(profileImageURL).create();

    assertEquals(firstName, data.getFirstName());
    assertEquals(lastName, data.getLastName());
    assertEquals(phoneNumber, data.getPhoneNumber());
    assertEquals(email, data.getEmail());
    assertEquals(location, data.getLocation());
  }

  @Test
  public void testSetterGetterFirstName() {
    EditProfileFormData data = new EditProfileFormData();
    data.setFirstName("andre");

    assertEquals("andre", data.getFirstName());
  }

  @Test
  public void testSetterGetterLastName() {
    EditProfileFormData data = new EditProfileFormData();
    data.setLastName("pratama");

    assertEquals("pratama", data.getLastName());
  }

  @Test
  public void testSetterGetterPhoneNumber() {
    EditProfileFormData data = new EditProfileFormData();
    data.setPhoneNumber("phoneNumber");

    assertEquals("phoneNumber", data.getPhoneNumber());
  }

  @Test
  public void testSetterGetterEmail() {
    EditProfileFormData data = new EditProfileFormData();
    data.setEmail("email");

    assertEquals("email", data.getEmail());
  }

  @Test
  public void testSetterGetterLocation() {
    EditProfileFormData data = new EditProfileFormData();
    data.setLocation("jakarta");

    assertEquals("jakarta", data.getLocation());
  }

  @Test
  public void testSetterGetterProfileImageURL() {
    EditProfileFormData data = new EditProfileFormData();
    data.setProfileImageURL("https://facebook.com");

    assertEquals("https://facebook.com", data.getProfileImageURL());
  }

}

