package id.meetu.meetu.module.group.presenter;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.model.UserProfile;
import id.meetu.meetu.module.group.contract.GroupMembersContract;
import id.meetu.meetu.module.group.model.GroupMemberResponse;
import id.meetu.meetu.module.group.model.GroupMembers;
import id.meetu.meetu.module.group.model.Member;
import id.meetu.meetu.module.group.service.GroupService;
import java.util.ArrayList;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest(Response.class)
public class GroupMemberPresenterTest {

  private GroupMembersContract.View viewMock;
  private GroupMemberPresenter presenter;
  private GroupMemberPresenter presenterWithoutService;
  private GroupService groupServiceMock;

  @Before
  public void setUp() {
    viewMock = PowerMockito.mock(GroupMembersContract.View.class);
    groupServiceMock = PowerMockito.mock(GroupService.class);
    presenterWithoutService = new GroupMemberPresenter(viewMock);
    presenter = new GroupMemberPresenter(viewMock, groupServiceMock);
  }

  @Test
  public void testGetGroupMembersSuccess() {
    final Call<GroupMemberResponse> callGroupMock = mock(Call.class);

    DateTime dateTime = new DateTime();
    GroupMemberResponse groupResponse = new GroupMemberResponse(1, new ArrayList<Member>(), "PPL",
        dateTime);
    groupResponse.getMembers().add(new Member(1, "+999999", "Fasilkom UI", new User(), "asd"));
    final GroupMemberResponse responseBody = groupResponse;

    int groupId = 1;

    final Response<GroupMemberResponse> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(true);
    when(responseMock.body()).thenReturn(responseBody);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callGroupMock, responseMock);

        return null;
      }
    }).when(groupServiceMock).getGroupMember(any(Callback.class), any(Integer.class));

    presenter.getMembersInGroup(groupId);
    verify(viewMock, times(1)).onGetMembersSuccess(any(GroupMembers.class));
  }

  @Test
  public void testGetGroupMembersNotSuccess() {
    final Call<GroupMemberResponse> callGroupMock = mock(Call.class);
    final Response<GroupMemberResponse> responseMock = mock(Response.class);
    int groupId = 1;

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callGroupMock, responseMock);

        return null;
      }
    }).when(groupServiceMock).getGroupMember(any(Callback.class), any(Integer.class));

    presenter.getMembersInGroup(groupId);

    verify(viewMock, times(1)).onGetMembersError("There is problem with your connection");
  }

  @Test
  public void testGetGroupMembersFailure() {
    final Call<GroupMemberResponse> callGroupMock = mock(Call.class);
    final Throwable throwable = mock(Throwable.class);

    int groupId = 1;

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callGroupMock, throwable);

        return null;
      }
    }).when(groupServiceMock).getGroupMember(any(Callback.class), any(Integer.class));

    presenter.getMembersInGroup(groupId);

    verify(viewMock, times(1)).onGetMembersError("There is problem with your connection");
  }
}