package id.meetu.meetu.module.group.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

import java.util.ArrayList;
import org.joda.time.DateTime;
import org.junit.Test;

public class GroupMemberResponseTest {

  @Test
  public void testConstructGroupMemberDetailWithParam() {
    GroupMemberResponse detail = new GroupMemberResponse(1, new ArrayList<Member>(),"PPL", new DateTime());
    assertNotNull(detail);
  }

  @Test
  public void testSetterGetterId() {
    GroupMemberResponse detail = new GroupMemberResponse(1, new ArrayList<Member>(),"PPL", new DateTime());
    detail.setId(2);
    assertEquals(detail.getId(), 2);
  }

  @Test
  public void testSetterGetterMember() {
    GroupMemberResponse detail = new GroupMemberResponse(1, new ArrayList<Member>(),"PPL", new DateTime());
    detail.setMembers(null);
    assertNull(detail.getMembers());
  }

  @Test
  public void testSetterGetterTitle() {
    GroupMemberResponse detail = new GroupMemberResponse(1, new ArrayList<Member>(),"PPL", new DateTime());
    detail.setTitle("BEM");
    assertEquals(detail.getTitle(), "BEM");
  }

  @Test
  public void testSetterGetterCreatedDate() {
    GroupMemberResponse detail = new GroupMemberResponse(1, new ArrayList<Member>(),"PPL", new DateTime());
    detail.setCreatedDate(new DateTime());
    assertNotNull(detail.getCreatedDate());
  }
}