package id.meetu.meetu.module.meetup.model;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
public class TimeRecommendationResponseTest {

  private TimeRecommendationResponse timeRecommendationResponse;

  @Before
  public void setup() {
    DateTime startTime = new DateTime(2018, 4, 5, 1, 1);
    DateTime endTime = new DateTime(2018, 4, 5, 1, 10);

    timeRecommendationResponse = new TimeRecommendationResponse(startTime, endTime);
  }

  @Test
  public void testConstructMeetUpResponse() {
    DateTime startTime = new DateTime(2018, 4, 5, 1, 1);
    DateTime endTime = new DateTime(2018, 4, 5, 1, 10);

    TimeRecommendationResponse response = new TimeRecommendationResponse(startTime, endTime);

    assertEquals(startTime, response.getStartTime());
    assertEquals(endTime, response.getEndTime());
  }

  @Test
  public void setterGetterStartTime() {
    DateTime startTime = new DateTime(2018, 4, 5, 1, 1);
    timeRecommendationResponse.setStartTime(startTime);
    assertEquals(startTime, timeRecommendationResponse.getStartTime());
  }


  @Test
  public void setterGetterEndTime() {
    DateTime endTime = new DateTime(2018, 4, 5, 1, 5);
    timeRecommendationResponse.setEndTime(endTime);
    assertEquals(endTime, timeRecommendationResponse.getEndTime());
  }
}