package id.meetu.meetu.module.meetup.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.meetup.model.TimeRecommendationRequest;
import id.meetu.meetu.module.meetup.model.TimeRecommendationResponse;
import id.meetu.meetu.service.TimeRecommendationApi;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
public class TimeRecommendationServiceTest {

  @Mock
  private TimeRecommendationApi.Service serviceMock;

  private TimeRecommendationService timeRecommendationService;

  @Before
  public void setUp() {
    serviceMock = mock(TimeRecommendationApi.Service.class);
    timeRecommendationService = new TimeRecommendationService(serviceMock);
  }

  @Test
  public void testConstructTimeRecommendationService1() {
    timeRecommendationService = new TimeRecommendationService();

    assertTrue(timeRecommendationService instanceof TimeRecommendationService);
  }

  @Test
  public void testConstructTimeRecommendationService2() {
    timeRecommendationService = new TimeRecommendationService(serviceMock);

    assertTrue(timeRecommendationService instanceof TimeRecommendationService);
    assertEquals(serviceMock, timeRecommendationService.getService());
  }

  @Test
  public void testPostTimeRecommendations() {
    final Call<List<List<TimeRecommendationResponse>>> callListMock = mock(Call.class);
    final Callback<List<List<TimeRecommendationResponse>>> callbackBaseMock = mock(Callback.class);
    when(serviceMock.postTimeRecommendations(any(TimeRecommendationRequest.class)))
        .thenReturn(callListMock);

    doAnswer(new Answer() {
      @Override
      public Object answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<List<TimeRecommendationResponse>>> callback = invocation
            .getArgumentAt(0, Callback.class);
        List<List<TimeRecommendationResponse>> response = new ArrayList<>();
        callback.onResponse(callListMock, Response.success(response));

        return null;
      }
    }).when(callListMock).enqueue(any(Callback.class));

    int groupId = 666;
    DateTime timeZone = new DateTime(1900, 1, 1, 0, 0);
    DateTime startDate = new DateTime(2018, 4, 11, 0, 0);
    DateTime endDate = new DateTime(2018, 4, 21, 0, 0);
    String repeatType = "ONCE";
    int repeatCount = 1;
    TimeRecommendationRequest request = new TimeRecommendationRequest(groupId, timeZone, startDate,
        endDate,
        repeatType, repeatCount);
    timeRecommendationService.postTimeRecommendations(request, callbackBaseMock);

    verify(callbackBaseMock, times(1)).onResponse(any(Call.class), any(Response.class));

  }
}
