package id.meetu.meetu.firebase;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import android.util.Log;
import id.meetu.meetu.module.auth.model.AuthResponse;
import id.meetu.meetu.module.auth.model.UpdateTokenRequest;
import id.meetu.meetu.module.auth.service.AuthService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
@PrepareForTest({Response.class, Log.class})
public class FirebasePresenterTest {

  private FirebaseContract.View viewMock;
  private FirebasePresenter presenter;
  private AuthService authServiceMock;

  @Before
  public void setUp() {
    PowerMockito.mockStatic(Log.class);
    authServiceMock = mock(AuthService.class);
    viewMock = mock(FirebaseContract.View.class);
    presenter = new FirebasePresenter(viewMock, authServiceMock);
  }

  @Test
  public void testConstruct() {
    presenter = new FirebasePresenter(viewMock);

    assertNotNull(presenter);
  }

  @Test
  public void testUpdateFirebaseTokenSuccess() {
    final Call<AuthResponse> callMock = mock(Call.class);
    final Response<AuthResponse> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(true);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(authServiceMock)
        .updateFirebaseToken(any(Callback.class), any(UpdateTokenRequest.class));

    presenter.updateFirebaseToken("asd");

    verify(responseMock, times(1)).isSuccessful();
    verify(viewMock, times(1)).onFinishUpdateFirebaseToken("asd");
  }

  @Test
  public void testUpdateFirebaseTokenNotSuccess() {
    final Call<AuthResponse> callMock = mock(Call.class);
    final Response<AuthResponse> responseMock = mock(Response.class);

    when(responseMock.isSuccessful()).thenReturn(false);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, responseMock);

        return null;
      }
    }).when(authServiceMock)
        .updateFirebaseToken(any(Callback.class), any(UpdateTokenRequest.class));

    presenter.updateFirebaseToken("asd");

    verify(responseMock, times(1)).isSuccessful();
    verify(viewMock, times(1)).onFailUpdateFirebaseToken("Failed to update firebase token, please restart the app");
  }

  @Test
  public void testUpdateFirebaseTokenFail() {
    final Call<AuthResponse> callMock = mock(Call.class);
    final Throwable throwableMock = mock(Throwable.class);

    when(throwableMock.getMessage()).thenReturn("batu");

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<AuthResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onFailure(callMock, throwableMock);

        return null;
      }
    }).when(authServiceMock)
        .updateFirebaseToken(any(Callback.class), any(UpdateTokenRequest.class));

    presenter.updateFirebaseToken("asd");

    verify(throwableMock, times(1)).getMessage();
    verify(viewMock, times(1)).onFailUpdateFirebaseToken("Something is wrong with your connection");
  }
}
