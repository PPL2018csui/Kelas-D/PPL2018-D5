package id.meetu.meetu.module.group.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

import id.meetu.meetu.module.auth.model.User;
import id.meetu.meetu.module.auth.model.UserSearchRequest;
import id.meetu.meetu.module.group.model.AddMemberRequest;
import id.meetu.meetu.module.group.model.CreateGroupRequest;
import id.meetu.meetu.module.group.model.GroupMemberResponse;
import id.meetu.meetu.module.group.model.GroupResponse;
import id.meetu.meetu.module.group.model.Member;
import id.meetu.meetu.service.GroupApi;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andre on 4/11/2018.
 */

@RunWith(PowerMockRunner.class)
@PowerMockIgnore("javax.net.ssl.*")
public class GroupServiceTest {

  @Mock
  private GroupApi.Service serviceMock;

  private GroupService groupService;

  @Before
  public void setUp() {
    serviceMock = mock(GroupApi.Service.class);
    groupService = new GroupService(serviceMock);
  }

  @Test
  public void testConstructMeetUpService1() {
    groupService = new GroupService();

    assertTrue(groupService instanceof GroupService);
  }

  @Test
  public void testConstructMeetUpService2() {
    groupService = new GroupService(serviceMock);

    assertTrue(groupService instanceof GroupService);
    assertEquals(serviceMock, groupService.getService());
  }

  @Test
  public void testGetGroups() {
    final Call<List<GroupResponse>> callGroupMock = mock(Call.class);
    final Callback<List<GroupResponse>> callbackGroupMock = mock(Callback.class);
    final List<GroupResponse> response = new ArrayList<>();

    when(serviceMock.getGroups()).thenReturn(callGroupMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<List<GroupResponse>> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callGroupMock, Response.success(response));

        return null;
      }
    }).when(callGroupMock).enqueue(any(Callback.class));

    groupService.getGroups(callbackGroupMock);

    verify(callbackGroupMock, times(1)).onResponse(any(Call.class), any(Response.class));
  }

  @Test
  public void testSearchUser() {
    final Call<User> callMock = mock(Call.class);
    final Callback<User> callbackMock = mock(Callback.class);
    final User response = new User();
    final UserSearchRequest request = new UserSearchRequest("ayaze");

    when(serviceMock.searchUser(request)).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<User> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, Response.success(response));

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    groupService.searchUser(callbackMock, request);

    verify(callbackMock, times(1)).onResponse(any(Call.class), any(Response.class));
  }

  @Test
  public void testGetGroupMembers() {
    final Call<GroupMemberResponse> callGroupMock = mock(Call.class);
    final Callback<GroupMemberResponse> callbackGroupMock = mock(Callback.class);
    final GroupMemberResponse response = new GroupMemberResponse(1, new ArrayList<Member>(), "PPL",
        new DateTime());

    when(serviceMock.getGroupMember(1)).thenReturn(callGroupMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callGroupMock, Response.success(response));

        return null;
      }
    }).when(callGroupMock).enqueue(any(Callback.class));

    groupService.getGroupMember(callbackGroupMock, 1);

    verify(callbackGroupMock, times(1)).onResponse(any(Call.class), any(Response.class));
  }

  @Test
  public void testCreateGroup() {
    final Call<GroupMemberResponse> callMock = mock(Call.class);
    final Callback<GroupMemberResponse> callbackMock = mock(Callback.class);
    final GroupMemberResponse response = new GroupMemberResponse(1, new ArrayList<Member>(), "PPL",
        new DateTime());
    final CreateGroupRequest request = new CreateGroupRequest("ayaze", Arrays.asList(1, 2));

    when(serviceMock.createGroup(request)).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, Response.success(response));

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    groupService.createGroup(callbackMock, request);

    verify(callbackMock, times(1)).onResponse(any(Call.class), any(Response.class));
  }

  @Test
  public void testAddMember() {
    final Call<GroupMemberResponse> callMock = mock(Call.class);
    final Callback<GroupMemberResponse> callbackMock = mock(Callback.class);
    final GroupMemberResponse response = new GroupMemberResponse(1, new ArrayList<Member>(), "PPL",
        new DateTime());
    final AddMemberRequest request = new AddMemberRequest(1, 2);

    when(serviceMock.addMember(request)).thenReturn(callMock);

    doAnswer(new Answer() {
      @Override
      public Void answer(InvocationOnMock invocation) throws Throwable {
        Callback<GroupMemberResponse> callback = invocation.getArgumentAt(0, Callback.class);
        callback.onResponse(callMock, Response.success(response));

        return null;
      }
    }).when(callMock).enqueue(any(Callback.class));

    groupService.addMember(callbackMock, request);

    verify(callbackMock, times(1)).onResponse(any(Call.class), any(Response.class));
  }
}

